<?php
	include 'manage_checklogin.php';
	
	$memcache = new Memcache;
    $memcache->connect('localhost', 11211);
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	if(		(isset($_POST['teamid']))
		&& 	(isset($_POST['nameth'])) 
		&& 	(isset($_POST['namethshort']))
		&& 	(isset($_POST['status']))
	)
	{
		//Remove all duplicate id
		$dataList 		= 	$collectionTeam->find(
			array( 'DuplicateList' => (int)$_POST['teamid'] )
		);
		if($dataList->count(true)>0){
			foreach($dataList as $tmpTeam){
				$countDup	=	count($tmpTeam['DuplicateList']);
				for( $i = 0 ; $i < $countDup ; $i++ ){
					if($tmpTeam['DuplicateList'][$i] == (int)$_POST['teamid']) {
						unset($tmpTeam['DuplicateList'][$i]);
						break;
					}
				}
				$collectionTeam->update(
					array('id' => (int)$tmpTeam['id']),
					array('$set' => array(
						'DuplicateList' 		=> 	$tmpTeam['DuplicateList']
					))
				);
			}
		}
		$dataMongo 		= 	$collectionTeam->findOne(array( 'id' => (int)$_POST['teamid'] ));
		if(!empty($dataMongo))
		{
			if(isset($_POST['duplicate-select'])){
				foreach($_POST['duplicate-select'] as $key => $value){
					$_POST['duplicate-select'][$key]		=		(int)$value;
					$tmpTeam 								= 		$collectionTeam->findOne(array( 'id' => (int)$value ));
					$tmpTeam['DuplicateList'][]				=		(int)$_POST['teamid'];
					
					$collectionTeam->update(
						array('id' => (int)$value),
						array('$set' => array(
							'DuplicateList' 		=> 	$tmpTeam['DuplicateList']
						))
					);
				}
			}else{
				$_POST['duplicate-select']					=		array();
			}
			
			$dataInsert		=	array(
				'NameTH' 		=> 	$_POST['nameth'],
				'NameTHShort'	=>	$_POST['namethshort'],
				'Status' 		=> 	(int)$_POST['status'],
				'DuplicateList' => 	$_POST['duplicate-select']
			);
			
			if(isset($_POST['info']))
				$dataInsert['Info']				=	$_POST['info'];
			if(isset($_POST['embedCode']))
				$dataInsert['EmbedCode']		=	$_POST['embedCode'];
				
			if(isset($_FILES['logopic']))
			{
				$filepic	=	$_FILES['logopic'];
				if	(($filepic['error'] != 4) && ($filepic['error'] != 0))
				{	
					echo 'Error:Cannot Upload File';
					echo '<a href="manage_team_detail.php?id='.$_POST['teamid'].'">กลับไปแก้ไข</a>';
					exit;
				}
				else if($filepic['error'] == 0)
				{
					// Check Type file
					if 	(	($filepic['type'] 	!= 'image/x-png')
						&& 	($filepic['type'] 	!= 'image/png')
					)
					{
						echo 'Error:Not PNG file.';
						echo '<a href="manage_team_detail.php?id='.$_POST['teamid'].'">กลับไปแก้ไข</a>';
						exit;
					}
						
					// Check directory for upload
					if(!is_dir('../uploads/logo/'))
					{
						if(!mkdir('../uploads/logo/',0777,true))
						{
							echo 'Error:Cannot Create a directory.';
							echo '<a href="manage_team_detail.php?id='.$_POST['teamid'].'">กลับไปแก้ไข</a>';
							exit;
						}
					}
					$Logo 		= str_replace(' ','-',$dataMongo['NameEN']);
					$extension	= 'png';
					$filename 	= $Logo . '.' . $extension;
						
					// Check size of picture,Must be "square"
					$infoPic = getimagesize($filepic['tmp_name']);
					if($infoPic[1] > 250)
					{
						$new_width		=	abs(($infoPic[0]/$infoPic[1])*250);
						$images_orig 	= 	imagecreatefrompng($filepic['tmp_name']);
						$images_resize 	= 	ImageCreateTrueColor($new_width, 250);
						
						imagealphablending($images_resize, FALSE);
						imagesavealpha($images_resize, TRUE); 
						imagealphablending($images_orig, TRUE); 
						$transparent = imagecolorallocatealpha($images_resize, 255, 255, 255, 127);
						imagefilledrectangle($images_resize, 0, 0, $new_width, 250, $transparent);
						
						ImageCopyResampled($images_resize, $images_orig, 0, 0, 0, 0, $new_width, 250, $infoPic[0], $infoPic[1]);
						ImagePNG($images_resize,'../uploads/logo/' . $filename , 9);
						ImageDestroy($images_orig);
						ImageDestroy($images_resize);
						echo $filename;
					}
					else
					{
						if(!move_uploaded_file($filepic['tmp_name'], '../uploads/logo/' . $filename ))
						{
							echo 'Error:Cannot Upload File';
							echo '<a href="manage_team_detail.php?id='.$_POST['teamid'].'">กลับไปแก้ไข</a>';
							exit;
						}
					}
					$memcache->set('Football2014-Team-Logo-' . $filename ,true);
				}
			}
			$collectionTeam->update(
				array('id' => (int)$_POST['teamid']),
				array('$set' => $dataInsert)
			);
		}
		header( "location: " . $_SERVER['HTTP_REFERER'] );
		exit(0);
	}
	else{
		header( "location: " . $_SERVER['HTTP_REFERER'] );
		exit(0);
	}
?>