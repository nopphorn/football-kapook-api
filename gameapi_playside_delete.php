<?php
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	header('Content-Type: application/json');
	
	function checkCookie()
    {
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }        
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMatch        =	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionMember		=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
	{
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	if(!checkCookie())
	{
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	$uid	=	(int)$_COOKIE['uid'];
	
	if(isset($_REQUEST['match_id'])&&(isset($_REQUEST['id']))){
		$returnJson	=	array(
			'code_id'	=>	405,
			'message'	=>	'Wrong arument type.'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	if(!isset($_REQUEST['match_id'])&&(!isset($_REQUEST['id']))){
		$returnJson	=	array(
			'code_id'	=>	405,
			'message'	=>	'Wrong arument type.'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	if(isset($_REQUEST['id']))
	{
		$dataOldGame		=	$collectionGame->findOne(
			array(
				'id' 			=> 	(int)$_REQUEST['id'],
				'user_id' 		=> 	$uid,
				'game_type'		=>	2,
				'status'		=>	1
			)
		);
		if(!$dataOldGame)
		{
			$returnJson	=	array(
				'code_id'	=>	404,
				'message'	=>	'Not found a game.'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
	}else{
		$dataOldGame		=	$collectionGame->findOne(
			array(
				'match_id' 		=> 	(int)$_REQUEST['match_id'],
				'user_id' 		=> 	$uid,
				'game_type'		=>	2,
				'status'		=>	1
			)
		);
		if(!$dataOldGame)
		{
			$returnJson	=	array(
				'code_id'	=>	404,
				'message'	=>	'Not found a game.'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
	}
	
	$size_game			=	count($dataOldGame['listGame']);
	$listGame			=	$dataOldGame['listGame'];
	for($i=0;$i<$size_game;$i++){
		$dataMatch			=	$collectionMatch->findOne(
			array( 
				'id' 			=> (int)$listGame[$i]['match_id'],
				'Status'		=>	1,
				'MatchStatus' 	=>  'Sched'
			)
		);
		if(!$dataMatch)
		{
			$returnJson	=	array(
				'code_id'		=>	402,
				'message'		=>	'Not Found a Match.'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
	}
	
	$collectionGame->remove(array('id' 		=> 	$dataOldGame['id']));
	
	$ops = array(
		array(
			'$match' => array(
				'user_id' 		=> 	$uid,
				'status'		=>	1,
				'game_type'		=>	2
			)
		),
		array(
			'$group' => array(
				'_id' => null,
				'point' => array('$sum' => '$point'),
			),
		),
	);
		
	$dataUsedPoint	=	$collectionGame->aggregate($ops);
	if(isset($dataUsedPoint['result'][0]['point'])){
		$point_used 	= 	$dataUsedPoint['result'][0]['point'];
	}else{
		$point_used		=	0;
	}
	
	// Point for playside
	$findArr['id']		=	$uid;
	$memberData			=	$collectionMember->findOne($findArr);
		
	$point_remain		=	$memberData['point']-$point_used-$thisGameOldPoint;
		
	if((int)$_REQUEST['match_id']>0){
		$memcache->delete( 'Football2014-game-playside-info-' . $uid . '-' . $_REQUEST['match_id'] );
	}else{
		$memcache->delete( 'Football2014-game-playside-multi-info-' . $_REQUEST['id'] );
	}
	$memcache->delete( 'Football2014-game-playside-list-info-' . $uid . '-' . $dataOldGame['date_play'] );
	
	$returnJson	=	array(
		'code_id'		=>	200,
		'message'		=>	'Delete data successful.',
		'id'			=>	$dataOldGame['id'],
		'uid'			=>	(int)$_COOKIE['uid'],
		'point_remain'	=>	$point_remain
	);
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>