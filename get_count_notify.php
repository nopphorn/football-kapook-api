<?php
	//$start_date['day']		=	1;
	//$start_date['month']	=	8;

	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	//header('Content-Type: application/json');
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionNotify		=	new MongoCollection($DatabaseMongoDB,"football_log_notify");

	// Start Date
	if(!isset($_REQUEST['start_date'])){
		$_REQUEST['start_date']		=		date('Y-m-d',strtotime('-1 day'));
	}
	if(!isset($_REQUEST['start_time'])){
		$_REQUEST['start_time']		=		date('H:i:s',strtotime('-1 day'));
	}
	$_REQUEST['start_date']			=		$_REQUEST['start_date'] . ' ' . $_REQUEST['start_time'];
	
	// End Date
	if(!isset($_REQUEST['end_date'])){
		$_REQUEST['end_date']		=		date('Y-m-d');
	}
	if(!isset($_REQUEST['end_time'])){
		$_REQUEST['end_time']		=		date('H:i:s');
	}

	$_REQUEST['end_date']			=		$_REQUEST['end_date'] . ' ' . $_REQUEST['end_time'];

	$returnJson['count']			=		0;
	
	while(strtotime($_REQUEST['start_date']) < strtotime($_REQUEST['end_date'])){
		/*
		 * Stat Notify
		 */
		//--------------------------------------------------------------------------------------------------------------//
		$ops = array(
			array(
				'$match' => array(
					'time_stamp' 	=> 	array(
						'$gte' => date('Y-m-d H:i:s',strtotime($_REQUEST['start_date'] . ' -1 hour')),
						'$lte' => $_REQUEST['start_date']
					),
				)
			),
			array(
				'$group' => array(
					'_id' => null,
					'count' => array('$sum' => 1),
				),
			),
		);
		
		$dataUsedPoint	=	$collectionNotify->aggregate($ops);
		if(isset($dataUsedPoint['result'][0]['count'])){
			$count 	= 	$dataUsedPoint['result'][0]['count'];
		}else{
			$count	=	0;
		}
		$returnJson['dataset'][date('Y-m-d H:i:s',strtotime($_REQUEST['start_date']))]['total_play']		=		$count;
		//--------------------------------------------------------------------------------------------------------------//
		/*
		 * Stat User
		 */
		//--------------------------------------------------------------------------------------------------------------//
		$ops = array(
			array(
				'$match' => array(
					'time_stamp' 	=> 	array(
						'$gte' => date('Y-m-d H:i:s',strtotime($_REQUEST['start_date'] . ' -1 hour')),
						'$lte' => $_REQUEST['start_date']
					),
				)
			),
			array(
				'$group' => array(
					'_id' => array( '_id' => '$user_id' ),
				),
			),
		);
				
		$dataUsedPoint	=	$collectionNotify->aggregate($ops);
		if(isset($dataUsedPoint['result'])){
			$count 	= 	count($dataUsedPoint['result']);
		}else{
			$count	=	0;
		}
		$returnJson['dataset'][date('Y-m-d H:i:s',strtotime($_REQUEST['start_date']))]['total_user']		=		$count;
		
		$_REQUEST['start_date']		=		date('Y-m-d H:i:s',strtotime($_REQUEST['start_date'] . ' +1 hour'));
		$returnJson['count']++;
	}
	
	if(date('Y-m-d H:i:s',strtotime($_REQUEST['start_date'] . ' -1 hour')) < strtotime($_REQUEST['end_date']) ){
		/*
		 * Stat Notify
		 */
		//--------------------------------------------------------------------------------------------------------------//
		$ops = array(
			array(
				'$match' => array(
					'time_stamp' 	=> 	array(
						'$gte' => date('Y-m-d H:i:s',strtotime($_REQUEST['start_date'] . ' -1 hour')),
						'$lte' => $_REQUEST['end_date']
					),
				)
			),
			array(
				'$group' => array(
					'_id' => null,
					'count' => array('$sum' => 1),
				),
			),
		);
		
		$dataUsedPoint	=	$collectionNotify->aggregate($ops);
		if(isset($dataUsedPoint['result'][0]['count'])){
			$count 	= 	$dataUsedPoint['result'][0]['count'];
		}else{
			$count	=	0;
		}
		$returnJson['dataset'][date('Y-m-d H:i:s',strtotime($_REQUEST['end_date']))]['total_play']		=		$count;
		//--------------------------------------------------------------------------------------------------------------//
		/*
		 * Stat User
		 */
		//--------------------------------------------------------------------------------------------------------------//
		$ops = array(
			array(
				'$match' => array(
					'time_stamp' 	=> 	array(
						'$gte' => date('Y-m-d H:i:s',strtotime($_REQUEST['start_date'] . ' -1 hour')),
						'$lte' => $_REQUEST['end_date']
					),
				)
			),
			array(
				'$group' => array(
					'_id' => array( '_id' => '$user_id' ),
				),
			),
		);
		
		$dataUsedPoint	=	$collectionNotify->aggregate($ops);
		if(isset($dataUsedPoint['result'])){
			$count 	= 	count($dataUsedPoint['result']);
		}else{
			$count	=	0;
		}
		$returnJson['dataset'][date('Y-m-d H:i:s',strtotime($_REQUEST['end_date']))]['total_user']		=		$count;
		
		$returnJson['count']++;
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}

?>