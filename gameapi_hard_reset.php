<?php
exit;

session_start();

$authorized = false;

if(isset($_GET['logout']) && ($_SESSION['auth'])) {
	$_SESSION['auth'] = null;
	session_destroy();
	echo "logging out...";
	?>
	<META HTTP-EQUIV="Refresh" CONTENT="5;URL=http://football.kapook.com/" />
	<?php
}

if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = 'football';
	$pass = 'cityunited';
	if (($user == $_SERVER['PHP_AUTH_USER']) && ($pass == ($_SERVER['PHP_AUTH_PW'])) && (!empty($_SESSION['auth']))) {
		$authorized = true;
	}
}

if (! $authorized) {
	header('WWW-Authenticate: Basic Realm="Login please"');
	header('HTTP/1.0 401 Unauthorized');
	$_SESSION['auth'] = true;
	print('Login now or forever hold your clicks...');
	exit;
}

else{

	//init 
	$game						=	50;
	
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	
	$collectionMatch        	=	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionGame				=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member");
	$collectionReward			=	new MongoCollection($DatabaseMongoDB,"football_reward");
	
	$collectionRanking_all		=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	$collectionRanking_season	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	$collectionRanking_month	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	$collectionRanking_day		=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	
	//remove all record in member
	$collectionMember->remove(array());
	
	//remove all record in all ranking
	$collectionRanking_all->remove(array());
	$collectionRanking_season->remove(array());
	$collectionRanking_month->remove(array());
	$collectionRanking_day->remove(array());
	
	//remove all game
	$collectionGame->remove(array());
	
	//remove all reward
	$collectionReward->remove(array());
	
	//set all game status form 1 -> 0
	$collectionMatch->update(
		array(),
		array('$set' =>
			array(
				'GameStatus'			=>	0,
				'count_playside_team1'	=>	0,
				'count_playside_team2'	=>	0,
				'count_playside'		=>	0
			)
		),
		array('multiple' => true)
	);

	echo 'Finish';
}
?>