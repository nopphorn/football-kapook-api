<?php if (!defined('MINIZONE')) exit;

class API Extends My_con
{
    
    ///////////////////////////////////////////////// Construct /////////////////////////////////////////////////
    function __construct() 
    {
        parent::__construct();
        
        /* -- Load : Model -- */
//        $this->content_model = $this->minizone->model('content_model');
    }
    
    ///////////////////////////////////////////////// _default /////////////////////////////////////////////////
    function index() 
    {
        echo "Default";exit;
        
    }
    
    function get_content($c_id) 
    {
        $content_model = $this->minizone->model('content_model');
        $row = $content_model->getContent($c_id);
        $key = 'api_football_news_'.$c_id;
        $McachePortal = $this->minizone->library('McachePortal');
        $McachePortal->setServer($this->minizone->config['memcache']['121']);
        if($row) { 
            $rows = $this->_setFormat($this->_iconv($row));
            $McachePortal->set($key, serialize($rows), 0, 1800);
        } else {
            $McachePortal->set($key, 'delete', 0, 900);
        }
        
//        echo '<pre>';
//        var_dump($rows);
    }
    
    function _setFormat($arr) {
        $rtn['id'] = $arr['kapookfootball_News_ID'];
        $rtn['subject'] = $arr['kapookfootball_News_Subject'];
        $rtn['title'] =  $arr['kapookfootball_News_Title'];
        $rtn['thumbnail'] =  'http://football.kapook.com/uploadnew/news/thumbnail/'.$arr['kapookfootball_News_ThumbnailPicture'];
        $rtn['link'] =  "http://football.kapook.com/news_inside.php?id={$rtn['id']}&key=news";
        if($arr['kapookfootball_News_URL'] != '') {
            $rtn['link'] =  $arr['kapookfootball_News_URL'];
        }
        
        
        $rtn['createdate'] =  $arr['kapookfootball_News_CreateDate'];
        
        return $rtn;
    }
    
    function _iconv($arr_txt) {
        foreach($arr_txt as $k=>$v) {
            $rtn[$k] = iconv('tis-620', 'utf-8', $v);
        }
        return $rtn;
    
    }
    
   
    
}
/* End of file home.php */
/* Location: ./controller/home.php */