<?php
  

        $mongo          = new MongoClient();
        $db             = $mongo->football;
        $zone           = $db->football_zone;
        $league         = $db->football_league;
        $match          = $db->football_match;
        $team           = $db->football_team;

        $memcache = new Memcache;
        $memcache->connect('192.168.1.189', 11211);
        
        
        $AllZone        = $zone->find();
        foreach($AllZone as $Zone){
            $memcache->set('Football2014-Zone-'.$Zone['id'],$Zone);
            $memcache->set('Football2014-Zone-NameEN-'.$Zone['id'],$Zone['NameEN']);
            $memcache->set('Football2014-Zone-NameTH-'.$Zone['id'],$Zone['NameTH']);
            
        }

        $AllLeague      = $league->find();
        
        foreach($AllLeague as $League){
            
            $memcache->set('Football2014-League-'.$League['id'],$League);
            $memcache->set('Football2014-League-NameEN-'.$League['id'],$League['NameEN']);
            $memcache->set('Football2014-League-NameTH-'.$League['id'],$League['NameTH']);
            
            if(intval($League['Priority'])==0){
                $memcache->set('Football2014-League-Order-'.$League['id'],99);
            }else{
                $memcache->set('Football2014-League-Order-'.$League['id'],intval($League['Priority']));
            }
            
            if($League['NameTHShort']==''){
                $memcache->set('Football2014-League-NameTHShort-'.$League['id'],$League['NameEN']);
            }else{
                $memcache->set('Football2014-League-NameTHShort-'.$League['id'],$League['NameTHShort']);
            }
            //echo $League['id'].' = > '.$memcache->get('Football2014-League-NameEN-'.$League['id']).'<br>';
            
            if($League['Status']==0){
                $DisableLeague[] =  $League['id'];              
            }
            
            
        }       


        $AllTeam        = $team->find();
        foreach($AllTeam as $Team){
            $memcache->set('Football2014-Team-'.$Team['id'],$Team);
            $memcache->set('Football2014-Team-NameEN-'.$Team['id'],$Team['NameEN']);
            $memcache->set('Football2014-Team-NameTH-'.$Team['id'],$Team['NameTH']);
            if($Team['NameTHShort']==''){
                $memcache->set('Football2014-Team-NameTHShort-'.$Team['id'],$Team['NameEN']);
            }else{
                $memcache->set('Football2014-Team-NameTHShort-'.$Team['id'],$Team['NameTHShort']);
            }
            
            $memcache->set('Football2014-Team-IDByName-'.str_replace(' ','-',$Team['NameEN']),$Team['id']);
			$memcache->set('Football2014-Team-IDByName-'.strtolower(str_replace(' ','-',$Team['NameEN'])),$Team['id']);
        }
		
		$countLogo	=	0;
		if ($handle = opendir('/var/web/football.kapook.com/html/uploads/logo')) {

			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					$memcache->set('Football2014-Team-Logo-' . $entry ,true);
					echo $entry .'<br>';
				}
				$countLogo++;
			}

			closedir($handle);
		}
        
        echo '<br>Zone : '.$AllZone->count();
        echo '<br>League : '.$AllLeague->count();
        echo '<br>Team : '.$AllTeam->count();
		echo '<br>Logo : '.$countLogo;
 
        /*
        echo '<hr>';
        print_r($DisableLeague);
        
        unset($FindArr);
        unset($UpdateArr);
        $FindArr['KPLeagueID'] = array('$in'=>$DisableLeague);
        $UpdateArr['Status'] = intval(0);
        $match->update($FindArr,array('$set'=>$UpdateArr),array('multiple' => true));
        */
        





        
        
?>