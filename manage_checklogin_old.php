<?php
session_start();

function checkCookie()
{
	/* -- Hash key ห้ามเปลี่ยน -- */
	$hash = 'kapook_sudyod';

	if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
		/* -- เช็คความถูกต้องของ Cookie -- */
		if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
			$kid = $_COOKIE['uid'];
                
			setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
			setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
			/* -- ดึงค่า member จาก userid -- */
			return $kid;
		} else {
			return false;
		}        
	} else {
		return false;
	}
}

if(!isset($_REQUEST['user_id'])){
	if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login'])){
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		echo json_encode($returnJson);
		return;
	}
	if(!checkCookie()){
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		echo json_encode($returnJson);
		return;
	}
	$uid	=	(int)$_COOKIE['uid'];
}else{
	$uid	=	(int)$_REQUEST['user_id'];
}

$list_id = array(
	898850, 	//pong
	188721, 	//jome
	747594,		//ping
	363252,		//buncha
	363300,		//jome2
	793519,		//pongpiti
	881241,		//champ
);

if (!in_array($uid, $list_id)) {
	print('Login now or forever hold your clicks...');
	exit;
}
?>