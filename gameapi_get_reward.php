<?php

	/*
	 * Return List
	 * 200 	- success
	 * 401 	- Error 	: Authention
	 */
	
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('192.168.1.189', 11211) or die ("Could not connect");
	
	header('Content-Type: application/json');
	
	function checkCookie()
    {
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }        
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	$collectionReward			=	new MongoCollection($DatabaseMongoDB,"football_reward");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	if(!isset($_REQUEST['user_id'])){
		if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
		{
			$returnJson	=	array(
				'code_id'	=>	401,
				'message'	=>	'Cannot Authention.',
				'uid'		=>	$_COOKIE['uid']
			);
			echo json_encode($returnJson);
			return;
		}
		if(!checkCookie())
		{
			$returnJson	=	array(
				'code_id'	=>	401,
				'message'	=>	'Cannot Authention.',
				'uid'		=>	$_COOKIE['uid']
			);
			echo json_encode($returnJson);
			return;
		}
		$uid	=	(int)$_COOKIE['uid'];
	}else{
		$uid	=	(int)$_REQUEST['user_id'];
	}
	
	/*
	 * Reward List
	 * 1 : Monthly Login
	 * 2 : Daily Login
	 * 3 : Tour Success
	 */
	 
	if(!isset($_REQUEST['date'])){
		if(date('G')>=6){
			$_REQUEST['date']	=	date('Y-m-d');
		}else{
			$_REQUEST['date']	=	date('Y-m-d',strtotime('now -1 day'));
		}
	}
	
	$FindArr['datetime'] 		=	array('$gte'=> $_REQUEST['date'] . ' 06:00:00' ,'$lte' => date('Y-m-d 05:59:59',strtotime($_REQUEST['date'] . '+ 1 day')));
	$FindArr['user_id'] 		=	$uid;
	
	$cursorReward				=	$collectionReward->find($FindArr)->sort(array( 'datetime' => 1 ));
	$countReward				=	$cursorReward->count();
	
	$totalPoint					=	0;
	
	foreach($cursorReward as $RewardInfo){
		$RewardList[]			=	array(
			'type'				=>	$RewardInfo['type'],
			'value'				=>	$RewardInfo['value'],
			'datetime'			=>	$RewardInfo['datetime'],
			'isReward'			=>	$RewardInfo['isReward'],
		);
		$totalPoint				=	$totalPoint+$RewardInfo['value'];
	}
	
	$returnJson	=	array(
		'code_id'		=>	200,
		'uid'			=>	$uid,
		'date_reward'	=>	$_REQUEST['date'],
		'count'			=>	$countReward,
		'total_point'	=>	$totalPoint,
		'list'			=>	$RewardList
	);
	
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
	return;
?>