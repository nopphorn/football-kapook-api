<?php

session_start();

$authorized = false;

$start_date['day']		=	1;
$start_date['month']	=	8;

if(date('G')>=6){
	$monthToday				=	date("Y-m");
}else{
	$monthToday				=	date("Y-m",strtotime("now -1 day"));
}

if(isset($_GET['logout']) && ($_SESSION['auth'])) {
	$_SESSION['auth'] = null;
	session_destroy();
	echo "logging out...";
	?>
	<META HTTP-EQUIV="Refresh" CONTENT="5;URL=http://football.kapook.com/" />
	<?php
}

if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = 'football';
	$pass = 'cityunited';
	if (($user == $_SERVER['PHP_AUTH_USER']) && ($pass == ($_SERVER['PHP_AUTH_PW'])) && (!empty($_SESSION['auth']))) {
		$authorized = true;
	}
}

if ((! $authorized)) {
	header('WWW-Authenticate: Basic Realm="Login please"');
	header('HTTP/1.0 401 Unauthorized');
	$_SESSION['auth'] = true;
	print('Login now or forever hold your clicks...');
	exit;
}

else{

	if(empty($_REQUEST['date'])){
		echo 'Not update';
		exit;
	}
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	
	$collectionMatch        	=	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionGame				=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member");
	$collectionReward			=	new MongoCollection($DatabaseMongoDB,"football_reward");
	
	$collectionRanking_all		=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	$collectionRanking_season	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	$collectionRanking_month	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	$collectionRanking_day		=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	
	//---------------------------------------------------------------------------------------//
	/*
	 * Ranking-Section
	 */
	$FindArrRankingDate['date']	=	array( '$gte' => $_REQUEST['date'] );
	
	echo $_REQUEST['date'];
	
	$dataRankingByDate			=	$collectionRanking_day->find($FindArrRankingDate);
	$countDate					=	$dataRankingByDate->count();
	
	echo $countDate;
	
	for( $i=0 ; $i<$countDate ; $i++ ){
	
		$rowDate				=	$dataRankingByDate->current();
		
		echo $rowDate['id']  . ' => ' . $rowDate['total_point'] . '<br>';
		
		// Prepare a data rollback
		$inc_update		=	array(
			'total_playside_point'						=> 	((-1)*$rowDate['total_playside_point']),
			'total_point'								=> 	((-1)*$rowDate['total_point']),
			'total_playside_single'						=> 	((-1)*$rowDate['total_playside_single']),
			'total_playside_single_win'					=>	((-1)*$rowDate['total_playside_single_win']),
			'total_playside_single_win_full'			=>	((-1)*$rowDate['total_playside_single_win_full']),
			'total_playside_single_win_half'			=>	((-1)*$rowDate['total_playside_single_win_half']),
			'total_playside_single_draw'				=>	((-1)*$rowDate['total_playside_single_draw']),
			'total_playside_single_lose_half'			=>	((-1)*$rowDate['total_playside_single_lose_half']),
			'total_playside_single_lose_full'			=>	((-1)*$rowDate['total_playside_single_lose_full']),
			'total_playside_multi'						=> 	((-1)*$rowDate['total_playside_multi']),
			'total_playside_multi_win'					=>	((-1)*$rowDate['total_playside_multi_win']),
			'total_playside_multi_draw'					=>	((-1)*$rowDate['total_playside_multi_draw']),
			'total_playside_multi_lose_full'			=>	((-1)*$rowDate['total_playside_multi_lose_full']),
			'total_play'								=>	((-1)*$rowDate['total_play']),
		);
		
		if($rowDate['id']==793519){
			var_dump($inc_update);
		}
		
		$tmpMonth				=	date("n",strtotime($rowDate['date']));
		$tmpDay					=	date("j",strtotime($rowDate['date']));
						
		if(	( $tmpMonth > $start_date['month'] )||
			(
				( $tmpMonth == $start_date['month'] ) &&
				( $tmpDay >= $start_date['day'] )
			)
		){
			$year	=	date("Y",strtotime($rowDate['date']));
		}
		else{
			$year	=	date("Y",strtotime($rowDate['date'] . ' - 1 year'));
		}
		$month	=	substr($rowDate['date'],0,7);
		
		/*
		 * UpdatePoint by All-Time
		 */
		$collectionRanking_all->update(
			array('id' 		=> 	$rowDate['id']),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	false)
		);
		
		/*
		 * UpdatePoint by Season
		 */
		$collectionRanking_season->update(
			array(
				'id' 		=> 	$rowDate['id'],
				'season'	=>	$year
			),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	false)
		);
					
		/*
		 * UpdatePoint by Month
		 */
		$collectionRanking_month->update(
			array(
				'id' 		=> 	$rowDate['id'],
				'month'		=>	$month
			),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	false)
		);
		
		/*
		 * UpdatePoint for User
		 */
		if( $monthToday == $month ){
			$collectionMember->update(
			array('id' => $rowDate['id']),
				array(
					'$inc' 	=> 	array(
						'point' 	=> ((-1)*$rowDate['total_point'])
					)
				),
				array('upsert'	=>	false)
			);
		}
		
		$dataRankingByDate->next();
	}
	
	$collectionRanking_day->remove($FindArrRankingDate);
	
	//---------------------------------------------------------------------------------------//
	
	/*
	 * Game-Section
	 */

	$FindArrGameType['game_type']	=	2;
	$FindArrGameType['status']		=	array( '$gte' => 1 );
	$FindArrGameType['date_play']	=	array( '$gte' => $_REQUEST['date'] );
	
	$dataGame					= 	$collectionGame->find($FindArrGameType);
	$countGame					=	$dataGame->count();
	$dataGame->next();
		
	//echo $countGame;
	
	for( $i=0 ; $i<$countGame ; $i++ ){
		$rowGame				=	$dataGame->current();
		$subgame				=	$rowGame['listGame'];
		$countSubGame			=	count($subgame);
		for( $j=0 ; $j<$countSubGame ; $j++ )
		{
			$subgame[$j]['status']		=	1;
			unset($subgame[$j]['isWDL']);
		}
		
		$collectionGame->update(
			array(
				'id' 	=> (int)$rowGame['id']
			),
			array('$set' =>
				array(
					'status'		=>	1,
					'point_return'	=> 	0.0,
					'multiple'		=>	1.0,
					'listGame'		=>	$subgame
				)
			)
		);
		
		$dataGame->next();
	}
	
	//set all game status form 2 -> 1
	$collectionGame->update(
		array(
			'status'		=>		array( '$gte' => 1 ),
			'date_play'		=>		array( '$gte' => $_REQUEST['date'] )
		),
		array('$set' =>
			array(
				'status'		=>	1,
				'point_return'	=> 	0.0,
				'multiple'		=>	1.0
			)
		),
		array('multiple' => true)
	);
	
	//set all game status form 1 -> 0
	$collectionMatch->update(
		array(
			'MatchDateTime'	=>		array( '$gte' => $_REQUEST['date'] . ' 06:00:00' )
		),
		array('$set' =>
			array(
				'GameStatus'	=>	0
			)
		),
		array('multiple' => true)
	);
	
	//set all reward status form true -> false
	$collectionReward->update(
		array( 'datetime' => array( '$gte' => $_REQUEST['date'] . ' 06:00:00' ) ),
		array( '$set' =>
			array(
				'isReward'	=>	false
			)
		),
		array( 'multiple' => true )
	);
	
	/*
	include 'gameapi_restore_reward_copy.php';
	include 'test_game_calculate_manual.php';
	*/
	echo 'Finish';
}
?>