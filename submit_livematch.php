<script type="text/javascript" src="../assets/js/jquery-1.11.0.min.js"></script>
<?php
session_start();

$authorized = false;

if(isset($_GET['logout']) && ($_SESSION['auth'])) {
	$_SESSION['auth'] = null;
	session_destroy();
	echo "logging out...";
	?>
	<META HTTP-EQUIV="Refresh" CONTENT="5;URL=http://football.kapook.com/" />
	<?php
}

if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = 'football';
	$pass = 'cityunited';
	if (($user == $_SERVER['PHP_AUTH_USER']) && ($pass == ($_SERVER['PHP_AUTH_PW'])) && (!empty($_SESSION['auth']))) {
		$authorized = true;
	}
}

if ((! $authorized)) {
	header('WWW-Authenticate: Basic Realm="Login please"');
	header('HTTP/1.0 401 Unauthorized');
	$_SESSION['auth'] = true;
	print('Login now or forever hold your clicks...');
	exit;
}

else{
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	
	if(!isset($_REQUEST['match_id'])){
		echo 'Error:Not include match id';
		exit;
	}
	
	$dataMongo 		= 	$collection->findOne(array( 'id' => (int)$_REQUEST['match_id'] ));
	if(!$dataMongo)
	{
		echo 'Error:Not found a match';
		exit;
	}
	
	if(isset($_REQUEST['home_score'])){
		if(	($dataMongo['MatchStatus']=='1 HF')	||
			($dataMongo['MatchStatus']=='H/T')
		){
			$dataupdate['HTScore']		=		$_REQUEST['home_score'] . '-' . $_REQUEST['away_score'];
			$dataupdate['FTScore']		=		$_REQUEST['home_score'] . '-' . $_REQUEST['away_score'];
			$dataupdate['Team1FTScore']	=		(int)$_REQUEST['home_score'];
			$dataupdate['Team2FTScore']	=		(int)$_REQUEST['away_score'];
		}else if($dataMongo['MatchStatus']=='E/T'){
			$dataupdate['ETScore']		=		$_REQUEST['home_score'] . '-' . $_REQUEST['away_score'];
		}else if($dataMongo['MatchStatus']=='Pen'){
			$dataupdate['PNScore']		=		$_REQUEST['home_score'] . '-' . $_REQUEST['away_score'];
		}else{
			$dataupdate['FTScore']		=		$_REQUEST['home_score'] . '-' . $_REQUEST['away_score'];
			$dataupdate['Team1FTScore']	=		(int)$_REQUEST['home_score'];
			$dataupdate['Team2FTScore']	=		(int)$_REQUEST['away_score'];
		}
	}
	
	if(isset($_REQUEST['status'])){
		if($_REQUEST['status']=='1HF'){
			$dataupdate['MatchStatus']		=		'1 HF';
			$dataupdate['1HFStart']			=		date('Y-m-d H:i:s');
		}else if($_REQUEST['status']=='HT'){
			$dataupdate['MatchStatus']		=		'H/T';
		}else if($_REQUEST['status']=='2HF'){
			$dataupdate['MatchStatus']		=		'2 HF';
			$dataupdate['2HFStart']			=		date('Y-m-d H:i:s');
		}else if($_REQUEST['status']=='WaitET'){
			$dataupdate['MatchStatus']		=		'E/T';
		}else if($_REQUEST['status']=='1ET'){
			$dataupdate['MatchStatus']		=		'E/T';
			$dataupdate['1ETStart']			=		date('Y-m-d H:i:s');
		}else if($_REQUEST['status']=='2ET'){
			$dataupdate['MatchStatus']		=		'E/T';
			$dataupdate['2ETStart']			=		date('Y-m-d H:i:s');
		}else{
			$dataupdate['MatchStatus']		=		$_REQUEST['status'];
		}
	}

	if(isset($_REQUEST['AutoMode'])){
		$dataupdate['AutoMode']		=	(int)$_REQUEST['AutoMode'];
	}
	
	$collection->update(
		array('id' => (int)$_REQUEST['match_id']),
		array('$set' => $dataupdate)
	);
	header('Location: manage_livematch.php');
}
?>