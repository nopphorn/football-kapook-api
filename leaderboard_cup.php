<?php

	function compare_point($a,$b)
	{
		if ($a['point'] == $b['point'])
		{
			if($a['custom_order'] == $b['custom_order'])
			{
				if ($a['GD'] == $b['GD'])
				{
					if ($a['GF'] == $b['GF'])
					{
						if ($a['NameEN'] == $b['NameEN'])
							return 0;
						return strcmp($a['NameEN'],$b['NameEN']);
					}
					return ($a['GF'] > $b['GF']) ? -1 : 1;
				}
				return ($a['GD'] > $b['GD']) ? -1 : 1;
			}
			return ($a['custom_order'] < $b['custom_order']) ? -1 : 1;
		}
		return ($a['point'] > $b['point']) ? -1 : 1;
	}
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");

	$leaderboard_MC = $memcache->get( 'Football2014-leaderboard-'.$_GET['id']);

	if(($leaderboard_MC)&&($_GET['clear']!='1')){
		$leaderboard_group		= 	$leaderboard_MC;
	}else{
		// init MongoDB
		$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
		$DatabaseMongoDB		=	$connectMongo->selectDB("football");
		$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
		$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
		$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
		$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
		
		// Available League List 
		$listCanGetLeague	=	array(
			563, 		// 	AFF Suzuki Cup
			325,		//	UEFA Champions League
			337,		//	UEFA Europa League
			310,		//	Euro 2016 (QUALIFY)
		);
		
		$listGroup[563]		=	array(
			1 	=> 	array(3420,3167,3396,3405),
			2 	=> 	array(3319,3398,3415,3322)
		);
		
		$listGroup[325]		=	array(
			1	=> 	array(110,140,374,1607),
			2	=> 	array(1214,1058,877,209),
			3 	=> 	array(1323,214,1574,166),
			4	=> 	array(1329,154,1040,1251),
			5 	=> 	array(224,997,972,210),
			6 	=> 	array(1573,1905,137,1006),
			7	=> 	array(156,169,120,152),
			8 	=> 	array(1548,1368,1290,88),
		);
		
		$listGroup[337]		=	array(
			1 	=> 	array(998,1005,147,159),
			2 	=> 	array(938,267,974,107),
			3 	=> 	array(1588,206,1589,336),
			4 	=> 	array(970,690,537,435),
			5 	=> 	array(283,279,163,269),
			6 	=> 	array(1059,447,1566,1369),
			7 	=> 	array(543,353,194,167),
			8 	=> 	array(105,286,1160,946),
			9 	=> 	array(1467,473,1155,1540),
			10 	=> 	array(1292,135,157,868),
			11 	=> 	array(130,278,983,118),
			12 	=> 	array(192,1640,1355,987),
		);
		
		$listGroup[310]		=	array(
			1 	=> 	array(3182,3142,3145,3151,3309,3315),
			2 	=> 	array(3177,3175,3310,3308,3180,3307),
			3 	=> 	array(3172,3165,3189,3197,3300,3296),
			4 	=> 	array(3147,3149,3264,3262,3270,3263),
			5 	=> 	array(3146,3301,3295,3184,3163,3299),
			6 	=> 	array(3265,3271,3266,3261,3272,3260),
			7 	=> 	array(3164,3183,3294,3297,3298,3178),
			8 	=> 	array(3181,3179,2898,3314,2683,3190),
			9 	=> 	array(3267,3144,3269,3152,3268),
		);
		
		$lastdateGroup[563]	=	'2014-11-30 12:00';
		$lastdateGroup[325]	=	'2014-12-12 12:00';
		$lastdateGroup[337]	=	'2014-12-12 12:00';
		$lastdateGroup[310]	=	'2015-08-16 12:00';
		
		if(!in_array((int)$_GET['id'], $listCanGetLeague)){
			//echo 'notFound.';
			exit;
		}
		
		$dataMatch 			= 	$collection->find(
			array( 
				'KPLeagueID' 	=> (int)$_GET['id'],
				'Status' 		=> 1,
				'MatchDateTime'	=> array( '$lte' => $lastdateGroup[(int)$_GET['id']] )
			)
		);
		
		$countMongo			=	$dataMatch->count();
		$dataMatch->next();
		
		for( $i=0 ; $i<$countMongo ; $i++ ){
			$data 	= 	$dataMatch->current();
			
			if(empty($leaderboard[$data['Team1KPID']]))
			{
				
				$leaderboard[$data['Team1KPID']]['NameTH']	=	$data['Team1KPID'];
			
				$dataTeam 			= 		$collectionTeam->findOne( array('id' => (int)$data['Team1KPID'] ) );
				
				$leaderboard[$data['Team1KPID']]['NameTH']			=	$dataTeam['NameTH'];
				$leaderboard[$data['Team1KPID']]['NameTHShort']		=	$dataTeam['NameTHShort'];	
				$leaderboard[$data['Team1KPID']]['NameEN']			=	$dataTeam['NameEN'];
				
				$Logo 									= 	str_replace(' ','-',$dataTeam['NameEN']).'.png';
				$Logo_MC								=	$memcache->get('Football2014-Team-Logo-' . $Logo);
				if($Logo_MC==true){
					$leaderboard[$data['Team1KPID']]['Logo'] = 'http://football.kapook.com/uploads/logo/' . $Logo;
				}else{
					$leaderboard[$data['Team1KPID']]['Logo'] = 'http://football.kapook.com/uploads/logo/default.png';
				}
			
				$leaderboard[$data['Team1KPID']]['win']				=	0;
				$leaderboard[$data['Team1KPID']]['lose']			=	0;
				$leaderboard[$data['Team1KPID']]['draw']			=	0;
			
				$leaderboard[$data['Team1KPID']]['GF']				=	0;
				$leaderboard[$data['Team1KPID']]['GA']				=	0;
				$leaderboard[$data['Team1KPID']]['GD']				=	0;
			
				$leaderboard[$data['Team1KPID']]['point']			=	0;
				$leaderboard[$data['Team1KPID']]['total_match']		=	0;
			}
			$leaderboard[$data['Team1KPID']]['id'] = (int)$data['Team1KPID'];
			
			if(empty($leaderboard[$data['Team2KPID']]))
			{
			
				$leaderboard[$data['Team2KPID']]['NameTH']	=	$data['Team2KPID'];
			
				$dataTeam 			= 		$collectionTeam->findOne( array('id' => (int)$data['Team2KPID'] ) );
				
				$leaderboard[$data['Team2KPID']]['NameTH']			=	$dataTeam['NameTH'];
				$leaderboard[$data['Team2KPID']]['NameTHShort']		=	$dataTeam['NameTHShort'];	
				$leaderboard[$data['Team2KPID']]['NameEN']			=	$dataTeam['NameEN'];
				
				$Logo 									= 	str_replace(' ','-',$dataTeam['NameEN']).'.png';
				$Logo_MC								=	$memcache->get('Football2014-Team-Logo-' . $Logo);
				if($Logo_MC==true){
					$leaderboard[$data['Team2KPID']]['Logo'] = 'http://football.kapook.com/uploads/logo/' . $Logo;
				}else{
					$leaderboard[$data['Team2KPID']]['Logo'] = 'http://football.kapook.com/uploads/logo/default.png';
				}
			
				$leaderboard[$data['Team2KPID']]['win']				=	0;
				$leaderboard[$data['Team2KPID']]['lose']			=	0;
				$leaderboard[$data['Team2KPID']]['draw']			=	0;
			
				$leaderboard[$data['Team2KPID']]['GF']				=	0;
				$leaderboard[$data['Team2KPID']]['GA']				=	0;
				$leaderboard[$data['Team2KPID']]['GD']				=	0;
			
				$leaderboard[$data['Team2KPID']]['point']			=	0;
				$leaderboard[$data['Team2KPID']]['total_match']		=	0;
			}
			$leaderboard[$data['Team2KPID']]['id'] = (int)$data['Team2KPID'];
			
			if($data['MatchStatus']=='Fin')
			{
				// T1 WIN
				if($data['Team1FTScore']>$data['Team2FTScore'])
				{
					$leaderboard[$data['Team1KPID']]['win']++;
					$leaderboard[$data['Team1KPID']]['point'] = $leaderboard[$data['Team1KPID']]['point'] + 3;
					
					$leaderboard[$data['Team2KPID']]['lose']++;
				}
				// T2 WIN
				else if($data['Team2FTScore']>$data['Team1FTScore'])
				{
					$leaderboard[$data['Team2KPID']]['win']++;
					$leaderboard[$data['Team2KPID']]['point'] = $leaderboard[$data['Team2KPID']]['point'] + 3;
						
					$leaderboard[$data['Team1KPID']]['lose']++;
				}
				// DRAW
				else
				{
					$leaderboard[$data['Team2KPID']]['draw']++;
					$leaderboard[$data['Team2KPID']]['point'] = $leaderboard[$data['Team2KPID']]['point'] + 1;
						
					$leaderboard[$data['Team1KPID']]['draw']++;
					$leaderboard[$data['Team1KPID']]['point'] = $leaderboard[$data['Team1KPID']]['point'] + 1;
				}
				
				// T1 GOAL
				$leaderboard[$data['Team1KPID']]['GF'] = $leaderboard[$data['Team1KPID']]['GF']+$data['Team1FTScore'];
				$leaderboard[$data['Team1KPID']]['GA'] = $leaderboard[$data['Team1KPID']]['GA']+$data['Team2FTScore'];
				
				// T2 GOAL
				$leaderboard[$data['Team2KPID']]['GF'] = $leaderboard[$data['Team2KPID']]['GF']+$data['Team2FTScore'];
				$leaderboard[$data['Team2KPID']]['GA'] = $leaderboard[$data['Team2KPID']]['GA']+$data['Team1FTScore'];
			
			}
			$dataMatch->next();
		}		
		
		foreach($leaderboard as $i => $value ){
			$leaderboard[$i]['GD']			=	$leaderboard[$i]['GF']	-	$leaderboard[$i]['GA'];
			$leaderboard[$i]['total_match']	=	$leaderboard[$i]['win']+$leaderboard[$i]['lose']+$leaderboard[$i]['draw'];
			
			$tmpGroupSize					=	count($listGroup[(int)$_GET['id']]);
			for( $j=1 ; $j<=$tmpGroupSize ; $j++ ){
				if(in_array($i, $listGroup[(int)$_GET['id']][$j])){
					$leaderboard_group[$j][]		=	$leaderboard[$i];
				}
			}
		}
		
		$size	=	count($leaderboard_group);
		for( $i=1 ; $i<=$size ; $i++ ){
			usort($leaderboard_group[$i],"compare_point");
		}

		// memcache
		$expire				=		3600;
		$memcache->set( 'Football2014-leaderboard-'.$_GET['id'] , $leaderboard_group , MEMCACHE_COMPRESSED, $expire );
	
	}
	
	echo json_encode($leaderboard_group);
	
?>