<?php

	/*
	 * Return List
	 * 200 	- success
	 * 401 	- Error 	: Authention
	 * 1 	- Success 	: is Exist
	 * 2 	- Error 	: "Type" argument is not found.
	 */
	 
	$start_date['day']		=	1;
	$start_date['month']	=	8;
	$isUpdate				=	false;
	
	// Get a date play
	if(date('G')>=6){
		$date	=	date("Y-m-d",strtotime(date('Y-m-d H:i:s')));
	}else{
		$date	=	date("Y-m-d",strtotime(date('Y-m-d H:i:s')  . ' - 1 day'));
	}
	// Get a month
	$month		=	date("Y-m",strtotime($date));
	// Get a year season
	if(	( date('n') > $start_date['month'] )	||
		(
			( $tmpMonth == $start_date['month'] ) &&
			( $tmpDay >= $start_date['day'] )
		)
	){
		$year	=	date("Y");
	}else{
		$year	=	date("Y",strtotime($date . ' - 1 year'));
	}
	
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('192.168.1.189', 11211) or die ("Could not connect");
	
	header('Content-Type: application/json');
	
	function checkCookie(){
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }        
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	$collectionReward			=	new MongoCollection($DatabaseMongoDB,"football_reward");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	$collectionRanking_all		=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	$collectionRanking_season	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	$collectionRanking_month	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	$collectionRanking_day		=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	
	if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
	{
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	if(!checkCookie())
	{
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	$uid	=	(int)$_COOKIE['uid'];
	
	/*
	 * Reward List
	 * 1 : Monthly Login // 2000 & clear_point
	 * 2 : Daily Login
	 * 3 : Tour Success
	 */
	if(isset($_REQUEST['type'])){
		if($_REQUEST['type'] == '1'){
			$keydata	=	$month;
			$point_reward		=		2000;
			$isExist	=	$collectionReward->findOne(
				array(
					'user_id' 	=> 		$uid,
					'type'		=>		1,
					'key'		=>		$keydata
				)
			);
			if($isExist){
				$returnJson	=	array(
					'code_id'	=>	1,
					'message'	=>	'This event is exist.'
				);
			}else{
				$cursorReward				=	$collectionReward->find();
				if($cursorReward->count(true)<=0){
					$dataupdate['id']		=	1;
				}else{
					$cursorReward->sort( array( 'id' => -1 ) );
					$cursorReward->limit(1);
					$cursorReward->next();		
					$dataReward					=	$cursorReward->current();
						
					$dataupdate['id']			=	((int)$dataReward['id']+1);
				}
				$dataupdate['user_id']		=	$uid;
				$dataupdate['type']			=	1;
				$dataupdate['description']	=	'Monthly login reward';
				$dataupdate['value']		=	$point_reward;
				$dataupdate['datetime']		=	date('Y-m-d H:i:s');
				$dataupdate['isReward']		=	true;

				$dataupdate['key']			=	$keydata;

				$collectionReward->insert($dataupdate);
					
				$isUpdate				=	true;
					
				$returnJson	=	array(
					'code_id'	=>	200,
					'message'	=>	'Adding success.',
					'key'		=>	$keydata
				);
			}
				
			$expire		=	(3600);
			$memcache->set( 'Football2014-reward-' . $uid . '-1-' . $keydata  , $returnJson , MEMCACHE_COMPRESSED, $expire );
		}else if($_REQUEST['type'] == '2'){
			$keydata	=	$date;
			
			$point_reward		=		200;
			$isExist	=	$collectionReward->findOne(
				array(
					'user_id' 	=> 		$uid,
					'type'		=>		2,
					'key'		=>		$keydata
				)
			);
			if($isExist){
				$returnJson	=	array(
					'code_id'	=>	1,
					'message'	=>	'This event is exist.'
				);
			}else{
				$cursorReward				=	$collectionReward->find();
				if($cursorReward->count(true)<=0)
				{
					$dataupdate['id']		=	1;
				}else{
					$cursorReward->sort( array( 'id' => -1 ) );
					$cursorReward->limit(1);
					$cursorReward->next();
					$dataReward					=	$cursorReward->current();
					$dataupdate['id']			=	((int)$dataReward['id']+1);
				}
					
				$dataupdate['user_id']		=	$uid;
				$dataupdate['type']			=	2;
				$dataupdate['description']	=	'Daily login reward';
				$dataupdate['value']		=	$point_reward;
				$dataupdate['datetime']		=	date('Y-m-d H:i:s');
				$dataupdate['key']			=	$keydata;
				$dataupdate['isReward']		=	true;
				
				$collectionReward->insert($dataupdate);
					
				$isUpdate				=	true;

				$returnJson	=	array(
					'code_id'	=>	200,
					'message'	=>	'Adding success.',
					'key'		=>	$keydata
				);
			}
			$expire		=	(3600);
			$memcache->set( 'Football2014-reward-' . $uid . '-2-' . $keydata , $returnJson , MEMCACHE_COMPRESSED, $expire );
		}else if($_REQUEST['type'] == '3'){
			$keydata	=	1;
			$point_reward		=		0;
			$isExist	=	$collectionReward->findOne(
				array(
					'user_id' 	=> 		$uid,
					'type'		=>		3
				)
			);
			if($isExist){
				$returnJson	=	array(
					'code_id'	=>	1,
					'message'	=>	'This event is exist.'
				);
			}else{
				$cursorReward				=	$collectionReward->find();
				if($cursorReward->count(true)<=0){
					$dataupdate['id']		=	1;
				}else{
					$cursorReward->sort( array( 'id' => -1 ) );
					$cursorReward->limit(1);
					$cursorReward->next();		
					$dataReward					=	$cursorReward->current();
						
					$dataupdate['id']			=	((int)$dataReward['id']+1);
				}
				$dataupdate['user_id']		=	$uid;
				$dataupdate['type']			=	3;
				$dataupdate['description']	=	'Tour reward';
				$dataupdate['value']		=	$point_reward;
				$dataupdate['datetime']		=	date('Y-m-d H:i:s');
				$dataupdate['isReward']		=	true;

				$dataupdate['key']			=	$keydata;

				$collectionReward->insert($dataupdate);
					
				$isUpdate				=	true;
					
				$returnJson	=	array(
					'code_id'	=>	200,
					'message'	=>	'Adding success.',
					'key'		=>	$keydata
				);
			}
				
			$expire		=	(3600);
			$memcache->set( 'Football2014-reward-' . $uid . '-3', $returnJson , MEMCACHE_COMPRESSED, $expire );
		}else{
			$returnJson	=	array(
				'code_id'	=>	2,
				'message'	=>	'"Type" argument is wrong.'
			);
		}
	}else{
		$returnJson	=	array(
			'code_id'	=>	2,
			'message'	=>	'"Type" argument is not found.'
		);
	}
	
	if($isUpdate){
		// Prepare a data reward
		$inc_update		=	array(
			'total_playside_point'						=> 	$point_reward,
			'total_point'								=> 	$point_reward,
		);
		
		$inc_new		=	array(
			'total_playside_point'						=> 	$point_reward,
			'total_point'								=> 	$point_reward,
			'total_playside_single_win_percent'			=> 	0.0,
			'total_playside_multi_win_percent'			=>	0.0,
			'total_rate_calculate'						=>	0.0,
			'total_rate_calculate_by_time'				=>	0.0
		);
					
		/*
		 * UpdatePoint by All-Time
		 */
		
		$isexist		=		$collectionRanking_all->findOne(array('id' => $uid));
		if($isexist){
			$collectionRanking_all->update(
				array('id' 		=> 	$uid),
				array('$inc' 	=> 	$inc_update),
				array('upsert'	=>	true)
			);
		}else{
			$collectionRanking_all->update(
				array('id' 		=> 	$uid),
				array('$inc' 	=> 	$inc_new),
				array('upsert'	=>	true)
			);
		}
					
		/*
		 * UpdatePoint by Season
		 */
		$isexist		=		$collectionRanking_season->findOne(array('id' => $uid,'season' => $year));			
		if($isexist){
			$collectionRanking_season->update(
				array(
					'id' 		=> 	$uid,
					'season'	=>	$year
				),
				array('$inc' 	=> 	$inc_update),
				array('upsert'	=>	true)
			);
		}else{
			$collectionRanking_season->update(
				array(
					'id' 		=> 	$uid,
					'season'	=>	$year
				),
				array('$inc' 	=> 	$inc_new),
				array('upsert'	=>	true)
			);
		}
					
		/*
		 * UpdatePoint by Month
		 */
		$isexist		=		$collectionRanking_month->findOne(array('id' => $uid,'month' => $month));			
		if($isexist){
			$collectionRanking_month->update(
				array(
					'id' 		=> 	$uid,
					'month'		=>	$month
				),
				array('$inc' 	=> 	$inc_update),
				array('upsert'	=>	true)
			);
		}else{
			$collectionRanking_month->update(
				array(
					'id' 		=> 	$uid,
					'month'		=>	$month
				),
				array('$inc' 	=> 	$inc_new),
				array('upsert'	=>	true)
			);
		}
				
		/*
		 * UpdatePoint by Date
		 */
		$isexist		=		$collectionRanking_day->findOne(array('id' => $uid,'date' => $date));
		if($isexist){
			$collectionRanking_day->update(
				array(
					'id' 		=> 	$uid,
					'date'		=>	$date
				),
				array('$inc' 	=> 	$inc_update),
				array('upsert'	=>	true)
			);
		}else{
			$collectionRanking_day->update(
				array(
					'id' 		=> 	$uid,
					'date'		=>	$date
				),
				array('$inc' 	=> 	$inc_new),
				array('upsert'	=>	true)
			);
		}
					
		/*
		 * UpdatePoint for User
		 */
		
		if($_REQUEST['type'] == '1'){
			$collectionMember->update(
				array('id' => $uid),
					array(
						'$set' 	=> 	array(
							'point' 	=> 	$point_reward
					)
				),
				array('upsert'	=>	true)
			);
		}else{
			$collectionMember->update(
				array('id' => $uid),
					array(
						'$inc' 	=> 	array(
							'point' 	=> 	$point_reward
					)
				),
				array('upsert'	=>	true)
			);
		}
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
	return;
?>