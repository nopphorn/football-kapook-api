<?php
header('Content-Type: application/json');
### Connect To VPS Singapore
	$mongo          = new MongoClient();
	$db             = $mongo->football;  
        
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211);

	$my_cup			=		$db->football_my_cup;
	$my_round 	 	=		$db->football_my_round;
	$my_match		=		$db->football_my_match;
	
	$match			=		$db->football_match;
	
	if($_REQUEST['cup']){
		$dataCup				=			$my_cup->findOne( array('title_url' => $_REQUEST['cup']) );
	}
	
	if($dataCup){
		$Result				=		$dataCup;
		if($_REQUEST['mix_round']=='1'){
			$search_attr['CupID'] 	=		$dataCup['id'];
			$size					=		20;
			if( isset($_REQUEST['end_date']) ){
				$search_attr['MatchDateTime']['$lt'] 	=	 date("Y-m-d 06:00:00",strtotime($_REQUEST['end_date'] .' 06:00:00 + 1 days'));
			}
			if( isset($_REQUEST['start_date']) ){
				$search_attr['MatchDateTime']['$gte'] 	=	$_REQUEST['start_date'] . ' 06:00:00';
			}
			
			if($_REQUEST['sort']=='asc'){
				$sort_attr['MatchDateTime']		=	1;
			}else{
				$sort_attr['MatchDateTime']		=	-1;
			}
			
			if(isset($_REQUEST['size'])){
				$size				=			intval($_REQUEST['size']);
			}
			
			$cursorMatch			=			$my_match->find( $search_attr )->sort($sort_attr)->limit($size);
			$Result['data']			=			array();
			
			foreach($cursorMatch as $tmpMatch){
					
				$dataMatch			=		null;
				if(($tmpMatch['isAutoMode']==1)&&(intval($tmpMatch['MatchID']))){
					$dataMatch		=		$match->findOne( array('id' => $tmpMatch['MatchID']) );
				}
					
				if(!$dataMatch){
					$dataMatch['MatchStatus']		=		$tmpMatch['MatchStatus'];
					
					$dataMatch['FTScore']			=		$tmpMatch['FTScore'];
						
					$arrTmpScore					=		explode('-',$tmpMatch['FTScore']);					
					if(count($arrTmpScore)>=2){
						$dataMatch['Team1FTScore']	=		intval($arrTmpScore[0]);
						$dataMatch['Team2FTScore']	=		intval($arrTmpScore[1]);
					}else{
						$dataMatch['Team1FTScore']	=		0;
						$dataMatch['Team2FTScore']	=		0;
					}
						
					$dataMatch['MatchDateTime']		=		$tmpMatch['MatchDateTime'];
					$dataMatch['MatchDate'] 		= 		substr($tmpMatch['MatchDateTime'],0,10);
					$dataMatch['MatchTime'] 		= 		substr($tmpMatch['MatchDateTime'],11,5);
						
					$dataMatch['Minute'] 			= 		intval($tmpMatch['MatchMinute']);
						
					$dataMatch['Odds'] 				= 		-1;
					$dataMatch['TeamOdds'] 			= 		-1;
							
				}else{
					$dataMatch['MatchDate'] 		= 		substr($dataMatch['MatchDateTime'],0,10);
					$dataMatch['MatchTime'] 		= 		substr($dataMatch['MatchDateTime'],11,5);
						
					$matchURL = strtolower('http://football.kapook.com/match-'.$dataMatch['id'].'-'.$dataMatch['Team1'].'-'.$dataMatch['Team2']);
					$dataMatch['MatchPageURL'] 		= 		preg_replace('/\s+/', '-', $matchURL);
				}
					
				unset($dataMatch['_id']);
				unset($dataMatch['XSMatchID']);
				unset($dataMatch['XSLeagueID']);
				unset($dataMatch['XSLeagueName']);
				unset($dataMatch['XSLeagueCountry']);
				unset($dataMatch['XSLeagueYear']);
				unset($dataMatch['XSLeagueDesc']);
				unset($dataMatch['SourceDate']);
				unset($dataMatch['SourceTime']);
				unset($dataMatch['MatchDateTimeMongo']);
				unset($dataMatch['LastUpdate']);
				unset($dataMatch['LastUpdateMongo']);
				unset($dataMatch['Team1LP']);
				unset($dataMatch['Team2LP']);
				unset($dataMatch['Team1YC']);
				unset($dataMatch['Team2YC']);
				unset($dataMatch['Team1RC']);
				unset($dataMatch['Team2RC']);
				unset($dataMatch['LastScorers']);
				unset($dataMatch['CreateDateMongo']);
				unset($dataMatch['CreateDate']);
				unset($dataMatch['KPLeagueID']);
				unset($dataMatch['KPLeagueCountryID']);
				unset($dataMatch['Team1KPID']);
				unset($dataMatch['Team2KPID']);
				unset($dataMatch['Priority']);
				unset($dataMatch['count_playside_team1']);
				unset($dataMatch['count_playside']);
				unset($dataMatch['count_playside_team2']);
				unset($dataMatch['Result']);
				unset($dataMatch['question']);
				unset($dataMatch['Picture']);
				unset($dataMatch['PictureOG']);
				unset($dataMatch['EmbedCode']);
				unset($dataMatch['TeamSelect']);
				unset($dataMatch['Analysis']);

				$dataMatch['Team1Name']			=		$tmpMatch['Team1Name'];
				$dataMatch['Team1Logo']			=		$tmpMatch['Team1Logo'];
				$dataMatch['Team1URL']			=		$tmpMatch['Team1URL'];
					
				$dataMatch['Team2Name']			=		$tmpMatch['Team2Name'];
				$dataMatch['Team2Logo']			=		$tmpMatch['Team2Logo'];
				$dataMatch['Team2URL']			=		$tmpMatch['Team2URL'];
					
				$Result['data'][]				=		$dataMatch;
			}
		}else{
	
			$cursorRound		=		$my_round->find( array('cup_id' => $dataCup['id']) )->sort(array( 'order' => 1));
			foreach($cursorRound as $tmpRound){
				$cursorMatch	=		$my_match->find( array('RoundID' => $tmpRound['id']) )->sort(array( 'MatchDateTime' => 1));
				foreach($cursorMatch as $tmpMatch){
					
					$dataMatch		=	null;
					if(($tmpMatch['isAutoMode']==1)&&(intval($tmpMatch['MatchID']))){
						$dataMatch	=	$match->findOne( array('id' => $tmpMatch['MatchID']) );
					}
					
					if(!$dataMatch){
						$dataMatch['MatchStatus']		=		$tmpMatch['MatchStatus'];
						
						$dataMatch['FTScore']			=		$tmpMatch['FTScore'];
						
						$arrTmpScore					=		explode('-',$tmpMatch['FTScore']);					
						if(count($arrTmpScore)>=2){
							$dataMatch['Team1FTScore']	=		intval($arrTmpScore[0]);
							$dataMatch['Team2FTScore']	=		intval($arrTmpScore[1]);
						}else{
							$dataMatch['Team1FTScore']	=		0;
							$dataMatch['Team2FTScore']	=		0;
						}
						
						$dataMatch['MatchDateTime']		=		$tmpMatch['MatchDateTime'];
						$dataMatch['MatchDate'] 		= 		substr($tmpMatch['MatchDateTime'],0,10);
						$dataMatch['MatchTime'] 		= 		substr($tmpMatch['MatchDateTime'],11,5);
						
						$dataMatch['Minute'] 			= 		intval($tmpMatch['MatchMinute']);
						
						$dataMatch['Odds'] 				= 		-1;
						$dataMatch['TeamOdds'] 			= 		-1;
							
					}else{
						$dataMatch['MatchDate'] 		= 		substr($dataMatch['MatchDateTime'],0,10);
						$dataMatch['MatchTime'] 		= 		substr($dataMatch['MatchDateTime'],11,5);
						
						$matchURL = strtolower('http://football.kapook.com/match-'.$dataMatch['id'].'-'.$dataMatch['Team1'].'-'.$dataMatch['Team2']);
						$dataMatch['MatchPageURL'] 		= 		preg_replace('/\s+/', '-', $matchURL);
					}
					
					unset($dataMatch['_id']);
					unset($dataMatch['XSMatchID']);
					unset($dataMatch['XSLeagueID']);
					unset($dataMatch['XSLeagueName']);
					unset($dataMatch['XSLeagueCountry']);
					unset($dataMatch['XSLeagueYear']);
					unset($dataMatch['XSLeagueDesc']);
					unset($dataMatch['SourceDate']);
					unset($dataMatch['SourceTime']);
					unset($dataMatch['MatchDateTimeMongo']);
					unset($dataMatch['LastUpdate']);
					unset($dataMatch['LastUpdateMongo']);
					unset($dataMatch['Team1LP']);
					unset($dataMatch['Team2LP']);
					unset($dataMatch['Team1YC']);
					unset($dataMatch['Team2YC']);
					unset($dataMatch['Team1RC']);
					unset($dataMatch['Team2RC']);
					unset($dataMatch['LastScorers']);
					unset($dataMatch['CreateDateMongo']);
					unset($dataMatch['CreateDate']);
					unset($dataMatch['KPLeagueID']);
					unset($dataMatch['KPLeagueCountryID']);
					unset($dataMatch['Team1KPID']);
					unset($dataMatch['Team2KPID']);
					unset($dataMatch['Priority']);
					unset($dataMatch['count_playside_team1']);
					unset($dataMatch['count_playside']);
					unset($dataMatch['count_playside_team2']);
					unset($dataMatch['Result']);
					unset($dataMatch['question']);
					unset($dataMatch['Picture']);
					unset($dataMatch['PictureOG']);
					unset($dataMatch['EmbedCode']);
					unset($dataMatch['TeamSelect']);
					unset($dataMatch['Analysis']);
					
					
					
					
					$dataMatch['Team1Name']			=		$tmpMatch['Team1Name'];
					$dataMatch['Team1Logo']			=		$tmpMatch['Team1Logo'];
					$dataMatch['Team1URL']			=		$tmpMatch['Team1URL'];
					
					$dataMatch['Team2Name']			=		$tmpMatch['Team2Name'];
					$dataMatch['Team2Logo']			=		$tmpMatch['Team2Logo'];
					$dataMatch['Team2URL']			=		$tmpMatch['Team2URL'];
					
					$tmpRound['listMatch'][]		=		$dataMatch;
				}
				$dataRound[]						=		$tmpRound;
			}
			$Result['listRound']					=		$dataRound;
		}
	}else{
		$Result									=		array();
	}
	
    if ($_REQUEST['callback'] != '') {
        echo $_REQUEST['callback'] . '(' . json_encode($Result) . ')';
    } else {
        echo json_encode($Result);
    }    
        
?>