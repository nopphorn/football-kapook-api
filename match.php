<?php
header('Content-Type: application/json');
### Connect To VPS Singapore
        $mongo          = new MongoClient();
        $db             = $mongo->football;  
        
        $memcache       = new Memcache;
        $memcache->connect('localhost', 11211);


        $match          = $db->football_match;

        $FindArr['id']  = intval($_REQUEST['id']);
        $Row = $match->findOne($FindArr);
			
            $Logo1 		= str_replace(' ','-',$Row['Team1']).'.png';
			$Logo1_MC	=	$memcache->get('Football2014-Team-Logo-' . $Logo1);
            $Logo2 		= str_replace(' ','-',$Row['Team2']).'.png';
			$Logo2_MC	=	$memcache->get('Football2014-Team-Logo-' . $Logo2);
            
            if($Logo1_MC){
                $Row['Team1Logo'] = 'http://football.kapook.com/uploads/logo/' . $Logo1;
            }else{
                $Row['Team1Logo'] = 'http://football.kapook.com/uploads/logo/default.png';
            }
            if($Logo2_MC){
                $Row['Team2Logo'] = 'http://football.kapook.com/uploads/logo/' . $Logo2;
            }else{
                $Row['Team2Logo'] = 'http://football.kapook.com/uploads/logo/default.png';
            }
			
			$matchURL = strtolower('http://football.kapook.com/match-'.$_REQUEST['id'].'-'.$Row['Team1'].'-'.$Row['Team2']);
			$Row['MatchPageURL'] = preg_replace('/\s+/', '-', $matchURL);
			
		// Picture(Gallery)
		if(isset($Row['Picture']))
		{
			$dateNoTime		=	substr($Row['MatchDateTime'],0,4) . substr($Row['MatchDateTime'],5,2) . substr($Row['MatchDateTime'],8,2);
			for( $i=0,$size=count($Row['Picture']) ; $i<$size ; $i++ )
			{
				$Row['Picture'][$i] = 'http://football.kapook.com/uploads/match/' . $dateNoTime . '/' . $Row['Picture'][$i];
			}
		}
		
		// Picture(OG)
		if(isset($Row['PictureOG']))
		{
			$Row['PictureOG'] = 'http://football.kapook.com/uploads/match/og/' . $Row['PictureOG'];
		}
        
        //$Row['Team1Logo'] = 'http://202.183.165.189/uploads/logo/'.str_replace(' ','-',$Row['Team1']).'.png';
        //$Row['Team2Logo'] = 'http://202.183.165.189/uploads/logo/'.str_replace(' ','-',$Row['Team2']).'.png';

    $Result['MatchInfo'] = $Row;
	
			// Fix Date 
			
			//$tmpSourceDate = $Row['SourceDate'].' '.$Row['SourceTime'].':00';
		//if($tmpSourceDate>="2014-10-26 00:00:00"){
		//		$Result['MatchInfo']['MatchDateTime'] = date("Y-m-d H:i:s",strtotime($tmpSourceDate.' + 5 hour'));
		//	}
            // End Fix	
	
    
    if ($_REQUEST['callback'] != '') {
        echo $_REQUEST['callback'] . '(' . json_encode($Result) . ')';
    } else {
        echo json_encode($Result);
    }   
        
?>