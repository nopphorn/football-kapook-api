<?php
	session_start();
	
	$start_date['day']		=	1;
	$start_date['month']	=	8;
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	header('Access-Control-Allow-Origin: http://football.kapook.com');
	
	header('Content-Type: application/json');
	
	function checkCookie(){
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }        
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionRankingYear	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	
	if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login'])){
		$uid	=	0;
	}
	if(!checkCookie()){
		$uid	=	0;
	}
	else{
		$uid	=	(int)$_COOKIE['uid'];
	}
	
	if(!isset($_REQUEST['start']))
		$_REQUEST['start']	=	0;
	if(!isset($_REQUEST['length']))
		$_REQUEST['length']	=	10;
		
	if(!isset($_REQUEST['order'][0]['column'])){
		$column_sort	=	'total_point';
		$orderType		=	'desc';
		$sort_style		=	-1;
	}else{
		if($_REQUEST['order'][0]['dir']=='asc')
		{
			$sort_style		=	1;
		}else{
			$sort_style		=	-1;
		}
		switch($_REQUEST['order'][0]['column']){
			case 0: $sort_style		=	($sort_style*-1);
			case 7:	$column_sort	=	'total_point';
					break;
			case 5:	$column_sort	=	'total_playside_single_win_percent';
					break;
			case 6:	$column_sort	=	'total_playside_multi_win_percent';
					break;
			case 4:	$column_sort	=	'total_rate_calculate';
					break;
		}
		if($sort_style==-1)
			$orderType		=	'desc';
		else
			$orderType		=	'asc';
		
	}
	
	if(isset($_REQUEST['year']))
	{
		$indexdata			=	'season';
		$indexsection		=	$_REQUEST['year'];
		$year				=	$_REQUEST['year'];
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	}else if(isset($_REQUEST['month'])){
		$indexdata			=	'month';
		$indexsection		=	$_REQUEST['month'];
		
		if(date('G')>=6){
			$date_play	=	date('Y-m-d');
		}else{
			$date_play	=	date('Y-m-d',strtotime('now -1 day'));
		}
				
		$tmpMonth	=	date("n",strtotime($date_play));
		$tmpDay		=	date("j",strtotime($date_play));
		if(	( $tmpMonth > $start_date['month'] )	||
			(
				( $tmpMonth == $start_date['month'] ) &&
				( $tmpDay >= $start_date['day'] )
			)
		){
			$year	= 	date("Y",strtotime($date_play));
		}else{
			$year	= 	date("Y",strtotime($date_play . ' - 1 year'));
		}
		
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	}else if(isset($_REQUEST['date'])){
		$indexdata			=	'date';
		$indexsection		=	$_REQUEST['date'];

		if(date('G')>=6){
			$date_play	=	date('Y-m-d');
		}else{
			$date_play	=	date('Y-m-d',strtotime('now -1 day'));
		}
				
		$tmpMonth	=	date("n",strtotime($date_play));
		$tmpDay		=	date("j",strtotime($date_play));
		if(	( $tmpMonth > $start_date['month'] )	||
			(
				( $tmpMonth == $start_date['month'] ) &&
				( $tmpDay >= $start_date['day'] )
			)
		){
			$year	= 	date("Y",strtotime($date_play));
		}else{
			$year	= 	date("Y",strtotime($date_play . ' - 1 year'));
		}
		
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	}else{
		$indexdata			=	'all-time';
		
		if(date('G')>=6){
			$date_play	=	date('Y-m-d');
		}else{
			$date_play	=	date('Y-m-d',strtotime('now -1 day'));
		}
				
		$tmpMonth	=	date("n",strtotime($date_play));
		$tmpDay		=	date("j",strtotime($date_play));
		if(	( $tmpMonth > $start_date['month'] )	||
			(
				( $tmpMonth == $start_date['month'] ) &&
				( $tmpDay >= $start_date['day'] )
			)
		){
			$year	= 	date("Y",strtotime($date_play));
		}else{
			$year	= 	date("Y",strtotime($date_play . ' - 1 year'));
		}
		
		$indexsection		=	null;
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	}
	
	if($_REQUEST['clear']!='1'){
		if($indexsection){
			if($_REQUEST['full']=='1'){
				$ranking_MC = $memcache->get( 'Football2014-rankingfull-'.$indexdata.'-'.$indexsection.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType );
			}else if($_REQUEST['comma_style']=='1'){
				$ranking_MC = $memcache->get( 'Football2014-rankingcomma-'.$indexdata.'-'.$indexsection.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType );
			}else{
				$ranking_MC = $memcache->get( 'Football2014-ranking-'.$indexdata.'-'.$indexsection.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType );
			}
		}else{
			if($_REQUEST['full']=='1'){
				$ranking_MC = $memcache->get( 'Football2014-rankingfull-'.$indexdata.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType );
			}else if($_REQUEST['comma_style']=='1'){
				$ranking_MC = $memcache->get( 'Football2014-rankingcomma-'.$indexdata.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType );
			}else{
				$ranking_MC = $memcache->get( 'Football2014-ranking-'.$indexdata.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType );
			}
		}
	}
	
	if($ranking_MC)
	{
		$returnJson 					=	$ranking_MC;
		$returnJson['draw']				= 	(int)$_REQUEST['draw'];
		//$returnJson['iscache']			= 	true;
	}
	else
	{
		if($indexdata!='all-time'){
			$findArr	=	array( $indexdata => $indexsection );
		}else{
			$findArr	=	array();
		}
		$dataMongo 				= 	$collectionRanking->find($findArr)
									->skip((int)$_REQUEST['start'])
									->limit((int)$_REQUEST['length'])
									->sort(array( $column_sort => $sort_style ));
		
		$countMongoAll			=	$dataMongo->count();
		$countMongo				=	$dataMongo->count(true);
		
		$dataMongo->next();
		
		$returnJson['recordsTotal']		=	$countMongoAll;
		$returnJson['recordsFiltered']	=	$countMongoAll;
		$returnJson['data']				=	array();
		for( $i=0 ; $i<$countMongo ; $i++ )
		{
			$data 					= 	$dataMongo->current();
			
			$ProfileArr				=	$memcache->get( 'Football2014-member-'.$data['id'] );
			
			if(!$ProfileArr){
				$APIProfile 		= 	'http://kapi.kapook.com/profile/member/userid/' . $data['id'];
				$ProfileArr 		=	json_decode(file_get_contents($APIProfile), true);
				if($ProfileArr){
					/*$isExistPic=getimagesize($ProfileArr['data']['picture']);
					if(!is_array($isExistPic)){
						$ProfileArr['data']['picture']		=		'http://signup-demo.kapook.com/images/nopic.png';
					}*/
					$memcache->set( 'Football2014-member-'.$data['id'] , $ProfileArr , MEMCACHE_COMPRESSED, (60*60*24) );
				}
			}

			if($indexsection!=null){
				$APIRanking			= 	'http://202.183.165.189/api/gameapi_member_rank.php?id=' . $data['id'] . '&' . $indexdata . '=' . $indexsection;
			}else{
				$APIRanking			= 	'http://202.183.165.189/api/gameapi_member_rank.php?id=' . $data['id'];
			}
			$RankingArr 			=	json_decode(file_get_contents($APIRanking), true);
			
			// Rate & Stat Playside
			/*$APIRate				= 	'http://202.183.165.189/api/gameapi_member_rate.php?id=' . $data['id'];
			$RateArr 				=	json_decode(file_get_contents($APIRate), true);*/
			$dataMemeber			=	$collectionRankingYear->findOne(array( 'id' => $data['id'], 'season' => $year ));
			
			//var_dump($dataMemeber);
			
			//Rate
			if(!isset($dataMemeber['total_rate_calculate_by_time'])){
				$RateArr['rate']	=	0;
			}else{
				$RateArr['rate']	=	$dataMemeber['total_rate_calculate_by_time'];
			}
			//------------------------------------------------------------------------------------------------//
			/*
			 * Single
			 */
			// Win full
			if(!isset($dataMemeber['total_playside_single_win_full'])){
				$RateArr['single_win_full']	=	0;
			}else{
				$RateArr['single_win_full']	=	$dataMemeber['total_playside_single_win_full'];
			}
			// Win half
			if(!isset($dataMemeber['total_playside_single_win_half'])){
				$RateArr['single_win_half']	=	0;
			}else{
				$RateArr['single_win_half']	=	$dataMemeber['total_playside_single_win_half'];
			}
			// Draw
			if(!isset($dataMemeber['total_playside_single_draw'])){
				$RateArr['single_draw']	=	0;
			}else{
				$RateArr['single_draw']	=	$dataMemeber['total_playside_single_draw'];
			}
			// Lose half
			if(!isset($dataMemeber['total_playside_single_lose_half'])){
				$RateArr['single_lose_half']	=	0;
			}else{
				$RateArr['single_lose_half']	=	$dataMemeber['total_playside_single_lose_half'];
			}
			// Lose full
			if(!isset($dataMemeber['total_playside_single_lose_full'])){
				$RateArr['single_lose_full']	=	0;
			}else{
				$RateArr['single_lose_full']	=	$dataMemeber['total_playside_single_lose_full'];
			}
			// Precent
			if(isset($dataMemeber['total_playside_single_win_percent'])){
				$RateArr['single_percent']		=	$dataMemeber['total_playside_single_win_percent'];
			}else{
				$RateArr['single_percent']		=	0;
			}
			//------------------------------------------------------------------------------------------------//
			/*
			 * Multi
			 */
			// Win
			if(!isset($dataMemeber['total_playside_multi_win'])){
				$RateArr['multi_win']	=	0;
			}else{
				$RateArr['multi_win']	=	$dataMemeber['total_playside_multi_win'];
			}
			// Draw
			if(!isset($dataMemeber['total_playside_multi_draw'])){
				$RateArr['multi_draw']	=	0;
			}else{
				$RateArr['multi_draw']	=	$dataMemeber['total_playside_multi_draw'];
			}
			// Lose
			if(!isset($dataMemeber['total_playside_multi_lose_full'])){
				$RateArr['multi_lose']	=	0;
			}else{
				$RateArr['multi_lose']	=	$dataMemeber['total_playside_multi_lose_full'];
			}
			// Precent
			if(isset($dataMemeber['total_playside_multi_win_percent'])){
				$RateArr['multi_percent']		=	$dataMemeber['total_playside_multi_win_percent'];
			}else{
				$RateArr['multi_percent']		=	0;
			}
			//------------------------------------------------------------------------------------------------//
			if($RateArr['rate']<3){
				$level	=	'<div class="rank-badges font-display"><span class="rank-badges-rookie"></span> เด็กฝึกหัด </div>';
			}
			else if($RateArr['rate']<5){
				$level	=	'<div class="rank-badges font-display"><span class="rank-badges-wonder-kid "> </span> ดาวรุ่ง </div>';
			}
			else if($RateArr['rate']<8){
				$level	=	'<div class="rank-badges font-display"><span class="rank-badges-pro "></span> เซียน </div>';
			}
			else{
				$level	=	'<div class="rank-badges font-display"><span class="rank-badges-angle "></span> เทพ </div>';
			}
			
			if($_REQUEST['full']=='1'){
				$returnJson['data'][]		=	array(
					'DT_RowId'					=>	($uid==$data['id']) ? 'your-rank' : '',
					'ranking'					=>	$RankingArr['rank'],
					'photo'						=>	'<a href="http://football.kapook.com/profile/'.$ProfileArr['data']['username'].'" class="game-user-avartar img-circle" title="'.$ProfileArr['data']['username'].'" style="background: url('.$ProfileArr['data']['picture'].') no-repeat; background-size: cover;"></a>',
					'name'						=>	'<a href="http://football.kapook.com/profile/'.$ProfileArr['data']['username'].'">'.$ProfileArr['data']['username'].'</a>',
					'position'					=>	$level,
					'gameA'						=>	number_format ( $RateArr['rate'] , 1 ),
					'gameB'						=>	round($data['total_playside_single_win_percent']) . '%',
					'gameC'						=>	round($data['total_playside_multi_win_percent']) . '%',
					'gameD'						=>	round($data['total_point']),
					'single_win_full'			=>	$RateArr['single_win_full'],
					'single_win_half'			=>	$RateArr['single_win_half'],
					'single_draw'				=>	$RateArr['single_draw'],
					'single_lose_half'			=>	$RateArr['single_lose_half'],
					'single_lose_full'			=>	$RateArr['single_lose_full'],
					'multi_win'					=>	$RateArr['multi_win'],
					'multi_draw'				=>	$RateArr['multi_draw'],
					'multi_lose'				=>	$RateArr['multi_lose'],
					'single_percent'			=>	round($RateArr['single_percent']) . '%',
					'multi_percent'				=>	round($RateArr['multi_percent']) . '%',
					'name_no_link'				=>	$ProfileArr['data']['username']
				);
			}else{
				$returnJson['data'][]		=	array(
					'DT_RowId'	=>	($uid==$data['id']) ? 'your-rank' : '',
					'ranking'	=>	$RankingArr['rank'],
					'photo'		=>	'<a href="http://football.kapook.com/profile/'.$ProfileArr['data']['username'].'" class="game-user-avartar img-circle" title="'.$ProfileArr['data']['username'].'" style="background: url('.$ProfileArr['data']['picture'].') no-repeat; background-size: cover;"></a>',
					'name'		=>	'<a href="http://football.kapook.com/profile/'.$ProfileArr['data']['username'].'">'.$ProfileArr['data']['username'].'</a>',
					'position'	=>	$level,
					'gameA'		=>	number_format ( $RateArr['rate'] , 1 ),
					'gameB'		=>	round($data['total_playside_single_win_percent']) . '%',
					'gameC'		=>	round($data['total_playside_multi_win_percent']) . '%',
					'gameD'		=>	($_REQUEST['comma_style']=='1') ?  number_format(round($data['total_point'])) : round($data['total_point']),
				);
			}
			
			$dataMongo->next();
		}
		$expire							=		900;
		$returnJson['date']				= 		date("Y-m-d H:i:s");
		if($indexsection){
			if($_REQUEST['full']=='1'){
				$memcache->set( 'Football2014-rankingfull-'.$indexdata.'-'.$indexsection.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType , $returnJson , MEMCACHE_COMPRESSED, $expire );
			}else if($_REQUEST['comma_style']=='1'){
				$memcache->set( 'Football2014-rankingcomma-'.$indexdata.'-'.$indexsection.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType , $returnJson , MEMCACHE_COMPRESSED, $expire );
			}else{
				$memcache->set( 'Football2014-ranking-'.$indexdata.'-'.$indexsection.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType , $returnJson , MEMCACHE_COMPRESSED, $expire );
			}
		}else{
			if($_REQUEST['full']=='1'){
				$memcache->set( 'Football2014-rankingfull-'.$indexdata.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType , $returnJson , MEMCACHE_COMPRESSED, $expire  );
			}else if($_REQUEST['comma_style']=='1'){
				$memcache->set( 'Football2014-rankingcomma-'.$indexdata.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType , $returnJson , MEMCACHE_COMPRESSED, $expire  );
			}else{
				$memcache->set( 'Football2014-ranking-'.$indexdata.'-'.$_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType , $returnJson , MEMCACHE_COMPRESSED, $expire  );
			}
		}
		$returnJson['draw']				= 	(int)$_REQUEST['draw'];
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>