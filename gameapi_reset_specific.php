<?php

exit;

session_start();

$authorized = false;

if(isset($_GET['logout']) && ($_SESSION['auth'])) {
	$_SESSION['auth'] = null;
	session_destroy();
	echo "logging out...";
	?>
	<META HTTP-EQUIV="Refresh" CONTENT="5;URL=http://football.kapook.com/" />
	<?php
}

if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = 'football';
	$pass = 'cityunited';
	if (($user == $_SERVER['PHP_AUTH_USER']) && ($pass == ($_SERVER['PHP_AUTH_PW'])) && (!empty($_SESSION['auth']))) {
		$authorized = true;
	}
}

if ((! $authorized)) {
	header('WWW-Authenticate: Basic Realm="Login please"');
	header('HTTP/1.0 401 Unauthorized');
	$_SESSION['auth'] = true;
	print('Login now or forever hold your clicks...');
	exit;
}

else{
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");

	$collectionGame				=	new MongoCollection($DatabaseMongoDB,"football_game");

	//---------------------------------------------------------------------------------------//
	$collectionGame->update(
		array(
			'listGame' 	=>	array(
				'$elemMatch' => array(
					'match_id' 	=> 	23059,
					'team_win'	=>	1
				),
			)
		),
		array(
			'$set' => array(
				'listGame.$.team_win' => 3,
			)
		),
		array('multiple'	=>	true)
	);
	
	$collectionGame->update(
		array(
			'listGame' 	=>	array(
				'$elemMatch' => array(
					'match_id' 	=> 	23059,
					'team_win'	=>	2
				),
			)
		),
		array(
			'$set' => array(
				'listGame.$.team_win' => 1,
			)
		),
		array('multiple'	=>	true)
	);
	
	$collectionGame->update(
		array(
			'listGame' 	=>	array(
				'$elemMatch' => array(
					'match_id' 	=> 	23059,
					'team_win'	=>	3
				),
			)
		),
		array(
			'$set' => array(
				'listGame.$.team_win' => 2,
			)
		),
		array('multiple'	=>	true)
	);

	echo 'Finish';
}
?>