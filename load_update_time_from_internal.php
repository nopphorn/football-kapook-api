<?php
### Connect To VPS Singapore
$mongo  = new MongoClient();
$db     = $mongo->football;
$tb     = $db->football_match;
if($_REQUEST['date']!=''){
    
    $MatchDate = $_REQUEST['date'].' 23:59:59';
    $MatchDateBack3Hours = $_REQUEST['date'].' 00:00:00';
    
}else{    
    
    $MatchDate = date('Y-m-d H:i:s');
    $MatchDateBack3Hours = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." - 240 minutes"));
    
}
echo 'CHECK BETWEEN => '.$MatchDateBack3Hours.' => '.$MatchDate;
echo '<hr>';

$FindArr['MatchDateTime'] 	= 	array('$gt'=>$MatchDateBack3Hours,'$lt'=>$MatchDate);
$FindArr['AutoMode'] 		= 	intval(0);
$Query  = $tb->find($FindArr);

foreach($Query as $Match){
	
	$updateArr		=		array();
	
	if($Match['MatchStatus']=='1 HF'){
		$to_time 	= 	strtotime(date('Y-m-d H:i:s'));
		$from_time 	= 	strtotime($Match['1HFStart']);
		$min		=	floor(abs($to_time - $from_time) / 60);
		
		if($min>45){
			$updateArr['Minute']	=	45;
		}else{
			$updateArr['Minute']	=	(int)$min;
		}
	}else if($Match['MatchStatus']=='2 HF'){
		$to_time 	= 	strtotime(date('Y-m-d H:i:s'));
		$from_time 	= 	strtotime($Match['2HFStart']);
		$min		=	floor(abs($to_time - $from_time) / 60);
		if($min>45){
			$updateArr['Minute']	=	90;
		}else{
			$updateArr['Minute']	=	(int)(45+$min);
		}
	}else if($Match['MatchStatus']=='E/T'){
		if(!empty($Match['2ETStart'])){
			$to_time 	= 	strtotime(date('Y-m-d H:i:s'));
			$from_time 	= 	strtotime($Match['2ETStart']);
			$min		=	floor(abs($to_time - $from_time) / 60);
			if($min>15){
				$updateArr['Minute']	=	120;
			}else{
				$updateArr['Minute']	=	(int)(105+$min);
			}
		}else if(!empty($Match['1ETStart'])){
			$to_time 	= 	strtotime(date('Y-m-d H:i:s'));
			$from_time 	= 	strtotime($Match['1ETStart']);
			$min		=	floor(abs($to_time - $from_time) / 60);
			if($min>15){
				$updateArr['Minute']	=	105;
			}else{
				$updateArr['Minute']	=	(int)(90+$min);
			}
		}else{
			$updateArr['Minute']		=	90;
		}
	}
    
    $FindUpdate['id']=	intval($Match['id']);
    
    $tb->update($FindUpdate,array('$set'=>$updateArr));
	
    echo 'XSMatchID : '.$FindUpdate['XSMatchID'];
    echo ' => '.$Match['Team1'].'  <font color="#990000">'.$Match['Team1FTScore'].' - '.$Match['Team2FTScore'].'</font>  '.$Match['Team2'];
    echo '<br>Match Date : '.$Match['MatchDateTime'];
    echo '<br>Match Status : '.$Match['MatchStatus'];
    echo '  ('.$Match['Minute'].'") ';
    echo '<br>';
    echo '<br>';
}
?>