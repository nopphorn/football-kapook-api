<?php
	//exit;
	$start_date['day']		=	1;
	$start_date['month']	=	8;
	
	$concurrent				=	10;
	$i						=	0;
	
	if(date('G')>=6){
		$monthToday				=	date("Y-m");
	}else{
		$monthToday				=	date("Y-m",strtotime("now -1 day"));
	}
	
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	$collectionReward			=	new MongoCollection($DatabaseMongoDB,"football_reward");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	$collectionRanking_all		=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	$collectionRanking_season	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	$collectionRanking_month	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	$collectionRanking_day		=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	
	/*
	 * Reward List
	 * 1 : Monthly Login
	 * 2 : Daily Login
	 */
	
	$FindArr['isReward'] 		=	false;
	
	$cursorReward				=	$collectionReward->find($FindArr)->sort(array( 'datetime' => 1 , 'id' => 1 ));
	$num_reward 				= 	$cursorReward->count(true);

	foreach($cursorReward as $RewardInfo){
		$i++;
		// Prepare a data reward
		$inc_update		=	array(
			'total_playside_point'						=> 	$RewardInfo['value'],
			'total_point'								=> 	$RewardInfo['value'],
			'total_playside_single_win_percent'			=> 	0.0,
			'total_playside_multi_win_percent'			=>	0.0,
			'total_rate_calculate'						=>	0.0,
			'total_rate_calculate_by_time'				=>	0.0
		);
		
		// Get a date play
		if( date("G",strtotime($RewardInfo['datetime'])) >= 6 ){
			$date	=	date("Y-m-d",strtotime($RewardInfo['datetime']));
		}else{
			$date	=	date("Y-m-d",strtotime($RewardInfo['datetime']  . ' - 1 day'));
		}
		// Get a month
		$month		=	date("Y-m",strtotime($date));
		// Get a year season
		if(	( date("n") > $start_date['month'] )	||
			(
				( $tmpMonth == $start_date['month'] ) &&
				( $tmpDay >= $start_date['day'] )
			)
		){
			$year	=	date("Y");
		}else{
			$year	=	date("Y",strtotime($date . ' - 1 year'));
		}
		/*
		 * UpdatePoint by All-Time
		 */
		$collectionRanking_all->update(
			array('id' 		=> 	$RewardInfo['user_id']),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	true)
		);
					
		/*
		 * UpdatePoint by Season
		 */
		$collectionRanking_season->update(
			array(
				'id' 		=> 	$RewardInfo['user_id'],
				'season'	=>	$year
			),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	true)
		);
					
		/*
		 * UpdatePoint by Month
		 */
		$collectionRanking_month->update(
			array(
				'id' 		=> 	$RewardInfo['user_id'],
				'month'		=>	$month
			),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	true)
		);
					
		/*
		 * UpdatePoint by Date
		 */
		$collectionRanking_day->update(
			array(
				'id' 		=> 	$RewardInfo['user_id'],
				'date'		=>	$date
			),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	true)
		);
		
		/*
		 * UpdatePoint for User
		 */
		if( $monthToday == $month ){
			if($RewardInfo['type'] == 1){
				$collectionMember->update(
					array('id' => $RewardInfo['user_id']),
						array(
							'$set' 	=> 	array(
								'point' 	=> 	$RewardInfo['value']
						)
					),
					array('upsert'	=>	true)
				);
			}else{
				$collectionMember->update(
					array('id' => $RewardInfo['user_id']),
						array(
							'$inc' 	=> 	array(
								'point' 	=> 	$RewardInfo['value']
						)
					),
					array('upsert'	=>	true)
				);
			}
		}
		
		$collectionReward->update(
			array('id' => $RewardInfo['id']),
				array(
					'$set' 	=> 	array(
						'isReward' 	=> 	true
				)
			),
			array('upsert'	=>	true)
		);
		
		$memcache->delete('Football2014-GameData-' . $RewardInfo['user_id']);
		
		echo 'reward No.' . $RewardInfo['id'] . ':Type '. $RewardInfo['type'] .':user '. $RewardInfo['user_id'] .':Point '. $RewardInfo['value'] .'<br>';
		
		if($i>=$concurrent){
			break;
		}
		
	}
	
	echo 'finish';
	
	if($num_reward){
		header( "refresh: 2; url=http://football.kapook.com/api/gameapi_restore_reward.php" );
	}

	return;
?>