<?php

if($_REQUEST['next_day']==''){    
    $next_day = 0;
}else{
    $next_day = $_REQUEST['next_day'];
}

if($next_day>14){
    $next_day=14;
}

$date = date("Y-m-d",strtotime(date("Y-m-d")." + $next_day day"));  



if($date!=''){     
    
    $FromDate   = $date.' 06:00:00';
    $ToDate     = date("Y-m-d 05:59:59",strtotime($FromDate." + 1 day"));   
    $FindArr['MatchDateTime'] = array('$gte'=>$FromDate,'$lte'=>$ToDate);
    
    echo 'CHECK BETWEEN => '.$FromDate.' => '.$ToDate;
    echo '<hr>';

    ### Select From Thailand
    $mongo1         = new MongoClient();
    $db1            = $mongo1->football;
    $tb1            = $db1->football_match;
    $zone1          = $db1->football_zone;
    $league1        = $db1->football_league;
    $team1          = $db1->football_team;
    
    $AllZone        = $zone1->find()->sort(array('id'=>1));
    $MaxZoneID      = 0;
    foreach($AllZone as $Zone){
        $ZoneIDByNameEN[$Zone['NameEN']]=$Zone['id'];   
        if($Zone['id']>$MaxZoneID){ $MaxZoneID=$Zone['id']; }
    }
        
    
    $AllLeague        = $league1->find()->sort(array('id'=>1));
    $MaxLeagueID      = 0;
    foreach($AllLeague as $League){
        $LeagueIDByNameEN[$League['NameEN']]=$League['id'];   
        $LeagueIDByXSLeagueID[$League['XSLeagueID']]=$League['id'];          
        if($League['id']>$MaxLeagueID){ $MaxLeagueID=$League['id']; }
    }
    
    
    
    $AllLeague        = $league1->find()->sort(array('id'=>1));
    $MaxLeagueID      = 0;
    foreach($AllLeague as $League){
        $LeagueAutoStatusByLeagueID[$League['id']] = intval($League['Status']);
        $LeagueIDByNameEN[$League['NameEN']]=$League['id'];   
        $LeagueIDByXSLeagueID[$League['XSLeagueID']]=$League['id'];          
        if($League['id']>$MaxLeagueID){ $MaxLeagueID=$League['id']; }
        
        
    }
    
    
    
    $AllTeam        = $team1->find()->sort(array('id'=>1));
    $MaxTeamID      = 0;
    foreach($AllTeam as $Team){
        $TeamIDByNameEN[$Team['NameEN']]=$Team['id'];           
        if($Team['id']>$MaxTeamID){ $MaxTeamID=$Team['id']; }
    }    
       
    
    
    $Query1         = $tb1->find($FindArr);     
    foreach($Query1 as $Match1){
        $FeedMath[] =  $Match1['XSMatchID'];    
    }


    $QueryAllMatch  = $tb1->find()->sort(array('id'=>1));
    $CurrentAllMatchIndex  = 0;
    foreach($QueryAllMatch as $tmpMatch){
        if($_REQUEST['reload_xsmid']==$tmpMatch['XSMatchID']){ continue; }
        $ExistMathByXSMatchID[$tmpMatch['XSMatchID']] = true;   
        if($tmpMatch['id'] > $CurrentAllMatchIndex){$CurrentAllMatchIndex=$tmpMatch['id']; }
    }
    
    
    
    ### Connect To VPS Singapore
    $mongo          = new MongoClient('mongodb://198.211.108.80');
    $db             = $mongo->football_xscore_feed;
    $tb             = $db->football_match_xscore;     



    $Query  = $tb->find($FindArr);     
    
    ### CHECK NEW MATCH ###    
    foreach($Query as $Match){        
        $FeedMathVPS[] =  $Match['XSMatchID'];              
        if(!in_array($Match['XSMatchID'], $FeedMath)){            
            $NewMatch[] = $Match;
        }        
    }    
    
    $NewItems = 0;
    foreach($NewMatch as $MatchInfo){     
        
            unset($InsertArr);
            $InsertArr['XSMatchID'] = $MatchInfo['XSMatchID'];
            $InsertArr['XSLeagueID'] = $MatchInfo['XSLeagueID'];
            $InsertArr['XSLeagueName'] = $MatchInfo['XSLeagueName'];
            $InsertArr['XSLeagueCountry'] = $MatchInfo['XSLeagueCountry'];
            $InsertArr['XSLeagueYear'] = $MatchInfo['XSLeagueYear'];
            $InsertArr['XSLeagueDesc'] = $MatchInfo['XSLeagueDesc'];
            $InsertArr['SourceDate'] = $MatchInfo['SourceDate'];
            $InsertArr['SourceTime'] = $MatchInfo['SourceTime'];
            $InsertArr['MatchDateTime'] = $MatchInfo['MatchDateTime'];
            $InsertArr['MatchDateTimeMongo'] = $MatchInfo['MatchDateTimeMongo'];
            $InsertArr['LastUpdate'] = $MatchInfo['LastUpdate'];
            $InsertArr['LastUpdateMongo'] = $MatchInfo['LastUpdateMongo'];
            $InsertArr['MatchStatus'] = strval($MatchInfo['MatchStatus']);
            $InsertArr['Minute'] = intval($MatchInfo['Minute']);
            $InsertArr['Team1'] = $MatchInfo['Team1'];
            $InsertArr['Team2'] = $MatchInfo['Team2'];
            $InsertArr['Team1LP'] = $MatchInfo['Team1LP'];
            $InsertArr['Team2LP'] = $MatchInfo['Team2LP'];
            $InsertArr['Team1YC'] = $MatchInfo['Team1YC'];
            $InsertArr['Team2YC'] = $MatchInfo['Team2YC'];
            $InsertArr['Team1RC'] = $MatchInfo['Team1RC'];
            $InsertArr['Team2RC'] = $MatchInfo['Team2RC'];
            $InsertArr['HTScore'] = $MatchInfo['HTScore'];
            $InsertArr['FTScore'] = $MatchInfo['FTScore'];
            $InsertArr['ETScore'] = $MatchInfo['ETScore'];
            $InsertArr['PNScore'] = $MatchInfo['PNScore'];
            $InsertArr['LastScorers'] = $MatchInfo['LastScorers'];
            $InsertArr['Team1FTScore'] = intval($MatchInfo['Team1FTScore']);
            $InsertArr['Team2FTScore'] = intval($MatchInfo['Team2FTScore']);
            $InsertArr['CreateDateMongo'] = $MatchInfo['CreateDateMongo'];
            $InsertArr['CreateDate'] = $MatchInfo['CreateDate'];
            $InsertArr['KPLeagueID'] = $LeagueIDByXSLeagueID[$MatchInfo['XSLeagueID']];
            $InsertArr['KPLeagueCountryID'] = $ZoneIDByNameEN[$MatchInfo['XSLeagueCountry']];
            $InsertArr['Team1KPID'] = intval($TeamIDByNameEN[str_replace("</div>","",$MatchInfo['Team1'])]);
            $InsertArr['Team2KPID'] = intval($TeamIDByNameEN[str_replace("</div>","",$MatchInfo['Team2'])]); 
            $InsertArr['GameStatus'] = intval(0);
            $InsertArr['View'] = intval(0);
			$InsertArr['AutoMode'] = intval(1);
            
            if($ExistMathByXSMatchID[$MatchInfo['XSMatchID']]){        
                
                echo '<hr><font color="#FF0000">UPDATE : </font><br>';   
                unset($FindUpdateArr);
                $FindUpdateArr['XSMatchID'] = intval($MatchInfo['XSMatchID']);
                $tb1->update($FindUpdateArr,array('$set'=>$InsertArr),array("multiple" => true));
                
            }else{        
                
                $CurrentAllMatchIndex+=1;                
                $InsertArr['id'] = intval($CurrentAllMatchIndex);                   
                $InsertArr['Odds'] = -1;
                $InsertArr['Status'] = intval($LeagueAutoStatusByLeagueID[$InsertArr['KPLeagueID']]);
                echo '<hr><font color="#009900">INSERT : </font><br>';
                $tb1->insert($InsertArr);
				
            }      

            print_r($InsertArr);    

    }
    

    echo '<br><br>Total New Item : '.$NewItems;
    
    
    
   
}
?>