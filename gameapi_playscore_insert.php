<?php
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");

	header('Content-Type: application/json');
	
	function checkCookie()
    {
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }        
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMatch        =	new MongoCollection($DatabaseMongoDB,"football_match");

	//$datamember			=	$collectionMember->findOne(array( 'id' => $idMember));
	
	if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
	{
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		echo json_encode($returnJson);
		return;
	}
	if(!checkCookie())
	{
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		echo json_encode($returnJson);
		return;
	}
	$uid	=	(int)$_COOKIE['uid'];
	
	if(
		(!is_numeric($_REQUEST['match_id']))	||
		(!is_numeric($_REQUEST['Team1Score']))	||
		(!is_numeric($_REQUEST['Team2Score']))	
	)
	{
		$returnJson	=	array(
			'code_id'	=>	403,
			'message'	=>	'Input is Not Numeric.'
		);
		echo json_encode($returnJson);
		return;
	}
	
	$dataMatch			=	$collectionMatch->findOne(
		array( 
			'id' 		=> (int)$_REQUEST['match_id'],
			'Status'	=>	1 ,
			'MatchStatus' =>  'Sched'
		)
	);
	if(!$dataMatch)
	{
		$returnJson	=	array(
			'code_id'	=>	402,
			'message'	=>	'Not Found a Match.'
		);
		echo json_encode($returnJson);
		return;
	}
	$dataOldGame		=	$collectionGame->findOne(
		array(
			'match_id' 	=> 	(int)$_REQUEST['match_id'],
			'user_id' 	=> 	$uid,
			'game_type'	=>	1
		)
	);
		
	if(!$dataOldGame)
	{
		$cursorGame			=	$collectionGame->find();
		$cursorGame->sort( array( 'id' => -1 ) );
		$cursorGame->limit(1);
		$cursorGame->next();
					
		$dataGame			=	$cursorGame->current();
						
		$dataupdate['id']	=	((int)$dataGame['id']+1);
	}
	else
	{
		$dataupdate['id']	=	$dataOldGame['id'];
	}
	
	if(date('G',strtotime($dataMatch['MatchDateTime']))>=6){
		$dataupdate['date_play']	=	date("Y-m-d",strtotime($dataMatch['MatchDateTime']));
	}
	else
	{
		$dataupdate['date_play']	=	date("Y-m-d",strtotime($dataMatch['MatchDateTime']  . ' - 1 day'));
	}
	$dataupdate['date_match']		=	$dataMatch['MatchDateTime'];
					
	$dataupdate['match_id']		=	(int)$_REQUEST['match_id'];
	$dataupdate['user_id']		=	$uid;
	$dataupdate['score_team1'] 	=	(int)$_REQUEST['Team1Score'];
	$dataupdate['score_team2'] 	= 	(int)$_REQUEST['Team2Score'];
	$dataupdate['status'] 		= 	1;
	$dataupdate['result_point'] = 	0;
	$dataupdate['score_point'] 	= 	0;
	$dataupdate['game_type'] 	= 	1;
						
	$collectionGame->update(
		array(
			'id' 	=> (int)$dataupdate['id']
		),
		array('$set' => $dataupdate),
		array("upsert" => true)
	);
			
	$returnJson	=	array(
		'code_id'		=>	200,
		'message'		=>	'Insert data successful.',
		'uid'			=>	$_COOKIE['uid'],
		//'memMatch'		=>	'Football2014-game-playscore-info-' . $uid . '-' . $_REQUEST['match_id'],
		//'memList'		=>	'Football2014-game-playscore-list-info-' . $uid . '-' . $dataupdate['date_play'],
	);
	
	$memcache->delete( 'Football2014-game-playscore-info-' . $uid . '-' . $_REQUEST['match_id'] );
	$memcache->delete( 'Football2014-game-playscore-list-info-' . $uid . '-' . $dataupdate['date_play'] );
	$memcache->delete( 'Football2014-MatchPlayScoreStat-' . $_REQUEST['match_id']);
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>