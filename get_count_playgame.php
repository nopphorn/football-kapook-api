<?php
	//$start_date['day']		=	1;
	//$start_date['month']	=	8;

	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	header('Content-Type: application/json');
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionNewPlayer	=	new MongoCollection($DatabaseMongoDB,"football_log_new");

	if(!isset($_REQUEST['start_date'])){
		$_REQUEST['start_date']		=		date('Y-m-d',strtotime('-7 day'));
	}
	
	if(!isset($_REQUEST['end_date'])){
		$_REQUEST['end_date']		=		date('Y-m-d',strtotime('+1 day'));
	}else{
		$_REQUEST['end_date']		=		date('Y-m-d',strtotime($_REQUEST['end_date'] . ' 00:00:00 +1 day'));
	}
	
	$returnJson['count']			=		0;
	
	while(strtotime($_REQUEST['start_date']) < strtotime($_REQUEST['end_date'])){
		/*
		$ops = array(
			array(
				'$match' => array(
					'timestamp' 	=> 	array(
						'$gte' => $_REQUEST['start_date'],
						'$lte' => date('Y-m-d 06:00:00',strtotime($_REQUEST['start_date'] . '+1 day'))
					),
					'status'		=>	1,
					'game_type'		=>	2
				)
			),
			array(
				'$group' => array(
					'_id' => null,
					'count' => array('$sum' => 1),
				),
			),
		);*/
		/*
		 * Total Play
		 */
		//----------------------------------------------------------------------------------//
		$ops = array(
			array(
				'$match' => array(
					'date_play' 	=> 	date('Y-m-d',strtotime($_REQUEST['start_date'])),
					'status'		=>	array('$gte' => 1),
					'game_type'		=>	2
				)
			),
			array(
				'$group' => array(
					'_id' => null,
					'count' => array('$sum' => 1),
				),
			),
		);
		$dataUsedPoint	=	$collectionGame->aggregate($ops);
		if(isset($dataUsedPoint['result'][0]['count'])){
			$count 	= 	$dataUsedPoint['result'][0]['count'];
		}else{
			$count	=	0;
		}
		$returnJson['dataset'][date('Y-m-d',strtotime($_REQUEST['start_date']))]['total_play']		=		$count;
		//----------------------------------------------------------------------------------//
		/*
		 * Total User
		 */
		//----------------------------------------------------------------------------------//
		$ops = array(
			array(
				'$match' => array(
					'date_play' 	=> 	date('Y-m-d',strtotime($_REQUEST['start_date'])),
					'status'		=>	array('$gte' => 1),
					'game_type'		=>	2
				)
			),
			array(
				'$group' => array(
					'_id' => array( '_id' => '$user_id' ),
				),
			),
		);
		
		$dataUsedPoint	=	$collectionGame->aggregate($ops);
		if(isset($dataUsedPoint['result'])){
			$count 	= 	count($dataUsedPoint['result']);
		}else{
			$count	=	0;
		}
		$returnJson['dataset'][date('Y-m-d',strtotime($_REQUEST['start_date']))]['total_user']		=		$count;
		//----------------------------------------------------------------------------------//
		/*
		 * Total New Player
		 */
		//----------------------------------------------------------------------------------//
		$ops = array(
			array(
				'$match' => array(
					'time_stamp' 	=> 	array(
						'$gte' => date('Y-m-d 06:00:00',strtotime($_REQUEST['start_date'])),
						'$lte' => date('Y-m-d 05:59:59',strtotime($_REQUEST['start_date'] . ' +1 day'))
					),
				)
			),
			array(
				'$group' => array(
					'_id' => array( '_id' => '$user_id' ),
				),
			),
		);
		
		$dataUsedPoint	=	$collectionNewPlayer->aggregate($ops);
		if(isset($dataUsedPoint['result'])){
			$count 	= 	count($dataUsedPoint['result']);
		}else{
			$count	=	0;
		}
		$returnJson['dataset'][date('Y-m-d',strtotime($_REQUEST['start_date']))]['total_new_user']		=		$count;

		$_REQUEST['start_date']		=		date('Y-m-d',strtotime($_REQUEST['start_date'] . '+1 day'));
		$returnJson['count']++;
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>