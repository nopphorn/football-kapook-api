<?php
	include 'manage_checklogin.php';

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");

		//echo 'not full';
		$dataMongo 		= 	$collection->findOne(array( 'id' => (int)$_POST['matchid'] ));
		if(!empty($dataMongo))
		{
			//---------------------------------------------------------------------------------------------//
			// Setting a data 
			//---------------------------------------------------------------------------------------------//
			
			$tvListData				=	array();
			if($_POST['tvList0']){
				$tvListData[]		=	$_POST['tvList0'];
			}
			if($_POST['tvList1']){
				$tvListData[]		=	$_POST['tvList1'];
			}
			if($_POST['tvList2']){
				$tvListData[]		=	$_POST['tvList2'];
			}
			
			$collection->update(
				array('id' => (int)$_POST['matchid']),
				array('$set' => array(
					'Team1FTScore' 	=> 	(int)$_POST['team1score'],
					'Team2FTScore' 	=> 	(int)$_POST['team2score'],
					'ETScore'		=>	empty($_POST['isExtra'])			? '-'	:	(int)$_POST['team1scoreET'] . '-' . (int)$_POST['team2scoreET'],
					'PNScore'		=>	empty($_POST['isPK'])				? '-'	:	(int)$_POST['team1scorePK'] . '-' . (int)$_POST['team2scorePK'],
					'TeamOdds'		=>	empty($_POST['inputteamodds']) 		? 0 	: 	(int)$_POST['inputteamodds'],
					'Odds'			=>	empty($_POST['inputodds']) 			? 0.0 	: 	(float)$_POST['inputodds'],
					'MatchStatus'	=> 	$_POST['matchstatus'],	
					'Status' 		=> (int)$_POST['status'],
					'TVLiveList'	=>	$tvListData,
					'LastUpdate_IP'		=>	$ip,
					'LastUpdate_UserID'	=>	isset($_COOKIE["UserID"]) 			? intval($_COOKIE["UserID"]) : "UNKNOWN",
					'LastUpdate_Time'	=>	date('Y-m-d H:i:s')
				))
			);
			$memcache->delete('Football2014-MatchContents-' . $_POST['matchid']);
		}
		
		header( "location: manage_match.php" );
		exit(0);
?>