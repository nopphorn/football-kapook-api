<?php

session_start();

$authorized = false;

$start_date['day']		=	1;
$start_date['month']	=	8;

if(date('G')>=6){
	$monthToday				=	date("Y-m");
}else{
	$monthToday				=	date("Y-m",strtotime("now -1 day"));
}

if(isset($_GET['logout']) && ($_SESSION['auth'])) {
	$_SESSION['auth'] = null;
	session_destroy();
	echo "logging out...";
	?>
	<META HTTP-EQUIV="Refresh" CONTENT="5;URL=http://football.kapook.com/" />
	<?php
}

if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = 'football';
	$pass = 'cityunited';
	if (($user == $_SERVER['PHP_AUTH_USER']) && ($pass == ($_SERVER['PHP_AUTH_PW'])) && (!empty($_SESSION['auth']))) {
		$authorized = true;
	}
}

if ((! $authorized)) {
	header('WWW-Authenticate: Basic Realm="Login please"');
	header('HTTP/1.0 401 Unauthorized');
	$_SESSION['auth'] = true;
	print('Login now or forever hold your clicks...');
	exit;
}

else{

	if(empty($_REQUEST['match_id'])){
		echo 'Not update';
		exit;
	}
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	
	$collectionGame				=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	$collectionRanking_all		=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	$collectionRanking_season	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	$collectionRanking_month	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	$collectionRanking_day		=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	
	//---------------------------------------------------------------------------------------//
	// Find A Game By Match
	$dataGame = $collectionGame->find(
		array(
			'listGame' => array(
				'$elemMatch' => array(
					'match_id' => intval($_REQUEST['match_id'])
				)
			)
		)
	);
	foreach($dataGame as $data){
		
		$inc_update		=	array(
			'total_playside_point'						=> 	((-1)*$data['point_return']),
			'total_point'								=> 	((-1)*$data['point_return']),
			'total_play'								=>	-1,
		);
		
		if(count($data['listGame'])>1){
			$inc_update['total_playside_single']					=	-1;
			if($data['listGame'][0]['isWDL'] > 0){
				$inc_update['total_playside_single_win']			=	-1;
				if($data['listGame'][0]['isWDL']>1){
					$inc_update['total_playside_single_win_full']	=	-1;
				}else{
					$inc_update['total_playside_single_win_half']	=	-1;
				}
			}else if($data['listGame'][0]['isWDL'] < 1){
				$inc_update['total_playside_single_lose_full']		=	-1;
			}else if($data['listGame'][0]['isWDL'] < 0){
				$inc_update['total_playside_single_win_half']		=	-1;
			}else{
				$inc_update['total_playside_single_draw']			=	-1;
			}
		}else{
			$inc_update['total_playside_multi']						=	-1;
			if($data['multiple']>1){
				$inc_update['total_playside_multi_win']				=	-1;
			}else if($data['multiple']<1){
				$inc_update['total_playside_multi_lose_full']		=	-1;
			}else{
				$inc_update['total_playside_multi_draw']			=	-1;
			}
		}
		
		// Set Time
		$tmpMonth				=	date("n",strtotime($data['date_play']));
		$tmpDay					=	date("j",strtotime($data['date_play']));
						
		if(	( $tmpMonth > $start_date['month'] )||
			(
				( $tmpMonth == $start_date['month'] ) &&
				( $tmpDay >= $start_date['day'] )
			)
		){
			$year	=	date("Y",strtotime($data['date_play']));
		}
		else{
			$year	=	date("Y",strtotime($data['date_play'] . ' - 1 year'));
		}
		$month	=	substr($data['date_play'],0,7);

		/*
		 * UpdatePoint by All-Time
		 */
		$collectionRanking_all->update(
			array('id' 		=> 	$data['user_id']),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	false)
		);
		
		/*
		 * UpdatePoint by Season
		 */
		$collectionRanking_season->update(
			array(
				'id' 		=> 	$data['user_id'],
				'season'	=>	$year
			),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	false)
		);
					
		/*
		 * UpdatePoint by Month
		 */
		$collectionRanking_month->update(
			array(
				'id' 		=> 	$data['user_id'],
				'month'		=>	$month
			),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	false)
		);
		
		/*
		 * UpdatePoint by Date
		 */
		$collectionRanking_day->update(
			array(
				'id' 		=> 	$data['user_id'],
				'date'		=>	$data['date_play']
			),
			array('$inc' 	=> 	$inc_update),
			array('upsert'	=>	false)
		);
		
		/*
		 * UpdatePoint for User
		 */
		if( $monthToday == $month ){
			$collectionMember->update(
			array('id' => $data['user_id']),
				array(
					'$inc' 	=> 	array(
						'point' 	=> ((-1)*$data['point_return'])
					)
				),
				array('upsert'	=>	false)
			);
		}

		$countSubGame			=	count($data['listGame']);
		for( $j=0 ; $j<$countSubGame ; $j++ )
		{
			$data['listGame'][$j]['status']		=	1;
			unset($data['listGame'][$j]['isWDL']);
		}
		
		$collectionGame->update(
			array(
				'id' 	=> (int)$data['id']
			),
			array('$set' =>
				array(
					'status'		=>	1,
					'point_return'	=> 	0.0,
					'multiple'		=>	1.0,
					'listGame'		=>	$data['listGame']
				)
			)
		);

		echo $data['id'] . "\n";
	}
	echo 'Finish';
}
?>