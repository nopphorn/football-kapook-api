<?php
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	header('Access-Control-Allow-Origin: http://football.kapook.com');
	
	header('Content-Type: application/json');

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionMatch		=	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");

	if(!isset($_REQUEST['start']))
		$_REQUEST['start']	=	0;
	
	if(!isset($_REQUEST['length']))
		$_REQUEST['length']	=	10;
		
	if(!isset($_REQUEST['sort_field'])){
		$column_sort	=	'count_playside';
		$sort_style		=	-1;
		$orderType		=	'desc';
	}else{
		if($_REQUEST['sort_dir']=='asc'){
			$sort_style		=	1;
			$orderType		=	'asc';
		}else{
			$sort_style		=	-1;
			$orderType		=	'desc';
		}
		
		if($_REQUEST['sort_field']=='total'){
			$column_sort	=	'count_playside';
		}else if($_REQUEST['sort_field']=='side1'){
			$column_sort	=	'count_playside_team1';
		}else if($_REQUEST['sort_field']=='side2'){
			$column_sort	=	'count_playside_team2';
		}else{
			$column_sort	=	'count_playside';
		}
	}
	
	if(isset($_REQUEST['date'])){
		$date			=	$_REQUEST['date'];
	}else{
		if(date('G')>=6){
			$date		=	date("Y-m-d");
		}else{
			$date		=	date("Y-m-d",strtotime('-1 day'));
		}
	}
	
	$FromDate   = $date.' 06:00:00';
	$ToDate     = date("Y-m-d 05:59:59",strtotime($FromDate." + 1 day"));
	//echo $FromDate . '===>' . $ToDate;
    $FindArr['MatchDateTime'] 	= 	array('$gte' => $FromDate,'$lte' => $ToDate);
	$FindArr['Status']			=	1;
	$FindArr['count_playside']	=	array('$gte' => 1);
	
	
	if($_REQUEST['clear']!='1'){
		$topplay_MC 			= $memcache->get( 'Football2014-topplay-' . $date . '-' . $_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType );
	}else{
		$topplay_MC 			= null;
		
	}
	
	if($topplay_MC){
		$returnJson 			=	$topplay_MC;

	}else{
		$dataMongo 				= 	$collectionMatch->find($FindArr)
									->skip((int)$_REQUEST['start'])
									->limit((int)$_REQUEST['length'])
									->sort(array( $column_sort => $sort_style ));

		$countMongo				=	$dataMongo->count(true);
		$dataMongo->next();
		$returnJson['data']				=	array();
		for( $i=0 ; $i<$countMongo ; $i++ )
		{
			$dataMatch 			= 		$dataMongo->current();
			
			// Team1 data
			$dataTeam1			= 		$collectionTeam->findOne(	array( 	'id' => $dataMatch['Team1KPID'] ));
			// Team2 data
			$dataTeam2			= 		$collectionTeam->findOne(	array( 	'id' => $dataMatch['Team2KPID'] ));
					
			$Team1Logo 			= 		str_replace(' ','-',$dataTeam1['NameEN']).'.png';
			$Team1Logo_MC		=		$memcache->get('Football2014-Team-Logo-' . $Team1Logo);
			if($Team1Logo_MC){
				$Team1LogoPath 	= 		'http://football.kapook.com/uploads/logo/' . $Team1Logo;
			}else{
				$Team1LogoPath	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
					
			$Team2Logo 			= 		str_replace(' ','-',$dataTeam2['NameEN']).'.png';
			$Team2Logo_MC		=		$memcache->get('Football2014-Team-Logo-' . $Team2Logo);
			if($Team2Logo_MC){
				$Team2LogoPath 	= 		'http://football.kapook.com/uploads/logo/' . $Team2Logo;
			}else{
				$Team2LogoPath 	= 		'http://football.kapook.com/uploads/logo/default.png';
			}
			
			if(isset($dataMatch['count_playside'])){
				$totalplayside		=	$dataMatch['count_playside'];
				if(isset($dataMatch['count_playside_team1'])){
					$playside1		=	$dataMatch['count_playside_team1'];
				}else{
					$playside1		=	0;
				}
			}else{
				$totalplayside		=	0;
				$playside1			=	0;
			}
			
			$tmpPercent		=	round(($playside1/$totalplayside)*100);

			$returnJson['data'][]		=	array(
				'Team1KPID'						=>		$dataTeam1['id'],
				'Team2KPID'						=>		$dataTeam2['id'],
				'Team1'							=>		empty($dataTeam1['NameTH'])				?	$dataTeam1['NameEN']		:	$dataTeam1['NameTH'],
				'Team2'							=>		empty($dataTeam2['NameTH'])				?	$dataTeam2['NameEN']		:	$dataTeam2['NameTH'],
				'Team1Short'					=>		empty($dataTeam1['NameTHShort'])		?	null						:	$dataTeam1['NameTHShort'],
				'Team2Short'					=>		empty($dataTeam2['NameTHShort'])		?	null						:	$dataTeam2['NameTHShort'],
				'Team1Logo'						=>		$Team1LogoPath,
				'Team2Logo'						=>		$Team2LogoPath,
				'Odds'							=>		($dataMatch['Odds']==-1)				?	null						:	$dataMatch['Odds'],
				'TeamOdds'						=>		($dataMatch['Odds']==-1)				?	null						:	$dataMatch['TeamOdds'],
				'Team1FTScore'					=>		empty($dataMatch['Team1FTScore'])		?	0							:	$dataMatch['Team1FTScore'],
				'Team2FTScore'					=>		empty($dataMatch['Team2FTScore'])		?	0							:	$dataMatch['Team2FTScore'],
				'MatchStatus'					=>		$dataMatch['MatchStatus'],
				'count_playside'				=>		$totalplayside,
				'count_playside_team1'			=>		$playside1,
				'count_playside_team2'			=>		$totalplayside - $playside1,
				'count_playside_team1_percent'	=>		$tmpPercent,
				'count_playside_team2_percent'	=>		100 - $tmpPercent,
				'MatchPageURL' 					=> 		'http://football.kapook.com/match-'.$dataMatch['id'].'-'.$dataTeam1['NameEN'].'-'.$dataTeam2['NameEN'],
			);
			
			$dataMongo->next();
		}
		
		$expire = 300;
		$memcache->set( 'Football2014-topplay-' . $date . '-' . $_REQUEST['start'].'-'.$_REQUEST['length'].'-'.$column_sort.'-'.$orderType , $returnJson , MEMCACHE_COMPRESSED, $expire );
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>