<?php header( "refresh: 0; url=http://football.kapook.com/api/adminfootball/team" ); /*
	include 'manage_checklogin.php';
	include 'manage_index.php';
	
	$memcache = new Memcache;
    $memcache->connect('localhost', 11211);
	
	if(isset($_REQUEST['filter_status']))
		$_SESSION["filter_status"] 	= 	$_REQUEST['filter_status'];
	else if(isset($_SESSION["filter_status"]))
		$_REQUEST['filter_status']	=	$_SESSION["filter_status"];
		
	if(isset($_REQUEST['filter_team']))
		$_SESSION["filter_team"] 	= 	$_REQUEST['filter_team'];
	else if(isset($_SESSION["filter_team"]))
		$_REQUEST['filter_team']	=	$_SESSION["filter_team"];
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	
	if(		(isset($_POST['teamid']))
		&& 	(isset($_POST['nameth'])) 
		&& 	(isset($_POST['namethshort']))
		&& 	(isset($_POST['status']))
	)
	{
		$dataMongo 		= 	$collectionTeam->findOne(array( 'id' => (int)$_POST['teamid'] ));
		
		if(!empty($dataMongo))
		{
			$dataInsert		=	array(
				'NameTH' 		=> 	$_POST['nameth'],
				'NameTHShort'	=>	$_POST['namethshort'],
				'Status' 		=> 	(int)$_POST['status']	
			);
			if(isset($_POST['info']))
				$dataInsert['Info']				=	$_POST['info'];
			if(isset($_POST['embedCode']))
				$dataInsert['EmbedCode']		=	$_POST['embedCode'];
				
			if(isset($_FILES['logopic']))
			{
				$filepic	=	$_FILES['logopic'];
				if	(($filepic['error'] != 4) && ($filepic['error'] != 0))
				{	
					echo 'Error:Cannot Upload File';
					echo '<a href="manage_team_detail.php?id='.$_POST['teamid'].'">กลับไปแก้ไข</a>';
					exit;
				}
				else if($filepic['error'] == 0)
				{
					// Check Type file
					if 	(	($filepic['type'] 	!= 'image/x-png')
						&& 	($filepic['type'] 	!= 'image/png')
					)
					{
						echo 'Error:Not PNG file.';
						echo '<a href="manage_team_detail.php?id='.$_POST['teamid'].'">กลับไปแก้ไข</a>';
						exit;
					}
						
					// Check directory for upload
					if(!is_dir('../uploads/logo/'))
					{
						if(!mkdir('../uploads/logo/',0777,true))
						{
							echo 'Error:Cannot Create a directory.';
							echo '<a href="manage_team_detail.php?id='.$_POST['teamid'].'">กลับไปแก้ไข</a>';
							exit;
						}
					}
					$Logo 		= str_replace(' ','-',$dataMongo['NameEN']);
					$extension	= 'png';
					$filename 	= $Logo . '.' . $extension;
						
					// Check size of picture,Must be "square"
					$infoPic = getimagesize($filepic['tmp_name']);
					if($infoPic[1] > 250)
					{
						$new_width		=	abs(($infoPic[0]/$infoPic[1])*250);
						$images_orig 	= 	imagecreatefrompng($filepic['tmp_name']);
						$images_resize 	= 	ImageCreateTrueColor($new_width, 250);
						
						imagealphablending($images_resize, FALSE);
						imagesavealpha($images_resize, TRUE); 
						imagealphablending($images_orig, TRUE); 
						$transparent = imagecolorallocatealpha($images_resize, 255, 255, 255, 127);
						imagefilledrectangle($images_resize, 0, 0, $new_width, 250, $transparent);
						
						ImageCopyResampled($images_resize, $images_orig, 0, 0, 0, 0, $new_width, 250, $infoPic[0], $infoPic[1]);
						ImagePNG($images_resize,'../uploads/logo/' . $filename , 9);
						ImageDestroy($images_orig);
						ImageDestroy($images_resize);
						echo $filename;
					}
					else
					{
						if(!move_uploaded_file($filepic['tmp_name'], '../uploads/logo/' . $filename ))
						{
							echo 'Error:Cannot Upload File';
							echo '<a href="manage_team_detail.php?id='.$_POST['teamid'].'">กลับไปแก้ไข</a>';
							exit;
						}
					}
					$memcache->set('Football2014-Team-Logo-' . $filename ,true);
				}
			}
			$collectionTeam->update(
				array('id' => (int)$_POST['teamid']),
				array('$set' => $dataInsert)
			);
		}
	}
	else if(!empty($_POST['T']))
	{
		foreach($_POST['T'] as $k => $value)
		{
			$collectionTeam->update(
				array('id' => (int)$k),
				array('$set' => array(
					'NameTH' => $value,
					'Status' => (int)$_POST['status'][$k],
					'NameTHShort'=>	$_POST['TS'][$k]
				))
			);
		}
	}
	
	?>
	
	<script type='text/javascript'>
		function myFunction(id){
		
			nameTH=document.getElementById('T' + id ).value;
			nameTHShort=document.getElementById('TS' + id ).value;
			status=document.getElementById('status' + id ).value;
			
			document.getElementById("teamid").value			=	id;
			document.getElementById("nameth").value			=	nameTH;
			document.getElementById("namethshort").value	=	nameTHShort;
			document.getElementById("status").value			=	status;
			
			document.getElementById("formzone").submit();
			
		}
	</script>
	
	<form action="manage_team.php" method="POST">
		<select name="filter_team" id="filter_team">
			<option value="">ทั้งหมด</option>
			<?php
				$dataZone = $collectionZone->find();
				$dataZone->sort(array( 'NameEN' => 1 ));
				$countZone		=	$dataZone->count();
				$dataZone->next();
				for( $i=0 ; $i<$countZone ; $i++ )
				{
					$tmpZone 	= 	$dataZone->current();
					$dataLeague = 	$collectionLeague->find( array( 'KPZoneID' => $tmpZone['id'] ) );
					$dataLeague->sort(array( 'NameEN' => 1 ));
					$countLeague		=	$dataLeague->count();
					$dataLeague->next();
					for( $j=0 ; $j<$countLeague ; $j++ )
					{
						$tmpLeague 	= 	$dataLeague->current();
						$zonename 	= 	empty($tmpZone['NameTH']) 	? $tmpZone['NameEN'] 	: $tmpZone['NameTH'];
						$leaguename = 	empty($tmpLeague['NameTH']) ? $tmpLeague['NameEN'] 	: $tmpLeague['NameTH'];
						$strValue = $tmpLeague['id'];
						
						echo '<option value="' . $strValue . '" >' . $zonename . '->' . $leaguename . '</option>';
						
						$dataLeague->next();
					}
					$dataZone->next();
				}
			?>
		</select>
		เลือกดู : 
		<select id="filter_status" name="filter_status">
			<option value="-1">ดูทั้งหมด</option>
			<option value="0" style="background-color: #FFCC8F">เฉพาะข้อมูลปิด</option>
			<option value="1" style="background-color: #BDE6C1">เฉพาะข้อมูลเปิด</option>
		</select>
		<input type="submit" value="เลือกลีก">
	</form>
	
	<?php
		if(empty($_REQUEST['filter_team']))
			$_REQUEST['filter_team'] = null;
	?>
	
	<form id="formzone" action="#" method="POST">
		<input type="hidden" name="teamid" id="teamid" value="">
		<input type="hidden" name="nameth" id="nameth" value="">
		<input type="hidden" name="namethshort" id="namethshort" value="">
		<input type="hidden" name="status" id="status" value="">
		<input type="hidden" name="filter_team" id="filter_team" value="<?php echo empty($_REQUEST['filter_team']) ? '' : $_REQUEST['filter_team'] ?>">
		<?php
			if(isset($_REQUEST['filter_status']))
			{
				?>
				<input type="hidden" name="filter_status" id="filter_status" value="<?php echo $_REQUEST['filter_status'];?>">
				<?php
			}
		?>
	</form>
	
	<form action="manage_team.php" method="POST">
	<input type="hidden" name="filter_team" id="filter_team" value="<?php echo empty($_REQUEST['filter_team']) ? '' : $_REQUEST['filter_team'] ?>">
	<input type="submit" value="บันทึกรวม">
	<table border="1" style="font-family: tahoma; font-size: 10px;">
	<?php
	*/
	
	/*$arr_filter = array();
	
	if(isset($_REQUEST['filter_status']))
	{
		?>
		<script language="JavaScript">
			document.getElementById('filter_status').value = '<?php echo $_REQUEST['filter_status'];?>';
		</script>
		<?php
		if((int)$_REQUEST['filter_status']!=-1)
			$arr_filter['Status'] = (int)$_REQUEST['filter_status'];
	}
	
	if(isset($_REQUEST['filter_team']))
	{
		?>
		<script language="JavaScript">
			document.getElementById('filter_team').value = '<?php echo $_REQUEST['filter_team'];?>';
		</script>
		<?php
		if((int)$_REQUEST['filter_team']!=0)
			$arr_filter['PlayingZoneLeagueID'] = $_REQUEST['filter_team'];
	}

	$dataMongo 			= 	$collectionTeam->find($arr_filter);
	$dataMongo->sort(array( 'NameEN' => 1 ));
	$countMongo			=	$dataMongo->count();
	$dataMongo->next();
	*/
	/*
	$APIListTeam				= 		'http://202.183.165.189/api/get_listteam.php?clear=1';
	if(isset($_REQUEST['filter_status'])){
		?><script language="JavaScript">
			document.getElementById('filter_status').value = '<?php echo $_REQUEST['filter_status'];?>';
		</script><?php
		if((int)$_REQUEST['filter_status']==0){
			$APIListTeam 		= 		$APIListTeam . '&status=-1';
		}else if((int)$_REQUEST['filter_status']==1){
			$APIListTeam 		= 		$APIListTeam . '&status=1';
		}
	}
	
	if(isset($_REQUEST['filter_team'])){
		?><script language="JavaScript">
			document.getElementById('filter_team').value = '<?php echo $_REQUEST['filter_team'];?>';
		</script><?php
		if((int)$_REQUEST['filter_team']>0){
			$APIListTeam 		= 		$APIListTeam . '&id=' . (int)$_REQUEST['filter_team'];
		}
	}
	
	$ListTeamArr 				=		json_decode(file_get_contents($APIListTeam), true);
	
	$ColorArr['1']				=		'#BDE6C1';
	$ColorArr['0']				=		'#FFCC8F';
	
	$countTeam					=		$ListTeamArr['num_team'];
	
	?>
		<tr>
			<td><b>ID</b></td>	
			<td><b>Logo</b></td>			
			<td><b>Team nameEN</b></td>
			<td><b>Team nameTH</b></td>
			<td><b>Team nameTH Short</b></td>
			<td><b>status</b></td>
			<td><b>บันทึก?</b></td>
			<td><b>รายละเอียด</b></td>
			<td><b>Link</b></td>
		</tr>
	<?php
	for( $i=0 ; $i<$countTeam ; $i++ )
	{
		
		$data 	= 	$ListTeamArr['list'][$i];

		$logopath = $data['Logo'];
		
		echo '<tr bgcolor="'.$ColorArr[$data['Status']].'";>';

			echo '<td>';
				echo $data['id'];
			echo '</td>';
			
			echo '<td>';
				echo '<img src="'. $logopath .'" width="25" >';
			echo '</td>';
		
			echo '<td>';
				echo $data['NameEN'];
			echo '</td>';
			
			echo '<td>';
				echo '<input type="input" id="T'. $data['id'] .'" name="T['. $data['id'] .']" value="'. $data['NameTH'] .'" >';
			echo '</td>';
			
			echo '<td>';
				echo '<input type="input" id="TS'. $data['id'] .'" name="TS['. $data['id'] .']" value="'. $data['NameTHShort'] .'" >';
			echo '</td>';
			
			echo '<td>';
				echo '<select name="status['. $data['id'] .']" id="status'. $data['id'] .'">';
					echo '<option value="0" ';
					echo empty($data['Status']) ? 'selected' : '';
					echo '>ปิด</option>';
					echo '<option value="1" ';
					echo empty($data['Status']) ? '' : 'selected';
					echo '>เปิด</option>';
				echo '</select>';
			echo '</td>';
			
			echo '<td>';
				echo '<input type="button" value="บันทึก"  onclick="myFunction('.$data['id'].')">';
			echo '</td>';
			
			echo '<td>';
				echo '<a href="manage_team_detail.php?id='. $data['id'] .'" target="_parent">รายละเอียด</a>';
			echo '</td>';
			
			// See in website
			echo '<td>';
				$teamURL = strtolower('http://football.kapook.com/team-'.$data['NameEN']);
				echo '<a href="'.preg_replace('/\s+/', '-', $teamURL).'" target="_blank">ไปยังเว็บไซด์</a>';
			echo '</td>';
			
		echo '</tr>';
		
		//$dataMongo->next();
	}
	?>
	<table>
	<input type="submit" value="บันทึกรวม">
	</form>
	*/ ?>