<?php
	//init 
	$rowperpage				=	10;
	
	$max_concurrent			=	20;
	$count_concurrent		=	0;
	
	$start_date['day']		=	1;
	$start_date['month']	=	8;
	
	if(date('G')>=6){
		$monthToday				=	date("Y-m");
	}else{
		$monthToday				=	date("Y-m",strtotime("now -1 day"));
	}
	
	$isClearRanking			=	false;
	
	$yearlist				=	array();
	$monthlist				=	array();
	$datelist				=	array();
	
	$column_list			=	array(
		0	=>	'total_point',
		1	=>	'total_playscore_win_percent',
		2	=>	'total_playscore_win_score_percent',
		3	=>	'total_playside_single_win_percent',
		4	=>	'total_playside_multi_win_percent'
	);
	
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	
	$collectionMatch        	=	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionGame				=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	$collectionRanking_all		=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	$collectionRanking_season	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	$collectionRanking_month	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	$collectionRanking_day		=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('192.168.1.189', 11211) or die ("Could not connect");
	
	function clear_ranking(){
		$numMember	=	$DatabaseMongoDB->command(array( 'count' => 'football_member' ));
		$numMember 	= 	$numMember['n'];
		
		$numColumn	=	count($column_list);
		
		for( $i=0 ; $i<$numMember ; $i=($i+$rowperpage) ){
			for( $j=0 ; $j<$numColumn ; $j++ )
			{
				$memcache->delete( 'Football2014-ranking-all-time-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-asc');
				$memcache->delete( 'Football2014-ranking-all-time-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-desc');
				foreach($yearlist as $key => $value){
					$memcache->delete( 'Football2014-ranking-season-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-asc');
					$memcache->delete( 'Football2014-ranking-season-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-desc');
					//echo 'Football2014-ranking-season-'.$key.'-'.$i .'<br>';
				}
				foreach($monthlist as $key => $value){
					$memcache->delete( 'Football2014-ranking-month-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-asc');
					$memcache->delete( 'Football2014-ranking-month-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-desc');
					//echo 'Football2014-ranking-month-'.$key.'-'.$i .'<br>';
				}
				foreach($datelist as $key => $value){
					$memcache->delete( 'Football2014-ranking-date-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-asc');
					$memcache->delete( 'Football2014-ranking-date-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-desc');
					//echo 'Football2014-ranking-date-' . $key . '-' . $i . '<br>';
				}
			}
		}
		
		//Specific for mini (full)ranking
		foreach($datelist as $key => $value){
			$memcache->delete( 'Football2014-rankingfull-date-' . $key . '-0-5-total_point-desc');
		}
	}
	
	$FindArr['MatchStatus']		=	array('$in' => array('Fin','Post','Abd','Canc'));
	$FindArr['Odds']			=	array('$gte' => 0);
	
	if($_REQUEST['date']!=''){
		$MatchFromDate = $_REQUEST['date'].' 06:00:00';
		$MatchToDate = date("Y-m-d 05:59:59",strtotime($_REQUEST['date'] . " 05:59:59 + 1 day"));
	}else{
		$MatchFromDate = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." - 240 minutes"));
		$MatchToDate  = date('Y-m-d H:i:s');
	}
	
	$FindArr['MatchDateTime'] = array('$gte'=>$MatchFromDate,'$lte'=>$MatchToDate);
	
	echo 'CHECK BETWEEN => '.$MatchFromDate.' => '.$MatchToDate;
	echo '<hr>';
	
	$dataMatch 			= 	$collectionMatch->find($FindArr);
	$dataMatch->limit();
	$dataMatch->sort(array( 'MatchDateTime' => 1 ));
	$countMatch			=	$dataMatch->count(true);
	$dataMatch->next();
	
	for( $i=0 ; $i<$countMatch ; $i++ ){
		$rowMatch						=	$dataMatch->current();
		echo '--------Match No.'.$rowMatch['id'].'--------<br/>';
		echo ' => '.$rowMatch['Team1'].'  <font color="#990000">'.$rowMatch['Team1FTScore'].' - '.$rowMatch['Team2FTScore'].'</font>  '.$rowMatch['Team2'].'<br/>';
		
		$FindArrGame					=	array();
		$gameType						=	0;
		
		/*
		 * Multi Play
		 */
		$FindArrGame['listGame']	=	array(
			'$elemMatch'	=>	array(
				'match_id' 	=>	$rowMatch['id'],
				'status' 	=>	1,
			)
		);
		$FindArrGame['status']		=	array('$gte' => 1,'$lte' => 3);  //a ตรวจทุกคู่แม้จะชุดนั้นจะไม่เข้าแล้วก็ตาม
		$FindArrGame['game_type']	=	2;
		
		// 	Init a type of game (Betting style)
		//	Init TeamOdds & Odds
		if($rowMatch['TeamOdds']==1){
			$rowMatch['Team2FTScoreBet']	=	$rowMatch['Team2FTScore']+$rowMatch['Odds'];
			$rowMatch['Team1FTScoreBet']	=	$rowMatch['Team1FTScore'];
		}else if($rowMatch['TeamOdds']==2){
			$rowMatch['Team1FTScoreBet']	=	$rowMatch['Team1FTScore']+$rowMatch['Odds'];
			$rowMatch['Team2FTScoreBet']	=	$rowMatch['Team2FTScore'];
		}else{
			$rowMatch['Team1FTScoreBet']	=	$rowMatch['Team1FTScore'];
			$rowMatch['Team2FTScoreBet']	=	$rowMatch['Team2FTScore'];
		}
			
		//	Team 1 Win
		
		if(	($rowMatch['MatchStatus'] == "Post")	||
			($rowMatch['MatchStatus'] == "Abd")		||
			($rowMatch['MatchStatus'] == "Canc")
		){
			$gameTypeBet	=	3;
		}
		else if($rowMatch['Team1FTScoreBet']>$rowMatch['Team2FTScoreBet']){
			$gameTypeBet	=	1;
		}
		//	Team 2 Win
		else if($rowMatch['Team1FTScoreBet']<$rowMatch['Team2FTScoreBet']){
			$gameTypeBet	=	2;
		}
		//	Draw
		else{
			$gameTypeBet	=	3;
		}
		
		$dataGame					= 	$collectionGame->find($FindArrGame);		
		$countGame					=	$dataGame->count();
		echo 'CountGame :' . $countGame . '<br/>';
		/*$dataGame->next();
		
		for( $j=0 ; $j<$countGame ; $j++ ){
			echo 'Checking Game no.'.$rowGame['id'].'<br>';
			
			$dataGame->next();
		}*/

		echo '-------------------------------<br/><br/>';
		$dataMatch->next();
	}
	clear_ranking();
	echo 'Finish';
?>