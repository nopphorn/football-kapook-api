<?php
$memcache = new Memcache;
$memcache->connect('localhost', 11211) or die ("Could not connect");


$MonthArr["01"]="ม.ค.";
$MonthArr["02"]="ก.พ.";
$MonthArr["03"]="มี.ค.";
$MonthArr["04"]="เม.ย.";
$MonthArr["05"]="พ.ค.";
$MonthArr["06"]="มิ.ย.";
$MonthArr["07"]="ก.ค.";
$MonthArr["08"]="ส.ค.";
$MonthArr["09"]="ก.ย.";
$MonthArr["10"]="ต.ค.";
$MonthArr["11"]="พ.ย.";
$MonthArr["12"]="ธ.ค.";


for($i=1;$i<=10;$i++){
		$mcRow=$memcache->get("Football2014-LastFinish-".$i);		
                $DateArr = explode('-',$mcRow["MatchDate"]);
                        
		$Data[$i-1]["LeagueIcon"]='http://football.kapook.com/livescore/upload/league/'.$memcache->get("FootballMatchLeagueImage".$mcRow["livescore_match_LeagueID"]);
		$Data[$i-1]["LeagueName"]=$memcache->get("Football2014-League-NameTHShort-".$mcRow["KPLeagueID"]);
		$Data[$i-1]["match_time"]=$mcRow["MatchTime"];
                
		$Data[$i-1]["match_date"]=intval($DateArr[2]).' '.$MonthArr[$DateArr[1]].' '.($DateArr[0]-1957);
		$Data[$i-1]["Team1"]=$mcRow["Team1"];
		$Data[$i-1]["Team2"]=$mcRow["Team2"];
		$Data[$i-1]["ScoreTeam1"]=$mcRow["Team1FTScore"];
		$Data[$i-1]["ScoreTeam2"]=$mcRow["Team2FTScore"];
		$Data[$i-1]["match_id"]=$mcRow["id"];
		$Data[$i-1]["MatchPageURL"]=  strtolower($mcRow["MatchPageURL"]);		
}

echo serialize($Data);




?>