<?php

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	
	$memcache 	= new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	(60*60*24);
	
	if(!isset($_GET['id'])){
		$listleague_MC 			= 	$memcache->get( 'Football2014-listleaguebyzone-all' );
		$arrAttr				= 	array();
	}else{
		$listleague_MC 			= 	$memcache->get( 'Football2014-listleaguebyzone-' . $_GET['id'] );
		$arrAttr['KPZoneID']	=	(int)$_GET['id'];
	}
	
	if((!$listleague_MC)||($_REQUEST['clear']==1)){
	
		$dataLeague		=	$collectionLeague->find($arrAttr)->sort( array('XSLeagueCountry' => 1) );
		if(empty($dataLeague)){
			$datajson	=	array(
				'list'	=>	array(),
				'count'	=>	0
			);
		}else{
			$tmpCount	=	0;
			foreach( $dataLeague as $tmpData ){
				$datajson['list'][]		=	$tmpData;
				$tmpCount++;
			}
			$datajson['count']	=	$tmpCount;
		}
		if(!isset($_GET['id'])){
			$memcache->set( 'Football2014-listleaguebyzone-all' , $datajson , MEMCACHE_COMPRESSED, $expire );
		}else{
			$memcache->set( 'Football2014-listleaguebyzone-' . $_GET['id'] , $datajson , MEMCACHE_COMPRESSED, $expire );
		}
		
	}else{
		$datajson	=	$listleague_MC;
	}
	echo json_encode($datajson);
?>