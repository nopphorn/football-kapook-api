<?php header( "refresh: 0; url=http://football.kapook.com/api/adminfootball/match/detail/" . (int)$_GET['id'] ); /*
<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<?php
	include 'manage_checklogin.php';
	$list_matchstatus = array(
		'Sched',
		'1 HF',
		'H/T',
		'2 HF',
		'E/T',
		'Pen',
		'Fin',
		'Int',
		'Abd',
		'Post',
		'Canc'
	);
	
	$list_TV = array(
	
		0 => array(
			'nameType' 	=> 	'TV Analog',
			'listTV'	=>	array(
				'tv3' 	=> 	'<img src="http://202.183.165.189/uploads/tvlogo/tv3.png" height="20">',
				'tv5'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv5.png" height="20">',
				'tv7'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv7.png" height="20">',
				'tv9'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv9.png" height="20">',
				'tv11'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tv11.png" height="20">',
				'tpbs'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tpbs.png" height="20">'
			)
		),

		1 => array(
			'nameType' 	=> 	'TV Digital',
			'listTV'	=>	array(
				'tv5hd'			=> '<img src="http://202.183.165.189/uploads/tvlogo/tv5hd.png" height="20">',
				'tv11hd'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv11hd.png" height="20">',
				'tpbshd'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tpbshd.png" height="20">',
				'tv3family'		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3family.png" height="20">',
				'mcotfamily'	=> '<img src="http://202.183.165.189/uploads/tvlogo/mcotfamily.png" height="20">',
				'tnn'			=> '<img src="http://202.183.165.189/uploads/tvlogo/tnn.png" height="20">',
				'newtv'			=> '<img src="http://202.183.165.189/uploads/tvlogo/newtv.png" height="20">',
				'springnews'	=> '<img src="http://202.183.165.189/uploads/tvlogo/springnews.png" height="20">',
				'nation'		=> '<img src="http://202.183.165.189/uploads/tvlogo/nation.png" height="20">',
				'workpointtv' 	=> '<img src="http://202.183.165.189/uploads/tvlogo/workpointtv.png" height="20">',
				'true4u' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/true4u.png" height="20">',
				'gmmchannel' 	=> '<img src="http://202.183.165.189/uploads/tvlogo/gmmchannel.png" height="20">',
				'now' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/now.png" height="20">',
				'ch8' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/ch8.png" height="20">',
				'tv3sd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3sd.png" height="20">',
				'mono' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/mono.png" height="20">',
				'mcothd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/mcothd.png" height="20">',
				'one' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/one.png" height="20">',
				'thairath' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/thairath.png" height="20">',
				'tv3hd' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/tv3hd.png" height="20">',
				'amarin' 		=> '<img src="http://202.183.165.189/uploads/tvlogo/amarin.png" height="20">',
				'pptv' 			=> '<img src="http://202.183.165.189/uploads/tvlogo/pptv.png" height="20">'
			)
		),
		
		2 => array(
			'nameType' 		=> 	'True Vision',						
			'listTV'		=>	array(
				'tshd1'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd1.png" height="20">',
				'tshd2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd2.png" height="20">',
				'tshd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd3.png" height="20">',
				'tshd4' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tshd4.png" height="20">',
				'ts1' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts1.png" height="20">',
				'ts2' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts2.png" height="20">',
				'ts3' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts3.png" height="20">',
				'ts4' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts4.png" height="20">',
				'ts5' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts5.png" height="20">',
				'ts6' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts6.png" height="20">',
				'ts7' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/ts7.png" height="20">',
				'tstennis'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/tstennis.png" height="20">',
				'hayha'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/hayha.png" height="20">(352)',
				'thaithai'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/thaithai.png" height="20"> (358)',
				'true' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/true.png" height="20">'
			)
		),
		
		3 => array(
			'nameType' 	=> 	'CTH',						
			'listTV'	=>	array(
				'cthstd1'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd1.png" height="20">',
				'cthstd2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd2.png" height="20">',
				'cthstd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd3.png" height="20">',
				'cthstd4' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd4.png" height="20">',
				'cthstd5' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd5.png" height="20">',
				'cthstd6' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstd6.png" height="20">',
				'cthstdx'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/cthstdx.png" height="20">'
			)
		),
		
		4 => array(
			'nameType' 	=> 	'GMM',						
			'listTV'	=>	array(
				'gmmclubchannel' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmclubchannel.png" height="20">',
				'gmmfootballplus' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballplus.png" height="20">',
				'gmmfootballmax' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballmax.png" height="20">',
				'gmmfootballeuro' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballeuro.png" height="20">',
				'gmmfootballextrahd'	=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmfootballextrahd.png" height="20">',
				'gmmsportextreme'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportextreme.png" height="20">',
				'gmmsportplus'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">',
				'gmmsportextra'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">',
				'gmmsportone'			=>	'<img src="http://202.183.165.189/uploads/tvlogo/gmmsportplus.png" height="20">'
			)
		),
		
		5 => array(
			'nameType' 	=> 	'Siamsport',						
			'listTV'	=>	array(
				'siamsportnew'		=>	'Siamsport News',
				'siamsportfootball' =>	'<img src="http://202.183.165.189/uploads/tvlogo/siamsportfootball.png" height="20">',
				'siamsportlive' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/siamsportlive.png" height="20">'
			)
		),
		
		6 => array(
			'nameType' 	=> 	'RS',						
			'listTV'	=>	array(
				'sunchannel'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/sunchannel.png" height="20">',
				'worldcupchannel' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/worldcupchannel.png" height="20">'
			)
		),
		
		7 => array(
			'nameType' 	=> 	'Bein Sport',						
			'listTV'	=>	array(
				'bein'		=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein.png" height="20">',
				'bein1' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein1.png" height="20">',
				'bein2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/bein2.png" height="20">'
			)
		),
		
		8 => array(
			'nameType' 	=> 	'Fox Sport',						
			'listTV'	=>	array(
				'foxsport' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport.png" height="20">',
				'foxsport2' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport2.png" height="20">',
				'foxsport3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsport3.png" height="20">',
				'foxsportplay' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/foxsportplay.png" height="20">'
			)
		),
		
		9 => array(
			'nameType' 	=> 	'อื่นๆ',						
			'listTV'	=>	array(
				'starsport' 			=>	'<img src="http://202.183.165.189/uploads/tvlogo/starsport.png" height="20">',
				'astrosupersport1' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersport1.png" height="20">',
				'astrosupersport2' 		=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersport2.png" height="20">',
				'astrosupersporthd3' 	=>	'<img src="http://202.183.165.189/uploads/tvlogo/astrosupersporthd3.png" height="20">',
				'asn' 					=>	'<img src="http://202.183.165.189/uploads/tvlogo/asn.png" height="20">',
				'asn2' 					=>	'<img src="http://202.183.165.189/uploads/tvlogo/asn2.png" height="20">',
				'bugaboo' 				=>	'<img src="http://202.183.165.189/uploads/tvlogo/bugaboo.png" height="20">'
			)
		)
	);
	
	$listOdd = array(
		'0.0' => 'เสมอ',
		'0.25' => 'เสมอควบครึ่ง',
		'0.5' => 'ครึ่งลูก',
		'0.75' => 'ครึ่งควบลูก',
		'1' => 'หนึ่งลูก',
		'1.25' => 'ลูกควบลูกครึ่ง',
		'1.5' => 'ลูกครึ่ง',
		'1.75' => 'ลูกครึ่งควบสองลูก',
		'2' => 'สองลูก',
		'2.25' => 'สองลูกควบสองลูกครึ่ง',
		'2.5' => 'สองลูกครึ่ง',
		'2.75' => 'สองลูกครึ่งควบสามลูก',
		'3' => 'สามลูก',
		'3.25' => 'สามลูกควบสามลูกครึ่ง',
		'3.5' => 'สามลูกครึ่ง',
		'3.75' => 'สามลูกครึ่งควบสี่ลูก',
		'4' => 'สี่ลูก',
		'4.25' => 'สี่ลูกควบสี่ลูกครึ่ง',
		'4.5' => 'สี่ลูกครึ่ง',
		'4.75' => 'สี่ลูกครึ่งควบห้าลูก',
		'5' => 'ห้าลูก',
		'5.25' => 'ห้าลูกควบห้าลูกครึ่ง',
		'5.5' => 'ห้าลูกครึ่ง',
		'5.75' => 'ห้าลูกครึ่งควบหกลูก',
		'6' => 'หกลูก',
		'6.25' => 'หกลูกควบหกลูกครึ่ง',
		'6.5' => 'หกลูกครึ่ง',
		'6.75' => 'หกลูกครึ่งควบเจ็ดลูก',
		'7' => 'เจ็ดลูก',
		'7.25' => 'เจ็ดลูกควบเจ็ดลูกครึ่ง',
		'7.5' => 'เจ็ดลูกครึ่ง',
		'7.75' => 'เจ็ดลูกครึ่งควบแปดลูก',
		'8' => 'แปดลูก',
		'8.25' => 'แปดลูกควบแปดลูกครึ่ง',
		'8.5' => 'แปดลูกครึ่ง',
		'8.75' => 'แปดลูกครึ่งควบเก้าลูก',
		'9' => 'เก้าลูก',
		'9.25' => 'เก้าลูกควบเก้าลูกครึ่ง',
		'9.5' => 'เก้าลูกครึ่ง',
		'9.75' => 'เก้าลูกครึ่งควบสิบลูก',
	);

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collection				=	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	
	$dataMongo				= 	$collection->findOne(array( 'id' => (int)$_GET['id'] ));
	
	if(empty($dataMongo))
	{
		echo 'Not Found a match.';
		exit;
	}
	$data 	= 	$dataMongo;
	
	$dateNoTime		=	substr($data['MatchDateTime'],0,4) . substr($data['MatchDateTime'],5,2) . substr($data['MatchDateTime'],8,2);
	
	?>
	
	<form id="formzone" action="manage_match_submit_full.php" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="matchid" id="matchid" value="<?php echo $_GET['id']; ?>">
		<input type="hidden" name="matchdate" id="matchdate" value="<?php echo $dateNoTime; ?>">
		<table border="1">

			<tr>
				<td><b>ID</b></td>
				<td><b>DateTime</b></td>
				<td><b>Zone Name</b></td>
				<td><b>League Name</b></td>
				<td><b>Team 1</b></td>
				<td colspan="3"><b>score</b></td>
				<td><b>Team 2</b></td>
				<td><b>Match Status</b></td>
				<td><b>Status</b></td>
			</tr>
			<?php
			
			echo '<tr>';
			
				// ID
				echo '<td>';
					echo $data['id'];
				echo '</td>';
					
				// DateTime
				echo '<td>';
					echo $data['MatchDateTime'];
				echo '</td>';
				
					
				// Zone Name
				echo '<td>';
					$dataZone			= 	$collectionZone->findOne(array( 'id' => $data['KPLeagueCountryID'] ));
					if(!empty($dataZone))
					{	if(empty($dataZone['NameTH']))
							echo $dataZone['NameEN'];
						else
							echo $dataZone['NameTH'];
					}
				echo '</td>';
					
				// League Name
				echo '<td>';
					$dataLeague			= 	$collectionLeague->findOne(array( 'id' => $data['KPLeagueID'] ));
					if(!empty($dataLeague))
					{	if(empty($dataLeague['NameTH']))
							echo $dataLeague['NameEN'];
						else
							echo $dataLeague['NameTH'];
					}
				echo '</td>';
			
				// Team 1
				echo '<td rowspan="3">';
					echo '<center>';
					$dataTeam1		= 	$collectionTeam->findOne(array( 'id' => $data['Team1KPID'] ));
					if(empty($dataTeam1['NameTH']))
						echo $dataTeam1['NameEN'];
					else
						echo $dataTeam1['NameTH'];
					echo '</center>';
				echo '</td>';
					
				// Score Team 1
				echo '<td align="right">';
					echo '<input type="input" id="team1score" size="4" name="team1score" dir="rtl" align="right" value="'. $data['Team1FTScore'] .'" >';
				echo '</td>';
				
				// 90' Full Time
				echo '<td>';
					echo '<center>เวลาปกติ</center>';
				echo '</td>';				
				
				// Score Team 2
				echo '<td>';
					echo '<input type="input" id="team2score" size="4" name="team2score" value="'. $data['Team2FTScore'] .'" >';
				echo '</td>';
					
				// Team 2
				echo '<td rowspan="3">';
					echo '<center>';
					$dataTeam2		= 	$collectionTeam->findOne(array( 'id' => $data['Team2KPID'] ));
					if(empty($dataTeam2['NameTH']))
						echo $dataTeam2['NameEN'];
					else
						echo $dataTeam2['NameTH'];
					echo '</center>';
				echo '</td>';
					
				// Match Status		
				echo '<td>';
					echo '<select name="matchstatus" id="matchstatus">';
						for( $j=0,$sizeList=count($list_matchstatus) ; $j<$sizeList ; $j++ )
						{
							echo '<option value="'. $list_matchstatus[$j] .'" ';
							echo ($list_matchstatus[$j]==$data['MatchStatus']) ? 'selected' : '';
							echo '>'. $list_matchstatus[$j] .'</option>';
						}
					echo '</select>';
				echo '</td>';
				
				// Show Status
				echo '<td>';
					echo '<select name="status" id="status">';
						echo '<option value="0" ';
						echo empty($data['Status']) ? 'selected' : '';
						echo '>ปิด</option>';
						echo '<option value="1" ';
						echo empty($data['Status']) ? '' : 'selected';
						echo '>เปิด</option>';
					echo '</select>';
				echo '</td>';
				
			echo '</tr>';
			
			//Extra Time	
			$dataETscore	=	explode('-',$data['ETScore']);
			echo '<tr>';
			
				echo '<td colspan="4" rowspan="2">';
				echo '</td>';
					
				// Score Team 1
				echo '<td align="right">';
					echo '<input type="input" id="team1scoreET" size="4" name="team1scoreET" dir="rtl" value="';
					echo $dataETscore[0];
					echo '" >';
				echo '</td>';
				
				// is Extra?
				echo '<td>';
					echo '<center><input name="isExtra" id="isExtra" type="checkbox" value="1" ';
					if(strlen($data['ETScore'])>1)
						echo 'CHECKED';
					echo ' onclick="checkEnableExtra()"> ต่อเวลาพิเศษ</center>';
				echo '</td>';				
				
				// Score Team 2
				echo '<td>';
					echo '<input type="input" id="team2scoreET" size="4" name="team2scoreET" value="';
					echo $dataETscore[1];
					echo '" >';
				echo '</td>';
				
				echo '<td colspan="2" rowspan="2">';
				echo '</td>';
				
			echo '</tr>';
			
			//PK
			$dataPKscore	=	explode('-',$data['PNScore']);
			echo '<tr>';
					
				// Score Team 1
				echo '<td align="right">';
					echo '<input type="input" id="team1scorePK" size="4" name="team1scorePK" dir="rtl" value="';
					echo $dataPKscore[0];
					echo '" >';
				echo '</td>';
				
				// is PK?
				echo '<td>';
					echo '<center><input name="isPK" id="isPK" type="checkbox" value="1" ';
					if(strlen($data['PNScore'])>1)
						echo 'CHECKED';
					echo ' onclick="checkEnablePK()"> ดวลจุดโทษ</center>';
				echo '</td>';				
				
				// Score Team 2
				echo '<td>';
					echo '<input type="input" id="team2scorePK" size="4" name="team2scorePK" value="';
					echo $dataPKscore[1];
					echo '" >';
				echo '</td>';
				
			echo '</tr>';
			
			if(empty($dataMongo['TVLiveList']))
				$dataMongo['TVLiveList'] = array();
		
		?>
			<tr>
				<td colspan="11"><b>Scorers List</b></td>
			</tr>
			<tr><td colspan="11">
			<table border="1" id="tableScorers">
				<tr>
					<td><b>นาที</b></td>
					<td colspan="3"><b>ชื่อผู้เล่น</b></td>
					<td colspan="3"><b>ทีม</b></td>
					<td><b>จุดโทษ</b></td>
					<td><b>ทำเข้าประตูตัวเอง</b></td>
					<td><b>ต่อเวลาพิเศษ</b></td>
					<td><b>ลบรายชื่อ</b></td>
				</tr>
				<?php
					$sizeScorers	=	count($data['LastScorers']);
					for( $i=0 ; $i<$sizeScorers ; $i++ ){
						if($data['LastScorers'][$i][0]=='('){
							$teamplay		=	1;
							$dataScorers = explode(")" , ltrim($data['LastScorers'][$i], "("));
							if(empty($dataTeam1['NameTH']))
								$teamName	=	$dataTeam1['NameEN'];
							else
								$teamName	=	$dataTeam1['NameTH'];
							//var_dump($dataScorers);
							$playerName		=	$dataScorers[1];
							$min			=	$dataScorers[0];
							
						}else{
							$teamplay		=	2;
							$dataScorers = explode("(" , rtrim($data['LastScorers'][$i], ")"));
							if(empty($dataTeam2['NameTH']))
								$teamName	=	$dataTeam2['NameEN'];
							else
								$teamName	=	$dataTeam2['NameTH'];
								
							$playerName		=	$dataScorers[0];
							$min			=	$dataScorers[1];
						}
						
						//Pen
						$pos = strpos($min, 'Pen');
						if($pos === false){
							$isPen	=	false;
						}else{
							$isPen	=	true;
							$min 		= 	str_replace('Pen', '', $min);
						}
						
						//Extra Time
						$pos = strpos($min, 'ET');
						if($pos === false){
							$isET		=	false;
						}else{
							$isET		=	true;
							$min 		= 	str_replace('ET', '', $min);
						}
						
						//Own Goal
						$pos = strpos($min, 'Own');
						if($pos === false){
							$isOwn	=	false;
						}else{
							$isOwn	=	true;
							$min 		= 	str_replace('Own', '', $min);
						}
						?>
						<tr>
							<td><input type="input" size="5" name="inputMinScorers[]" value="<?php echo $min; ?>"></td>
							<td colspan="3"><input name="inputNameScorers[]" type="input" value="<?php echo $playerName; ?>"></td>
							<td colspan="3">
								<select name="inputTeamScorers[]">
									<option value="0">----กรุณาเลือกทีม----</option>
									<option value="1" <?php if($teamplay==1) echo 'SELECTED'; ?>><?php
										if(empty($dataTeam1['NameTH']))
											echo $dataTeam1['NameEN'];
										else
											echo $dataTeam1['NameTH'];?>
									</option>
									<option value="2" <?php if($teamplay==2) echo 'SELECTED'; ?>><?php
										if(empty($dataTeam2['NameTH']))
											echo $dataTeam2['NameEN'];
										else
											echo $dataTeam2['NameTH'];?>
									</option>
								</select>
							</td>
							<td><input name="inputPN[]" type="checkbox" value="<?php echo $i; ?>" <?php if($isPen) echo 'CHECKED'; ?> ></td>
							<td><input name="inputOwnGoal[]" type="checkbox" value="<?php echo $i; ?>" <?php if($isOwn) echo 'CHECKED'; ?> ></td>
							<td><input name="inputET[]" type="checkbox" value="<?php echo $i; ?>" <?php if($isET) echo 'CHECKED'; ?> ></td>
							<td><button class="deleteIt" type="button" >ลบ</button></td>
						</tr>
						<?php
					}
				?>
			</table>
			<br><button type="button" onclick="scorersAdd()">เพิ่ม</button>
			</td>
			</tr>
			
			<tr>
				<td colspan="11"><b>Tv Live Select</b></td>
			</tr>
			
			<tr>
				<td colspan="11">
					<table class="chkbox_tv">
						<?php
						
							for( $j=0,$sizeTV=count($list_TV) ; $j< $sizeTV ; $j++ )
							{
								?><tr>
									<td colspan="10"><b><?php echo $list_TV[$j]['nameType']; ?></b><td>
								</tr>
								<?php
								echo '<tr>';
									$countTV = 10;
									foreach ($list_TV[$j]['listTV'] as $k => $v) {
										if (in_array($k, $dataMongo['TVLiveList']))
											$check = 'CHECKED';
										else
											$check = '';
										echo '<td><input name="inputBroadCastChannel[]" type="checkbox" value="' . $k . '" ' . $check . '>' . $v . '</td>';
										
										$countTV--;
										if($countTV <= 0)
										{
											$countTV = 10;
											echo '</tr><tr>';
										}
									}
									if($countTV!=10)
										echo '<td colspan="'. $countTV .'"></td>';
								echo '</tr>';
							}
						?>
					</table>
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					<b>อัปโหลดรูปข่าว : OG</b><br>
					<input type="file" name="fileog" id="fileog"><br>
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					<b>รูปข่าว : OG</b><br>
					<table border="1">
						<tr>
							<td>ลบ?</td>
							<td>รูปภาพ</td>
						</tr>
						<tr>
							<td><input name="listDeleteOG" type="checkbox" value="1"></td>
							<td><?php
								if(empty($data['PictureOG']))
									echo 'No Picture';
								else
									echo '<img src="../uploads/match/og/' . $data['PictureOG'] . '" width="512">';
							?></td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td colspan="11">
					<b>อัปโหลดรูปข่าว</b><br>
					<input type="file" name="fileUpload[0]" id="fileUpload"><br>
					<input type="file" name="fileUpload[1]" id="fileUpload"><br>
					<input type="file" name="fileUpload[2]" id="fileUpload"><br>
					<input type="file" name="fileUpload[3]" id="fileUpload"><br>
					<input type="file" name="fileUpload[4]" id="fileUpload"><br>
					<input type="file" name="fileUpload[5]" id="fileUpload"><br>
					<input type="file" name="fileUpload[6]" id="fileUpload"><br>
					<input type="file" name="fileUpload[7]" id="fileUpload"><br>
					<input type="file" name="fileUpload[8]" id="fileUpload"><br>
					<input type="file" name="fileUpload[9]" id="fileUpload"><br>
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					<b>รูปข่าวที่มีอยู่แล้ว</b><br>
					
					<table border="1">
						<tr>
							<td>ลบ?</td>
							<td>รูปภาพ</td>
						</tr>
						<?php
						for( $j=0,$sizeList=count($data['Picture']) ; $j<$sizeList ; $j++ )
						{
							?>
							<tr>
								<td><input name="listDeletePicture[]" type="checkbox" value="<?php echo $j; ?>"></td>
								<td><img src="../uploads/match/<?php echo $dateNoTime . '/' . $data['Picture'][$j]; ?>" width="256"></td>
							</tr>
							<?php
						}
						?>
					</table>
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					<b>Embed Code</b><br>
					<textarea name="embedCode" style="width:100%;height:150px;"><?php echo empty($data['EmbedCode']) ? '' : $data['EmbedCode'] ; ?></textarea>
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					<b>วิเคราะห์บอล</b><br>
					<?php 
						include("/var/web/football.kapook.com/html/api/spaw2/spaw.inc.php");
						$analysis = new SpawEditor("analysis", $data['Analysis']);
						$analysis->addToolbars("format","edit");
						$analysis->show();?>
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					<b>ทรรศนะฟุตบอล</b><br>
					<input type="input" name="predict" style="width:100%;" value="<?php echo empty($data['Predict']) ? '' : $data['Predict'] ; ?>">
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					ฟันธงทีม : 
					<select name="inputteamselect" id="inputteamselect">
						<?php
							if(isset($dataTeam['TeamSelect']))
								$dataTeam['TeamSelect'] = 0;
						?>
						<option value="0"> - - เลือกทีม - - </option>
						<option value="1"<?php echo ((int)$data['TeamSelect']==1) ? 'selected' : ''; ?> >
						<?php 
							$dataTeam		= 	$collectionTeam->findOne(array( 'id' => $data['Team1KPID'] ));
							if(!empty($dataTeam))
							{
								if(empty($dataTeam['NameTH']))
									echo $dataTeam['NameEN'];
								else
									echo $dataTeam['NameTH'];
							} ?>
						</option>
						<option value="2"<?php echo ((int)$data['TeamSelect']==2) ? 'selected' : ''; ?> >
						<?php 
							$dataTeam		= 	$collectionTeam->findOne(array( 'id' => $data['Team2KPID'] ));
							if(!empty($dataTeam))
							{
								if(empty($dataTeam['NameTH']))
									echo $dataTeam['NameEN'];
								else
									echo $dataTeam['NameTH'];
							} ?>
						</option>
					</select>
					อัตราต่อรอง : 
					<select name="inputodds" id="inputodds">
						<option value="-1.0">-- select --</option>
						<?php
							if(!isset($data['Odds']))
								$data['Odds'] = -1.0;
							foreach ($listOdd as $k => $v) {
								echo '<option value="' . (float)$k . '" ';
								if((float)$data['Odds']===(float)$k)
								{
									echo 'selected';
								}
								echo '>' . $v . '</option>';
							}
						?>
					</select>
					<select name="inputteamodds" id="inputteamodds">
						<?php
							if(!isset($data['TeamOdds']))
								$data['TeamOdds'] = 0;
						?>
						<option value="0"> - - เลือกทีม - - </option>
						<option value="1"<?php echo ((int)$data['TeamOdds']==1) ? 'selected' : ''; ?> >
						<?php 
							$dataTeam		= 	$collectionTeam->findOne(array( 'id' => $data['Team1KPID'] ));
							if(!empty($dataTeam))
							{
								if(empty($dataTeam['NameTH']))
									echo $dataTeam['NameEN'];
								else
									echo $dataTeam['NameTH'];
							} ?>
						</option>
						<option value="2"<?php echo ((int)$data['TeamOdds']==2) ? 'selected' : ''; ?> >
						<?php 
							$dataTeam		= 	$collectionTeam->findOne(array( 'id' => $data['Team2KPID'] ));
							if(!empty($dataTeam))
							{
								if(empty($dataTeam['NameTH']))
									echo $dataTeam['NameEN'];
								else
									echo $dataTeam['NameTH'];
							} ?>
						</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td colspan="11"><b>สรุปผลบอล</b></td>
			</tr>
			
			<tr>
				<td colspan="11">
				<?php 
						$result = new SpawEditor("result", $data['Result']);
						$result->addToolbars("format","edit");
						$result->show();?>
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					<b>คำถามชวนคุย</b><br>
					<input type="input" name="question" style="width:100%;" value="<?php echo empty($data['question']) ? '' : $data['question'] ; ?>">
				</td>
			</tr>
			
			<tr>
				<td colspan="11">
					<center><input type="submit" value="บันทึก"></center>
				</td>
			</tr>
		<table>
	</form>
	
	<script language="JavaScript">
		function resetNumCheckBox(){
			var i=0;
			$('#tableScorers input[type="checkbox"]').each(function() {
				$(this).val(i/3>>0);
				i = i+1;
			});
		}
		
		$(document).ready(function(){
			$( ".deleteIt" ).click(function() {
				$(this).parent().parent().remove();
				resetNumCheckBox();
			});
		}); // End Ready Function 
		
		function checkEnableExtra(){
			if(document.getElementById('isExtra').checked)
			{
				document.getElementById('team1scoreET').value = '0';
				document.getElementById('team1scoreET').disabled =false;
				document.getElementById('team2scoreET').value = '0';
				document.getElementById('team2scoreET').disabled =false;
			}
			else
			{
				document.getElementById('team1scoreET').value = '';
				document.getElementById('team1scoreET').disabled =true;
				document.getElementById('team2scoreET').value = '';
				document.getElementById('team2scoreET').disabled =true;
			}
		}
		
		function checkEnablePK(){
			if(document.getElementById('isPK').checked)
			{
				document.getElementById('team1scorePK').value = '0';
				document.getElementById('team1scorePK').disabled =false;
				document.getElementById('team2scorePK').value = '0';
				document.getElementById('team2scorePK').disabled =false;
			}
			else
			{
				document.getElementById('team1scorePK').value = '';
				document.getElementById('team1scorePK').disabled =true;
				document.getElementById('team2scorePK').value = '';
				document.getElementById('team2scorePK').disabled =true;
			}
		}
		
		if(!document.getElementById('isExtra').checked)
		{
			document.getElementById('team1scoreET').disabled =true;
			document.getElementById('team2scoreET').disabled =true;
		}
		
		if(!document.getElementById('isPK').checked)
		{
			document.getElementById('team1scorePK').disabled =true;
			document.getElementById('team2scorePK').disabled =true;
		}
		
		function scorersAdd() {
			
			$("#tableScorers")
			.append($('<tr>')
				.append($('<td>')
					.append('<input type="input" size="5" name="inputMinScorers[]" value="">')
				)
				.append($('<td>')
					.attr('colspan', '3')
					.append('<input name="inputNameScorers[]" type="input" value="">')
				)
				.append($('<td>')
					.attr('colspan', '3')
					.append($('<select>')
						.attr('name', 'inputTeamScorers[]')
						.append($('<option>')
							.text('----กรุณาเลือกทีม----')
							.attr('value', '0')
						)
						.append($('<option>')
							.text('<?php
							if(empty($dataTeam1['NameTH']))
								echo $dataTeam1['NameEN'];
							else
								echo $dataTeam1['NameTH'];
							?>')
							.attr('value', '1')
						)
						.append($('<option>')
							.text('<?php
							if(empty($dataTeam2['NameTH']))
								echo $dataTeam2['NameEN'];
							else
								echo $dataTeam2['NameTH'];
							?>')
							.attr('value', '2')
						)
					)
				)
				.append($('<td>')
					.append('<input name="inputPN[]" type="checkbox" value="" >')
				)
				.append($('<td>')
					.append('<input name="inputOwnGoal[]" type="checkbox" value="" >')
				)
				.append($('<td>')
					.append('<input name="inputET[]" type="checkbox" value="" >')
				)
				.append($('<td>')
					.append('<button class="deleteIt" type="button" >ลบ</button>')
				)
			);
			
			$( ".deleteIt" ).unbind( "click" ).click(function() {
				$(this).parent().parent().remove();
				resetNumCheckBox();
			});
			
			resetNumCheckBox();
		}
		
		$(".chkbox_tv input:checkbox").on("change", function() {
			if(($(".chkbox_tv input:checkbox:checked ").length)>3){
				alert('จำนวนช่องห้ามเกิน 3 ช่อง');
				this.checked = false;
			}
		});
		
    </script>
	
	*/ ?>