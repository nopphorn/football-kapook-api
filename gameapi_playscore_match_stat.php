<?php/*
	header('Content-Type: application/json');
	if(!isset($_REQUEST['match_id']))
	{
		$returnJson	=	array(
			'code_id'	=>	402,
			'message'	=>	'Invalid match id.'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}

	session_start();

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	
	$ops = array(
		array(
			'$match' => array(
				'match_id' => (int)$_REQUEST['match_id'],
				'status' => array( '$gte' => 1 )
			)
		),
		array(
			'$group' => array(
				'_id' => null,
				'count' => array('$sum' => 1),
			),
		)
	);
	$dataGameAll	=	$collectionGame->aggregate($ops);
	if(!isset($dataGameAll['result'][0]['count']))
	{
		$returnJson	=	array(
			'code_id'		=>	202,
			'total_play'	=>	0,
			'data'			=>	null
		);
		
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	if( $dataGameAll['result'][0]['count'] <= 0 )
	{
		$returnJson	=	array(
			'code_id'		=>	202,
			'total_play'	=>	0,
			'data'			=>	null
		);
		
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	$ops = array(
		array(
			'$match' => array(
				'match_id'	=> 	(int)$_REQUEST['match_id'],
				'status' 	=> 	array( '$gte' => 1 ),
				'game_type'	=>	1
			)
		),
		array(
	    	'$group' => array(
				'_id' => array(
					'score_team1'	=> 	'$score_team1',
					'score_team2'	=>	'$score_team2'
				),
	           	'count'	=>	array( '$sum' => 1 )
	        )
		),
		array( '$sort' => array( 'count' => -1 ) )
	);
	$dataGame	=	$collectionGame->aggregate($ops);
	
	if(isset($_REQUEST['show_size']))
	{
		$show_size	=	(int)$_REQUEST['show_size'];
	}
	else
	{
		$show_size	=	5;
	}
	
	$size		=	count($dataGame['result']);
	$sum_total	=	0;
	for( $i=0 ; $i<$size ; $i++ )
	{
		$data[]		=	array(
			'score'		=>		$dataGame['result'][$i]['_id']['score_team1'] . ' - ' . $dataGame['result'][$i]['_id']['score_team2'],
			'count'		=>		$dataGame['result'][$i]['count']
		);
		$sum_total	=	$sum_total + $dataGame['result'][$i]['count'];
		if(($show_size>0)&&(($i+1)==$show_size)&&(($i+1)<$size))
		{
			$data[]		=	array(
				'score'		=>		'อื่นๆ',
				'count'		=>		$dataGameAll['result'][0]['count'] - $sum_total
			);
			break;
		}
	}
	
	$returnJson	=	array(
		'code_id'		=>	202,
		'total_play'	=>	$dataGameAll['result'][0]['count'],
		'data'			=>	$data
	);

	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
*/?>