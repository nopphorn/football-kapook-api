<?php

	function compare_point($a,$b)
	{
		if ($a['NameEN'] == $b['NameEN'])
			return 0;
		return strcmp($a['NameEN'],$b['NameEN']);
	}
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	
	if(isset($_GET['id']))
	{
		if($_REQUEST['status']=='1'){
			$listteam_MC = $memcache->get( 'Football2014-listteam-'.$_GET['id'].'-enable' );
		}else if($_REQUEST['status']=='-1'){
			$listteam_MC = $memcache->get( 'Football2014-listteam-'.$_GET['id'].'-disable' );
		}else{
			$listteam_MC = $memcache->get( 'Football2014-listteam-'.$_GET['id'] );
		}
		
		if(($listteam_MC)&&($_GET['clear']!='1'))
		{
			$listteam 	= 	$listteam_MC;
		}else{
			// init MongoDB
			$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
			$DatabaseMongoDB		=	$connectMongo->selectDB("football");
			$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
			$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
			$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
			
			$dataLeague	=	$collectionLeague->findOne(array( 'id' => (int)$_GET['id'] ));
			if(empty($dataLeague))
			{
				$listteam		=		array(
					'league_id'		=>	null,
					'league_name'	=>	null,
					'num_team'		=> 	0,
					'list'			=>	null
				);
				echo json_encode($listteam);
				exit;
			}

			$listteam['list']	=		array();
			
			$dataMatch 			= 	$collection->find(
				array( 
					'KPLeagueID' 	=> (int)$_GET['id'],
				)
			);
			
			$countMongo			=	$dataMatch->count();
			$dataMatch->next();
			
			for( $i=0 ; $i<$countMongo ; $i++ ){
				$data 	= 	$dataMatch->current();
				
				if(empty($listteam['list'][$data['Team1KPID']])){
					$dataTeam 			= 		$collectionTeam->findOne( array('id' => (int)$data['Team1KPID'] ) );
					
					if(	(($_REQUEST['status']=='1')&&($dataTeam['status']==1))		||
						(($_REQUEST['status']=='-1')&&($dataTeam['status']==0))		||
						(($_REQUEST['status']!='1')&&($_REQUEST['status']!='-1'))
					){
					
						$listteam['list'][$data['Team1KPID']]['NameTH']			=	$dataTeam['NameTH'];
						$listteam['list'][$data['Team1KPID']]['NameTHShort']	=	$dataTeam['NameTHShort'];	
						$listteam['list'][$data['Team1KPID']]['NameEN']			=	$dataTeam['NameEN'];
						$listteam['list'][$data['Team1KPID']]['id'] 			= 	(int)$data['Team1KPID'];
						$listteam['list'][$data['Team1KPID']]['Status'] 		= 	$data['Status'];
						
						$Logo 													= 	str_replace(' ','-',$dataTeam['NameEN']).'.png';
						$Logo_MC												=	$memcache->get('Football2014-Team-Logo-' . $Logo);
						if($Logo_MC==true){
							$listteam['list'][$data['Team1KPID']]['Logo'] 		= 	'http://football.kapook.com/uploads/logo/' . $Logo;
						}else{
							$listteam['list'][$data['Team1KPID']]['Logo'] 		= 	'http://football.kapook.com/uploads/logo/default.png';
						}
					}
				}
				
				if(empty($listteam['list'][$data['Team2KPID']])){
					$dataTeam 			= 		$collectionTeam->findOne( array('id' => (int)$data['Team2KPID'] ) );
					
					if(	(($_REQUEST['status']=='1')&&($dataTeam['status']==1))		||
						(($_REQUEST['status']=='-1')&&($dataTeam['status']==0))		||
						(($_REQUEST['status']!='1')&&($_REQUEST['status']!='-1'))
					){
					
						$listteam['list'][$data['Team2KPID']]['NameTH']			=	$dataTeam['NameTH'];
						$listteam['list'][$data['Team2KPID']]['NameTHShort']	=	$dataTeam['NameTHShort'];	
						$listteam['list'][$data['Team2KPID']]['NameEN']			=	$dataTeam['NameEN'];
						$listteam['list'][$data['Team2KPID']]['id'] 			= 	(int)$data['Team2KPID'];
						$listteam['list'][$data['Team2KPID']]['Status'] 		= 	$data['Status'];
						
						$Logo 													= 	str_replace(' ','-',$dataTeam['NameEN']).'.png';
						$Logo_MC												=	$memcache->get('Football2014-Team-Logo-' . $Logo);
						if($Logo_MC==true){
							$listteam['list'][$data['Team2KPID']]['Logo'] 		= 	'http://football.kapook.com/uploads/logo/' . $Logo;
						}else{
							$listteam['list'][$data['Team2KPID']]['Logo'] 		= 	'http://football.kapook.com/uploads/logo/default.png';
						}
					}
				}
				$dataMatch->next();
			}
			
			usort($listteam['list'],"compare_point");
			$listteam['league_id']			=		(int)$_GET['id'];
			$listteam['NameEN']				=		$dataLeague['NameEN'];
			$listteam['NameTH']				=		$dataLeague['NameTH'];
			$listteam['NameTHShort']		=		$dataLeague['NameTHShort'];
			$listteam['description']		=		$dataLeague['Description'];
			$listteam['num_team']			=		count($listteam['list']);

			// memcache
			$expire				=		3600;
			if($_REQUEST['status']=='1'){
				$memcache->set( 'Football2014-listteam-'.$_GET['id'].'-enable' , $listteam , MEMCACHE_COMPRESSED, $expire );
			}else if($_REQUEST['status']=='-1'){
				$memcache->set( 'Football2014-listteam-'.$_GET['id'].'-disable' , $listteam , MEMCACHE_COMPRESSED, $expire );
			}else{
				$memcache->set( 'Football2014-listteam-'.$_GET['id'] , $listteam , MEMCACHE_COMPRESSED, $expire );
			}
		}
	
	}else{
		if($_REQUEST['status']=='1'){
			$listteam_MC = $memcache->get( 'Football2014-listteam-all-enable' );
		}else if($_REQUEST['status']=='-1'){
			$listteam_MC = $memcache->get( 'Football2014-listteam-all-disable' );
		}else{
			$listteam_MC = $memcache->get( 'Football2014-listteam-all' );
		}
		
		if(($listteam_MC)&&($_GET['clear']!='1')){
			$listteam 	= 	$listteam_MC;
		}else{
			// init MongoDB
			$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
			$DatabaseMongoDB		=	$connectMongo->selectDB("football");
			$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
			if($_REQUEST['status']=='1'){
				$dataTeam 	= 	$collectionTeam->find( array('Status' => 1 ) );
			}else if($_REQUEST['status']=='-1'){
				$dataTeam 	= 	$collectionTeam->find( array('Status' => 0 ) );
			}else{
				$dataTeam 	= 	$collectionTeam->find();
			}
			
			$listteam['list']	=		array();
			
			foreach($dataTeam as $tmpTeam){
				
				$listteam['list'][$tmpTeam['id']]['NameTH']			=	$tmpTeam['NameTH'];
				$listteam['list'][$tmpTeam['id']]['NameTHShort']	=	$tmpTeam['NameTHShort'];	
				$listteam['list'][$tmpTeam['id']]['NameEN']			=	$tmpTeam['NameEN'];
				$listteam['list'][$tmpTeam['id']]['id'] 			= 	(int)$tmpTeam['id'];
				$listteam['list'][$tmpTeam['id']]['Status'] 		= 	(int)$tmpTeam['Status'];
				
				$Logo 												= 	str_replace(' ','-',$tmpTeam['NameEN']).'.png';
				$Logo_MC											=	$memcache->get('Football2014-Team-Logo-' . $Logo);
				if($Logo_MC==true){
					$listteam['list'][$tmpTeam['id']]['Logo'] 	= 	'http://football.kapook.com/uploads/logo/' . $Logo;
				}else{
					$listteam['list'][$tmpTeam['id']]['Logo'] 	= 	'http://football.kapook.com/uploads/logo/default.png';
				}
			}
			
			usort($listteam['list'],"compare_point");
			$listteam['league_id']			=		0;
			$listteam['NameEN']				=		'All';
			$listteam['NameTH']				=		'ทั้งหมด';
			$listteam['NameTHShort']		=		'ทั้งหมด';
			$listteam['description']		=		null;
			$listteam['num_team']			=		count($listteam['list']);
			
			// memcache
			$expire				=		3600;
			if($_REQUEST['status']=='1'){
				$memcache->set( 'Football2014-listteam-all-enable' , $listteam , MEMCACHE_COMPRESSED, $expire );
			}else if($_REQUEST['status']=='-1'){
				$memcache->set( 'Football2014-listteam-all-disable' , $listteam , MEMCACHE_COMPRESSED, $expire );
			}else{
				$memcache->set( 'Football2014-listteam-all' , $listteam , MEMCACHE_COMPRESSED, $expire );
			}
		}
	}
	echo json_encode($listteam);
	
?>