<?php
	//exit;
	//init 
	$match					=	100;
	$rowperpage				=	10;
	
	$max_concurrent			=	5000;
	$count_concurrent		=	0;
	
	$start_date['day']		=	1;
	$start_date['month']	=	8;
	
	if(date('G')>=6){
		$monthToday				=	date("Y-m");
	}else{
		$monthToday				=	date("Y-m",strtotime("now -1 day"));
	}
	
	$isClearRanking			=	false;
	
	$yearlist				=	array();
	$monthlist				=	array();
	$datelist				=	array();
	
	$column_list			=	array(
		0	=>	'total_point',
		1	=>	'total_playscore_win_percent',
		2	=>	'total_playscore_win_score_percent',
		3	=>	'total_playside_single_win_percent',
		4	=>	'total_playside_multi_win_percent'
	);
	
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	
	$collectionMatch        	=	new MongoCollection($DatabaseMongoDB,"football_match_copy");
	$collectionGame				=	new MongoCollection($DatabaseMongoDB,"football_game_copy");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member_copy");
	
	$collectionRanking_all		=	new MongoCollection($DatabaseMongoDB,"football_ranking_all_copy");
	$collectionRanking_season	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season_copy");
	$collectionRanking_month	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month_copy");
	$collectionRanking_day		=	new MongoCollection($DatabaseMongoDB,"football_ranking_day_copy");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	
	$FindArr['GameStatus'] 		= 	0;
	$FindArr['MatchStatus']		=	array('$in' => array('Fin','Post','Abd','Canc'));
	//$FindArr['Status']			=	1;
	$FindArr['MatchDateTime']	=	array('$gte' => '2014-11-05 06:00:00');
	
	$dataMatch 			= 	$collectionMatch->find($FindArr);
	$dataMatch->limit($match);
	$dataMatch->sort(array( 'id' => 1 ));
	$countMatch			=	$dataMatch->count(true);
	//echo	$countMatch;
	//exit;
	$dataMatch->next();
	
	for( $i=0 ; $i<$countMatch ; $i++ )
	{
		$rowMatch						=	$dataMatch->current();
		//var_dump($rowMatch);
		$FindArrGameType1				=	array();
		$gameType						=	0;
		
		/*
		 * Single Play
		 */
		$FindArrGameType1['match_id']	=	$rowMatch['id'];
		$FindArrGameType1['status']		=	array('$gte' => 1,'$lte' => 2);
		$FindArrGameType1['game_type']	=	1;
		
		/*
		 * Multi Play
		 */
		$FindArrGameType2['listGame']	=	array(
			'$elemMatch'	=>	array(
				'match_id' 	=>	$rowMatch['id'],
				'status' 	=>	1,
			)
		);
		$FindArrGameType2['status']		=	array('$gte' => 1,'$lte' => 3);  //a ตรวจทุกคู่แม้จะชุดนั้นจะไม่เข้าแล้วก็ตาม
		$FindArrGameType2['game_type']	=	2;

		// 	Init a type of playscore-game
		//	Team 1 Win
		if($rowMatch['Team1FTScore']>$rowMatch['Team2FTScore']){
			$gameType	=	1;
		}
		//	Team 2 Win
		else if($rowMatch['Team1FTScore']<$rowMatch['Team2FTScore']){
			$gameType	=	2;
		}
		//	Draw
		else if($rowMatch['MatchStatus']=='Fin'){
			$gameType	=	3;
		}
		//	Not Finish
		else{
			$gameType	=	4;
		}
		
		// 	Init a type of game (Betting style)
		//	Init TeamOdds & Odds
		if(($rowMatch['Odds']==-1)||($rowMatch['MatchStatus']!='Fin')){
			$gameTypeBet	=	3;
		}
		else{
			if($rowMatch['TeamOdds']==1)
			{
				$rowMatch['Team2FTScoreBet']	=	$rowMatch['Team2FTScore']+$rowMatch['Odds'];
				$rowMatch['Team1FTScoreBet']	=	$rowMatch['Team1FTScore'];
			}
			else if($rowMatch['TeamOdds']==2)
			{
				$rowMatch['Team1FTScoreBet']	=	$rowMatch['Team1FTScore']+$rowMatch['Odds'];
				$rowMatch['Team2FTScoreBet']	=	$rowMatch['Team2FTScore'];
			}
			else
			{
				$rowMatch['Team1FTScoreBet']	=	$rowMatch['Team1FTScore'];
				$rowMatch['Team2FTScoreBet']	=	$rowMatch['Team2FTScore'];
			}
			
			//	Team 1 Win
			if($rowMatch['Team1FTScoreBet']>$rowMatch['Team2FTScoreBet']){
				$gameTypeBet	=	1;
			}
			//	Team 2 Win
			else if($rowMatch['Team1FTScoreBet']<$rowMatch['Team2FTScoreBet']){
				$gameTypeBet	=	2;
			}
			//	Draw
			else{
				$gameTypeBet	=	3;
			}
		}
		
		/*
		 *-------------------------Playscore Play----------------------------------
		 */
		/*
		$dataGame					= 	$collectionGame->find($FindArrGameType1);
		$countGame					=	$dataGame->count();
		$dataGame->next();
		for( $j=0 ; $j<$countGame ; $j++ )
		{
			$isClearRanking			=	true;
			$rowGame				=	$dataGame->current();
			
			$result_point			=	0;
			$score_point			=	0;
			$win					=	false;
			$winScore				=	false;
				
			//	Team 1 Win
			if(($rowGame['score_team1']>$rowGame['score_team2'])&&($gameType==1)){
				$result_point	=	10;
				$win	=	true;
			}
				
			//	Team 2 Win
			else if(($rowGame['score_team1']<$rowGame['score_team2'])&&($gameType==2)){
				$result_point	=	10;
				$win	=	true;
			}
			//	Draw
			else if(($rowGame['score_team1']==$rowGame['score_team2'])&&($gameType==3)){
				$result_point	=	10;
				$win	=	true;
			}
				
			// score match
			if(($rowGame['score_team1']==$rowMatch['Team1FTScore'])&&($rowGame['score_team2']==$rowMatch['Team2FTScore'])){
				$score_point	=	20;
				$winScore		=	true;
			}
				
			$collectionGame->update(
				array('id' => (int)$rowGame['id']),
				array('$set' => array(
					'status' 		=> 	3,
					'result_point' 	=> 	$result_point,
					'score_point' 	=> 	$score_point
				))
			);
				
			$tmpMonth	=	date("n",strtotime($rowGame['date_play']));
			$tmpDay		=	date("j",strtotime($rowGame['date_play']));
				
			if(	( $tmpMonth > $start_date['month'] )	||
				(
					( $tmpMonth = $start_date['month'] ) &&
					( $tmpDay >= $start_date['day'] )
				)
			)
			{
				$year	=	date("Y",strtotime($rowGame['date_play']));
			}
			else
			{
				$year	=	date("Y",strtotime($rowGame['date_play'] . ' - 1 year'));
			}
			$month	=	substr($rowGame['date_play'],0,7);
				
			//List of year,month,date for clear a ranking cache
			$yearlist[$year]					=	true;
			$monthlist[$month]					=	true;
			$datelist[$rowGame['date_play']]	=	true;
			*/
			/*
			 * UpdatePoint by All-Time
			 */
			/*$collectionRanking_all->update(
				array(
					'id' 			=> 	(int)$rowGame['user_id']
				),
				array(
					'$inc' 	=> 	array(
						'total_playscore_point'						=> 	((int)$score_point+(int)$result_point),
						'total_point'								=> 	((int)$score_point+(int)$result_point),
						'total_playscore'							=> 	1,
						'total_playscore_win_side'					=>	$win ? 1 : 0,
						'total_playscore_win_score'					=>	$winScore ? 1 : 0
					)
				),
				array('upsert'	=>	true)
			);
			
			$dataTemp		=		$collectionRanking_all->findOne(array( 'id' => (int)$rowGame['user_id']));
			
			$collectionRanking_all->update(
				array(
					'id' 			=> 	(int)$rowGame['user_id']
				),
				array(
					'$set' 	=> 	array(
						'total_playscore_win_percent'			=> 	($dataTemp['total_playscore_win_side']/$dataTemp['total_playscore'])*100.0,
						'total_playscore_win_score_percent'		=>	($dataTemp['total_playscore_win_score']/$dataTemp['total_playscore'])*100.0
					)
				)
			);
			*/
			/*
			 * UpdatePoint by Season
			 */
			/*$collectionRanking_season->update(
				array(
					'id' 			=> 	(int)$rowGame['user_id'],
					'season'		=>	$year
				),
				array(
					'$inc' 	=> 	array(
						'total_playscore_point'						=> 	((int)$score_point+(int)$result_point),
						'total_point'								=> 	((int)$score_point+(int)$result_point),
						'total_playscore'							=> 	1,
						'total_playscore_win_side'					=>	$win ? 1 : 0,
						'total_playscore_win_score'					=>	$winScore ? 1 : 0
					)
				),
				array('upsert'	=>	true)
			);
			
			$dataTemp		=		$collectionRanking_season->findOne(
				array(
					'id' 			=> (int)$rowGame['user_id'],
					'season'		=>	$year
				)
			);
			
			$collectionRanking_season->update(
				array(
					'id' 			=> 	(int)$rowGame['user_id'],
					'season'		=>	$year
				),
				array(
					'$set' 	=> 	array(
						'total_playscore_win_percent'			=> 	($dataTemp['total_playscore_win_side']/$dataTemp['total_playscore'])*100.0,
						'total_playscore_win_score_percent'		=>	($dataTemp['total_playscore_win_score']/$dataTemp['total_playscore'])*100.0
					)
				)
			);
			*/
			/*
			 * UpdatePoint by Month
			 */
			/*$collectionRanking_month->update(
				array(
					'id' 			=> 	(int)$rowGame['user_id'],
					'month'			=>	$month
				),
				array(
					'$inc' 	=> 	array(
						'total_playscore_point'						=> 	((int)$score_point+(int)$result_point),
						'total_point'								=> 	((int)$score_point+(int)$result_point),
						'total_playscore'							=> 	1,
						'total_playscore_win_side'					=>	$win ? 1 : 0,
						'total_playscore_win_score'					=>	$winScore ? 1 : 0
					)
				),
				array('upsert'	=>	true)
			);
			
			$dataTemp		=		$collectionRanking_month->findOne(
				array(
					'id' 			=> (int)$rowGame['user_id'],
					'month'			=>	$month
				)
			);
			
			$collectionRanking_month->update(
				array(
					'id' 			=> 	(int)$rowGame['user_id'],
					'month'			=>	$month
				),
				array(
					'$set' 	=> 	array(
						'total_playscore_win_percent'			=> 	($dataTemp['total_playscore_win_side']/$dataTemp['total_playscore'])*100.0,
						'total_playscore_win_score_percent'		=>	($dataTemp['total_playscore_win_score']/$dataTemp['total_playscore'])*100.0
					)
				)
			);
			*/
			/*
			 * UpdatePoint by Date
			 */
			/*$collectionRanking_day->update(
				array(
					'id' 			=> 	(int)$rowGame['user_id'],
					'date'			=>	$rowGame['date_play']
				),
				array(
					'$inc' 	=> 	array(
						'total_playscore_point'						=> 	((int)$score_point+(int)$result_point),
						'total_point'								=> 	((int)$score_point+(int)$result_point),
						'total_playscore'							=> 	1,
						'total_playscore_win_side'					=>	$win ? 1 : 0,
						'total_playscore_win_score'					=>	$winScore ? 1 : 0
					)
				),
				array('upsert'	=>	true)
			);
			
			$dataTemp		=		$collectionRanking_day->findOne(
				array(
					'id' 			=> (int)$rowGame['user_id'],
					'date'			=>	$rowGame['date_play']
				)
			);
			
			$collectionRanking_day->update(
				array(
					'id' 			=> 	(int)$rowGame['user_id'],
					'date'			=>	$rowGame['date_play']
				),
				array(
					'$set' 	=> 	array(
						'total_playscore_win_percent'			=> 	($dataTemp['total_playscore_win_side']/$dataTemp['total_playscore'])*100.0,
						'total_playscore_win_score_percent'		=>	($dataTemp['total_playscore_win_score']/$dataTemp['total_playscore'])*100.0
					)
				)
			);
			*/
			/*
			 * UpdatePoint for User
			 */
			/*$collectionMember->update(
				array('id' => (int)$rowGame['user_id']),
				array(
					'$inc' 	=> 	array(
						'point' 	=> 	(double)($score_point+$result_point)
					)
				),
				array('upsert'	=>	true)
			);
				
			$memcache->delete( 'Football2014-game-playscore-info-' . $rowGame['user_id'] . '-' . $rowGame['match_id'] );
			$memcache->delete( 'Football2014-game-playscore-list-info-' . $rowGame['user_id'] . '-' . $rowGame['date_play'] );

			echo 'update a point of game no.'.$rowGame['id'].'<br>';
			
			$count_concurrent++;
			if($count_concurrent >= $maxconcurrent){
				echo 'Stop Process @ Match No.'.$rowMatch['id'].'<br>';
				exit;
			}
			
			$dataGame->next();
		}
		*/
		/*
		 *-------------------------Playside Play----------------------------------
		 */
		$dataGame					= 	$collectionGame->find($FindArrGameType2);		
		$countGame					=	$dataGame->count();
		$dataGame->next();
		
		for( $j=0 ; $j<$countGame ; $j++ )
		{
			$isClearRanking			=	true;
			$rowGame				=	$dataGame->current();
			$isEnd					=	false;
			
			$resultType				=	0; /* 2=เข้าเต็ม 1=เข้าครึ่ง 0=เสมอ -1=เสียครึ่ง -2=เสียเต็ม*/
			
			$sizeListGame	=	count($rowGame['listGame']);
			$isWDL			=	0;
			for($k=0;$k<$sizeListGame;$k++)
			{
				if($rowGame['listGame'][$k]['match_id']==$rowMatch['id']){
					$tmpGame	=	$rowGame['listGame'][$k];
					break;
				}
			}
			if(($rowGame['status']>=1)||($rowGame['status']<=3)){
				if($tmpGame['team_win']==$gameTypeBet){
					if( abs($rowMatch['Team1FTScoreBet']-$rowMatch['Team2FTScoreBet']) >= 0.5 ){
						if($rowGame['match_id']>0){
							$rowGame['multiple']	=	2.0;
						}else{
							$rowGame['multiple']	=	$rowGame['multiple'] * 2.0;
						}
						$resultType					=	2;
					}else{
						if($rowGame['match_id']>0){
							$rowGame['multiple']	=	1.5;
						}else{
							$rowGame['multiple']	=	$rowGame['multiple'] * 1.5;
						}
						$resultType					=	1;
					}
				}else if($gameTypeBet!=3){
					if( abs($rowMatch['Team1FTScoreBet']-$rowMatch['Team2FTScoreBet']) >= 0.5 ){
						$rowGame['multiple']	=	0;
						$isEnd					=	true;
						$resultType				=	-2;
					}else{
						$rowGame['multiple']	=	$rowGame['multiple'] * 0.5;
						$resultType				=	-1;
					}
				}
			}
			$isWDL	=	$resultType;
			
			$collectionGame->update(
				array(
					'id' => (int)$rowGame['id'],
					'listGame' 	=>	array(
						'$elemMatch' => array('match_id' => $rowMatch['id'])
					)
				),
				array(
					'$set' => array(
						'multiple' 			=> 	$rowGame['multiple'],
						'listGame.$.status' => 2,
						'listGame.$.isWDL'	=> $isWDL,
					)
				),
				array('upsert'	=>	true)
			);
				
			if(($rowGame['status']==1)||($rowGame['status']==2)){
				// If Lose
				$isChange	=	false;
				if($isEnd){
					$collectionGame->update(
						array('id' => (int)$rowGame['id']),
						array('$set' => 
							array(
								'status' 		=> 	3,
								'point_return'	=>	(double)(($rowGame['point']*$rowGame['multiple'])-$rowGame['point'])
							)
						),
						array('upsert'	=>	true)
					);
					$isChange	=	true;
				}else{
					$dataGameReCheck	= 	$collectionGame->findOne(
						array(
							'id'		=> 	$rowGame['id'],
							'listGame'	=>	array( '$elemMatch' => array( 'status' => 1 ) )
						)
					);
					if(!$dataGameReCheck){
					
						$collectionGame->update(
							array('id' => (int)$rowGame['id']),
							array('$set' => 
								array(
									'status' 		=> 	3,
									'point_return'	=>	(double)(($rowGame['point']*$rowGame['multiple'])-$rowGame['point'])
								)
							),
							array('upsert'	=>	true)
						);
						$isChange	=	true;
					}else if($rowGame['status']<2){
						$collectionGame->update(
							array('id' => (int)$rowGame['id']),
							array('$set' => 
								array('status' => 2)
							),
							array('upsert'	=>	true)
						);
					}
				}
				if($isChange){
					/*
					 *------------------update member info-----------------------
					 */
					$tmpMonth	=	date("n",strtotime($rowGame['date_play']));
					$tmpDay		=	date("j",strtotime($rowGame['date_play']));
						
					if(	( $tmpMonth > $start_date['month'] )	||
						(
							( $tmpMonth == $start_date['month'] ) &&
							( $tmpDay >= $start_date['day'] )
						)
					)
					{
						$year	=	date("Y",strtotime($rowGame['date_play']));
					}
					else
					{
						$year	=	date("Y",strtotime($rowGame['date_play'] . ' - 1 year'));
					}
					$month	=	substr($rowGame['date_play'],0,7);
						
					//List of year,month,date for clear a ranking cache
					$yearlist[$year]					=	true;
					$monthlist[$month]					=	true;
					$datelist[$rowGame['date_play']]	=	true;
					
					$inc_update		=	array(
						'total_playside_point'						=> 	(double)(($rowGame['point']*$rowGame['multiple'])-$rowGame['point']),
						'total_point'								=> 	(double)(($rowGame['point']*$rowGame['multiple'])-$rowGame['point']),
						'total_playside_single'						=> 	($rowGame['match_id']>0) ? 1 : 0,
						'total_playside_single_win'					=>	(($rowGame['multiple']>1)&&($rowGame['match_id']>0)) 	? 1 : 0,
						'total_playside_single_win_full'			=>	(($rowGame['multiple']>=2)&&($rowGame['match_id']>0)) 	? 1 : 0,
						'total_playside_single_win_half'			=>	(($rowGame['multiple']<2)&&($rowGame['multiple']>=1.5)&&($rowGame['match_id']>0)) 	? 1 : 0,
						'total_playside_single_draw'				=>	(($rowGame['multiple']==1)&&($rowGame['match_id']>0)) 	? 1 : 0,
						'total_playside_single_lose_half'			=>	(($rowGame['multiple']<1)&&($rowGame['multiple']>=0.5)&&($rowGame['match_id']>0)) 	? 1 : 0,
						'total_playside_single_lose_full'			=>	(($rowGame['multiple']<0.5)&&($rowGame['match_id']>0)) 	? 1 : 0,					
						'total_playside_multi'						=> 	($rowGame['match_id']<=0) ? 1 : 0 ,
						'total_playside_multi_win'					=>	(($rowGame['multiple']>1)&&($rowGame['match_id']<=0)) 	? 1 : 0,
						'total_playside_multi_draw'					=>	(($rowGame['multiple']==1)&&($rowGame['match_id']<=0)) 	? 1 : 0,
						'total_playside_multi_lose_full'			=>	(($rowGame['multiple']<1)&&($rowGame['match_id']<=0)) 	? 1 : 0,	
						'total_play'								=>	1,
					);
					
					if($rowGame['match_id']>0){
						if(($rowGame['multiple']>1)&&(($rowGame['multiple']<=1.5))){
							$inc_update['total_rate']			=	5;
						}else if($rowGame['multiple']>1.5){
							$inc_update['total_rate']			=	10;
						}else{
							$inc_update['total_rate']			=	0;
						}
					}else{
						if($rowGame['multiple']>1){
							$inc_update['total_rate']			=	10;
						}else{
							$inc_update['total_rate']			=	0;
						}
					}
					
					/*
					 * UpdatePoint by Season
					 */
					$collectionRanking_season->update(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'season'	=>	$year
						),
						array('$inc' 	=> 	$inc_update),
						array('upsert'	=>	true)
					);
					$dataTempSeason 	= 	$collectionRanking_season->findOne(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'season'	=>	$year
						)
					);

					$collectionRanking_season->update(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'season'	=>	$year
						),
						array(
							'$set' 	=> 	array(
								'total_playside_single_win_percent'			=> 	($dataTempSeason['total_playside_single_win']/$dataTempSeason['total_playside_single'])*100.0,
								'total_playside_multi_win_percent'			=>	($dataTempSeason['total_playside_multi_win']/$dataTempSeason['total_playside_multi'])*100.0,
								'total_rate_calculate'						=>	($dataTempSeason['total_rate']/$dataTempSeason['total_play'])*1.0,
								'total_rate_calculate_by_time'				=>	($dataTempSeason['total_rate']/$dataTempSeason['total_play'])*1.0
							)
						)
					);
					
					/*
					 * UpdatePoint by All-Time
					 */
					$collectionRanking_all->update(
						array('id' => (int)$rowGame['user_id']),
						array('$inc' 	=> $inc_update),
						array('upsert'	=>	true)
					);
					$dataTemp 		= 	$collectionRanking_all->findOne(array( 'id' => (int)$rowGame['user_id']));
						
					$collectionRanking_all->update(
						array('id' => (int)$rowGame['user_id']),
						array(
							'$set' 	=> 	array(
								'total_playside_single_win_percent'			=> 	($dataTemp['total_playside_single_win']/$dataTemp['total_playside_single'])*100.0,
								'total_playside_multi_win_percent'			=>	($dataTemp['total_playside_multi_win']/$dataTemp['total_playside_multi'])*100.0,
								'total_rate_calculate'						=>	($dataTempSeason['total_rate']/$dataTempSeason['total_play'])*1.0,
								'total_rate_calculate_by_time'				=>	($dataTemp['total_rate']/$dataTemp['total_play'])*1.0
							)
						)
					);
					
					/*
					 * UpdatePoint by Month
					 */
					$collectionRanking_month->update(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'month'		=>	$month
						),
						array('$inc' 	=> 	$inc_update),
						array('upsert'	=>	true)
					);
					$dataTemp 		= 	$collectionRanking_month->findOne(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'month'		=>	$month
						)
					);
						
					$collectionRanking_month->update(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'month'		=>	$month
						),
						array(
							'$set' 	=> 	array(
								'total_playside_single_win_percent'			=> 	($dataTemp['total_playside_single_win']/$dataTemp['total_playside_single'])*100.0,
								'total_playside_multi_win_percent'			=>	($dataTemp['total_playside_multi_win']/$dataTemp['total_playside_multi'])*100.0,
								'total_rate_calculate'						=>	($dataTempSeason['total_rate']/$dataTempSeason['total_play'])*1.0,
								'total_rate_calculate_by_time'				=>	($dataTemp['total_rate']/$dataTemp['total_play'])*1.0
							)
						)
					);
					
					/*
					 * UpdatePoint by Date
					 */
					$collectionRanking_day->update(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'date'		=>	$rowGame['date_play']
						),
						array('$inc' 	=> 	$inc_update),
						array('upsert'	=>	true)
					);
					$dataTemp 		= 	$collectionRanking_day->findOne(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'date'		=>	$rowGame['date_play']
						)
					);
						
					$collectionRanking_day->update(
						array(
							'id' 		=> 	(int)$rowGame['user_id'],
							'date'		=>	$rowGame['date_play']
						),
						array(
							'$set' 	=> 	array(
								'total_playside_single_win_percent'			=> 	($dataTemp['total_playside_single_win']/$dataTemp['total_playside_single'])*100.0,
								'total_playside_multi_win_percent'			=>	($dataTemp['total_playside_multi_win']/$dataTemp['total_playside_multi'])*100.0,
								'total_rate_calculate'						=>	($dataTempSeason['total_rate']/$dataTempSeason['total_play'])*1.0,
								'total_rate_calculate_by_time'				=>	($dataTemp['total_rate']/$dataTemp['total_play'])*1.0
							)
						)
					);
					
					/*
					 * UpdatePoint for User
					 */
					if( $monthToday == $month ){
						$collectionMember->update(
							array('id' => (int)$rowGame['user_id']),
							array(
								'$inc' 	=> 	array(
									'point' 	=> 	(double)(($rowGame['point']*$rowGame['multiple'])-$rowGame['point'])
								)
							),
							array('upsert'	=>	true)
						);
					}

					if($rowGame['match_id']>0)
						$memcache->delete( 'Football2014-game-playside-info-' . $rowGame['user_id'] . '-' . $rowGame['match_id'] );
					else
						$memcache->delete( 'Football2014-game-playside-multi-info-' . $rowGame['id'] );
						
					$memcache->delete( 'Football2014-game-playside-list-info-' . $rowGame['user_id'] . '-' . $rowGame['date_play'] );

					echo 'update a point of game no.'.$rowGame['id'].'<br>';

				}
			}
			
			if($rowGame['match_id']>0)
				$memcache->delete( 'Football2014-game-playside-info-' . $rowGame['user_id'] . '-' . $rowGame['match_id'] );
			else
				$memcache->delete( 'Football2014-game-playside-multi-info-' . $rowGame['id'] );
						
			$memcache->delete( 'Football2014-game-playside-list-info-' . $rowGame['user_id'] . '-' . $rowGame['date_play'] );

			echo 'update a point of game no.'.$rowGame['id'].'<br>';
			
			$dataGame->next();
			
			$count_concurrent = $count_concurrent+1;
			if($count_concurrent >= $max_concurrent){
				echo 'Stop Process @ Match No.'.$rowMatch['id'].'<br>';
				exit;
			}
			
		}
		
		$collectionMatch->update(
			array('id' => (int)$rowMatch['id']),
			array('$set' => array(
				'GameStatus' 		=> 	1,
			))
		);
		echo 'Match No.'.$rowMatch['id'].' Finish<br>';
		$dataMatch->next();
	}
	
	if($isClearRanking){
	
		$numMember	=	$DatabaseMongoDB->command(array( 'count' => 'football_member' ));
		$numMember 	= 	$numMember['n'];
		
		$numColumn	=	count($column_list);
		
		for( $i=0 ; $i<$numMember ; $i=($i+$rowperpage) ){
			for( $j=0 ; $j<$numColumn ; $j++ )
			{
				$memcache->delete( 'Football2014-ranking-all-time-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-asc');
				$memcache->delete( 'Football2014-ranking-all-time-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-desc');
				foreach($yearlist as $key => $value){
					$memcache->delete( 'Football2014-ranking-season-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-asc');
					$memcache->delete( 'Football2014-ranking-season-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-desc');
					//echo 'Football2014-ranking-season-'.$key.'-'.$i .'<br>';
				}
				foreach($monthlist as $key => $value){
					$memcache->delete( 'Football2014-ranking-month-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-asc');
					$memcache->delete( 'Football2014-ranking-month-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-desc');
					//echo 'Football2014-ranking-month-'.$key.'-'.$i .'<br>';
				}
				foreach($datelist as $key => $value){
					$memcache->delete( 'Football2014-ranking-date-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-asc');
					$memcache->delete( 'Football2014-ranking-date-' . $key . '-' . $i . '-' . $rowperpage . '-' . $column_list[$j] . '-desc');
					//echo 'Football2014-ranking-date-' . $key . '-' . $i . '<br>';
				}
			}
		}
		
		//Specific for mini (full)ranking
		foreach($datelist as $key => $value){
			$memcache->delete( 'Football2014-rankingfull-date-' . $key . '-0-5-total_point-desc');
		}
	}
	
	echo 'Finish';
	
?>