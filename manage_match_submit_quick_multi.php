<?php
	include 'manage_checklogin.php';

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");

		foreach($_POST['ScoreT1M'] as $k => $value)
		{
			
			$tvListData				=	array();
			if($_POST['TVLiveList'][$k][0]){
				$tvListData[]		=	$_POST['TVLiveList'][$k][0];
			}
			if($_POST['TVLiveList'][$k][1]){
				$tvListData[]		=	$_POST['TVLiveList'][$k][1];
			}
			if($_POST['TVLiveList'][$k][2]){
				$tvListData[]		=	$_POST['TVLiveList'][$k][2];
			}
		
			$dataupdate = array(
				'Team1FTScore' 		=> 	(int)$value,
				'Team2FTScore' 		=> 	(int)$_POST['ScoreT2M'][$k],
				'ETScore'			=>	empty($_POST['isExtra'][$k])		? '-'	:	(int)$_POST['team1scoreET'][$k] . '-' . (int)$_POST['team2scoreET'][$k],
				'PNScore'			=>	empty($_POST['isPK'][$k])			? '-'	:	(int)$_POST['team1scorePK'][$k] . '-' . (int)$_POST['team2scorePK'][$k],
				'TeamOdds'			=>	empty($_POST['inputteamodds'][$k]) 	? 0 	: 	(int)$_POST['inputteamodds'][$k],
				'Odds'				=>	empty($_POST['inputodds'][$k]) 		? 0.0 	: 	(float)$_POST['inputodds'][$k],
				'Status' 			=> 	(int)$_POST['status'][$k],
				'MatchStatus' 		=> 	$_POST['matchstatus'][$k],
				'TVLiveList'		=>	$tvListData,
				'LastUpdate_IP'		=>	$ip,
				'LastUpdate_UserID'	=>	isset($_COOKIE["UserID"]) 			? intval($_COOKIE["UserID"]) : "UNKNOWN",
				'LastUpdate_Time'	=>	date('Y-m-d H:i:s')
			);
			
			if(empty($_POST['LP'][$k]))
				$dataupdate['Priority'] = 99;
			else if(!is_numeric($_POST['LP'][$k]))
				$dataupdate['Priority'] = 99;
			else if(is_float($_POST['LP'][$k]))
				$dataupdate['Priority'] = round($_POST['LP'][$k]);
			else
				$dataupdate['Priority'] = (int)$_POST['LP'][$k];
		
			$collection->update(
				array('id' => (int)$k),
				array('$set' => $dataupdate)
			);
			// clear memcache
			$memcache->delete('Football2014-MatchContents-' . $k);
		}
		
		header( "location: manage_match.php" );
		exit(0);
?>