<?php
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	header('Content-Type: application/json');

	function checkCookie()
    {
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionMatch        =	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMember		=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	if(!isset($_REQUEST['user_id'])){
		if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
		{
			$returnJson	=	array(
				'code_id'	=>	401,
				'message'	=>	'Cannot Authention.',
				'uid'		=>	$_COOKIE['uid']
			);
			echo json_encode($returnJson);
			return;
		}
		if(!checkCookie())
		{
			$returnJson	=	array(
				'code_id'	=>	401,
				'message'	=>	'Cannot Authention.',
				'uid'		=>	$_COOKIE['uid']
			);
			echo json_encode($returnJson);
			return;
		}
		$uid	=	(int)$_COOKIE['uid'];
	}else{
		$uid	=	(int)$_REQUEST['user_id'];
	}
	
	
	
	if(isset($_REQUEST['match_id']))
	{
		$matchid	=	(int)$_REQUEST['match_id'];
		$matchinfo_MC = $memcache->get( 'Football2014-game-playscore-info-' . $uid . '-' . $matchid );
		
		if($matchinfo_MC && ($_REQUEST['clear']!='1'))
		{
			$returnJson			=	$matchinfo_MC;
		}
		else
		{
			$FindArr['status'] 		=	array( '$gte' => 1 );
			$FindArr['user_id'] 	= 	$uid;
			$FindArr['game_type']	=	1;
			$FindArr['match_id']	=	$matchid;
			
			$dataGame 				= 	$collectionGame->findOne($FindArr);
			
			if(!$dataGame)
			{
				$gameInfo	=	array(
					'isPlay'	=>	false,
					'Message'	=>	'Not Found.'
				);
			}
			else if($dataGame['status']==1)
			{
				$gameInfo	=	array(
					'isPlay'		=>	true,
					'isFinish'		=>	false,
					'id'			=>	$matchid,
					'score_team1'	=>	$dataGame['score_team1'],
					'score_team2'	=>	$dataGame['score_team2'],
				);
			}
			else if($dataGame['status']==2)
			{
				$gameInfo	=	array(
					'isPlay'		=>		true,
					'isFinish'		=>		true,
					'id'			=>		$matchid,
					'score_team1'	=>		$dataGame['score_team1'],
					'score_team2'	=>		$dataGame['score_team2'],
					'isWinBySide'	=>		($dataGame['result_point']>0)	?	true	:	false,
					'isWinByScore'	=>		($dataGame['score_point']>0)	?	true	:	false,
					'totalPoint'	=>		($dataGame['result_point']+$dataGame['score_point'])
				);
			}
			$returnJson	=	$gameInfo;
			$memcache->set( 'Football2014-game-playscore-info-' . $uid . '-' . $matchid , $returnJson , MEMCACHE_COMPRESSED, $expire );
		}
	}
	else//Get list game by date
	{
		$FindArr['status'] 				=	array( '$gte' => 1 );
		$FindArr['user_id'] 			= 	$uid;
		$FindArr['game_type']			=	1;
		
		if(isset($_REQUEST['date_play']))
		{
			$FindArr['date_play']		=	$_REQUEST['date_play'];
		}
		else
		{
			if(date('G',strtotime('now'))>=6){
				$FindArr['date_play']	=	date("Y-m-d");
			}
			else
			{
				$FindArr['date_play']	=	date("Y-m-d",' - 1 day');
			}
		}
		
		$matchlistinfo_MC 				= 	$memcache->get( 'Football2014-game-playscore-list-info-' . $uid . '-' . $FindArr['date_play'] );
		if($matchlistinfo_MC && ($_REQUEST['clear']!='1'))
		{
			$returnJson					=	$matchlistinfo_MC;
		}
		else
		{
			$dataMongo 					= 	$collectionGame->find($FindArr)->sort(array( 'date_match' => 1 ));
			$countMongo					=	$dataMongo->count();
			$dataMongo->next();
			
			if($countMongo>0)
			{
				$gameInfo					=	array();
			}
			else
			{
				$gameInfo					=	null;
			}
			$totalAll					=	0;

			for( $i=0 ; $i<$countMongo ; $i++ )
			{
				$data 			= 	$dataMongo->current();

				// Match Data
				$dataMatch		=		$collectionMatch->findOne(	array(	'id' =>	$data['match_id']));
				// Zone data
				$dataZone		= 		$collectionZone->findOne(	array( 	'id' => $dataMatch['KPLeagueCountryID'] ));
				// League data
				$dataLeague		= 		$collectionLeague->findOne(	array( 	'id' => $dataMatch['KPLeagueID'] ));
				// Team1 data
				$dataTeam1		= 		$collectionTeam->findOne(	array( 	'id' => $dataMatch['Team1KPID'] ));
				// Team2 data
				$dataTeam2		= 		$collectionTeam->findOne(	array( 	'id' => $dataMatch['Team2KPID'] ));
				
				$Team1Logo 								= 	str_replace(' ','-',$dataTeam1['NameEN']).'.png';
				$Team1Logo_MC							=	$memcache->get('Football2014-Team-Logo-' . $Team1Logo);
				if($Team1Logo_MC){
					$Team1LogoPath = 'http://football.kapook.com/uploads/logo/' . $Team1Logo;
				}else{
					$Team1LogoPath = 'http://football.kapook.com/uploads/logo/default.png';
				}
				
				$Team2Logo 								= 	str_replace(' ','-',$dataTeam2['NameEN']).'.png';
				$Team2Logo_MC							=	$memcache->get('Football2014-Team-Logo-' . $Team2Logo);
				if($Team2Logo_MC){
					$Team2LogoPath = 'http://football.kapook.com/uploads/logo/' . $Team2Logo;
				}else{
					$Team2LogoPath = 'http://football.kapook.com/uploads/logo/default.png';
				}
				
				$gameInfo[]	=	array(
					'id'				=>		$data['id'],
					'match_id'			=>		$data['match_id'],
					'MatchDateTime'		=>		$dataMatch['MatchDateTime'],
					'Zone'				=>		empty($dataZone['NameTH']) 			? 	$dataZone['NameEN'] 		: 	$dataZone['NameTH'],
					'League'			=>		empty($dataLeague['NameTH']) 		? 	$dataLeague['NameEN'] 		: 	$dataLeague['NameTH'],
					'Team1KPID'			=>		$dataTeam1['id'],
					'Team2KPID'			=>		$dataTeam2['id'],
					'Team1'				=>		empty($dataTeam1['NameTH'])			?	$dataTeam1['NameEN']		:	$dataTeam1['NameTH'],
					'Team2'				=>		empty($dataTeam2['NameTH'])			?	$dataTeam2['NameEN']		:	$dataTeam2['NameTH'],
					'Team1Logo'			=>		$Team1LogoPath,
					'Team2Logo'			=>		$Team2LogoPath,
					'Odds'				=>		($dataMatch['Odds']==-1)			?	null						:	$dataMatch['Odds'],
					'TeamOdds'			=>		($dataMatch['Odds']==-1)			?	null						:	$dataMatch['TeamOdds'],
					'Team1FTScore'		=>		empty($dataMatch['Team1FTScore'])	?	0							:	$dataMatch['Team1FTScore'],
					'Team2FTScore'		=>		empty($dataMatch['Team2FTScore'])	?	0							:	$dataMatch['Team2FTScore'],
					'MyScoreTeam1'		=>		empty($data['score_team1'])			?	0							:	$data['score_team1'],
					'MyScoreTeam2'		=>		empty($data['score_team2'])			?	0							:	$data['score_team2'],	
					'MatchStatus'		=>		$dataMatch['MatchStatus'],
					'isCorrect_Side'	=>		($data['result_point']>0)			?	true						:	false,
					'isCorrect_Score'	=>		($data['score_point']>0)			?	true						:	false,
					'result_point'		=>		$data['result_point'],
					'score_point'		=>		$data['score_point'],
					'total_point'		=>		($data['score_point']+$data['result_point']),
					'isCalculatePoint'	=>		($data['status']==2)				?	true						:	false,
					'MatchPageURL' 		=> 		'http://football.kapook.com/match-'.$data['match_id'].'-'.$dataTeam1['NameEN'].'-'.$dataTeam2['NameEN']
				);
				
				$totalAll	=	($totalAll+($data['score_point']+$data['result_point']));
				$dataMongo->next();
				
			}

			$returnJson	=	array(
				'uid'			=>	$uid,
				'total_game'	=>	$countMongo,
				'date_play'		=>	$FindArr['date_play'],
				'Total_Point'	=>	$totalAll,
				'game_info'		=>	$gameInfo
			);
			$memcache->set( 'Football2014-game-playscore-list-info-' . $uid . '-' . $FindArr['date_play'] , $returnJson , MEMCACHE_COMPRESSED, $expire );
		}
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>