<?php
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	header('Content-Type: application/json');

	function checkCookie()
    {
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionMatch        =	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMember		=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	if(!isset($_REQUEST['user_id'])){
		if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
		{
			$returnJson	=	array(
				'code_id'	=>	401,
				'message'	=>	'Cannot Authention.',
				'uid'		=>	$_COOKIE['uid']
			);
			echo json_encode($returnJson);
			return;
		}
		if(!checkCookie())
		{
			$returnJson	=	array(
				'code_id'	=>	401,
				'message'	=>	'Cannot Authention.',
				'uid'		=>	$_COOKIE['uid']
			);
			echo json_encode($returnJson);
			return;
		}
		$uid	=	(int)$_COOKIE['uid'];
	}else{
		$uid	=	(int)$_REQUEST['user_id'];
	}

	$FindArr['status'] 				=	array( '$gte' => 1 );
	$FindArr['user_id'] 			= 	$uid;
	//$FindArr['game_type']			=	2;
		
	if(isset($_REQUEST['date_play'])){
		$FindArr['date_play']		=	$_REQUEST['date_play'];
	}else{
		if(date('G',strtotime('now'))>=6){
			$FindArr['date_play']	=	date("Y-m-d");
		}else{
			$FindArr['date_play']	=	date("Y-m-d",' - 1 day');
		}
	}
		
	$matchlistinfo_MC 				= 	$memcache->get( 'Football2014-game-playside-list-info-' . $uid . '-' . $FindArr['date_play'] );
	//$matchlistinfo_MC					=	null;
	if($matchlistinfo_MC && ($_REQUEST['clear']!='1')){
		$returnJson					=	$matchlistinfo_MC;
	}else{
		$dataMongo 					= 	$collectionGame->find($FindArr)->sort(array( 'date_match' => 1 ));
		$countMongo					=	$dataMongo->count();
		$dataMongo->next();
			
		if($countMongo>0){
			$gameInfo					=	array();
		}else{
			$gameInfo					=	array( 0 => array() );
		}
		$totalAll					=	0;
		$countSet					=	0;

		for( $i=0 ; $i<$countMongo ; $i++ ){
			$data 			= 	$dataMongo->current();
			if($data['match_id']>0){
				//Single Play
					
				// Match Data
				$dataMatch		=		$collectionMatch->findOne(	array(	'id' =>	$data['match_id']));
				// Zone data
				$dataZone		= 		$collectionZone->findOne(	array( 	'id' => $dataMatch['KPLeagueCountryID'] ));
				// League data
				$dataLeague		= 		$collectionLeague->findOne(	array( 	'id' => $dataMatch['KPLeagueID'] ));
				// Team1 data
				$dataTeam1		= 		$collectionTeam->findOne(	array( 	'id' => $dataMatch['Team1KPID'] ));
				// Team2 data
				$dataTeam2		= 		$collectionTeam->findOne(	array( 	'id' => $dataMatch['Team2KPID'] ));
					
				$Team1Logo 								= 	str_replace(' ','-',$dataTeam1['NameEN']).'.png';
				$Team1Logo_MC							=	$memcache->get('Football2014-Team-Logo-' . $Team1Logo);
				if($Team1Logo_MC){
					$Team1LogoPath = 'http://football.kapook.com/uploads/logo/' . $Team1Logo;
				}else{
					$Team1LogoPath = 'http://football.kapook.com/uploads/logo/default.png';
				}
					
				$Team2Logo 								= 	str_replace(' ','-',$dataTeam2['NameEN']).'.png';
				$Team2Logo_MC							=	$memcache->get('Football2014-Team-Logo-' . $Team2Logo);
				if($Team2Logo_MC){
					$Team2LogoPath = 'http://football.kapook.com/uploads/logo/' . $Team2Logo;
				}else{
					$Team2LogoPath = 'http://football.kapook.com/uploads/logo/default.png';
				}
					
				$gameInfo[0]['list'][]	=	array(
					'id'				=>		$data['id'],
					'match_id'			=>		$data['match_id'],
					'MatchDateTime'		=>		$dataMatch['MatchDateTime'],
					'Zone'				=>		empty($dataZone['NameTH']) 				? 	$dataZone['NameEN'] 		: 	$dataZone['NameTH'],
					'League'			=>		empty($dataLeague['NameTH']) 			? 	$dataLeague['NameEN'] 		: 	$dataLeague['NameTH'],
					'Team1KPID'			=>		$dataTeam1['id'],
					'Team2KPID'			=>		$dataTeam2['id'],
					'Team1'				=>		empty($dataTeam1['NameTH'])				?	$dataTeam1['NameEN']		:	$dataTeam1['NameTH'],
					'Team2'				=>		empty($dataTeam2['NameTH'])				?	$dataTeam2['NameEN']		:	$dataTeam2['NameTH'],
					'Team1Logo'			=>		$Team1LogoPath,
					'Team2Logo'			=>		$Team2LogoPath,
					'Odds'				=>		($dataMatch['Odds']==-1)				?	null						:	$dataMatch['Odds'],
					'TeamOdds'			=>		($dataMatch['Odds']==-1)				?	null						:	$dataMatch['TeamOdds'],
					'Team1FTScore'		=>		empty($dataMatch['Team1FTScore'])		?	0							:	$dataMatch['Team1FTScore'],
					'Team2FTScore'		=>		empty($dataMatch['Team2FTScore'])		?	0							:	$dataMatch['Team2FTScore'],
					'team_win'			=>		empty($data['listGame'][0]['team_win'])	?	0							:	$data['listGame'][0]['team_win'],
					'MatchStatus'		=>		$dataMatch['MatchStatus'],
					'isCorrect'			=>		empty($data['listGame'][0]['isWDL'])	?	0							:	$data['listGame'][0]['isWDL'],
					'point'				=>		$data['point'],
					'point_return'		=>		$data['point_return'],
					'isCalculatePoint'	=>		($data['status']==3)					?	true						:	false,
					'MatchPageURL' 		=> 		'http://football.kapook.com/match-'.$data['match_id'].'-'.$dataTeam1['NameEN'].'-'.$dataTeam2['NameEN']
				);
				if(!isset($gameInfo[0]['point_return'])){
					$gameInfo[0]['point_return']	=	0;
				}
				$gameInfo[0]['point_return']			=	($gameInfo[0]['point_return']+$data['point_return']);
				$gameInfo[0]['point']					=	($gameInfo[0]['point']+$data['point']);
				$gameInfo[0]['multiple']				=	1;
				//$gameInfo[0]['status']					=	($data['status']==2) ? true : false ;
			}else{
				// Multi play
				$countSet++;
					
				if(isset($data['number_in_set'])){
					$indexSet	=	$data['number_in_set'];
				}else{
					$indexSet	=	$countSet;
				}
					
				$sizeMulti	=	count($data['listGame']);
				for( $j=0 ; $j<$sizeMulti ; $j++ ){
						
					$dataPlayTmp	=	$data['listGame'][$j];
					
					// Match Data
					$dataMatch		=		$collectionMatch->findOne(	array(	'id' =>	$dataPlayTmp['match_id']));
					// Zone data
					$dataZone		= 		$collectionZone->findOne(	array( 	'id' => $dataMatch['KPLeagueCountryID'] ));
					// League data
					$dataLeague		= 		$collectionLeague->findOne(	array( 	'id' => $dataMatch['KPLeagueID'] ));
					// Team1 data
					$dataTeam1		= 		$collectionTeam->findOne(	array( 	'id' => $dataMatch['Team1KPID'] ));
					// Team2 data
					$dataTeam2		= 		$collectionTeam->findOne(	array( 	'id' => $dataMatch['Team2KPID'] ));
				
					$Team1Logo 								= 	str_replace(' ','-',$dataTeam1['NameEN']).'.png';
					$Team1Logo_MC							=	$memcache->get('Football2014-Team-Logo-' . $Team1Logo);
					if($Team1Logo_MC){
						$Team1LogoPath = 'http://football.kapook.com/uploads/logo/' . $Team1Logo;
					}else{
						$Team1LogoPath = 'http://football.kapook.com/uploads/logo/default.png';
					}
						
					$Team2Logo 								= 	str_replace(' ','-',$dataTeam2['NameEN']).'.png';
					$Team2Logo_MC							=	$memcache->get('Football2014-Team-Logo-' . $Team2Logo);
					if($Team2Logo_MC){
						$Team2LogoPath = 'http://football.kapook.com/uploads/logo/' . $Team2Logo;
					}else{
						$Team2LogoPath = 'http://football.kapook.com/uploads/logo/default.png';
					}
						
					$gameInfo[$indexSet]['list'][]	=	array(
						'match_id'			=>		$dataPlayTmp['match_id'],
						'MatchDateTime'		=>		$dataMatch['MatchDateTime'],
						'Zone'				=>		empty($dataZone['NameTH']) 				? 	$dataZone['NameEN'] 		: 	$dataZone['NameTH'],
						'League'			=>		empty($dataLeague['NameTH']) 			? 	$dataLeague['NameEN'] 		: 	$dataLeague['NameTH'],
						'Team1KPID'			=>		$dataTeam1['id'],
						'Team2KPID'			=>		$dataTeam2['id'],
						'Team1'				=>		empty($dataTeam1['NameTH'])				?	$dataTeam1['NameEN']		:	$dataTeam1['NameTH'],
						'Team2'				=>		empty($dataTeam2['NameTH'])				?	$dataTeam2['NameEN']		:	$dataTeam2['NameTH'],
						'Team1Logo'			=>		$Team1LogoPath,
						'Team2Logo'			=>		$Team2LogoPath,
						'Odds'				=>		($dataMatch['Odds']==-1)				?	null						:	$dataMatch['Odds'],
						'TeamOdds'			=>		($dataMatch['Odds']==-1)				?	null						:	$dataMatch['TeamOdds'],
						'Team1FTScore'		=>		empty($dataMatch['Team1FTScore'])		?	0							:	$dataMatch['Team1FTScore'],
						'Team2FTScore'		=>		empty($dataMatch['Team2FTScore'])		?	0							:	$dataMatch['Team2FTScore'],
						'team_win'			=>		empty($dataPlayTmp['team_win'])			?	0							:	$dataPlayTmp['team_win'],
						'MatchStatus'		=>		$dataMatch['MatchStatus'],
						'isCorrect'			=>		empty($dataPlayTmp['isWDL'])			?	0							:	$dataPlayTmp['isWDL'],
						'isCalculatePoint'	=>		($dataPlayTmp['status']==2)				?	true						:	false,
						'MatchPageURL' 		=> 		'http://football.kapook.com/match-'.$dataPlayTmp['match_id'].'-'.$dataTeam1['NameEN'].'-'.$dataTeam2['NameEN']
					);
				}
				$gameInfo[$indexSet]['point_return']	=	$data['point_return'];
				$gameInfo[$indexSet]['point']			=	$data['point'];
				$gameInfo[$indexSet]['multiple']		=	$data['multiple'];
				$gameInfo[$indexSet]['status']			=	($data['status']>=2) ? true : false ;
			}
				
			$totalAll		=	($totalAll+$data['point_return']);
			$dataMongo->next();
				
		}

		$returnJson	=	array(
			'uid'					=>	$uid,
			'total_game'			=>	$countMongo,
			'date_play'				=>	$FindArr['date_play'],
			'Total_Point'			=>	$totalAll,
			'game_info'				=>	$gameInfo
		);
		$memcache->set( 'Football2014-game-playside-list-info-' . $uid . '-' . $FindArr['date_play'] , $returnJson , MEMCACHE_COMPRESSED, $expire );
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>