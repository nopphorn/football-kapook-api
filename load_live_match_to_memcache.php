<?php

$memcache       = new Memcache;
$memcache->connect('localhost', 11211);

if($_REQUEST['mid']!=''){
    
        $midArr = explode(',',$_REQUEST['mid']);
        
        foreach($midArr as $mid){    
            if($mid>0){
                
                $MatchInfo = $memcache->get('Football2014-MatchInfo-'.$mid);
                if(($_REQUEST['VsStyle']==1) && ($MatchInfo)){                     
                    $Result[$mid] = $MatchInfo['MatchStatus'].':'.$MatchInfo['FTScore'].':'.$MatchInfo['Minute'].':'.$MatchInfo['KPLeagueID'];
                }else{
                    $Result[$mid] = $MatchInfo;                   
                }
            }
        }      
    
        
        if ($_REQUEST['callback'] != '') {
            echo $_REQUEST['callback'] . '(' . json_encode($Result) . ')';
        } else {
            echo json_encode($Result);
        }
    
}else{

		$mongo          = new MongoClient();
		$db             = $mongo->football;
		$match          = $db->football_match;


        if($_REQUEST['date']!=''){

            $MatchDate = date("Y-m-d 05:59:59",strtotime($_REQUEST['date'] ." +1 day"));
            $MatchDateBack3Hours = $_REQUEST['date'].' 06:00:00';

        }else{    

            $MatchDate = date('Y-m-d H:i:s');
            $MatchDateBack3Hours = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." - 360 minutes"));

        }
        if($_REQUEST['show']==1){
            echo 'CHECK BETWEEN => '.$MatchDateBack3Hours.' => '.$MatchDate;
            echo '<hr>';
        }

        $FindArr['MatchDateTime'] = array('$gte'=>$MatchDateBack3Hours,'$lt'=>$MatchDate);
        $Query  = $match->find($FindArr); 

        foreach($Query as $MatchInfo){

			$memcache->set('Football2014-MatchInfo-'.$MatchInfo['id'],$MatchInfo);  
                
			if($_REQUEST['show']==1){
				//var_dump($memcache->get('Football2014-MatchInfo-'.$MatchInfo['id']));
				echo $MatchInfo['id'].' : '.$MatchInfo['MatchStatus'].' : '.$MatchInfo['Minute'].':'.$MatchInfo['KPLeagueID'];
				echo '<br>';
			}
        }
        
}        
           
?>