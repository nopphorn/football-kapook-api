<?php
header('Content-Type: application/json');

	$mongo          = new MongoClient();
	$db             = $mongo->football;  
        
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211);

	$my_cup			=		$db->football_my_cup;
	$my_table		=		$db->football_my_table;
	$my_group		=		$db->football_my_group;
	
	if($_REQUEST['cup']){
		$dataCup			=		$my_cup->findOne( array('title_url' => $_REQUEST['cup']) );
	}

	if($dataCup){
		$Result				=		$dataCup;
		$dataTable			=		$my_table->find( array('cup_id' => $dataCup['id']) );
		foreach($dataTable as $tmpTable){
			$dataGroup				=		$my_group->find( array('TableID' => $tmpTable['id']) );
			foreach($dataGroup as $tmpGroup){
				$tmpTable['Group'][]	=	$tmpGroup;
			}
			$Result['Table'][]		=		$tmpTable;
		}
	}else{
		$Result				=		array();
	}
	
    if ($_REQUEST['callback'] != '') {
        echo $_REQUEST['callback'] . '(' . json_encode($Result) . ')';
    } else {
        echo json_encode($Result);
    }    
        
?>