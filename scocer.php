<?php

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionScorer		=	new MongoCollection($DatabaseMongoDB,"football_scorer");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	
	$memcache 	= new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	$dataLeague	=	$collectionLeague->findOne(array( 'id' => (int)$_GET['id'] ));
	if(empty($dataLeague)){
		echo 'notFound.';
		exit;
	}
	
	$scorer_MC = $memcache->get( 'Football2014-scorer-' . $_GET['id'] );
	if((!$scorer_MC)||($_REQUEST['clear']==1)){
		$PlayingZoneLeagueID	=	$dataLeague['KPZoneID'] . ':' . $_GET['id'];
		
		$dataScorer 			= 	$collectionScorer->find( array('PlayingZoneLeagueID' => $PlayingZoneLeagueID) );
		$dataScorer->sort(array( 'Goal' => -1 ));
		$countScorer				=	$dataScorer->count();
		$dataScorer->next();
		for( $i=0 ; $i<$countScorer ; $i++ )
		{
			$data 		= 	$dataScorer->current();
			$dataTeam	=	$collectionTeam->findOne(array( 'id' => (int)$data['TeamKPID'] ));
			
			$Logo 									= 	str_replace(' ','-',$dataTeam['NameEN']).'.png';
			$Logo_MC								=	$memcache->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC){
				$logoPath = 'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$logoPath = 'http://football.kapook.com/uploads/logo/default.png';
			}
			$datajson[]	=	array(
				'id'					=> 	$data['id'],
				'NameEN' 				=> 	$data['NameEN'],
				'NameTH' 				=> 	$data['NameTH'],
				'NameTHShort' 			=> 	$data['NameTHShort'],
				'TeamKPID' 				=> 	(int)$data['TeamKPID'],
				'Goal' 					=> 	(int)$data['Goal'],
				'PlayingZoneLeagueID' 	=>	$data['PlayingZoneLeagueID'],
				'Picture' 				=> 	empty($data['Picture']) ? 'http://football.kapook.com/uploads/scorer/noface.jpg' : 'http://football.kapook.com/uploads/scorer/' . $data['Picture'],
				'TeamLogo'				=>	$logoPath,
				'TeamName'				=>	empty($dataTeam['NameTH']) ? $dataTeam['NameEN'] : $dataTeam['NameTH'],
				'TeamNameShort'			=>	empty($dataTeam['NameTHShort']) ? $dataTeam['NameEN'] : $dataTeam['NameTHShort']
			);
			$dataScorer->next();
		}
		$memcache->set( 'Football2014-scorer-' . $_GET['id'] , $datajson , MEMCACHE_COMPRESSED, $expire );
	}else{
		$datajson	=	$scorer_MC;
	}
	echo json_encode($datajson);
?>