<?php

	/*
	 * Return List
	 * 200 	- success
	 * 401 	- Error 	: Authention
	 * 1 	- Error 	: Point under 1
	 * 2 	- Error 	: Point not enough
	 * 3	- Error		: Input is not Single,but you want single
	 * 4	- Error		: Input is Single or Two or None,but you want Multi.
	 * 5	- Error		: Number of set is wrong(For multi play)
	 * 6	- Error		: Number team select(teamwin) is wrong.(Must 1 or 2)
	 * 7	- Error		: Not Found a Match
	 * 8	- Error		: Match date is conflict.
	 * 9	- Error		: Quota of single-playside is full.
	 * 10	- Error		: some slot has been.
	 * 11	- Error		: Quota of Multi-playside is full.
	 * 12	- Error		: Input is over qouta for Multi.
	 */
	
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	$quota_playside_single			=	3;
	$quota_playside_single_vip		=	10;
	
	$quota_playside_multi			=	1;
	$quota_playside_multi_vip		=	5;
	$qouta_playside_multi_pre_set_min	=	2;
	$qouta_playside_multi_pre_set_max	=	6;
	
	header('Content-Type: application/json');
	
	function checkCookie()
    {
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }        
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMatch        =	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionMember		=	new MongoCollection($DatabaseMongoDB,"football_member");
	$collectionNewPlayer	=	new MongoCollection($DatabaseMongoDB,"football_log_new");
	
	if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
	{
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	if(!checkCookie())
	{
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	$uid	=	(int)$_COOKIE['uid'];
	
	if(($_REQUEST['point'])<=0)
	{
		$returnJson	=	array(
			'code_id'	=>	1,
			'message'	=>	'Point must over 0.'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	$datamember				=	$collectionMember->findOne(array( 'id' => $uid));
	if(!$datamember)
	{
		$returnJson	=	array(
			'code_id'	=>	2,
			'message'	=>	'Point has not enough.'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	if(($_REQUEST['isSingle']==1)&&(count($_REQUEST['team_win'])!=1)){
		$returnJson	=	array(
			'code_id'		=>	3,
			'message'		=>	'Input is not Single,but you want single.'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	if(($_REQUEST['isSingle']!=1)&&(count($_REQUEST['team_win'])<$qouta_playside_multi_pre_set_min)){
		$returnJson	=	array(
			'code_id'		=>	4,
			'message'		=>	'Input less minimum quota.'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	if(($_REQUEST['isSingle']!=1)&&(count($_REQUEST['team_win'])>$qouta_playside_multi_pre_set_max)){
		$returnJson	=	array(
			'code_id'		=>	12,
			'message'		=>	'Input is over qouta for Multi.'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	if(($_REQUEST['isSingle']!=1)&&($_REQUEST['number_in_set']<1)&&($_REQUEST['number_in_set']>5)){
		$returnJson	=	array(
			'code_id'		=>	5,
			'message'		=>	'Number in set is wrong'
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}
	
	// Find Highest of game id 
	$cursorGame					=	$collectionGame->find();
	$cursorGame->sort( array( 'id' => -1 ) );
	$cursorGame->limit(1);
	$cursorGame->next();		
	$dataGame					=	$cursorGame->current();			
	$dataupdate['id']			=	((int)$dataGame['id']+1);
	
	/*$ops = array(
		array(
			'$match' => array(
				'user_id' 		=> 	$uid,
				'status'		=>	array( '$gte' => 1 , '$lte' => 2 ),
				'id'			=>	array( '$ne' => $dataupdate['id'] ),
				'game_type'		=>	2
			)
		),
		array(
			'$group' => array(
				'_id' => null,
				'point' => array('$sum' => '$point'),
			),
		),
	);
		
	$dataUsedPoint	=	$collectionGame->aggregate($ops);
	if(isset($dataUsedPoint['result'][0]['point'])){
		$point_used 	= 	$dataUsedPoint['result'][0]['point'];
	}else{
		$point_used		=	0;
	}*/
	
	$point_used			=	0;
	
	$cursorGame 		=	$collectionGame->find(
		array(
			'user_id' 		=> 	$uid,
			'status'		=>	array( '$gte' => 1 , '$lte' => 2 ),
			'id'			=>	array( '$ne' => $dataupdate['id'] ),
			'game_type'		=>	2
		)
	);
	
	foreach($cursorGame as $tmpGames ){
		$point_used		=	$point_used		+	$tmpGames['point'];
	}
		
	$point_remain		=	$datamember['point']-$point_used;
	
	if($point_remain<(double)$_REQUEST['point']){
		$returnJson	=	array(
			'code_id'		=>	2,
			'message'		=>	'Point has not enough:' . $point_remain,
			'point_remain'	=>	$point_remain,
		);
		if ($_REQUEST['callback'] != '') {
			echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
		} else {
			echo json_encode($returnJson);
		}
		return;
	}

	$playside_stat		=		array();
	$dateCheck	=	null;
	foreach( $_REQUEST['team_win'] as $match_id => $value ){
		if(($value!='1')&&($value!='2'))
		{
			$returnJson	=	array(
				'code_id'	=>	6,
				'message'	=>	'number team select is wrong.'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
		$playside_stat[(int)$match_id]		=		(int)$value;
		$dataMatch			=	$collectionMatch->findOne(
			array( 
				'id' 			=> (int)$match_id,
				'Status'		=>	1 ,
				'MatchStatus'	=>  'Sched'
			)
		);
		if(!$dataMatch)
		{
			$returnJson	=	array(
				'code_id'		=>	7,
				'message'		=>	'Not Found a Match(Match No.'. $match_id .').'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
		
		/*Check Date in every Match*/
		if(date('G',strtotime($dataMatch['MatchDateTime']))>=6){
			$dataupdate['date_play']	=	date("Y-m-d",strtotime($dataMatch['MatchDateTime']));
		}
		else{
			$dataupdate['date_play']	=	date("Y-m-d",strtotime($dataMatch['MatchDateTime']  . ' - 1 day'));
		}
		if(!$dateCheck){
			$dateCheck					=	$dataupdate['date_play'];
		}else if($dataupdate['date_play']!=$dateCheck){
			$returnJson	=	array(
				'code_id'		=>	8,
				'message'		=>	'Match date is conflict.'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
				
		$dataupdate['listGame'][]	=	array(
			'match_id'				=>	(int)$match_id,
			'team_win'				=>	(int)$value,
			'status'				=>	1,
			'team_win_nameTHShort'	=>	$memcache->get('Football2014-Team-NameTHShort-'.$dataMatch['Team'. $value .'KPID'])
		);
		
		$datashow[]	=	array(
			'match_id'				=>	(int)$match_id,
			'team_win'				=>	(int)$value,
			'team_win_nameTHShort'	=>	$memcache->get('Football2014-Team-NameTHShort-'.$dataMatch['Team'. $value .'KPID'])
		);
	}

	$dataupdate['user_id']			=	$uid;
	$dataupdate['status'] 			= 	1;
	$dataupdate['point']			= 	(double)$_REQUEST['point'];
	$dataupdate['point_return']		= 	0.0;
	$dataupdate['multiple']			= 	1.0;
	$dataupdate['game_type'] 		= 	2;
	$dataupdate['timestamp']		=	date('Y-m-d H:i:s');
	
	if(($_REQUEST['isSingle']==1)&&(count($_REQUEST['team_win'])==1)){
		$dataupdate['match_id'] 	= 	(int)$match_id;
		
		/*
		$ops = array(
			array(
				'$match' => array(
					'user_id' 		=> 	$uid,
					'status'		=>	1,
					'match_id'		=>	array( '$gt' => 0 ),
					'game_type'		=>	2,
					'date_play'		=>	$dataupdate['date_play']
				)
			),
			array(
				'$group' => array(
					'_id' => null,
					'count' => array('$sum' => 1),
				),
			),
		);
		
		
		$dataCountGame	=	$collectionGame->aggregate($ops);
		if(isset($dataCountGame['result'][0]['count'])){
			$count_game 	= 	$dataCountGame['result'][0]['count'];
		}else{
			$count_game		=	0;
		}*/
		
		$cursorGame			=		$collectionGame->find(
			array(
				'user_id' 		=> 	$uid,
				'status'		=>	1,
				'match_id'		=>	array( '$gt' => 0 ),
				'game_type'		=>	2,
				'date_play'		=>	$dataupdate['date_play']
			)
		)->limit(1);
		
		$count_game				=	$cursorGame->count();
		
		if( $count_game >= $quota_playside_single ){
			$returnJson	=	array(
				'code_id'		=>	9,
				'message'		=>	'Quota of single-playside is full.'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
		
	}else{
		$checkExist		=		$collectionGame->findOne(
			array( 
				'user_id' 			=> 	$uid,
				'number_in_set'		=>	(int)$_REQUEST['number_in_set'],
				'date_play'			=>  $dataupdate['date_play']
			)
		);
		if($checkExist){
			$returnJson	=	array(
				'code_id'		=>	10,
				'message'		=>	'This slot is used.'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
		
		$dataupdate['number_in_set']		=	(int)$_REQUEST['number_in_set'];
	
		/*
		$ops = array(
			array(
				'$match' => array(
					'user_id' 		=> 	$uid,
					'status'		=>	1,
					'match_id'		=>	array( '$lte' => 0 ),
					'game_type'		=>	2,
					'date_play'		=>	$dateCheck
				)
			),
			array(
				'$group' => array(
					'_id' => null,
					'count' => array('$sum' => 1),
				),
			),
		);
		
		$dataCountGame	=	$collectionGame->aggregate($ops);
		if(isset($dataCountGame['result'][0]['count'])){
			$count_game 	= 	$dataCountGame['result'][0]['count'];
		}else{
			$count_game		=	0;
		}*/
		
		$cursorGame			=		$collectionGame->find(
			array(
				'user_id' 		=> 	$uid,
				'status'		=>	1,
				'match_id'		=>	array( '$lte' => 0 ),
				'game_type'		=>	2,
				'date_play'		=>	$dateCheck
			)
		)->limit(1);
		
		$count_game				=	$cursorGame->count();
		
		if($count_game>=$quota_playside_multi){
			$returnJson	=	array(
				'code_id'		=>	11,
				'message'		=>	'Quota of Multi-playside is full.'
			);
			if ($_REQUEST['callback'] != '') {
				echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
			} else {
				echo json_encode($returnJson);
			}
			return;
		}
		$dataupdate['match_id'] 	= 	-1;
	}
	
	if(!$collectionGame->findOne(array('user_id' => $uid))){
		$collectionNewPlayer->insert(
			array(
				'user_id'		=>	$uid,
				'time_stamp' 	=> 	date('Y-m-d H:i:s')
			)
		);
	}
	
	$collectionGame->update(
		array(
			'id' 	=> (int)$dataupdate['id']
		),
		array('$set' => $dataupdate),
		array("upsert" => true)
	);
	
	foreach($playside_stat as $mid => $dataside){
		$collectionMatch->update(
			array(
				'id' 	=> (int)$mid
			),
			array('$inc' => array(
				'count_playside_team' . $dataside 	=>	1,
				'count_playside'					=>	1
			) ),
			array("upsert" => true)
		);
		
		$oldList = $memcache->get( 'Football2014-ListGameID-' . $mid );
		if($oldList){
			$oldList = $oldList . "," . $dataupdate['id'];
		}else{
			$oldList = $dataupdate['id'];
		}
		$memcache->set( 'Football2014-ListGameID-' . $mid , $oldList , false, 86400 );
		
	}

	$returnJson	=	array( 
		'code_id'		=>	200,
		'message'		=>	'Insert data successful.',
		'id'			=>	$dataupdate['id'],
		'uid'			=>	(int)$_COOKIE['uid'],
		'point_remain'	=>	$point_remain+$thisGameOldPoint-(double)$_REQUEST['point'],
		'point'			=>	(double)$_REQUEST['point'],
		'isSingle'		=>	($_REQUEST['isSingle']==1) ? true : false,
		'data'			=>	$datashow
	);

	$memcache->set( 'Football2014-game-point-remain-' . $uid , $point_remain+$thisGameOldPoint-(double)$_REQUEST['point'] , MEMCACHE_COMPRESSED, $expire );
	
	if($dataupdate['match_id']>0){
		$memcache->delete( 'Football2014-game-playside-info-' . $uid . '-' . $dataupdate['match_id'] );
	}else{
		$memcache->delete( 'Football2014-game-playside-multi-info-' . $dataupdate['id'] );
	}
	$memcache->delete( 'Football2014-game-playside-list-info-' . $uid . '-' . $dataupdate['date_play'] );
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>