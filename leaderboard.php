<?php

	function compare_point($a,$b)
	{
		if ($a['point'] == $b['point'])
		{
			if($a['custom_order'] == $b['custom_order'])
			{
				if ($a['GD'] == $b['GD'])
				{
					if ($a['GF'] == $b['GF'])
					{
						if ($a['NameEN'] == $b['NameEN'])
							return 0;
						return strcmp($a['NameEN'],$b['NameEN']);
					}
					return ($a['GF'] > $b['GF']) ? -1 : 1;
				}
				return ($a['GD'] > $b['GD']) ? -1 : 1;
			}
			return ($a['custom_order'] < $b['custom_order']) ? -1 : 1;
		}
		return ($a['point'] > $b['point']) ? -1 : 1;
	}
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");

	$leaderboard_MC = $memcache->get( 'Football2014-leaderboard-'.$_GET['id'] );

	if(($leaderboard_MC)&&($_GET['clear']!='1'))
	{
		$leaderboard 	= 	$leaderboard_MC;
	}
	else
	{
		// init MongoDB
		$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
		$DatabaseMongoDB		=	$connectMongo->selectDB("football");
		$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
		$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
		$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
		$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
		
		// Available League List 
		$listCanGetLeague	=	array(
			170, 	// Ligue 1
			227,	// EPL
			248,	// Bundesliga
			255,	// La Liga
			284,	// Serie A
			845, 	// Thai League 2015
		);
		
		if(!in_array((int)$_GET['id'], $listCanGetLeague))
		{
			//echo 'notFound.';
			exit;
		}

		$leaderboard		=		array();
		
		$dataMatch 			= 	$collection->find(
			array( 
				'KPLeagueID' 	=> (int)$_GET['id'],
				'Status' 		=> 1,
			)
		);
		
		// memcache
		$expire				=		3600;
		$memcache->set( 'Football2014-dataMatch-'.$_GET['id'] , $dataMatch , MEMCACHE_COMPRESSED, $expire );
		
		$countMongo			=	$dataMatch->count();
		$dataMatch->next();
		
		for( $i=0 ; $i<$countMongo ; $i++ )
		{
			$data 	= 	$dataMatch->current();
			
			if(empty($leaderboard[$data['Team1KPID']]))
			{
				
				$leaderboard[$data['Team1KPID']]['NameTH']	=	$data['Team1KPID'];
			
				$dataTeam 			= 		$collectionTeam->findOne( array('id' => (int)$data['Team1KPID'] ) );
				
				$leaderboard[$data['Team1KPID']]['NameTH']			=	$dataTeam['NameTH'];
				$leaderboard[$data['Team1KPID']]['NameTHShort']		=	$dataTeam['NameTHShort'];	
				$leaderboard[$data['Team1KPID']]['NameEN']			=	$dataTeam['NameEN'];
				
				$Logo 									= 	str_replace(' ','-',$dataTeam['NameEN']).'.png';
				$Logo_MC								=	$memcache->get('Football2014-Team-Logo-' . $Logo);
				if($Logo_MC==true){
					$leaderboard[$data['Team1KPID']]['Logo'] = 'http://football.kapook.com/uploads/logo/' . $Logo;
				}else{
					$leaderboard[$data['Team1KPID']]['Logo'] = 'http://football.kapook.com/uploads/logo/default.png';
				}
			
				$leaderboard[$data['Team1KPID']]['win']				=	0;
				$leaderboard[$data['Team1KPID']]['lose']			=	0;
				$leaderboard[$data['Team1KPID']]['draw']			=	0;
			
				$leaderboard[$data['Team1KPID']]['GF']				=	0;
				$leaderboard[$data['Team1KPID']]['GA']				=	0;
				$leaderboard[$data['Team1KPID']]['GD']				=	0;
			
				$leaderboard[$data['Team1KPID']]['point']			=	0;
				$leaderboard[$data['Team1KPID']]['total_match']		=	0;
			}
			$leaderboard[$data['Team1KPID']]['id'] = (int)$data['Team1KPID'];
			
			if(empty($leaderboard[$data['Team2KPID']]))
			{
			
				$leaderboard[$data['Team2KPID']]['NameTH']	=	$data['Team2KPID'];
			
				$dataTeam 			= 		$collectionTeam->findOne( array('id' => (int)$data['Team2KPID'] ) );
				
				$leaderboard[$data['Team2KPID']]['NameTH']			=	$dataTeam['NameTH'];
				$leaderboard[$data['Team2KPID']]['NameTHShort']		=	$dataTeam['NameTHShort'];	
				$leaderboard[$data['Team2KPID']]['NameEN']			=	$dataTeam['NameEN'];
				
				$Logo 									= 	str_replace(' ','-',$dataTeam['NameEN']).'.png';
				$Logo_MC								=	$memcache->get('Football2014-Team-Logo-' . $Logo);
				if($Logo_MC==true){
					$leaderboard[$data['Team2KPID']]['Logo'] = 'http://football.kapook.com/uploads/logo/' . $Logo;
				}else{
					$leaderboard[$data['Team2KPID']]['Logo'] = 'http://football.kapook.com/uploads/logo/default.png';
				}
			
				$leaderboard[$data['Team2KPID']]['win']				=	0;
				$leaderboard[$data['Team2KPID']]['lose']			=	0;
				$leaderboard[$data['Team2KPID']]['draw']			=	0;
			
				$leaderboard[$data['Team2KPID']]['GF']				=	0;
				$leaderboard[$data['Team2KPID']]['GA']				=	0;
				$leaderboard[$data['Team2KPID']]['GD']				=	0;
			
				$leaderboard[$data['Team2KPID']]['point']			=	0;
				$leaderboard[$data['Team2KPID']]['total_match']		=	0;
			}
			$leaderboard[$data['Team2KPID']]['id'] = (int)$data['Team2KPID'];
			
			if($data['MatchStatus']=='Fin')
			{
				// T1 WIN
				if($data['Team1FTScore']>$data['Team2FTScore'])
				{
					$leaderboard[$data['Team1KPID']]['win']++;
					$leaderboard[$data['Team1KPID']]['point'] = $leaderboard[$data['Team1KPID']]['point'] + 3;
					
					$leaderboard[$data['Team2KPID']]['lose']++;
				}
				// T2 WIN
				else if($data['Team2FTScore']>$data['Team1FTScore'])
				{
					$leaderboard[$data['Team2KPID']]['win']++;
					$leaderboard[$data['Team2KPID']]['point'] = $leaderboard[$data['Team2KPID']]['point'] + 3;
						
					$leaderboard[$data['Team1KPID']]['lose']++;
				}
				// DRAW
				else
				{
					$leaderboard[$data['Team2KPID']]['draw']++;
					$leaderboard[$data['Team2KPID']]['point'] = $leaderboard[$data['Team2KPID']]['point'] + 1;
						
					$leaderboard[$data['Team1KPID']]['draw']++;
					$leaderboard[$data['Team1KPID']]['point'] = $leaderboard[$data['Team1KPID']]['point'] + 1;
				}
				
				// T1 GOAL
				$leaderboard[$data['Team1KPID']]['GF'] = $leaderboard[$data['Team1KPID']]['GF']+$data['Team1FTScore'];
				$leaderboard[$data['Team1KPID']]['GA'] = $leaderboard[$data['Team1KPID']]['GA']+$data['Team2FTScore'];
				
				// T2 GOAL
				$leaderboard[$data['Team2KPID']]['GF'] = $leaderboard[$data['Team2KPID']]['GF']+$data['Team2FTScore'];
				$leaderboard[$data['Team2KPID']]['GA'] = $leaderboard[$data['Team2KPID']]['GA']+$data['Team1FTScore'];
			
			}
			$dataMatch->next();
		}
		
		foreach($leaderboard as $i => $value )
		{
			$leaderboard[$i]['GD']	=	$leaderboard[$i]['GF']	-	$leaderboard[$i]['GA'];
			$leaderboard[$i]['total_match']	=	$leaderboard[$i]['win']+$leaderboard[$i]['lose']+$leaderboard[$i]['draw'];
			
			//TPL 2014
			if( (int)$_GET['id'] == 9 ){
				
				// Cut a point rule
				if(($i == 77 /*MUANGTHONG-UNITED*/ )||($idTeam == 55 /*SINGHTARUA*/ )){
					$leaderboard[$i]['point']			=	$leaderboard[$i]['point']-9;
				}
				
				//Head To Head [ Custom Order]
				if($i == 80 /*TOT-SC*/){
					$leaderboard[$i]['custom_order']			=	1;
				}else if($i == 58 /*POLICE-UNITED*/){
					$leaderboard[$i]['custom_order']			=	2;
				}else{
					$leaderboard[$i]['custom_order']			=	0;
				}
			
			}
			
			elseif( (int)$_GET['id'] == 845 ){
				
				//Head To Head [ Custom Order]
				
				// 4th
				if($i == 69 /*CHONBURI FC*/){
					$leaderboard[$i]['custom_order']			=	1;
				}
				// 5th
				else if($i == 79 /*BANGKOK UNITED*/){
					$leaderboard[$i]['custom_order']			=	2;
				}
				
				// 14th
				else if($i == 4725 /*SARABURI FC*/){
					$leaderboard[$i]['custom_order']			=	1;
				}
				// 15th
				else if($i == 5362 /*NAVY FC*/){
					$leaderboard[$i]['custom_order']			=	2;
				}
				// 16th
				else if($i == 78 /*BEC TERO SASANA*/){
					$leaderboard[$i]['custom_order']			=	3;
				}
				
				//other
				else{
					$leaderboard[$i]['custom_order']			=	0;
				}
			
			}
			
		}
		
		usort($leaderboard,"compare_point");
		
		//$leaderboard['match'] = $countMongo;
		
		// memcache
		$expire				=		3600;
		$memcache->set( 'Football2014-leaderboard-'.$_GET['id'] , $leaderboard , MEMCACHE_COMPRESSED, $expire );
	
	}
	
	echo json_encode($leaderboard);
	
?>