<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<?php
	include 'manage_checklogin.php';
	include 'manage_index.php';
	
	$maxsizeshow = 20;
	
	if(empty($_POST['filter'])){
		$id_league = -1;
	}else{
		$arrLeague		=	explode($_POST['filter'],':');
		if(!isset($arrLeague[1])){
			$id_league = -1;
		}else{
			$id_league = (int)$arrLeague[1];
		}
	}
	
	function uploadPicture($filepic, $id)
	{
		if	(($filepic['error'] != 4) && ($filepic['error'] != 0))
		{	
			return array(
				'error' => 	1,
				'text'	=>	'Error:Cannot Upload File'
			);
		}
		else if($filepic['error'] == 0)
		{
			// Check Type file
			if 	(	($filepic['type'] 	!= 'image/gif')
				&& 	($filepic['type'] 	!= 'image/jpeg')
				&& 	($filepic['type'] 	!= 'image/jpg')
				&& 	($filepic['type']	!= 'image/pjpeg')
				&& 	($filepic['type'] 	!= 'image/x-png')
				&& 	($filepic['type'] 	!= 'image/png')
			)
			{
				return array(
					'error' => 	2,
					'text'	=>	'Error:Wrong type file.'
				);
			}
			else
			{		
				// gif
				if($filepic['type'] == 'image/gif')
				{
					$extension	=	'gif';
				}
						
				// jpeg
				else if(	($filepic['type'] == 'image/jpeg')
						||	($filepic['type'] == 'image/jpg')
						||	($filepic['type'] == 'image/pjpeg')
				)
				{
					$extension	=	'jpg';
				}
						
				// png
				else if(	($filepic['type'] == 'image/x-png')
						||	($filepic['type'] == 'image/png'))
				{
					$extension	=	'png';
				}
				
				// Check size of picture,Must be "square"
				$infoPic = getimagesize($filepic['tmp_name']);
				if($infoPic[0]!=$infoPic[1])
				{
					return array(
						'error' => 	3,
						'text'	=>	'Error:Picture is not square.'
					);
				}
				
				// Check directory for upload
				if(!is_dir('../uploads/scorer/'))
				{
					if(!mkdir('../uploads/scorer/',0777,true))
					{
						return array(
							'error' => 	4,
							'text'	=>	'Error:Cannot Create a directory.'
						);
					}
				}
				
				if(!$error)
				{
					$filename = $id . '.' . $extension;
					if(!move_uploaded_file($filepic['tmp_name'], '../uploads/scorer/' . $filename ))
					{
						return array(
							'error' => 	1,
							'text'	=>	'Error:Cannot Upload File'
						);
					}
					return array(
						'error' => 	0,
						'filename'	=>	$filename
					);
				}
			}
		}
		else
		{
			return array(
				'error' => 	0,
				'filename'	=>	null
			);
		}
	}
	
	$minsize = 250;
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionScorer		=	new MongoCollection($DatabaseMongoDB,"football_scorer");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	
	if(		(isset($_POST['playerid']))
		&& 	(isset($_POST['teamid'])) 
		&& 	(isset($_POST['nameen'])) 
		&& 	(isset($_POST['nameth'])) 
		&& 	(isset($_POST['namethshort']))
		&& 	(isset($_POST['goal']))
	)
	{
		$dataMongo 		= 	$collectionScorer->findOne(array( 'id' => (int)$_POST['playerid'] ));
		if(!empty($dataMongo))
		{
			echo (int)$_POST['teamid'];
			if(!empty($dataMongo['Picture']))
				$oldfile	=	$dataMongo['Picture'];
			else
				$oldfile	=	null;
			$collectionScorer->update(
				array('id' => (int)$_POST['playerid']),
				array('$set' => array(
					'NameEN' 		=> 	$_POST['nameen'],
					'NameTH' 		=> 	$_POST['nameth'],
					'NameTHShort'	=>	$_POST['namethshort'],
					'TeamKPID' 		=> 	(int)$_POST['teamid'],
					'Goal' 			=> 	(int)$_POST['goal'],
					'Picture'		=>	empty($_POST['filename']) ? $oldfile : $_POST['filename']
				))
			);
			if($id_league>-1){
				$scorer_MC = $memcache->delete( 'Football2014-scorer-' . $id_league );
			}
		}
	}

	else if(!empty($_POST['SNameTH']))
	{
		foreach($_POST['SNameTH'] as $k => $value)
		{
			$data = array(
				'error'		=>	$_FILES['picture']['error'][$k],
				'type'		=>	empty($_FILES['picture']['type'][$k]) ? null : $_FILES['picture']['type'][$k],
				'tmp_name'	=>	empty($_FILES['picture']['tmp_name'][$k]) ? null : $_FILES['picture']['tmp_name'][$k]
			);
			
			$checking = uploadPicture($data,$k);
			
			if($checking['error']!=0)
			{
				echo 'error : @id' . $k . ' : ' . $checking['text'] . '<br>';
			}
			else
			{
				$dataMongo 		= 	$collectionScorer->findOne(array( 'id' => (int)$k ));
				if(!empty($dataMongo))
				{
					if(!empty($dataMongo['Picture']))
						$oldfile	=	$dataMongo['Picture'];
					else
						$oldfile	=	null;
					$collectionScorer->update(
						array('id' => (int)$k),
						array('$set' => array(
							'NameEN' 		=> 	$_POST['SNameEN'][$k],
							'NameTH' 		=> 	$value,
							'NameTHShort'	=>	$_POST['SNameTHShort'][$k],
							'TeamKPID' 		=> 	(int)$_POST['STeamID'][$k],
							'Goal' 			=> 	(int)$_POST['SGoal'][$k],
							'Picture'		=>	empty($checking['filename']) ? $oldfile : $checking['filename']
						))
					);
				}
			}
		}
		if($id_league>-1){
			$scorer_MC = $memcache->delete( 'Football2014-scorer-' . $id_league );
		}
	}
	
	else if(!empty($_POST['new_scocer_nameth']))
	{
		$error = false;
		$collectionTmpData	=	new MongoCollection($DatabaseMongoDB,"tmpKey");
		$dataTmp 			= 	$collectionTmpData->findOne( array( 'id' => 2 ) );
		
		//echo var_dump($_FILES);
		$checking = uploadPicture($_FILES['new_scocer_picture'],((int)$dataTmp['value']+1));
		if($checking['error']!=0)
			echo $checking['text'] . '<br>';
		else
		{
			$data = array(
				'id'					=>	((int)$dataTmp['value']+1),
				'NameEN' 				=> 	$_POST['new_scocer_nameen'],
				'NameTH' 				=> 	$_POST['new_scocer_nameth'],
				'NameTHShort'			=>	$_POST['new_scocer_namethshort'],
				'TeamKPID' 				=> 	(int)$_POST['new_scocer_team'],
				'Goal' 					=> 	(int)$_POST['new_scocer_ball'],
				'PlayingZoneLeagueID'	=>	$_POST['filter'],
				'Picture'				=>	empty($checking['filename']) ? null : $checking['filename']
			);
		
			$checking = $collectionScorer->insert( $data );
				
			if(!empty($checking['ok']))
			{
				echo $_POST['new_scocer_nameen'] .' has insert.<br>';
			
				$collectionTmpData->update( 
					array('id' => 2),
					array('$set' => array( 'value' => ((int)$dataTmp['value']+1) ) )
				);
			}
			
			if($id_league>-1){
				$scorer_MC = $memcache->delete( 'Football2014-scorer-' . $id_league );
			}
		}
	}
	
	else if(!empty($_POST['playerid_delete']))
	{
		$dataTmp 			= 	$collectionScorer->findOne( array( 'id' => (int)$_POST['playerid_delete'] ) );
		if(!empty($dataTmp))
		{
			echo $dataTmp['Picture'];
			if(!empty($dataTmp['Picture']))
				unlink('../uploads/scorer/' . $dataTmp['Picture']);
				
			$checking = $collectionScorer->remove( array('id' => (int)$_POST['playerid_delete']), array('justOne' => true));
			if(!empty($checking['ok']))
				echo $dataTmp['NameEN'] .' has deleted.<br>';
			if($id_league>-1){
				$scorer_MC = $memcache->delete( 'Football2014-scorer-' . $id_league );
			}
		}
	}
	
	?>
	
	<script type='text/javascript'>
		function myFunction(id){
		
			nameEN=document.getElementById('SNameEN' + id ).value;
			nameTH=document.getElementById('SNameTH' + id ).value;
			nameTHShort=document.getElementById('SNameTHShort' + id ).value;
			teamID=document.getElementById('STeamID' + id ).value;
			goal=document.getElementById('SGoal' + id ).value;
			
			document.getElementById("playerid").value		=	id;
			document.getElementById("teamid").value			=	teamID;
			document.getElementById("nameen").value			=	nameEN;
			document.getElementById("nameth").value			=	nameTH;
			document.getElementById("namethshort").value	=	nameTHShort;
			document.getElementById("goal").value			=	goal;
			
			// Image Upload
			photo = document.getElementById("picture" + id);
			var file = photo.files[0];
			console.log(file);
			if(file === undefined)
			{
				document.getElementById("filename").value	=	null;
				document.getElementById("formsoccer").submit();
			}
			else
			{
				
				// Create a new FormData object.
				formData = new FormData();
				formData.append('picture', file);
				formData.append('filename', id);
				
				
				$.ajax({
					url: 'upload_file.php',
					type: 'POST',
					data: formData,
					cache: false,
					dataType: 'json',
					processData: false,
					contentType: false,
					success: function(data, textStatus, jqXHR)
					{
						if( data.error == 0 )
						{
							//console.log(data);
							document.getElementById("filename").value	=	data.filename;
							document.getElementById("formsoccer").submit();
						}
						else
						{
							alert(data.text);
						}
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
						alert(textStatus);
					}
				});
			}
			
			//document.getElementById("formsoccer").submit();
			
		}
		
		function myFunctionDelete(id) {
			if (confirm("ยืนยันจะลบข้อมูลนี้หรือไม่?") == true) {
				document.getElementById("playerid_delete").value		=	id;
				document.getElementById("formsoccerdelete").submit();
			}
		}
</script>
	</script>
	
	<?php
		if(empty($_POST['filter']))
			$_POST['filter'] = null;
	?>
	
	<form id="formsoccerdelete" action="#" method="POST">
		<input type="hidden" name="playerid_delete" id="playerid_delete" value="">
		<input type="hidden" name="filter" id="filter" value="<?php echo empty($_POST['filter']) ? '' : $_POST['filter'] ?>">
	</form>
	
	<form id="formsoccer" action="#" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="playerid" id="playerid" value="">
		<input type="hidden" name="teamid" id="teamid" value="">
		<input type="hidden" name="nameen" id="nameen" value="">
		<input type="hidden" name="nameth" id="nameth" value="">
		<input type="hidden" name="namethshort" id="namethshort" value="">
		<input type="hidden" name="goal" id="goal" value="">
		<input type="hidden" name="filename" id="filename" value="">
		<input type="hidden" name="filter" id="filter" value="<?php echo empty($_POST['filter']) ? '' : $_POST['filter'] ?>">
		<input type="hidden" name="filter_page" id="filter_page" value="<?php echo empty($_POST['filter_page']) ? '1' : $_POST['filter_page'] ?>">
	</form>
	
	<b>เลือกลีกที่ต้องการแสดงผล</b>
	<form action="manage_scocer.php" method="POST">
		<select name="filter" id="filter">
			<option value="">ทั้งหมด</option>
			<?php
				$dataZone = $collectionZone->find();
				$dataZone->sort(array( 'NameEN' => 1 ));
				$countZone		=	$dataZone->count();
				$dataZone->next();
				for( $i=0 ; $i<$countZone ; $i++ )
				{
					$tmpZone 	= 	$dataZone->current();
					$dataLeague = 	$collectionLeague->find( array( 'KPZoneID' => $tmpZone['id'] ) );
					$dataLeague->sort(array( 'NameEN' => 1 ));
					$countLeague		=	$dataLeague->count();
					$dataLeague->next();
					for( $j=0 ; $j<$countLeague ; $j++ )
					{
						$tmpLeague 	= 	$dataLeague->current();
						$zonename 	= 	empty($tmpZone['NameTH']) 	? $tmpZone['NameEN'] 	: $tmpZone['NameTH'];
						$leaguename = 	empty($tmpLeague['NameTH']) ? $tmpLeague['NameEN'] 	: $tmpLeague['NameTH'];
						$strValue 	= 	$tmpZone['id'] . ':' . $tmpLeague['id'];
						
						echo '<option value="' . $strValue . '" ';
						if($_POST['filter'] == $strValue )
							echo 'selected';
						echo '>' . $zonename . '->' . $leaguename . '</option>';
						
						$dataLeague->next();
					}
					$dataZone->next();
				}
			?>
		</select>
		<input type="submit" value="เลือกลีก">
	</form><br>
	
	<hr>
	
	<b>เลือกลีกด่วน</b>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="12:796">
		<input type="submit" value="King Cup 2015">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="76:227">
		<input type="submit" value="พรีเมียร์ลีก อังกฤษ">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="63:170">
		<input type="submit" value="ลีกเอิง ฝรั่งเศส">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="33:248">
		<input type="submit" value="บุนเดสลีกา เยอรมนี">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="90:284">
		<input type="submit" value="เซเรีย อา อิตาลี">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="92:255">
		<input type="submit" value="ลา ลีกา สเปน">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="7:845">
		<input type="submit" value="ไทยพรีเมียร์ลีก">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="9:325">
		<input type="submit" value="UEFA CHAMPIONS LEAGUE">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="9:337">
		<input type="submit" value="UEFA EUROPA LEAGUE">
	</form>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" value="9:310">
		<input type="submit" value="EURO 2016(Qualify)">
	</form>
	
	<hr>
	
	<?php
	if(empty($_POST['filter']))
	{
		echo 'กรุณาเลือกลีก';
		exit;
	}
	?>
	
	<b>เพิ่มผู้เล่นคนใหม่</b>
	<form action="manage_scocer.php" method="POST" enctype="multipart/form-data">
		ชื่อผู้เล่นไทย(ยาว)<input type="input" name="new_scocer_nameth">
		ชื่อผู้เล่นไทย(สั้น)<input type="input" name="new_scocer_namethshort">
		ชื่อผู้เล่นอังกฤษ<input type="input" name="new_scocer_nameen"><br>
		เลือกทีม<select name="new_scocer_team">
			<option value="-1" style="background-color: #FFCC8F">ไร้สังกัด</option>
			<option value="-2" style="background-color: #FFCC8F">ไม่สังกัดทีมใดในลีก</option>
			<?php
			/*$dataTeam 			= 	$collectionTeam->find( array('PlayingZoneLeagueID' => $_POST['filter']) );
			$dataTeam->sort(array( 'NameTH' => 1 ));
			$countTeam		=	$dataTeam->count();
			$dataTeam->next();
			for( $i=0 ; $i<$countTeam ; $i++ )
			{
				$tmpTeam 	= 	$dataTeam->current();
				echo '<option value="' . $tmpTeam['id'] . '" >' . $tmpTeam['NameTH'] . '</option>';
				$dataTeam->next();
			}*/
			$APIListTeam				= 		'http://202.183.165.189/api/get_listteam.php?clear=1';
				
			if(isset($_POST['filter'])){
				$pieces = explode(":", $_POST['filter']);
				if(isset($pieces[1])){
					if((int)$pieces[1]>0){
						$APIListTeam 		= 		$APIListTeam . '&id=' . (int)$pieces[1];
					}
				}
			}
				
			$ListTeamArr 				=		json_decode(file_get_contents($APIListTeam), true);
			$countTeam					=		$ListTeamArr['num_team'];
				
			for( $i=0 ; $i<$countTeam ; $i++ ){
				$tmpTeam 	= 	$ListTeamArr['list'][$i];
				echo '<option value="' . $tmpTeam['id'] . '" >' . $tmpTeam['NameTH'] . '</option>';
			}
			?>
		</select>
		จำนวนประตู <input type="input" name="new_scocer_ball">
		อัปโหลดรูปนัดเตะ <input type="file" name="new_scocer_picture"><br>
		<input type="hidden" name="filter" id="filter" value="<?php echo $_POST['filter']; ?>">
		<input type="submit" value="เพิ่มผู้เล่น">
	</form>
	<hr>
	<?php
	$dataScorer 			= 	$collectionScorer->find( array('PlayingZoneLeagueID' => $_POST['filter']) );
	$dataScorer->sort(array( 'Goal' => -1 ));

	if(!isset($_POST['filter_page']))
	{
		$_POST['filter_page']	=	1;
	}
	else if(!is_numeric($_POST['filter_page']))
	{
		$_POST['filter_page']	=	1;
	}
	$dataScorer->skip(((int)$_POST['filter_page']-1)*20);
	$dataScorer->limit(20);
	
	$countScorer				=	$dataScorer->count();
	$countpage					=	$dataScorer->count(true);
	
	$dataScorer->next();
	
	?>
	<br><b>เลือกหน้า</b>
	<form action="manage_scocer.php" method="POST">
		<input type="hidden" name="filter" id="filter" value="<?php echo empty($_POST['filter']) ? '' : $_POST['filter'] ?>">
		<select name="filter_page" id="filter_page">
			<?php
				$pagesize = ceil($countScorer/$maxsizeshow);
				for($i=1;$i<=$pagesize;$i++)
				{
				
					echo '<option value="' . $i . '" ';
						if((int)$_POST['filter_page'] == $i )
							echo 'selected';
						echo '>'.$i.'</option>';
				}
			?>
		<input type="submit" value="เลือกหน้า">
	</form><br>
	<form action="manage_scocer.php" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="filter" id="filter" value="<?php echo empty($_POST['filter']) ? '' : $_POST['filter'] ?>">
	<input type="hidden" name="filter_page" id="filter_page" value="<?php echo empty($_POST['filter_page']) ? '' : $_POST['filter_page'] ?>">
	<input type="submit" value="บันทึกรวม">
	<table border="1">
	<tr>
		<td><b>ID</b></td>			
		<td><b>Player nameEN</b></td>
		<td><b>Player nameTH</b></td>
		<td><b>Player nameTH Short</b></td>
		<td><b>Team</b></td>
		<td><b>Goal</b></td>
		<td><b>อัปโหลดรูปนัดเตะ</b></td>
		<td><b>รูป</b></td>
		<td><b>บันทึก?</b></td>
		<td><b>ลบ?</b></td>
	</tr>
	<?php
	for( $i=0 ; $i<$countpage ; $i++ )
	{
		$data 	= 	$dataScorer->current();
		echo '<tr>';

			echo '<td>';
				echo $data['id'];
			echo '</td>';
		
			echo '<td>';
				echo '<input type="input" id="SNameEN'. $data['id'] .'" name="SNameEN['. $data['id'] .']" value="'. $data['NameEN'] .'" >';
			echo '</td>';
			
			echo '<td>';
				echo '<input type="input" id="SNameTH'. $data['id'] .'" name="SNameTH['. $data['id'] .']" value="'. $data['NameTH'] .'" >';
			echo '</td>';
			
			echo '<td>';
				echo '<input type="input" id="SNameTHShort'. $data['id'] .'" name="SNameTHShort['. $data['id'] .']" value="'. $data['NameTHShort'] .'" >';
			echo '</td>';
			
			echo '<td>';
				echo '<select name="STeamID['. $data['id'] .']" id="STeamID'. $data['id'] .'">';
				
				echo '<option value="-1" ';
				echo ((int)$data['TeamKPID']==-1) ? 'selected' : '';
				echo ' style="background-color: #FFCC8F" >ไร้สังกัด</option>';
					
				echo '<option value="-2" ';
				echo ((int)$data['TeamKPID']==-2) ? 'selected' : '';
				echo ' style="background-color: #FFCC8F" >ไม่สังกัดทีมใดในลีก</option>';
					
				for( $j=0 ; $j<$countTeam ; $j++ ){
					$tmpTeam 	= 	$ListTeamArr['list'][$j];
					echo '<option value="' . $tmpTeam['id'] . '"';
					echo ((int)$tmpTeam['id']==(int)$data['TeamKPID']) ? 'selected' : '';
					echo ' >' . $tmpTeam['NameTH'] . '</option>';
				}
				echo '</select>';
			echo '</td>';
			
			echo '<td>';
				echo '<input type="input" id="SGoal'. $data['id'] .'" name="SGoal['. $data['id'] .']" value="'. $data['Goal'] .'" >';
			echo '</td>';
			
			
			echo '<td>';
				echo '<input type="file" name="picture['. $data['id'] .']" id="picture'. $data['id'] .'">';
			echo '</td>';
			
			echo '<td>';
				if(!empty($data['Picture']))
					echo '<img src="../uploads/scorer/'. $data['Picture'] .'" width="120">';
				else
					echo 'No Picture';
			echo '</td>';
			
			echo '<td>';
				echo '<input type="button" value="บันทึก"  onclick="myFunction('.$data['id'].')">';
			echo '</td>';
			
			echo '<td>';
				echo '<input type="button" value="ลบ"  onclick="myFunctionDelete('.$data['id'].')">';
			echo '</td>';
			
		echo '</tr>';

		$dataScorer->next();
	}
	?>
	<table>
	<input type="submit" value="บันทึกรวม">
	</form>