<?php
session_start();

$authorized = false;

if(isset($_GET['logout']) && ($_SESSION['auth'])) {
	$_SESSION['auth'] = null;
	session_destroy();
	echo "logging out...";
	?>
	<META HTTP-EQUIV="Refresh" CONTENT="5;URL=http://football.kapook.com/" />
	<?php
}

if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = 'football';
	$pass = 'cityunited';
	if (($user == $_SERVER['PHP_AUTH_USER']) && ($pass == ($_SERVER['PHP_AUTH_PW'])) && (!empty($_SESSION['auth']))) {
		$authorized = true;
	}
}

if ((! $authorized)) {
	header('WWW-Authenticate: Basic Realm="Login please"');
	header('HTTP/1.0 401 Unauthorized');
	$_SESSION['auth'] = true;
	print('Login now or forever hold your clicks...');
	exit;
}

else{
	$data = array();
	$minsize = 250;

	if	(	($_FILES['picture']['error'] != 4) &&
			($_FILES['picture']['error'] != 0)
	)
	{
		$data = array(
			'error' => 	1,
			'text'	=>	'Error:Cannot Upload File'
		);
		echo json_encode($data);
		exit;		
	}
	else if($_FILES['picture']['error'] == 0)
	{
		// Check Type file
		if 	(	($_FILES['picture']['type'] 	!= 'image/gif')
			&& 	($_FILES['picture']['type'] 	!= 'image/jpeg')
			&& 	($_FILES['picture']['type'] 	!= 'image/jpg')
			&& 	($_FILES['picture']['type']		!= 'image/pjpeg')
			&& 	($_FILES['picture']['type'] 	!= 'image/x-png')
			&& 	($_FILES['picture']['type'] 	!= 'image/png')
		)
		{
			$data = array(
				'error' => 	2,
				'text'	=>	'Error:Wrong type file.'
			);
			echo json_encode($data);
			exit;
		}
		else
		{		
			// gif
			if($_FILES['picture']['type'] == 'image/gif')
			{
				$extension	=	'gif';
			}
						
			// jpeg
			else if(	($_FILES['picture']['type'] == 'image/jpeg')
					||	($_FILES['picture']['type'] == 'image/jpg')
					||	($_FILES['picture']['type'] == 'image/pjpeg')
			)
			{
				$extension	=	'jpg';
			}
						
			// png
			else if(	($_FILES['picture']['type'] == 'image/x-png')
					||	($_FILES['picture']['type'] == 'image/png'))
			{
				$extension	=	'png';
			}
				
			// Check size of picture,Must be "square"
			$infoPic = getimagesize($_FILES['picture']['tmp_name']);
			if($infoPic[0]!=$infoPic[1])
			{
				$data = array(
					'error' => 	3,
					'text'	=>	'Error:Picture is not square.'
				);
				echo json_encode($data);
				exit;
			}
				
			// Check directory for upload
			if(!is_dir('../uploads/scorer/'))
			{
				if(!mkdir('../uploads/scorer/',0777,true))
				{
					$data = array(
						'error' => 	4,
						'text'	=>	'Error:Cannot Create a directory.'
					);
					echo json_encode($data);
					exit;
				}
			}
				
			if(!$error)
			{
				$filename = $_POST['filename'] . '.' . $extension;
				if(!move_uploaded_file($_FILES['picture']['tmp_name'], '../uploads/scorer/' . $filename ))
				{
					$data = array(
						'error' => 	1,
						'text'	=>	'Error:Cannot Upload File.'
					);
					echo json_encode($data);
					exit;
				}
			}
		}
	}
	else
	{
		$filename	=	null;
	}

	$data = array(
		'error' 	=> 	0,
		'filename'	=>	$filename
	);
	echo json_encode($data);
}
?>