<?php header( "refresh: 0; url=http://football.kapook.com/api/adminfootball/team/detail/" . $_GET['id'] ); /*
<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="assets/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/js/jquery.quicksearch.js"></script>
<link href="assets/multiselect/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">

<?php
	include 'manage_checklogin.php';
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$dataMongo 				= 	$collectionTeam->findOne( array( 'id' => (int)$_GET['id'] ) );
	
	if(empty($dataMongo))
	{
		echo 'error : Not Found.';
	}
	
	$filelogo				=	str_replace(' ','-',$dataMongo['NameEN']).'.png';
	if ($handle = file_exists('/var/web/football.kapook.com/html/uploads/logo/' . $filelogo ))
		$logopath = 'http://202.183.165.189/uploads/logo/' . $filelogo;
	else
		$logopath = 'http://202.183.165.189/uploads/logo/default.png';
	
	?>
	<form action="manage_team_submit_full.php" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="teamid" id="teamid" value="<?php echo $_GET['id']; ?>">
	<table border="1" style="font-family: tahoma; font-size: 10px;">
		<tr>
			<td><b>ID</b></td>			
			<td><b>Team nameEN</b></td>
			<td><b>Team nameTH</b></td>
			<td><b>Team nameTH Short</b></td>
			<td><b>status</b></td>
		</tr>
	<?php
		echo '<tr bgcolor="'.$ColorArr[$dataMongo['Status']].'";>';

			echo '<td style="width:3%">';
				echo $dataMongo['id'];
			echo '</td>';
		
			echo '<td style="width:20%">';
				echo $dataMongo['NameEN'];
			echo '</td>';
			
			echo '<td style="width:20%">';
				echo '<input style="width:100%" type="input" id="nameth" name="nameth" value="'. $dataMongo['NameTH'] .'" >';
			echo '</td>';
			
			echo '<td style="width:20%">';
				echo '<input style="width:100%" type="input" id="namethshort" name="namethshort" value="'. $dataMongo['NameTHShort'] .'" >';
			echo '</td>';
			
			echo '<td style="vertical-align:right">';
				echo '<select name="status" id="status">';
					echo '<option value="0" ';
					echo empty($dataMongo['Status']) ? 'selected' : '';
					echo '>ปิด</option>';
					echo '<option value="1" ';
					echo empty($dataMongo['Status']) ? '' : 'selected';
					echo '>เปิด</option>';
				echo '</select>';
			echo '</td>';
		echo '</tr>';
	?>
	
	<tr>
		<td colspan="2" rowspan="2">
			<center>
				<b>Logo</b><br>
				<img src="<?php echo $logopath; ?>" ><br>
			</center>
		</td>
		<td colspan="4">
			<b>Video Embed Code</b><br>
			<textarea name="embedCode" style="width:100%;height:100px;"><?php echo empty($dataMongo['EmbedCode']) ? '' : $dataMongo['EmbedCode'] ; ?></textarea>
		</td>
	</tr>
	
	<tr>
		<td colspan="4" rowspan="2">
			<b>Info Team</b><br>
			<textarea name="info" style="width:100%;height:200px;"><?php echo empty($dataMongo['Info']) ? '' : $dataMongo['Info'] ; ?></textarea>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<center>
				<b>Upload new logo</b><br>
				<input type="file" name="logopic">
			</center>
		</td>
	</tr>
	
	<tr>
		<td colspan="5">
			<center>
			<b>Select a Duplicate Team</b><br>
			<select multiple="multiple" id="my-select" name="duplicate-select[]">
				<?php
				$dataTeam 	= 	$collectionTeam->find( array( 'id' => array( '$ne' => $dataMongo['id'])))->sort(array( 'NameEN' => 1 ));
				foreach($dataTeam as $tmpTeam){
					if(empty($tmpTeam['NameTH'])){
						$tmpTeamName	=	$tmpTeam['NameEN'];
					}else{
						$tmpTeamName	=	$tmpTeam['NameTH'];
					}
					?><option value="<?php echo $tmpTeam['id'];?>" <?php if(in_array($tmpTeam['id'],$dataMongo['DuplicateList'])){ echo 'selected'; } ?>><?php echo $tmpTeamName; ?></option><?php
				}
				?>
			</select>
			</center>
		</td>
	</tr>
	
	<tr>
		<td colspan="5">
			<center><input type="submit" value="บันทึก"></center>
		</td>
	</tr>
	</table>
	</form>

<script language="JavaScript">
	$(document).ready(function(){
		$('#my-select').multiSelect({
			selectableHeader: "<div style=\"font-size: large;\">รายชื่อทีมทั้งหมด</div><input type='text' class='search-input' autocomplete='off' placeholder='พิมพ์ชื่อทีมต้องการเลือก'>",
			selectionHeader: "<div style=\"font-size: large;\">รายชื่อทีมที่ช้ำกัน</div><input type='text' class='search-input' autocomplete='off' placeholder='พิมพ์ชื่อทีมต้องการเอาออก'>",
			afterInit: function(ms){
				var that = this,
				$selectableSearch = that.$selectableUl.prev(),
				$selectionSearch = that.$selectionUl.prev(),
				selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)';
				selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

				that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
				.on('keydown', function(e){
				  if (e.which === 40){
					that.$selectableUl.focus();
					return false;
				  }
				});

				that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
				.on('keydown', function(e){
				  if (e.which == 40){
					that.$selectionUl.focus();
					return false;
				  }
				});
			},
			afterSelect: function(){
				this.qs1.cache();
				this.qs2.cache();
			},
			afterDeselect: function(){
				this.qs1.cache();
				this.qs2.cache();
			}
		});
	}); // End Ready Function 
</script>
*/ ?>