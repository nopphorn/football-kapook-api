<script type="text/javascript" src="../assets/js/jquery-1.11.0.min.js"></script>
<?php
session_start();

// init MongoDB
$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
$DatabaseMongoDB		=	$connectMongo->selectDB("football");
$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
$collectionMember		=	new MongoCollection($DatabaseMongoDB,"football_member");
	
// init memcache
$memcache = new Memcache;
$memcache->connect('localhost', 11211) or die ("Could not connect");

$authorized = false;
include 'manage_index.php';

if(isset($_GET['logout'])) {
	$_SESSION['USER_GAME'] 	=	null;
	$_SESSION['PASS_GAME']	=	null;
	echo "logging out...";
}

if(isset($_POST['user']))
	$_SESSION['USER_GAME']	=	$_POST['user'];
if(isset($_POST['pass']))
	$_SESSION['PASS_GAME']	=	$_POST['pass'];

if(isset($_SESSION['USER_GAME']) && isset($_SESSION['PASS_GAME'])) {
	$user 	= 	array('football','cha','jome','pong','ping','tea','pong','champ');
	$pass 	= 	array('cityunited','12345','12345','12345','12345','12345','12345','12345');
	$id		=	array(2,3,4,5,6,7,8,9);
	
	$key	=	array_search($_SESSION['USER_GAME'], $user);
	
	if ( $key !== false ){
		if( $pass[$key]==$_SESSION['PASS_GAME'] ){
			
			$authorized = 	true;
			$idMember	=	$id[$key];
		}
	}
	else
	{
		$_SESSION['USER_GAME'] 	=	null;
		$_SESSION['PASS_GAME']	=	null;
	}
}

if ((! $authorized)) {
	?>
	<form method="post" action="test_game.php">
		user : <input type="input" name="user"><br>
		pass : <input type="password" name="pass"><br>
		<input type="submit" name="login"><br>
	</form>
	<?php
		exit;
}

else{
	$datamember		=	$collectionMember->findOne(array( 'id' => $idMember));
	
	echo 'You are "'.$_SESSION['USER_GAME'].'".<br>';
	echo 'You money : '.$datamember['money'].'<br>';
	
	include 'test_game_menu.php';

	if(!empty($_POST['bet_type']))
	{
		foreach($_POST['listAll'] as $k => $value)
		{
			if(isset($_POST['isPlay'][$k]))
			{
				$dataOldGame		=	$collectionGame->findOne(
					array(
						'match_id' 	=> 	(int)$k,
						'user_id' 	=> 	(int)$idMember,
						'game_type'	=>	2
					)
				);
				if(!$dataOldGame)
				{
					$cursorGame			=	$collectionGame->find();
					$cursorGame->sort( array( 'id' => -1 ) );
					$cursorGame->limit(1);
					$cursorGame->next();
				
					$dataGame			=	$cursorGame->current();
					
					$dataupdate['id']	=	((int)$dataGame['id']+1);
				}
				else
				{
					$dataupdate['id']	=	$dataOldGame['id'];
				}
				
				$dataupdate['match_id']		=	(int)$k;
				$dataupdate['user_id']		=	(int)$idMember;
				$dataupdate['teamwin'] 		=	(int)$_POST['teamwin'][$k];
				$dataupdate['status'] 		= 	1;
				$dataupdate['game_type']	=	2;
				$dataupdate['bet_type']		=	(int)$_POST['bet_type'];
				$dataupdate['multiple'] 	= 	(float)1.0;
				$dataupdate['date_play'] 	=	$_POST['date_play'];
				$dataupdate['money_return'] =	(float)-1.0;
				
				if((int)$_POST['bet_type'] == 2)
				{
					$dataupdate['money_paid'] 	= 	(float)$_POST['Price'][$k];
				}
				else
				{
					$dataupdate['money_paid'] 	=	(float)$_POST['price_multi'];
				}
				
				$collectionGame->update(
					array(
						'id' 	=> (int)$dataupdate['id']
					),
					array('$set' => $dataupdate),
					array("upsert" => true)
				);
			}
			else{
				$dataupdate = array(
					'status' 			=> 	0
				);
				$collectionGame->update(
					array( 
						'match_id' 	=> 	(int)$k,
						'user_id' 	=> 	(int)$idMember,
						'game_type'	=>	2
					),
					array('$set' => $dataupdate),
					array("upsert" => false)
				);
			}
		}
		echo 'Saved Data.';
	}
	
	
	if(date('G')>=6){
		$date_lower = 	date("Y-m-d H:i:s");
		$date_upper = 	date("Y-m-d",strtotime($date_lower . ' + 1 day')) . ' 05:59:59';
		$date_play	=	date("Y-m-d",strtotime($date_lower));
	}
	else
	{
		$date_upper = 	date("Y-m-d") . ' 05:59:59';
		$date_lower = 	date("Y-m-d H:i:s");
		$date_play	=	date("Y-m-d",strtotime($date_lower . ' - 1 day'));
	}
	
	echo $date_lower .' -> '. $date_upper;

	$FindArr		=	array();
	$bet_type		=	0;
	$money_paid		=	0;
    
	$FindArr['MatchDateTime'] = array( '$gte'=>$date_lower,'$lte'=>$date_upper);
	$FindArr['Status'] = 1;
	$FindArr['MatchStatus'] = 'Sched';
	$FindArr['Odds'] = array( '$gte' => 0 );
	
	?>
	
	<form action="test_game_bet.php" id="match" method="POST">
	
	<b>รูปแบบการวางบอล</b>
	<input type="radio" name="bet_type" class="bet_type" id="bet_type_2" value="2">บอลเดี่ยว
	<input type="radio" name="bet_type" class="bet_type" id="bet_type_3" value="3">บอลชุด
	<input type="hidden" name="date_play" value="<?php echo $date_play;?>">
	
	<table border="1" style="font-family: tahoma; font-size: 10px;">
	<?php
	
		$dataMongo 			= 	$collection->find($FindArr)->sort(array( 'MatchDateTimeMongo' => -1 ));
		$countMongo			=	$dataMongo->count();
		$dataMongo->next();
	?>
		<tr>
			<td><b>เลือกเล่น</b></td>
			<td><b>ID</b></td>
			<td><b>XscoreID</b></td>
			<td><b>DateTime</b></td>
			<td><b>Zone Name</b></td>
			<td><b>League Name</b></td>
			<td colspan="2"><b>Team</b></td>
			<td><b>อัตราต่อรอง</b></td>
			<td colspan="2"><b>วางบอล</b></td>
			<td><b>ราคา</b></td>
		</tr>
		<?php
		for( $i=0 ; $i<$countMongo ; $i++ )
		{
			$data 		= 	$dataMongo->current();
			
			$dataGame	=	$collectionGame->findOne(
				array(
					'match_id'		=>	$data['id'],
					'user_id'		=>	$idMember,
					'game_type'		=>	2
				)
			);
			
			if((!isset($dataGame))||empty($dataGame['status']))
			{
				$dataGame	=	null;
			}
			else{
				$bet_type	=	$dataGame['bet_type'];
			}
			
			echo '<tr bgcolor="';
			echo isset($dataGame) ? '#BDE6C1' : '#555555';
			echo '" style=color:';
			echo isset($dataGame) ? '#000000' : '#FFFFFF';
			echo ' id="bg'. $data['id'] .'">';
				echo '<input name="listAll['.$data['id'].']" type="hidden" value="1">';
			
				// isPlay?
				echo '<td>';
					echo '<input class="isPlay" name="isPlay['.$data['id'].']" type="checkbox" value="'.$data['id'].'" ';
					echo isset($dataGame) ? 'checked' : '' ;
					echo ' ';
					echo isset($dataGame) ? '' : 'disabled' ;
					echo '>';
				echo '</td>';
			
				// ID
				echo '<td>';
					echo $data['id'];
				echo '</td>';
				
				// XscoreID
				echo '<td>';
					echo $data['XSMatchID'];
				echo '</td>';
				
				// DateTime
				echo '<td>';
					echo $data['MatchDateTime'];

				echo '</td>';
				
				// Zone Name
				echo '<td>';
					$dataZone			= 	$collectionZone->findOne(array( 'id' => $data['KPLeagueCountryID'] ));
					if(!empty($dataZone))
					{	if(empty($dataZone['NameTH']))
							echo $dataZone['NameEN'];
						else
							echo $dataZone['NameTH'];
					}
				echo '</td>';
				
				// League Name
				echo '<td>';
					$dataLeague		= 	$collectionLeague->findOne(array( 'id' => $data['KPLeagueID'] ));
					if(!empty($dataLeague))
					{	if(empty($dataLeague['NameTH']))
							echo $dataLeague['NameEN'];
						else
							echo $dataLeague['NameTH'];
					}
				echo '</td>';

				// Team 1
				echo '<td>';
					$dataTeam1		= 	$collectionTeam->findOne(array( 'id' => $data['Team1KPID'] ));
					if(!empty($dataTeam1))
					{	if(empty($dataTeam1['NameTH']))
							echo $dataTeam1['NameEN'];
						else
							echo $dataTeam1['NameTH'];
					}
				echo '</td>';
				// Team 2
				echo '<td>';
					$dataTeam2		= 	$collectionTeam->findOne(array( 'id' => $data['Team2KPID'] ));
					if(!empty($dataTeam2))
					{	if(empty($dataTeam2['NameTH']))
							echo $dataTeam2['NameEN'];
						else
							echo $dataTeam2['NameTH'];
					}
				echo '</td>';
				// Odd
				echo '<td>';
					if($data['Odds']>0)
					{
						if($data['TeamOdds']==1)
						{
							if(!empty($dataTeam1))
							{	if(empty($dataTeam1['NameTH']))
									echo $dataTeam1['NameEN'];
								else
									echo $dataTeam1['NameTH'];
							}
						}
						else
						{
							if(!empty($dataTeam2))
							{	if(empty($dataTeam2['NameTH']))
									echo $dataTeam2['NameEN'];
								else
									echo $dataTeam2['NameTH'];
							}
						}
						
						echo ' ต่อ ' . $data['Odds'];
					}
					else if($data['Odds']==0)
					{
						echo 'เสมอ';
					}
				echo '</td>';
				
				//Team 1 radiobox
							
				echo '<td>';
					echo '<input type="radio" name="teamwin['. $data['id'] .']" id="teamwin'. $data['id'] .'_1" value="1" ';
					if(isset($dataGame))
					{
						if($dataGame['teamwin']==1)
							echo 'checked';
					}
					else
						echo 'disabled';
					echo '>'. $dataTeam1['NameTH'] .' ชนะ<br>';
				echo '</td>';
				// Team 2 radiobox
				echo '<td>';
					echo '<input type="radio" name="teamwin['. $data['id'] .']" id="teamwin'. $data['id'] .'_2" value="2" ';
					if(isset($dataGame))
					{
						if($dataGame['teamwin']==2)
							echo 'checked';
					}
					else
						echo 'disabled';
					echo '>'. $dataTeam2['NameTH'] .' ชนะ<br>';
				echo '</td>';
				// Price( for Single play )
				echo '<td>';
					echo '<input type="input" id="Price'. $data['id'] .'" name="Price['. $data['id'] .']" value="';
					
					if(isset($dataGame)){
						if($dataGame['bet_type']==2){
							echo $dataGame['money_paid'];
						}
						else if($dataGame['bet_type']==3){
							$money_paid		=	$dataGame['money_paid'];
						}
					}
						
					echo '" size="8" ';
					
					if(!isset($dataGame['bet_type'])){
						echo 'disabled';
					}
					else if($dataGame['bet_type']!=2)
						echo 'disabled';
					
					echo'>';
				echo '</td>';
			echo '</tr>';
			
			$dataMongo->next();
		}
		?>
	<table>
	ราคา(สำหรับบอลชุด)<input type="input" name="price_multi" class="price_multi" id="price_multi" size="8"
	<?php
		if($bet_type==3)
		{
			echo 'value="' . $money_paid . '"';
		}
		else{
			echo ' disabled';
		}
	?> ><br>
	<input type="submit" onclick="checkSubmit()" value="บันทึกรวม">
	</form>
	
	<script language="JavaScript">
		$('#bet_type_<?php echo (int)$bet_type; ?>').prop('checked', true);
		
		if( ($('#bet_type_2').is(":checked")) || ($('#bet_type_3').is(":checked")) ){
			$(".isPlay").each(function() {
				$(this).prop('disabled', false);
			});
		}
		
		function checkSubmit(){
			if(($(".isPlay:checked").length != 5)&&($('#bet_type_3').is(":checked")))
			{
				alert('การเล่นบอลชุด ต้องเลือกเล่น 5 คู่');
				event.preventDefault();
			}
			if(($(".price_multi").val() > <?php echo $datamember['money']; ?>)&&($('#bet_type_3').is(":checked")))
			{
				alert('ตัวเลขลงบอลมีค่ามากกว่าแต้มที่คุณมีอยู่ กรุณากรอกใหม่อีกครั้ง');
				event.preventDefault();
			}
			else if($('#bet_type_2').is(":checked"))
			{
				var total	=	0;
				$("#match input[type=input]").each(function() {
					total = total + Number($(this).val());
				});
				if( total > <?php echo $datamember['money']; ?>)
				{
					alert('ตัวเลขลงบอลของทุกคู่รวมกัน มีค่ามากกว่าแต้มที่คุณมีอยู่ กรุณากรอกใหม่อีกครั้ง');
					event.preventDefault();
				}
			}
		}
	
		$('.isPlay').change(function() {
			if(this.checked){
				if($(".isPlay:checked").length > 5)
				{
					alert('จำนวนคู่ที่เลือกห้ามเกิน 5 คู่');
					this.checked = false;
				}
				else
				{
					if($('#bet_type_2').is(":checked"))
					{
						$( '#Price' + this.value ).prop('disabled', false);
						$( '#Price' + this.value ).val("0");
					}
					
					$( '#teamwin' + this.value + '_1' ).prop('disabled', false);
					$( '#teamwin' + this.value + '_2' ).prop('disabled', false);

					$( '#bg' + this.value ).attr("bgcolor","#BDE6C1");
					$( '#bg' + this.value ).attr("style","color:#000000");
				}
				
			}
			else
			{
				$( '#Price' + this.value ).prop('disabled', true);
				$( '#Price' + this.value ).val("");
				
				$( '#teamwin' + this.value + '_1' ).prop('disabled', true);
				$( '#teamwin' + this.value + '_1' ).prop('checked', false);
				
				$( '#teamwin' + this.value + '_2' ).prop('disabled', true);
				$( '#teamwin' + this.value + '_2' ).prop('checked', false);
				
				$( '#bg' + this.value ).attr("bgcolor","#555555");
				$( '#bg' + this.value ).attr("style","color:#FFFFFF");
			}
		});
		
		$('.bet_type').change(function() {
			var bettype	=	this.value;
			
			$(".isPlay").each(function() {
				$(this).prop('disabled', false);
				if( ( bettype == 2 ) && ( this.checked == true ) ){
					{
						$( '#Price' + this.value ).prop('disabled', false);
						$( '#Price' + this.value ).val("0");
					}
				}
			});
		
			if(bettype == 3 ){
				$("#match input[type=input]").each(function() {
					$(this).prop('disabled', true);
					$(this).val("");
				});
				$(".price_multi").prop('disabled', false);
				$(".price_multi").val("0");
			}
			else if(bettype == 2 ){
				$(".price_multi").prop('disabled', true);
				$(".price_multi").val("");
			}
		});
    </script>
	
	<?php
	}
?>