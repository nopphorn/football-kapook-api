<?php
	//$start_date['day']		=	1;
	//$start_date['month']	=	8;

	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	header('Content-Type: application/json');
	
	function checkCookie(){
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {           
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	
	// init MongoDB
	$connectMongo 			=	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionNotify		=	new MongoCollection($DatabaseMongoDB,"football_log_notify");
	
	if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login'])){
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		echo json_encode($returnJson);
		return;
	}
	if(!checkCookie()){
		$returnJson	=	array(
			'code_id'	=>	401,
			'message'	=>	'Cannot Authention.',
			'uid'		=>	$_COOKIE['uid']
		);
		echo json_encode($returnJson);
		return;
	}
	$uid	=	(int)$_COOKIE['uid'];
	
	if(!isset($_REQUEST['type'])){
		$returnJson	=	array(
			'code_id'	=>	404,
			'message'	=>	'Not found a type.',
			'uid'		=>	$_COOKIE['uid']
		);
		echo json_encode($returnJson);
		return;
	}
	
	$collectionNotify->insert(
		array(
			'user_id'		=>	$uid,
			'time_stamp' 	=> 	date('Y-m-d H:i:s'),
			'type'			=>	$_REQUEST['type']
		)
	);
	
	$returnJson	=	array(
		'code_id'	=>	200,
		'message'	=>	'Success.',
		'uid'		=>	$_COOKIE['uid'],
		'time_stamp' 	=> 	date('Y-m-d H:i:s')
	);
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>