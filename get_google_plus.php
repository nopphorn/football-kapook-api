<?php
	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	900;
	
	$url	=	$_GET['url'];
	
	if ((!(substr($url, 0, 7) == 'http://')) && 	(!(substr($url, 0, 8) == 'https://'))) 
	{ 
		$url = 'http://' . $url;
	}
	$url = preg_replace('/\s+/', '%20', $url);
	
	$data_MC = $memcache->get( 'Football2014-google-' . md5($url) );
	if($data_MC){
		$data				=	$data_MC;
		$data['cache']		=	1;
	}else{
	 
		$ch = curl_init();  
		curl_setopt($ch, CURLOPT_URL, "https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
	 
	   
		$curl_results = curl_exec ($ch);
		curl_close ($ch);
	 
		$parsed_results = json_decode($curl_results, true);
		
		if(empty($parsed_results[0]['error']))
		{
			$data	=	array(
				'count'	=>	$parsed_results[0]['result']['metadata']['globalCounts']['count'],
				'url'	=>	$parsed_results[0]['result']['id']
			);
		}
		else
		{
			$data	=	array(
				'count'	=>	0,
				'url'	=>	$url
			);
		}
		
		$memcache->set( 'Football2014-google-' . md5($url) , $data , MEMCACHE_COMPRESSED, $expire );
		
	}
	echo json_encode($data);
 
	//$count	=	empty($parsed_results[0]['result']['metadata']['globalCounts']['count']) ? 0 : $parsed_results[0]['result']['metadata']['globalCounts']['count'];
 
?>