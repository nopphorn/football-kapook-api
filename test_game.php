<script type="text/javascript" src="../assets/js/jquery-1.11.0.min.js"></script>
<?php
session_start();

// init MongoDB
$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
$DatabaseMongoDB		=	$connectMongo->selectDB("football");
$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
$collectionMember		=	new MongoCollection($DatabaseMongoDB,"football_member");
	
// init memcache
$memcache = new Memcache;
$memcache->connect('localhost', 11211) or die ("Could not connect");

$authorized = false;
include 'manage_index.php';

if(isset($_GET['logout'])) {
	$_SESSION['USER_GAME'] 	=	null;
	$_SESSION['PASS_GAME']	=	null;
	echo "logging out...";
}

if(isset($_POST['user']))
	$_SESSION['USER_GAME']	=	$_POST['user'];
if(isset($_POST['pass']))
	$_SESSION['PASS_GAME']	=	$_POST['pass'];

if(isset($_SESSION['USER_GAME']) && isset($_SESSION['PASS_GAME'])) {
	$user 	= 	array('football','cha','jome','pong','ping','tea','pong','champ');
	$pass 	= 	array('cityunited','12345','12345','12345','12345','12345','12345','12345');
	$id		=	array(2,3,4,5,6,7,8,9);
	
	$key	=	array_search($_SESSION['USER_GAME'], $user);
	
	if ( $key !== false ){
		if( $pass[$key]==$_SESSION['PASS_GAME'] ){
			
			$authorized = 	true;
			$idMember	=	$id[$key];
		}
	}
	else
	{
		$_SESSION['USER_GAME'] 	=	null;
		$_SESSION['PASS_GAME']	=	null;
	}
}

if ((! $authorized)) {
	?>
	<form method="post" action="test_game.php">
		user : <input type="input" name="user"><br>
		pass : <input type="password" name="pass"><br>
		<input type="submit" name="login"><br>
	</form>
	<?php
		exit;
}

else{
	$datamember		=	$collectionMember->findOne(array( 'id' => $idMember));
	
	echo 'You are "'.$_SESSION['USER_GAME'].'".<br>';
	echo 'You money : '.$datamember['money'].'<br>';
	
	include 'test_game_menu.php';

	if(!empty($_POST['listAll']))
	{
		foreach($_POST['listAll'] as $k => $value)
		{
			if(isset($_POST['isPlay'][$k]))
			{
				$dataOldGame		=	$collectionGame->findOne(
					array(
						'match_id' 	=> (int)$k,
						'user_id' 	=> (int)$idMember,
						'game_type'	=>	1
					)
				);
				if(!$dataOldGame)
				{
					$cursorGame			=	$collectionGame->find();
					$cursorGame->sort( array( 'id' => -1 ) );
					$cursorGame->limit(1);
					$cursorGame->next();
				
					$dataGame			=	$cursorGame->current();
					
					$dataupdate['id']	=	((int)$dataGame['id']+1);
				}
				else
				{
					$dataupdate['id']	=	$dataOldGame['id'];
				}
				
				$dataupdate['match_id']		=	(int)$k;
				$dataupdate['user_id']		=	(int)$idMember;
				$dataupdate['score_team1'] 	=	(int)$_POST['ScoreT1M'][$k];
				$dataupdate['score_team2'] 	= 	(int)$_POST['ScoreT2M'][$k];
				$dataupdate['status'] 		= 	1;
				$dataupdate['result_point'] = 	0;
				$dataupdate['score_point'] 	= 	0;
				$dataupdate['game_type'] 	= 	1;
				
				$collectionGame->update(
					array(
						'id' 	=> (int)$dataupdate['id']
					),
					array('$set' => $dataupdate),
					array("upsert" => true)
				);
			}
			else{
				$dataupdate = array(
					'status' 			=> 	0
				);
				$collectionGame->update(
					array( 
						'match_id' 	=> (int)$k,
						'user_id' 	=> (int)$idMember,
						'game_type'	=>	1
					),
					array('$set' => $dataupdate),
					array("upsert" => false)
				);
			}
		}
		echo 'Saved Data.';
	}
	
	
	if(date('G')>=6){
		$date_lower = date("Y-m-d H:i:s");
		$date_upper = date("Y-m-d",strtotime($date_lower . ' + 1 day')) . ' 05:59:59';
	}
	else
	{
		$date_upper = date("Y-m-d") . ' 05:59:59';
		$date_lower = date("Y-m-d H:i:s");
	}
	
	echo $date_lower .' -> '. $date_upper;

	$FindArr	=	array();
    
	$FindArr['MatchDateTime'] = array( '$gte'=>$date_lower,'$lte'=>$date_upper);
	$FindArr['Status'] = 1;
	$FindArr['MatchStatus'] = 'Sched';
	
	?>
	
	<form id="formzone" action="#" method="POST">
		<input type="hidden" name="matchid" id="matchid" value="">
		<input type="hidden" name="matchstatus" id="matchstatus" value="">
		<input type="hidden" name="status" id="status" value="">
		<input type="hidden" name="team1score" id="team1score" value="">
		<input type="hidden" name="team2score" id="team2score" value="">
	</form>
	
	<form action="test_game.php" id="match" method="POST">
	<input type="submit" value="บันทึกรวม">
	<table border="1" style="font-family: tahoma; font-size: 10px;">
	<?php
	
		$dataMongo 			= 	$collection->find($FindArr)->sort(array( 'MatchDateTimeMongo' => -1 ));
		$countMongo			=	$dataMongo->count();
		$dataMongo->next();
	?>
		<tr>
			<td><b>เลือกเล่น</b></td>
			<td><b>ID</b></td>
			<td><b>XscoreID</b></td>
			<td><b>DateTime</b></td>
			<td><b>Zone Name</b></td>
			<td><b>League Name</b></td>
			<td colspan="4"><b>ทายสกอร์</b></td>
			<!--<td colspan="3"><b>ทายผล</b></td>-->
		</tr>
		<?php
		for( $i=0 ; $i<$countMongo ; $i++ )
		{
			$data 		= 	$dataMongo->current();
			
			$dataGame	=	$collectionGame->findOne(
				array(
					'match_id'	=>	$data['id'],
					'user_id'	=>	$idMember,
					'game_type'	=>	1
				)
			);
			
			if((!isset($dataGame))||empty($dataGame['status']))
				$dataGame	=	null;
			
			echo '<tr bgcolor="';
			echo isset($dataGame) ? '#BDE6C1' : '#555555';
			echo '" style=color:';
			echo isset($dataGame) ? '#000000' : '#FFFFFF';
			echo ' id="bg'. $data['id'] .'">';
				echo '<input name="listAll['.$data['id'].']" type="hidden" value="1">';
			
				// isPlay?
				echo '<td>';
					echo '<input class="isPlay" name="isPlay['.$data['id'].']" type="checkbox" value="'.$data['id'].'" ';
					echo isset($dataGame) ? 'checked' : '' ;
					echo '>';
				echo '</td>';
			
				// ID
				echo '<td>';
					echo $data['id'];
				echo '</td>';
				
				// XscoreID
				echo '<td>';
					echo $data['XSMatchID'];
				echo '</td>';
				
				// DateTime
				echo '<td>';
					echo $data['MatchDateTime'];

				echo '</td>';
				
				// Zone Name
				echo '<td>';
					$dataZone			= 	$collectionZone->findOne(array( 'id' => $data['KPLeagueCountryID'] ));
					if(!empty($dataZone))
					{	if(empty($dataZone['NameTH']))
							echo $dataZone['NameEN'];
						else
							echo $dataZone['NameTH'];
					}
				echo '</td>';
				
				// League Name
				echo '<td>';
					$dataLeague		= 	$collectionLeague->findOne(array( 'id' => $data['KPLeagueID'] ));
					if(!empty($dataLeague))
					{	if(empty($dataLeague['NameTH']))
							echo $dataLeague['NameEN'];
						else
							echo $dataLeague['NameTH'];
					}
				echo '</td>';

				// Team 1
				echo '<td>';
					$dataTeam1		= 	$collectionTeam->findOne(array( 'id' => $data['Team1KPID'] ));
					if(!empty($dataTeam1))
					{	if(empty($dataTeam1['NameTH']))
							echo $dataTeam1['NameEN'];
						else
							echo $dataTeam1['NameTH'];
					}
				echo '</td>';
				
				// Score Team 1
				echo '<td>';
					echo '<input type="input" id="ScoreT1M'. $data['id'] .'" name="ScoreT1M['. $data['id'] .']" value="';
					echo isset($dataGame['score_team1']) ? $dataGame['score_team1'] : '';
					echo '" size="3" ';
					echo isset($dataGame) ? '' : 'disabled';
					echo'>';
				echo '</td>';
				// Score Team 2
				echo '<td>';
					echo '<input type="input" id="ScoreT2M'. $data['id'] .'" name="ScoreT2M['. $data['id'] .']" value="';
					echo isset($dataGame['score_team2']) ? $dataGame['score_team2'] : '';
					echo '" size="3" ';
					echo isset($dataGame) ? '' : 'disabled';
					echo'>';
				echo '</td>';
				
				// Team 2
				echo '<td>';
					$dataTeam2		= 	$collectionTeam->findOne(array( 'id' => $data['Team2KPID'] ));
					if(!empty($dataTeam2))
					{	if(empty($dataTeam2['NameTH']))
							echo $dataTeam2['NameEN'];
						else
							echo $dataTeam2['NameTH'];
					}
				echo '</td>';
				
				// Team 1 radiobox
				/*				
				echo '<td>';
					echo '<input type="radio" name="teamwin['. $data['id'] .']" id="teamwin'. $data['id'] .'_1" value="1" ';
					if(isset($dataGame))
					{
						if($dataGame['win']==1)
							echo 'checked';
					}
					else
						echo 'disabled';
					echo '>'. $dataTeam1['NameTH'] .' ชนะ<br>';
				echo '</td>';
				// draw radiobox
				echo '<td>';
					echo '<input type="radio" name="teamwin['. $data['id'] .']" id="teamwin'. $data['id'] .'_3" value="3" ';
					if(isset($dataGame))
					{
						if($dataGame['win']==3)
							echo 'checked';
					}
					else
						echo 'disabled';
					echo '>เสมอ<br>';
				echo '</td>';
				// Team 2 radiobox
				echo '<td>';
					echo '<input type="radio" name="teamwin['. $data['id'] .']" id="teamwin'. $data['id'] .'_2" value="2" ';
					if(isset($dataGame))
					{
						if($dataGame['win']==2)
							echo 'checked';
					}
					else
						echo 'disabled';
					echo '>'. $dataTeam2['NameTH'] .' ชนะ<br>';
				echo '</td>';
				*/
			echo '</tr>';
			
			$dataMongo->next();
		}
		?>
	<table>
	<input type="submit" value="บันทึกรวม">
	</form>
	
	<script language="JavaScript">

		$('.isPlay').change(function() {
			if(this.checked){
				$( '#ScoreT1M' + this.value ).prop('disabled', false);
				$( '#ScoreT1M' + this.value ).val("0");
				$( '#ScoreT2M' + this.value ).prop('disabled', false);
				$( '#ScoreT2M' + this.value ).val("0");
				/*
				$( '#teamwin' + this.value + '_1' ).prop('disabled', false);
				$( '#teamwin' + this.value + '_2' ).prop('disabled', false);
				$( '#teamwin' + this.value + '_3' ).prop('disabled', false);*/
				$( '#bg' + this.value ).attr("bgcolor","#BDE6C1");
				$( '#bg' + this.value ).attr("style","color:#000000");
			}
			else
			{
				$( '#ScoreT1M' + this.value ).prop('disabled', true);
				$( '#ScoreT1M' + this.value ).val("");
				$( '#ScoreT2M' + this.value ).prop('disabled', true);
				$( '#ScoreT2M' + this.value ).val("");
				/*
				$( '#teamwin' + this.value + '_1' ).prop('disabled', true);
				$( '#teamwin' + this.value + '_1' ).prop('checked', false);
				
				$( '#teamwin' + this.value + '_2' ).prop('disabled', true);
				$( '#teamwin' + this.value + '_2' ).prop('checked', false);
				
				$( '#teamwin' + this.value + '_3' ).prop('disabled', true);
				$( '#teamwin' + this.value + '_3' ).prop('checked', false);
				*/
				$( '#bg' + this.value ).attr("bgcolor","#555555");
				$( '#bg' + this.value ).attr("style","color:#FFFFFF");
			}
		});
    </script>
	
	<?php
	}
?>