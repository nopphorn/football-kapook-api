<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<?php
	include 'manage_checklogin.php';
	include 'manage_index.php';
	// init a match status type
	/*
		'Sched' : 	Scheduled
		'1 HF'	: 	First Half
		'H/T'	:	Half Time
		'2 HF'	: 	Second Half
		'E/T'	:	Extra Time
		'Pen'	:	Penalties
		'Fin'	:	Finished
		'Int'	:	Interrupted
		'Abd'	:	Abandoned
		'Post'	:	Postponed
		'Canc'	:	Cancelled
	*/	
	$list_matchstatus = array(
		'Sched',
		'1 HF',
		'H/T',
		'2 HF',
		'E/T',
		'Pen',
		'Fin',
		'Int',
		'Abd',
		'Post',
		'Canc'
	);
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
    
	if(date('G')>=6){
		$date_lower = date("Y-m-d");
		$date_upper = date("Y-m-d",strtotime($date_upper . ' + 1 day'));
	}
	else
	{
		$date_upper = date("Y-m-d");
		$date_lower = date("Y-m-d",strtotime($date_upper . ' - 1 day'));
	}
	
		//$date_lower = date("2014-10-01");
		//$date_upper = date("2014-10-02");
	
	$FromDate  = $date_lower.' 06:00:00';
	$ToDate    = $date_upper . ' 05:59:59';

	$FindArr['MatchDateTime'] = array( '$gte'=>$FromDate,'$lte'=>$ToDate);
	//$FindArr['Status'] = 1;
	?>

	<?php
        
        $ColorArr['1'][0]='#BDE6C1';
        $ColorArr['0'][0]='#FFCC8F';
		
		$ColorArr['1'][1]='#8EAD91';
        $ColorArr['0'][1]='#B89264';
		
		$dataMongo 			= 	$collection->find($FindArr)->sort(array( 'MatchDateTimeMongo' => 1 ));
		$countMongo			=	$dataMongo->count();
		$dataMongo->next();
		for( $i=0 ; $i<$countMongo ; $i++ )
		{
			$data 	= 	$dataMongo->current();
			$dataETscore	=	explode('-',$data['ETScore']);
			$dataPKscore	=	explode('-',$data['PNScore']);
			
			?>
			<div style="float:left; width:23%; margin:1%; display: inline-table;">
			<table border="1" style="width:100%; font-family: tahoma; font-size: 10px; text-align:center;">
			<tr style="font-family: tahoma; font-size: 12px;">
				<td style="width:33%;"><b>
				<p style="white-space: nowrap; text-overflow: ellipsis; -o-text-overflow: ellipsis; -ms-text-overflow: ellipsis; overflow: hidden;width: 80px;"><?php
					$dataTeam1		= 	$collectionTeam->findOne(array( 'id' => $data['Team1KPID'] ));
					if(!empty($dataTeam1))
					{	if(empty($dataTeam1['NameTH']))
							echo $dataTeam1['NameEN'];
						else
							echo $dataTeam1['NameTH'];
					}
				?></p>
				</b></td>
				<td style="width:33%;"><?php
					if($data['AutoMode'])
						echo 'Auto';
					else
						echo 'Manual';
				?><br><b>
				<?php
					echo $data['MatchStatus'];
				?><b>
				</td>
				<td style="width:33%;"><b>
				<p style="white-space: nowrap;text-overflow: ellipsis;-o-text-overflow: ellipsis;-ms-text-overflow: ellipsis;overflow: hidden;width: 80px;"><?php
					$dataTeam2		= 	$collectionTeam->findOne(array( 'id' => $data['Team2KPID'] ));
					if(!empty($dataTeam2))
					{	if(empty($dataTeam2['NameTH']))
							echo $dataTeam2['NameEN'];
						else
							echo $dataTeam2['NameTH'];
					}
				?></p>
				</b></td>
			</tr>
			<tr>
				<td rowspan="2" style="font-family: tahoma; font-size: 60px;"><b id="Team1Score<?php echo $data['id'];?>">
				<?php
					if(strlen($data['ETScore'])>1){
						echo $dataETscore[0];
					}
					else{
						echo $data['Team1FTScore'];
					}
				?>
				</b></td>
				<td>
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&AutoMode=1">
						<input type="button" value="AUTO MODE">
					</a>
				</td>
				<td rowspan="2" style="font-family: tahoma; font-size: 60px;"><b id="Team2Score<?php echo $data['id'];?>">
				<?php
					if(strlen($data['ETScore'])>1){
						echo $dataETscore[1];
					}
					else{
						echo $data['Team2FTScore'];
					}
				?>
				</b></td>
			</tr>
			<tr>
				<td>
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&AutoMode=0">
						<input type="button" value="MANUAL MODE" >
					</a>
				</td>
			</tr>
			<tr>
				<td style="font-family: tahoma; font-size: 20px;"><b id="Team1ScorePK<?php echo $data['id'];?>">
				<?php
					echo $dataPKscore[0];
				?>
				</b></td>
				<td style="font-family: tahoma; font-size: 20px;"><b>PK</b></td>
				<td style="font-family: tahoma; font-size: 20px;"><b id="Team2ScorePK<?php echo $data['id'];?>">
				<?php
					echo $dataPKscore[1];
				?>
				</b></td>
			</tr>
			<tr>
				<td style="font-family: tahoma; font-size: 20px;">
					<input type="button" value="+" onclick="plus_home(<?php
						echo $data['id'];
						if( $data['MatchStatus']=='Pen'){
							echo ',1';
						}else{
							echo ',0';
						}
					?>)" <?php if($data['AutoMode']) echo 'disabled';?>>
					<input type="button" value="-" onclick="minus_home(<?php
						echo $data['id'];
						if( $data['MatchStatus']=='Pen'){
							echo ',1';
						}else{
							echo ',0';
						}
					?>)" <?php if($data['AutoMode']) echo 'disabled';?>>
				</td>
				<td style="font-family: tahoma; font-size: 20px;"><b>score</b></td>
				<td style="font-family: tahoma; font-size: 20px;">
					<input type="button" value="+" onclick="plus_away(<?php
						echo $data['id'];
						if( $data['MatchStatus']=='Pen'){
							echo ',1';
						}else{
							echo ',0';
						}
					?>)" <?php if($data['AutoMode']) echo 'disabled';?>>
					<input type="button" value="-" onclick="minus_away(<?php
						echo $data['id'];
						if( $data['MatchStatus']=='Pen'){
							echo ',1';
						}else{
							echo ',0';
						}
					?>)" <?php if($data['AutoMode']) echo 'disabled';?>>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<input type="button" onclick="submit_score(<?php echo $data['id'] . ',\'' . $data['MatchStatus'] . '\'';?>)" value="submit score" style="font-family: tahoma; font-size: 30px;" <?php if($data['AutoMode']) echo 'disabled';?>>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="font-family: tahoma; font-size: 20px;">
					
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=Sched">
						<input type="button" value="Sched" <?php if($data['AutoMode']) echo 'disabled';?>></a>
					
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=1HF">
						<input type="button" value="1 HF" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=HT">
						<input type="button" value="H/T" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=2HF">
						<input type="button" value="2 HF" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=WaitET">
						<input type="button" value="Wait to E/T" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=1ET">
						<input type="button" value="1H E/T" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=2ET">
						<input type="button" value="2H E/T" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=Pen">
						<input type="button" value="Pen" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=Fin">
						<input type="button" value="Fin" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
					<a href="submit_livematch.php?match_id=<?php echo $data['id'];?>&status=Post">
						<input type="button" value="Post" <?php if($data['AutoMode']) echo 'disabled';?>></a>
						
				</td>
			</tr>
			</table>
			</div>
			<?php
			$dataMongo->next();
		}
		?>
	<style>
	div:hover {
		background-color: #58FA82;
	}
	</style>
	<script language="JavaScript">
		function plus_home(id,isPK){
			if(isPK==0){
				var $temp 	=	$('#Team1Score'+id).text();
				$('#Team1Score'+id).text(Number($temp)+1);
			}else{
				var $temp 	=	$('#Team1ScorePK'+id).text();
				$('#Team1ScorePK'+id).text(Number($temp)+1);
			}
		}
		
		function minus_home(id,isPK){
			if(isPK==0){
				var $temp 	=	$('#Team1Score'+id).text();
				$('#Team1Score'+id).text(Number($temp)-1);
			}else{
				var $temp 	=	$('#Team1ScorePK'+id).text();
				$('#Team1ScorePK'+id).text(Number($temp)-1);
			}
		}
		function plus_away(id,isPK){
			if(isPK==0){
				var $temp 	=	$('#Team2Score'+id).text();
				$('#Team2Score'+id).text(Number($temp)+1);
			}else{
				var $temp 	=	$('#Team2ScorePK'+id).text();
				$('#Team2ScorePK'+id).text(Number($temp)+1);
			}
		}
		function minus_away(id,isPK){
			if(isPK==0){
				var $temp 	=	$('#Team2Score'+id).text();
				$('#Team2Score'+id).text(Number($temp)-1);
			}else{
				var $temp 	=	$('#Team2ScorePK'+id).text();
				$('#Team2ScorePK'+id).text(Number($temp)-1);
			}
		}
		
		function submit_score(id,status_match){
			if(status_match=='Pen'){
				var $temp1 	=	Number($('#Team1ScorePK'+id).text());
				var $temp2 	=	Number($('#Team2ScorePK'+id).text());
			}else{
				var $temp1 	=	Number($('#Team1Score'+id).text());
				var $temp2 	=	Number($('#Team2Score'+id).text());
			}
			window.location.replace("submit_livematch.php?match_id="+id+"&home_score="+$temp1+"&away_score="+$temp2);
		}
		
    </script>