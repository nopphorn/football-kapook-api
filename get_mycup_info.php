<?php
header('Content-Type: application/json');

	$mongo          = new MongoClient();
	$db             = $mongo->football;  
        
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211);

	$my_cup			=		$db->football_my_cup;
	
	if($_REQUEST['cup']){
		$dataCup			=		$my_cup->findOne( array('title_url' => $_REQUEST['cup']) );
	}
	
	if($dataCup){
		$Result				=		$dataCup;
	}else{
		$Result				=		array();
	}
	
    if ($_REQUEST['callback'] != '') {
        echo $_REQUEST['callback'] . '(' . json_encode($Result) . ')';
    } else {
        echo json_encode($Result);
    }    
        
?>