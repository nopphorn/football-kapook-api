<?php

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	
	$memcache 	= new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	(60*60*24);
	
	$listzone_MC 			= 	$memcache->get( 'Football2014-listzone-all' );
	$arrAttr				= 	array();
	
	if((!$listzone_MC)||($_REQUEST['clear']==1)){
	
		$dataZone		=	$collectionZone->find()->sort( array('NameEN' => 1) );
		if(empty($dataZone)){
			$datajson	=	array(
				'list'	=>	array(),
				'count'	=>	0
			);
		}else{
			$tmpCount	=	0;
			foreach( $dataZone as $tmpData ){
				$datajson['list'][]		=	$tmpData;
				$tmpCount++;
			}
			$datajson['count']	=	$tmpCount;
		}

		$memcache->set( 'Football2014-listzone-all' , $datajson , MEMCACHE_COMPRESSED, $expire );
		
	}else{
		$datajson	=	$listzone_MC;
	}
	echo json_encode($datajson);
?>