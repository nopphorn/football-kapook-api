<?php
	header('Content-Type: application/json');
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	
	if(isset($_REQUEST['year']))
	{
		$indexdata			=	'season';
		$indexsection		=	$_REQUEST['year'];
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	}
	else if(isset($_REQUEST['month']))
	{
		$indexdata			=	'month';
		$indexsection		=	$_REQUEST['month'];
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	}
	else if(isset($_REQUEST['date']))
	{
		$indexdata			=	'date';
		$indexsection		=	$_REQUEST['date'];
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	}
	else
	{
		$indexdata			=	'all-time';
		$indexsection		=	null;
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	}
	
	$uid		=	(int)$_REQUEST['id'];
	
	$findArr['id']					=	$uid;
	if($indexdata!='all-time'){
		$findArr[$indexdata]		=	$indexsection;
	}
	$data		=	$collectionRanking->findOne($findArr);
	
	if(!$data)
	{
		$returnJson		=	array(
			'uid'		=>	$uid,
			'ranktype'	=>	$indexdata,
			'ranktime'	=>	$indexsection,
			'score'		=>	'N/A',
			'rank'		=>	'N/A',
			'rate'		=>	'N/A'
		);
	}
	else
	{
		/*
		 * Get a ranking
		 */
		
		/*$match['total_point']	=	array('$gt'	=>	$data['total_point']);
		if($indexdata!='all-time'){
			$match[$indexdata]	=	$indexsection;
		}
		
		$ops = array(
			array(
				'$match' => $match
			),
			array(
				'$group' => array(
					'_id' => null,
					'count' => array('$sum' => 1),
				),
			),
		);
		$dataranking	=	$collectionRanking->aggregate($ops);
		if(isset($dataranking['result'][0]['count'])){
			$ranking	=	(int)$dataranking['result'][0]['count'] + 1;
			$score		=	$data['total_point'];
			$rate		=	number_format ( $data['total_rate_calculate'] , 1 );
		}else if(isset($data['total_point'])){
			$ranking	=	1;
			$score		=	$data['total_point'];
			$rate		=	number_format ( $data['total_rate_calculate'] , 1 );
		}else{
			$ranking	=	'N/A';
			$score		=	'N/A';
			$rate		=	'N/A';
		}*/
		
		$findArr				=	array();
		
		$findArr['total_point']	=	array('$gt'	=>	$data['total_point']);
		if($indexdata!='all-time'){
			$findArr[$indexdata]	=	$indexsection;
		}
		
		$cursorRanking				=	$collectionRanking->find($findArr)->limit(1);
		

		$ranking	=	($cursorRanking->count() + 1);
		$score		=	$data['total_point'];
		$rate		=	number_format ( $data['total_rate_calculate'] , 1 );
		
		$returnJson		=	array(
			'uid'		=>	$uid,
			'ranktype'	=>	$indexdata,
			'ranktime'	=>	$indexsection,
			'score'		=>	$score,
			'rank'		=>	$ranking,
			'rate'		=>	$rate
		);
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>