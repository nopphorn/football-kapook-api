<?php
header('Content-Type: application/json');
### Connect To VPS Singapore
        $mongo          = new MongoClient();
        $db             = $mongo->football;  
        
        $memcache = new Memcache;
        $memcache->connect('localhost', 11211);

        $player_market    = $db->football_player_market;
        $AllPlayer        = $player_market->find()->sort(array('Date'=>-1));
        $index=0;
        foreach($AllPlayer as $Player){
            $index+=1;
            $Result[$index] = $Player;
            
            $Result[$index]['TeamFromName'] = $memcache->get('Football2014-Team-NameTHShort-'.$Player['FromTeamKPID']);
            $Result[$index]['TeamToName'] =   $memcache->get('Football2014-Team-NameTHShort-'.$Player['ToTeamKPID']);

            $Logo1      = 	str_replace(' ','-',$memcache->get('Football2014-Team-NameEN-'.$Player['FromTeamKPID'])).'.png';
	    $Logo1_MC	=	$memcache->get('Football2014-Team-Logo-' . $Logo1);
            $Logo2      = 	str_replace(' ','-',$memcache->get('Football2014-Team-NameEN-'.$Player['ToTeamKPID'])).'.png';
            $Logo2_MC	=	$memcache->get('Football2014-Team-Logo-' . $Logo2);
			
            if($Logo1_MC){
                $Result[$index]['TeamFromLogo'] = 'http://football.kapook.com/uploads/logo/' . $Logo1;
            }else{
                $Result[$index]['TeamFromLogo'] = 'http://football.kapook.com/uploads/logo/default.png';
            }
            if($Logo2_MC){
                $Result[$index]['TeamToLogo'] = 'http://football.kapook.com/uploads/logo/' . $Logo2;
            }else{
                $Result[$index]['TeamToLogo'] = 'http://football.kapook.com/uploads/logo/default.png';
            }
            

            if($Player['Picture']!=''){
                $Result[$index]['Picture'] = 'http://football.kapook.com/uploads/player_market/'.$Player['Picture'];
            }
        }

if ($_REQUEST['callback'] != '') {
    echo $_REQUEST['callback'] . '(' . json_encode($Result) . ')';
} else {
    echo json_encode($Result);
}    

        
?>