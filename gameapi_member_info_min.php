<?php
	$start_date['day']		=	1;
	$start_date['month']	=	8;

	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	function checkCookie(){
        /* -- Hash key ห้ามเปลี่ยน -- */
        $hash = 'kapook_sudyod';

        if ($_COOKIE['uid'] && $_COOKIE['is_login']) {
            /* -- เช็คความถูกต้องของ Cookie -- */
            if (md5($_COOKIE['uid'].$hash) == $_COOKIE['is_login']) {
                $kid = $_COOKIE['uid'];
                
                setcookie("uid", $_COOKIE['uid'], time() + 172800, "/", ".kapook.com");
                setcookie("is_login", $_COOKIE['is_login'], time() + 172800, "/", ".kapook.com");
                
                /* -- ดึงค่า member จาก userid -- */
                return $kid;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	
	header('Content-Type: application/json');
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionRanking		=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	$collectionMember		=	new MongoCollection($DatabaseMongoDB,"football_member");
	
	
	if(!isset($_REQUEST['id'])){
		if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
		{
			$returnJson	=	array(
				'code_id'	=>	401,
				'message'	=>	'Cannot Authention.',
				'uid'		=>	$_COOKIE['uid']
			);
			echo json_encode($returnJson);
			return;
		}
		if(!checkCookie())
		{
			$returnJson	=	array(
				'code_id'	=>	401,
				'message'	=>	'Cannot Authention.',
				'uid'		=>	$_COOKIE['uid']
			);
			echo json_encode($returnJson);
			return;
		}
		$uid		=	(int)$_COOKIE['uid'];
		$isLogin	=	true;
	}else{
		if(($_COOKIE['uid']<=0)||(!$_COOKIE['is_login']))
		{
			$uid		=	(int)$_REQUEST['id'];
			$isLogin	=	false;
		}else if(!checkCookie()){
			$uid		=	(int)$_REQUEST['id'];
			$isLogin	=	false;
		}else if((int)$_COOKIE['uid']!=(int)$_REQUEST['id']){
			$uid		=	(int)$_REQUEST['id'];
			$isLogin	=	false;
		}else{
			$uid		=	(int)$_COOKIE['uid'];
			$isLogin	=	true;
		}
	}
	
	if(date('G')>=6)
	{
		$today		=	date('Y-m-d',strtotime('now'));
	}
	else
	{
		$today		=	date('Y-m-d',strtotime('now -1 day'));
	}
	$tmpMonth	=	date("n",strtotime($today));
	$tmpDay		=	date("j",strtotime($today));
				
	if(	( $tmpMonth > $start_date['month'] )	||
		(
			( $tmpMonth == $start_date['month'] ) &&
			( $tmpDay >= $start_date['day'] )
		)
	){
		$season	=	date("Y",strtotime($today));
	}else{
		$season	=	date("Y",strtotime($today . ' - 1 year'));
	}
	
	// Point for playside
	$findArr['id']			=	$uid;
	$memberData				=	$collectionMember->findOne($findArr);
	//stat
	$findArr['season']		=	$season;
	$statData				=	$collectionRanking->findOne($findArr);
	
	if($_REQUEST['get_debug']==1){
		var_dump($findArr);
	}
	
	//point remain
	if($isLogin){
		$point_used		=	0;
		$cursorGame		=	$collectionGame->find(array( 'user_id' => $uid,  'status' => 1 ));
		foreach($cursorGame as $tmpGames ){
			$point_used		=	$point_used		+	$tmpGames['point'];
		}
		
		if(!isset($memberData['point'])){
			$memberData['point']		=		0;
		}
		$point_remain		=	$memberData['point']	-	$point_used;
	}
	
	$returnJson	=	array(
		'uid'											=>	$uid,
		'total_rate_calculate'							=>	(!isset($statData['total_rate_calculate_by_time']))  		? 	0.0		:	$statData['total_rate_calculate_by_time']
	);
	
	if($isLogin){
		$returnJson['point_remain']		=	$point_remain;
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>