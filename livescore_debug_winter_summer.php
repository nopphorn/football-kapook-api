<?php

### Connect To VPS Singapore
        $mongo          = new MongoClient();
        $db             = $mongo->football;  
        
        $memcache = new Memcache;
        $memcache->connect('localhost', 11211);

        $zone           = $db->football_zone;
        $league         = $db->football_league;
        $match          = $db->football_match;
        $team           = $db->football_team;

		$FindArr["SourceDate"] = array('$gte'=>'2014-10-26');
		$AllMatch = $match->find($FindArr)->sort(array('MatchDateTimeMongo'=>1));


?>

<table style="font-family: tahoma; font-size: 10px;" border="1" align="center" cellspacing="1" cellspadding="5">
    
    <tr style="background-color: #999999;" align="center">
        <td>SourceMMDD</td>
        <td>XSID</td>
        <td>Date</td>
        <td>Team 1</td>
        <td>VS</td>
        <td>Team 2</td>
        <td>Status</td>
        <td>LeagueName</td>  
        <td>Zone</td>  
        <td>LastUpdate</td>  
    </tr>
    <?php 
    
    $TodayDate = date("Y-m-d");    
    
    foreach($AllMatch as $Match){ 
	
			$SourceDate = $Match['SourceDate'];
			$MatchTime = $Match['SourceTime'];
			
			// $NewMatchDateTime = date('Y-m-d H:i:00', strtotime($SourceDate.' '.$MatchTime.':00 + 5 hours'));
            // $['MatchDateTimeMongo']   = new MongoDate(strtotime($Result[$index]['MatchDateTime']));
			unset($UpdateArr);
			$UpdateArr['MatchDateTime']        		= date('Y-m-d H:i:00', strtotime($SourceDate.' '.$MatchTime.':00 + 5 hours'));
			$UpdateArr['MatchDateTimeMongo']   	= new MongoDate(strtotime($UpdateArr['MatchDateTime']));
			$UpdateArr['Remark'] 						= strval('+ 5 hours'); 
			
			$FindArr['XSMatchID'] =  intval($Match['XSMatchID']);
			
		//	$match->update($FindArr,array('$set'=>$UpdateArr)); 
			
	?>
    <tr <?php if($TodayDate==date("Y-m-d",$Match['MatchDateTimeMongo']->sec)){?> style="background-color: #009900;"<? }?>>
        <td><?php echo $Match['SourceDate']; ?> <?php echo $Match['SourceTime']; ?></td>
        <td><?php echo $Match['XSMatchID']; ?></td>
        <td><?php echo date("Y-m-d H:i",$Match['MatchDateTimeMongo']->sec);?> // <?php echo $UpdateArr['MatchDateTime']; ?></td>
        <td><?php echo $Match['Team1']; ?></td>
        <td align="center"><?php echo $Match['FTScore']; ?></td>
        <td><?php echo $Match['Team2']; ?></td>
        <td><?php echo $Match['MatchStatus']; ?></td>
        <td><?php echo $Match['XSLeagueName']; ?> (<?php echo $Match['XSLeagueID']; ?>)</td>  
        <td><?php echo $Match['XSLeagueCountry']; ?></td>  
        <td><?php echo date("Y-m-d H:i",$Match['LastUpdateMongo']->sec);?></td>
    </tr>
    <?php } ?>

</table>
