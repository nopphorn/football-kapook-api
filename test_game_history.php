<script type="text/javascript" src="../assets/js/jquery-1.11.0.min.js"></script>
<?php
session_start();

// init MongoDB
$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
$DatabaseMongoDB		=	$connectMongo->selectDB("football");
$collectionMatch        =	new MongoCollection($DatabaseMongoDB,"football_match");
$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");
$collectionMember		=	new MongoCollection($DatabaseMongoDB,"football_member");
	
// init memcache
$memcache = new Memcache;
$memcache->connect('localhost', 11211) or die ("Could not connect");

$authorized = false;
include 'manage_index.php';

if(isset($_GET['logout'])) {
	$_SESSION['USER_GAME'] 	=	null;
	$_SESSION['PASS_GAME']	=	null;
	echo "logging out...";
}

if(isset($_POST['user']))
	$_SESSION['USER_GAME']	=	$_POST['user'];
if(isset($_POST['pass']))
	$_SESSION['PASS_GAME']	=	$_POST['pass'];

if(isset($_SESSION['USER_GAME']) && isset($_SESSION['PASS_GAME'])) {
	$user 	= 	array('football','cha','jome','pong','ping','tea','pong','champ');
	$pass 	= 	array('cityunited','12345','12345','12345','12345','12345','12345','12345');
	$id		=	array(2,3,4,5,6,7,8,9);
	
	$key	=	array_search($_SESSION['USER_GAME'], $user);
	
	if ( $key !== false ){
		if( $pass[$key]==$_SESSION['PASS_GAME'] ){
			
			$authorized = 	true;
			$idMember	=	$id[$key];
		}
	}
	else
	{
		$_SESSION['USER_GAME'] 	=	null;
		$_SESSION['PASS_GAME']	=	null;
	}
}

if ((! $authorized)) {
	?>
	<form method="post" action="test_game.php">
		user : <input type="input" name="user"><br>
		pass : <input type="password" name="pass"><br>
		<input type="submit" name="login"><br>
	</form>
	<?php
		exit;
}

else{
	$datamember		=	$collectionMember->findOne(array( 'id' => $idMember));
	
	echo 'You are "'.$_SESSION['USER_GAME'].'".<br>';
	echo 'You money : '.$datamember['money'].'<br>';
	
	include 'test_game_menu.php';

	$FindArr	=	array();
    
	$FindArr['status'] 		= array( '$gte' => 1 );
	$FindArr['user_id'] 	= $idMember;
	$FindArr['game_type']	=	1;
	
	?>
	
	<table border="1" style="font-family: tahoma; font-size: 10px;">
	<?php
	
		$dataMongo 			= 	$collectionGame->find($FindArr)->sort(array( 'match_id' => -1 ));
		$countMongo			=	$dataMongo->count();
		$dataMongo->next();
	?>
		<tr>
			<td><b>ID</b></td>
			<td><b>MatchID</b></td>
			<td><b>Match Date Time</b></td>
			<td><b>Zone Name</b></td>
			<td><b>League Name</b></td>
			<td><b>1st Team</b></td>
			<td><b>1st Score</b></td>
			<td><b>สถานะ</b></td>
			<td><b>2nd Score</b></td>
			<td><b>2nd Team</b></td>
			<td><b>คะแนนจากการแข่งขัน(ชนะ/แพ้/เสมอ)</b></td>
			<td><b>คะแนนจากผลการแข่งขัน(สกอร์)</b></td>
		</tr>
		<?php
		$total 	=	0;
		for( $i=0 ; $i<$countMongo ; $i++ )
		{
			$data 			= 	$dataMongo->current();
			
			// Match Data
			$dataMatch		=	$collectionMatch->findOne(array('id'	=>	$data['match_id']));
			// Zone data
			$dataZone		= 	$collectionZone->findOne(array( 'id' => $dataMatch['KPLeagueCountryID'] ));
			// League data
			$dataLeague		= 	$collectionLeague->findOne(array( 'id' => $dataMatch['KPLeagueID'] ));
			// Team1 data
			$dataTeam1		= 	$collectionTeam->findOne(array( 'id' => $dataMatch['Team1KPID'] ));
			// Team2 data
			$dataTeam2		= 	$collectionTeam->findOne(array( 'id' => $dataMatch['Team2KPID'] ));
			
			echo '<tr>';
				
				// id
				echo '<td rowspan="2">'.$data['id'].'</td>';
				
				// match id
				echo '<td rowspan="2">'.$data['match_id'].'</td>';
				
				// match date
				echo '<td rowspan="2">'.$dataMatch['MatchDateTime'].'</td>';
				
				// zone
				echo '<td rowspan="2">';
					if(empty($dataZone['NameTH']))
						echo $dataZone['NameEN'];
					else
						echo $dataZone['NameTH'];
				echo '</td>';
				
				// league
				echo '<td rowspan="2">';
					if(empty($dataLeague['NameTH']))
						echo $dataLeague['NameEN'];
					else
						echo $dataLeague['NameTH'];
				echo '</td>';
			
				// team 1 name
				echo '<td rowspan="2">';
					if(empty($dataTeam1['NameTH']))
						echo $dataTeam1['NameEN'];
					else
						echo $dataTeam1['NameTH'];
				echo '</td>';
				
				echo '<td>'.$dataMatch['Team1FTScore'].'</td>';
				echo '<td><b><center>'.$dataMatch['MatchStatus'].'</center></b></td>';
				echo '<td>'.$dataMatch['Team2FTScore'].'</td>';
				
				// team 2 name
				echo '<td rowspan="2">';
					if(empty($dataTeam2['NameTH']))
						echo $dataTeam2['NameEN'];
					else
						echo $dataTeam2['NameTH'];
				echo '</td>';
				
				echo '<td>'. $data['result_point'] .'</td>';
				echo '<td>'. $data['score_point'] .'</td>';
			echo '</tr>';
			
			echo '<tr>';
				
				echo '<td>'.$data['score_team1'].'</td>';
				echo '<td><b><center>สกอร์ทายผล</center></b></td>';
				echo '<td>'.$data['score_team2'].'</td>';
				
				echo '<td><b>คะแนนที่ได้จากคู่นี้</b></td>';
				echo '<td>'. ($data['score_point']+$data['result_point']) .'</td>';
			echo '</tr>';
			
			$total 	= 	($total + ($data['score_point']+$data['result_point']));
			
			$dataMongo->next();
		}
		?>
		<tr>
			<td colspan="11" style="font-family: tahoma; font-size: 20px;"><b>คะแนนรวม</b></td>
			<td style="font-family: tahoma; font-size: 20px;"><b><?php echo $total; ?></b></td>
		</tr>
	<table>
	<?php
	}
?>