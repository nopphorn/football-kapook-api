<?php header( "refresh: 0; url=http://football.kapook.com/api/adminfootball/market" ); /*

<div style="font-family: tahoma; font-size: 11;">
<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<?php
	include 'manage_checklogin.php';
	include 'manage_index.php';
	
	$maxsizeshow = 20;
	
	function uploadPicture($filepic, $id)
	{
		if	(($filepic['error'] != 4) && ($filepic['error'] != 0))
		{	
			return array(
				'error' => 	1,
				'text'	=>	'Error:Cannot Upload File'
			);
		}
		else if($filepic['error'] == 0)
		{
			// Check Type file
			if 	(	($filepic['type'] 	!= 'image/gif')
				&& 	($filepic['type'] 	!= 'image/jpeg')
				&& 	($filepic['type'] 	!= 'image/jpg')
				&& 	($filepic['type']	!= 'image/pjpeg')
				&& 	($filepic['type'] 	!= 'image/x-png')
				&& 	($filepic['type'] 	!= 'image/png')
			)
			{
				return array(
					'error' => 	2,
					'text'	=>	'Error:Wrong type file.'
				);
			}
			else
			{		
				// gif
				if($filepic['type'] == 'image/gif')
				{
					$extension	=	'gif';
				}
						
				// jpeg
				else if(	($filepic['type'] == 'image/jpeg')
						||	($filepic['type'] == 'image/jpg')
						||	($filepic['type'] == 'image/pjpeg')
				)
				{
					$extension	=	'jpg';
				}
						
				// png
				else if(	($filepic['type'] == 'image/x-png')
						||	($filepic['type'] == 'image/png'))
				{
					$extension	=	'png';
				}
				
				// Check size of picture,Must be "square"
				$infoPic = getimagesize($filepic['tmp_name']);
				if($infoPic[0]!=$infoPic[1])
				{
					return array(
						'error' => 	3,
						'text'	=>	'Error:Picture is not square.'
					);
				}
				
				// Check directory for upload
				if(!is_dir('../uploads/player_market/'))
				{
					if(!mkdir('../uploads/player_market/',0777,true))
					{
						return array(
							'error' => 	4,
							'text'	=>	'Error:Cannot Create a directory.'
						);
					}
				}
				
				if(!$error)
				{
					$filename = $id . '.' . $extension;
					if(!move_uploaded_file($filepic['tmp_name'], '../uploads/player_market/' . $filename ))
					{
						return array(
							'error' => 	1,
							'text'	=>	'Error:Cannot Upload File'
						);
					}
					return array(
						'error' => 	0,
						'filename'	=>	$filename
					);
				}
			}
		}
		else
		{
			return array(
				'error' => 	0,
				'filename'	=>	null
			);
		}
	}
	
	$minsize = 250;

	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionPlayerMarket		=	new MongoCollection($DatabaseMongoDB,"football_player_market");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
        
        
        $AllTeam = $collectionTeam->find(array('NameTH' => array('$ne'=>'')))->sort(array('NameTH'=>1));
        
        
	
	if(		(isset($_POST['playerid']))
		&& 	(isset($_POST['teamid'])) 
		&& 	(isset($_POST['nameen'])) 
		&& 	(isset($_POST['nameth'])) 
		&& 	(isset($_POST['namethshort']))
		&& 	(isset($_POST['goal']))
	)
	{
		$dataMongo 		= 	$collectionPlayerMarket->findOne(array( 'id' => (int)$_POST['playerid'] ));
		if(!empty($dataMongo))
		{
			if(!empty($dataMongo['Picture']))
				$oldfile	=	$dataMongo['Picture'];
			else
				$oldfile	=	null;
			$collectionPlayerMarket->update(
				array('id' => (int)$_POST['playerid']),
				array('$set' => array(
					'NameEN' 		=> 	$_POST['nameen'],
					'NameTH' 		=> 	$_POST['nameth'],
					'NameTHShort'	=>	$_POST['namethshort'],
					'TeamKPID' 		=> 	(int)$_POST['teamid'],
					'Goal' 			=> 	(int)$_POST['goal'],
					'Picture'		=>	empty($_POST['filename']) ? $oldfile : $_POST['filename']
				))
			);
		}
	}

	else if(!empty($_POST['SNameTH']))
	{
		foreach($_POST['SNameTH'] as $k => $value)
		{
			$data = array(
				'error'		=>	$_FILES['picture']['error'][$k],
				'type'		=>	empty($_FILES['picture']['type'][$k]) ? null : $_FILES['picture']['type'][$k],
				'tmp_name'	=>	empty($_FILES['picture']['tmp_name'][$k]) ? null : $_FILES['picture']['tmp_name'][$k]
			);
			
			$checking = uploadPicture($data,$k);
			
			if($checking['error']!=0)
			{
				echo 'error : @id' . $k . ' : ' . $checking['text'] . '<br>';
			}
			else
			{
				$dataMongo 		= 	$collectionPlayerMarket->findOne(array( 'id' => (int)$k ));
				if(!empty($dataMongo))
				{
					if(!empty($dataMongo['Picture']))
						$oldfile	=	$dataMongo['Picture'];
					else
						$oldfile	=	null;
					$collectionPlayerMarket->update(
						array('id' => (int)$k),
						array('$set' => array(
							'NameEN' 		=> 	$_POST['SNameEN'][$k],
							'NameTH' 		=> 	$value,
							'NameTHShort'	=>	$_POST['SNameTHShort'][$k],
							'TeamKPID' 		=> 	(int)$_POST['STeamID'][$k],
							'Goal' 			=> 	(int)$_POST['SGoal'][$k],
							'Picture'		=>	empty($checking['filename']) ? $oldfile : $checking['filename']
						))
					);
				}
			}
		}
	}
	
	else if(!empty($_POST['new_scocer_nameth']))
	{
		$error = false;
		$collectionTmpData	=	new MongoCollection($DatabaseMongoDB,"tmpKey");
		$dataTmp 			= 	$collectionTmpData->findOne( array( 'id' => 3 ) );
		
		//echo var_dump($_FILES);
		$checking = uploadPicture($_FILES['new_scocer_picture'],((int)$dataTmp['value']+1));
		if($checking['error']!=0)
			echo $checking['text'] . '<br>';
		else
		{
			$data = array(
				'id'					=>	((int)$dataTmp['value']+1),
                                'Date'                                  =>      $_POST['YYYY'].'-'.$_POST['MM'].'-'.$_POST['DD'],
				'NameEN' 				=> 	$_POST['new_scocer_nameen'],
				'NameTH' 				=> 	$_POST['new_scocer_nameth'],
				'NameTHShort'                           =>	$_POST['new_scocer_namethshort'],
				'FromTeamKPID' 				=> 	(int)$_POST['new_scocer_from_team'],
				'ToTeamKPID' 				=> 	(int)$_POST['new_scocer_to_team'],
				'Price'                                 => 	strval($_POST['new_scocer_price']),
                                'PriceText'                             => 	strval($_POST['new_scocer_price_text']),
                                'NewsURL'                             => 	strval($_POST['new_scocer_news']),                            
				'Picture'				=>	empty($checking['filename']) ? null : $checking['filename']
			);
		
			$checking = $collectionPlayerMarket->insert( $data );
				
			if(!empty($checking['ok']))
			{
				echo $_POST['new_scocer_nameen'] .' has insert.<br>';
			
				$collectionTmpData->update( 
					array('id' => 3),
					array('$set' => array( 'value' => ((int)$dataTmp['value']+1) ) )
				);
			}
		}
                header('Location: manage_player_market.php');
	}
	
	else if(!empty($_POST['playerid_delete']))
	{
		$dataTmp 			= 	$collectionPlayerMarket->findOne( array( 'id' => (int)$_POST['playerid_delete'] ) );
		if(!empty($dataTmp))
		{
			echo $dataTmp['Picture'];
			if(!empty($dataTmp['Picture']))
				unlink('../uploads/player_market/' . $dataTmp['Picture']);
				
			$checking = $collectionPlayerMarket->remove( array('id' => (int)$_POST['playerid_delete']), array('justOne' => true));
			if(!empty($checking['ok']))
				echo $dataTmp['NameEN'] .' has deleted.<br>';
		}
	}
	
	?>
	
	<script type='text/javascript'>
		function myFunction(id){
		
			nameEN=document.getElementById('SNameEN' + id ).value;
			nameTH=document.getElementById('SNameTH' + id ).value;
			nameTHShort=document.getElementById('SNameTHShort' + id ).value;
			teamID=document.getElementById('STeamID' + id ).value;
			goal=document.getElementById('SGoal' + id ).value;
			
			document.getElementById("playerid").value		=	id;
			document.getElementById("teamid").value			=	teamID;
			document.getElementById("nameen").value			=	nameEN;
			document.getElementById("nameth").value			=	nameTH;
			document.getElementById("namethshort").value	=	nameTHShort;
			document.getElementById("goal").value			=	goal;
			
			// Image Upload
			photo = document.getElementById("picture" + id);
			var file = photo.files[0];
			console.log(file);
			if(file === undefined)
			{
				document.getElementById("filename").value	=	null;
				document.getElementById("formsoccer").submit();
			}
			else
			{
				
				// Create a new FormData object.
				formData = new FormData();
				formData.append('picture', file);
				formData.append('filename', id);
				
				
				$.ajax({
					url: 'upload_file.php',
					type: 'POST',
					data: formData,
					cache: false,
					dataType: 'json',
					processData: false,
					contentType: false,
					success: function(data, textStatus, jqXHR)
					{
						if( data.error == 0 )
						{
							//console.log(data);
							document.getElementById("filename").value	=	data.filename;
							document.getElementById("formsoccer").submit();
						}
						else
						{
							alert(data.text);
						}
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
						alert(textStatus);
					}
				});
			}
			
			//document.getElementById("formsoccer").submit();
			
		}
		
		function myFunctionDelete(id) {
			if (confirm("ยืนยันจะลบข้อมูลนี้หรือไม่?") == true) {
				document.getElementById("playerid_delete").value		=	id;
				document.getElementById("formsoccerdelete").submit();
			}
		}
</script>
	</script>
	

	
	<form id="formsoccerdelete" action="#" method="POST">
		<input type="hidden" name="playerid_delete" id="playerid_delete" value="">
		<input type="hidden" name="filter" id="filter" value="<?php echo empty($_POST['filter']) ? '' : $_POST['filter'] ?>">
	</form>
	
	<form id="formsoccer" action="#" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="playerid" id="playerid" value="">
		<input type="hidden" name="teamid" id="teamid" value="">
		<input type="hidden" name="nameen" id="nameen" value="">
		<input type="hidden" name="nameth" id="nameth" value="">
		<input type="hidden" name="namethshort" id="namethshort" value="">
		<input type="hidden" name="goal" id="goal" value="">
		<input type="hidden" name="filename" id="filename" value="">
		<input type="hidden" name="filter" id="filter" value="<?php echo empty($_POST['filter']) ? '' : $_POST['filter'] ?>">
		<input type="hidden" name="filter_page" id="filter_page" value="<?php echo empty($_POST['filter_page']) ? '1' : $_POST['filter_page'] ?>">
	</form>


	<b>เพิ่มผู้เล่นคนใหม่</b>
        <br><br> 
	<form action="manage_player_market.php" method="POST" enctype="multipart/form-data">
		วันที่        ปี : 
        <select name="YYYY" id="YYYY">

            <option value="2014">2014</option>
            <option value="2015">2015</option>
        </select>
        เดือน : 
        <select name="MM" id="MM">
            <?php for($i=1;$i<=12;$i++){?>
                <?php if($i<=9){?>
                <option value="0<?=$i?>">0<?=$i?></option>
                <?php }else{?>
                <option value="<?=$i?>"><?=$i?></option>
                <?php }?>            
            <?php }?>
        </select>
        วัน : 
        <select name="DD" id="DD">
            <?php for($i=1;$i<=31;$i++){?>
                <?php if($i<=9){?>
                <option value="0<?=$i?>">0<?=$i?></option>
                <?php }else{?>
                <option value="<?=$i?>"><?=$i?></option>
                <?php }?>
            <?php }?>
        </select><br>
		ชื่อผู้เล่น (ไทย-ยาว)<input type="input" name="new_scocer_nameth">
		ชื่อผู้เล่น (ไทย-สั้น)<input type="input" name="new_scocer_namethshort">
		ชื่อผู้เล่นอังกฤษ<input type="input" name="new_scocer_nameen"><br>
		ย้ายจากทีม<select name="new_scocer_from_team">
			<option value="-1" style="background-color: #FFCC8F">ไร้สังกัด</option>
			<option value="-2" style="background-color: #FFCC8F">ไม่สังกัดทีมใดในลีก</option>
                        <?php foreach($AllTeam as $Team){ $TeamNameByIDArr[$Team['id']]=$Team['NameTH']; ?>
                        <option value="<?php echo $Team['id']?>"><?php echo $Team['NameTH']?> </option>
                        <?php } ?>
		</select>
		ไปทีม<select name="new_scocer_to_team">
			<option value="-1" style="background-color: #FFCC8F">ไร้สังกัด</option>
			<option value="-2" style="background-color: #FFCC8F">ไม่สังกัดทีมใดในลีก</option>
                        <?php foreach($AllTeam as $Team){?>
                        <option value="<?php echo $Team['id']?>"><?php echo $Team['NameTH']?> </option>
                        <?php } ?>
		</select>
		ราคา <input type="input" name="new_scocer_price"> สกุลเงิน/ข้อความ <input type="input" name="new_scocer_price_text"><br>
                ลิงค์ข่าว<input type="input" name="new_scocer_news" size="100"><br>
		อัปโหลดรูปนัดเตะ <input type="file" name="new_scocer_picture"><br>
                <br>   
 
		<input type="submit" value="เพิ่มผู้เล่น">
	</form>

	<?php

	$dataScorer 			= 	$collectionPlayerMarket->find();	
	$countScorer			=	$dataScorer->count();
	$countpage			=	$dataScorer->count(true);
	
	$dataScorer->next();
	
	?>
	<br>
        <hr>
        
	<form action="manage_player_market.php" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="filter" id="filter" value="<?php echo empty($_POST['filter']) ? '' : $_POST['filter'] ?>">
	<input type="hidden" name="filter_page" id="filter_page" value="<?php echo empty($_POST['filter_page']) ? '' : $_POST['filter_page'] ?>">

        
        <table border="1" style="font-family: tahoma; font-size: 11px;" width="100%">
	<tr align="center">
		<td width="60"><b>ID</b></td>	
		<td width="120"><b>Picture</b></td>
		<td><b>Player Name</b></td>
                 <td width="100"><b>Date</b></td>
		<td width="200"><b>From Team</b></td>
		<td width="200"><b>To Team</b></td>
		<td width="120"><b>Price</b></td>
		<td width="120"><b>Price Text</b></td>
	</tr>
	<?php
	for( $i=0 ; $i<$countpage ; $i++ )
	{
		$data 	= 	$dataScorer->current();
                ?>
        	<tr>
		<td align="center"><?php echo $data['id']?></td>	
		<td><img src="../uploads/player_market/<?php echo $data['Picture'];?>" width="120"></td>
		<td><?php echo $data['NameTH']?><br><?php echo $data['NameEN']?><br><?php echo $data['NameTHShort']?><br><br>News : <?php echo $data['NewsURL']?></td>
		<td><?php echo $data['Date']?></td>
                <td><?php echo $TeamNameByIDArr[$data['FromTeamKPID']]?></td>
		<td><?php echo $TeamNameByIDArr[$data['ToTeamKPID']]?></td>
		<td><?php echo $data['Price']?></td>
		<td><?php echo $data['PriceText']?></td>
               </tr>
                <?php		

		$dataScorer->next();
	}
	?>
	</table>

	</form>
</div>

*/ ?>