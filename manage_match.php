<?php header( "refresh: 0; url=http://football.kapook.com/api/adminfootball/match" ); /*
<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
<?php
	include 'manage_checklogin.php';
	include 'manage_index.php';

	$list_matchstatus = array(
		'Sched',
		'1 HF',
		'H/T',
		'2 HF',
		'E/T',
		'Pen',
		'Fin',
		'Int',
		'Abd',
		'Post',
		'Canc'
	);
	
	$list_TV = array(
	
		0 => array(
			'nameType' 	=> 	'TV Analog',
			'listTV'	=>	array(
				'tv3' 	=> 	'ช่อง 3',
				'tv5'	=>	'ช่อง 5',
				'tv7'	=>	'ช่อง 7',
				'tv9'	=>	'ช่อง 9',
				'tv11'	=>	'NBT',
				'tpbs'	=>	'ThaiPBS'
			)
		),

		1 => array(
			'nameType' 	=> 	'TV Digital',
			'listTV'	=>	array(
				'tv5hd'			=> 'ชอง 5 HD',
				'tv11hd'		=> 'NBT HD',
				'tpbshd'		=> 'TPBS HD',
				'tv3family'		=> 'ช่อง 3 Family',
				'mcotfamily'	=> 'MCOT Family',
				'tnn'			=> 'TNN',
				'newtv'			=> 'New TV',
				'springnews'	=> 'Spring News',
				'nation'		=> 'Nation TV',
				'workpointtv' 	=> 'WorkpointTV',
				'true4u' 		=> 'True4U',
				'gmmchannel' 	=> 'GMM Channel',
				'now' 			=> 'NOW',
				'ch8' 			=> 'ช่อง 8(RS)',
				'tv3sd' 		=> 'ช่อง 3 SD',
				'mono' 			=> 'Mono 29',
				'mcotdh' 		=> 'MCOT HD',
				'one' 			=> 'ONE',
				'thairath' 		=> 'ไทยรัฐทีวี',
				'tv3hd' 		=> 'ช่อง 3 HD',
				'amarin' 		=> 'Amarin TV',
				'pptv' 			=> 'PPTV'
			)
		),
		
		2 => array(
			'nameType' 		=> 	'True Vision',						
			'listTV'		=>	array(
				'tshd1'		=>	'True Sport HD 1',
				'tshd2' 	=>	'True Sport HD 2',
				'tshd3' 	=>	'True Sport HD 3',
				'tshd4' 	=>	'True Sport HD 4',
				'ts1' 		=>	'True Sport 1',
				'ts2' 		=>	'True Sport 2',
				'ts3' 		=>	'True Sport 3',
				'ts4' 		=>	'True Sport 4',
				'ts5' 		=>	'True Sport 5',
				'ts6' 		=>	'True Sport 6',
				'ts7' 		=>	'True Sport 7',
				'tstennis'	=>	'True Tennis HD',
				'hayha'		=>	'Hay Ha(352)',
				'thaithai'	=>	'ไทย Thai(358)',
				'true' 		=>	'ช่องอื่นๆใน True Vision'
			)
		),
		
		3 => array(
			'nameType' 	=> 	'CTH',						
			'listTV'	=>	array(
				'cthstd1'	=>	'stadium 1',
				'cthstd2' 	=>	'stadium 2',
				'cthstd3' 	=>	'stadium 3',
				'cthstd4' 	=>	'stadium 4',
				'cthstd5' 	=>	'stadium 5',
				'cthstd6' 	=>	'stadium 6',
				'cthstdx'	=>	'stadium X'
			)
		),
		
		4 => array(
			'nameType' 	=> 	'GMM',						
			'listTV'	=>	array(
				'gmmclubchannel' 			=>	'GMM Club Channel',
				'gmmfootballplus' 			=>	'GMM Football Plus',
				'gmmfootballmax' 			=>	'GMM Football Max',
				'gmmfootballeuro' 			=>	'GMM Football Euro',
				'gmmfootballextrahd'		=>	'GMM Football Extra HD',
				'gmmsportextreme'			=>	'GMM Sport Extreme',
				'gmmsportplus'				=>	'GMM Sport Plus',
				'gmmsportextra'				=>	'GMM Sport Extra',
				'gmmsportone'				=>	'GMM Sport One'
				
			)
		),
		
		5 => array(
			'nameType' 	=> 	'Siamsport',						
			'listTV'	=>	array(
				'siamsportnew'		=>	'Siamsport News',
				'siamsportfootball' =>	'Siamsport Football',
				'siamsportlive' 	=>	'Siamsport Live'
			)
		),
		
		6 => array(
			'nameType' 	=> 	'RS',						
			'listTV'	=>	array(
				'sunchannel'		=>	'Sun Channel',
				'worldcupchannel' 	=>	'World Cup Channel'
			)
		),
		
		7 => array(
			'nameType' 	=> 	'Bein Sport',						
			'listTV'	=>	array(
				'bein'		=>	'bein sports',
				'bein1' 	=>	'bein sports 1',
				'bein2' 	=>	'bein sports 2'
			)
		),
		
		8 => array(
			'nameType' 	=> 	'Fox Sport',						
			'listTV'	=>	array(
				'foxsport' 		=>	'Fox Sport HD',
				'foxsport2' 	=>	'Fox Sport 2',
				'foxsport3' 	=>	'Fox Sport 3',
				'foxsportplay' 	=>	'Fox Sport Play'
			)
		),
		
		9 => array(
			'nameType' 	=> 	'อื่นๆ',						
			'listTV'	=>	array(
				'starsport' 			=>	'Star Sport',
				'astrosupersport1' 		=>	'Astro Supersport 1',
				'astrosupersport2' 		=>	'Astro Supersport 2',
				'astrosupersporthd3' 	=>	'Astro Supersport HD 3',
				'asn' 					=>	'ASN',
				'asn2' 					=>	'ASN 2',
				'bugaboo'				=>	'Bugaboo.tv'
			)
		)
	);
	
	$listOdd = array(
		'0.0' => 'เสมอ',
		'0.25' => 'เสมอควบครึ่ง',
		'0.5' => 'ครึ่งลูก',
		'0.75' => 'ครึ่งควบลูก',
		'1' => 'หนึ่งลูก',
		'1.25' => 'ลูกควบลูกครึ่ง',
		'1.5' => 'ลูกครึ่ง',
		'1.75' => 'ลูกครึ่งควบสองลูก',
		'2' => 'สองลูก',
		'2.25' => 'สองลูกควบสองลูกครึ่ง',
		'2.5' => 'สองลูกครึ่ง',
		'2.75' => 'สองลูกครึ่งควบสามลูก',
		'3' => 'สามลูก',
		'3.25' => 'สามลูกควบสามลูกครึ่ง',
		'3.5' => 'สามลูกครึ่ง',
		'3.75' => 'สามลูกครึ่งควบสี่ลูก',
		'4' => 'สี่ลูก',
		'4.25' => 'สี่ลูกควบสี่ลูกครึ่ง',
		'4.5' => 'สี่ลูกครึ่ง',
		'4.75' => 'สี่ลูกครึ่งควบห้าลูก',
		'5' => 'ห้าลูก',
		'5.25' => 'ห้าลูกควบห้าลูกครึ่ง',
		'5.5' => 'ห้าลูกครึ่ง',
		'5.75' => 'ห้าลูกครึ่งควบหกลูก',
		'6' => 'หกลูก',
		'6.25' => 'หกลูกควบหกลูกครึ่ง',
		'6.5' => 'หกลูกครึ่ง',
		'6.75' => 'หกลูกครึ่งควบเจ็ดลูก',
		'7' => 'เจ็ดลูก',
		'7.25' => 'เจ็ดลูกควบเจ็ดลูกครึ่ง',
		'7.5' => 'เจ็ดลูกครึ่ง',
		'7.75' => 'เจ็ดลูกครึ่งควบแปดลูก',
		'8' => 'แปดลูก',
		'8.25' => 'แปดลูกควบแปดลูกครึ่ง',
		'8.5' => 'แปดลูกครึ่ง',
		'8.75' => 'แปดลูกครึ่งควบเก้าลูก',
		'9' => 'เก้าลูก',
		'9.25' => 'เก้าลูกควบเก้าลูกครึ่ง',
		'9.5' => 'เก้าลูกครึ่ง',
		'9.75' => 'เก้าลูกครึ่งควบสิบลูก',
	);
	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");

	if(isset($_REQUEST['time_scape']))
	{
		if($_REQUEST['time_scape']=='0')
			$_SESSION["time_scape"] = '0';
		else if($_REQUEST['time_scape']=='1')
			$_SESSION["time_scape"] = '1';
		else
			$_SESSION["time_scape"] = '2';
	}
	else if(!isset($_SESSION["time_scape"]))
		$_SESSION["time_scape"] = '1';
	
	if($_SESSION['time_scape']=='0'){
		if($_REQUEST['date_upper'])
		{
			$date_lower = $_REQUEST['date_lower'];
			$date_upper = $_REQUEST['date_upper'];
		}
		else if(isset($_SESSION["date_upper"]))
		{
			$date_lower = $_SESSION["date_lower"];
			$date_upper = $_SESSION["date_upper"];
		}
		else{
			if(date('G')>=6){
				$date_lower = date("Y-m-d");
				$date_upper = date("Y-m-d",strtotime($date_upper . ' + 1 day'));
			}
			else
			{
				$date_upper = date("Y-m-d");
				$date_lower = date("Y-m-d",strtotime($date_upper . ' - 1 day'));
			}
		}
		$_SESSION["date_upper"] = $date_upper;
		$_SESSION["date_lower"] = $date_lower;
	}
	else if($_SESSION['time_scape']=='1'){
		if($_REQUEST['date'])
		{
			$date_lower = $_REQUEST['date'];
			$date_upper = date("Y-m-d",strtotime($date_lower . ' + 1 day'));
		}
		else if(isset($_SESSION["date"]))
		{
			$date_lower = $_SESSION["date"];
			$date_upper = date("Y-m-d",strtotime($_SESSION["date"] . ' + 1 day'));
		}
		else{
			if(date('G')>=6){
				$date_lower = date("Y-m-d");
				$date_upper = date("Y-m-d",strtotime($date_lower . ' + 1 day'));
			}
			else
			{
				$date_upper = date("Y-m-d");
				$date_lower = date("Y-m-d",strtotime($date_upper . ' - 1 day'));
			}
		}
		$_SESSION["date"] = $date_lower;
	}
	else
	{
		if(date('G')>=6){
			$date_lower = date("Y-m-d");
			$date_upper = date("Y-m-d",strtotime($date_upper . ' + 1 day'));
		}
		else
		{
			$date_upper = date("Y-m-d");
			$date_lower = date("Y-m-d",strtotime($date_upper . ' - 1 day'));
		}
	}
		
	$FromDate  = $date_lower.' 06:00:00';
	$ToDate    = $date_upper . ' 05:59:59';
       
	$dateArr_lower = explode('-',$date_lower);
	$dateArr_upper = explode('-',$date_upper);  

	$FindArr	=	array();
    
	if( ($_SESSION['time_scape']=='0') || ($_SESSION['time_scape']=='1') )
		$FindArr['MatchDateTime'] = array( '$gte'=>$FromDate,'$lte'=>$ToDate);
		
	if(is_numeric($_REQUEST['filterleague'])){ 
		if((int)$_REQUEST['filterleague']>0)
			$FindArr['KPLeagueID'] = (int)$_REQUEST['filterleague'];
		$_SESSION['filterleague'] = $_REQUEST['filterleague'];
	}
	else if((isset($_SESSION['filterleague']))&&((int)$_SESSION['filterleague']>0)){
		$FindArr['KPLeagueID'] = (int)$_SESSION['filterleague'];
	}
	else
	{
		$_SESSION['filterleague'] = 0;
	}
	
	if(isset($_REQUEST['filter_status']))
	{
		if((int)$_REQUEST['filter_status']!=-1)
			$FindArr['Status'] = (int)$_REQUEST['filter_status'];
		$_SESSION['filter_status']	=	$FindArr['Status'];
	}
	else if(isset($_SESSION['filter_status']))
	{
		if((int)$_SESSION['filter_status']!=-1)
			$FindArr['Status'] = (int)$_SESSION['filter_status'];
	}
	else
		$_SESSION['filter_status']	= -1;
	
	?>
	
	<script type='text/javascript'>
		function myFunction(id){
		
			ScoreT1M		=	document.getElementById('ScoreT1M' + id ).value;
			ScoreT2M		=	document.getElementById('ScoreT2M' + id ).value;
			
			if(document.getElementById('isExtra' + id ).checked)
				isExtra			=	'1';
			else
				isExtra			=	'';
			team1scoreET	=	document.getElementById('team1scoreET' + id ).value;
			team2scoreET	=	document.getElementById('team2scoreET' + id ).value;
			
			if(document.getElementById('isPK' + id ).checked)
				isPK			=	'1';
			else
				isPK			=	'';
			team1scorePK	=	document.getElementById('team1scorePK' + id ).value;
			team2scorePK	=	document.getElementById('team2scorePK' + id ).value;
			
			matchstatus		=	document.getElementById('matchstatus' + id ).value;
			status			=	document.getElementById('status' + id ).value;
			
			inputodds		=	document.getElementById('inputodds' + id ).value;
			inputteamodds	=	document.getElementById('inputteamodds' + id ).value;
			
			tvList0		=	document.getElementById('TVLiveList' + id + '_0' ).value;
			tvList1		=	document.getElementById('TVLiveList' + id + '_1' ).value;
			tvList2		=	document.getElementById('TVLiveList' + id + '_2' ).value;
			
			document.getElementById("matchid").value			=	id;
			document.getElementById("matchstatus").value		=	matchstatus;
			document.getElementById("status").value				=	status;
			
			document.getElementById("team1score").value			=	ScoreT1M;
			document.getElementById("team2score").value			=	ScoreT2M;
			
			document.getElementById("isExtra").value			=	isExtra;			
			document.getElementById("team1scoreET").value		=	team1scoreET;
			document.getElementById("team2scoreET").value		=	team2scoreET;
			
			document.getElementById("isPK").value				=	isPK;
			document.getElementById("team1scorePK").value		=	team1scorePK;
			document.getElementById("team2scorePK").value		=	team2scorePK;
			
			document.getElementById("inputodds").value			=	inputodds;
			document.getElementById("inputteamodds").value		=	inputteamodds;
			
			document.getElementById("tvList0").value			=	tvList0;
			document.getElementById("tvList1").value			=	tvList1;
			document.getElementById("tvList2").value			=	tvList2;
			
			document.getElementById("formzone").submit();
			
		}
		
		function checkEnableExtra(id){
			if(document.getElementById('isExtra' + id ).checked)
			{
				document.getElementById('team1scoreET' + id ).value = '0';
				document.getElementById('team1scoreET' + id ).disabled =false;
				document.getElementById('team2scoreET' + id ).value = '0';
				document.getElementById('team2scoreET' + id ).disabled =false;
			}
			else
			{
				document.getElementById('team1scoreET' + id ).value = '';
				document.getElementById('team1scoreET' + id ).disabled =true;
				document.getElementById('team2scoreET' + id ).value = '';
				document.getElementById('team2scoreET' + id ).disabled =true;
			}
		}
		
		function checkEnablePK(id){
			if(document.getElementById('isPK' + id ).checked)
			{
				document.getElementById('team1scorePK' + id ).value = '0';
				document.getElementById('team1scorePK' + id ).disabled =false;
				document.getElementById('team2scorePK' + id ).value = '0';
				document.getElementById('team2scorePK' + id ).disabled =false;
			}
			else
			{
				document.getElementById('team1scorePK' + id ).value = '';
				document.getElementById('team1scorePK' + id ).disabled =true;
				document.getElementById('team2scorePK' + id ).value = '';
				document.getElementById('team2scorePK' + id ).disabled =true;
			}
		}
	</script>
	
	<table border="1">
		<tr>
			<td "width: 100%">
				<input type="radio" name="time_scape" id="time_scape" value="0" onClick="checkEnable(0)">เลือกแบบช่วงเวลา<br>
				ปี : 
				<select name="YYYY_lower" id="YYYY_lower">
					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
				</select>
				เดือน : 
				<select name="MM_lower" id="MM_lower">
					<?php for($i=1;$i<=12;$i++){?>
						<?php if($i<=9){?>
						<option value="0<?=$i?>">0<?=$i?></option>
						<?php }else{?>
						<option value="<?=$i?>"><?=$i?></option>
						<?php }?>
					<?php }?>
				</select>
				วัน : 
				<select name="DD_lower" id="DD_lower">
					<?php for($i=1;$i<=31;$i++){?>
						<?php if($i<=9){?>
						<option value="0<?=$i?>">0<?=$i?></option>
						<?php }else{?>
						<option value="<?=$i?>"><?=$i?></option>
						<?php }?>
					<?php }?>
				</select>
				<br>ถึง<br>
				ปี : 
				<select name="YYYY_upper" id="YYYY_upper">

					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
				</select>
				เดือน : 
				<select name="MM_upper" id="MM_upper">
					<?php for($i=1;$i<=12;$i++){?>
						<?php if($i<=9){?>
						<option value="0<?=$i?>">0<?=$i?></option>
						<?php }else{?>
						<option value="<?=$i?>"><?=$i?></option>
						<?php }?>            
					<?php }?>
				</select>
				วัน : 
				<select name="DD_upper" id="DD_upper">
					<?php for($i=1;$i<=31;$i++){?>
						<?php if($i<=9){?>
						<option value="0<?=$i?>">0<?=$i?></option>
						<?php }else{?>
						<option value="<?=$i?>"><?=$i?></option>
						<?php }?>
					<?php }?>
				</select>
			</td "width: 100%">
			<td>
				<input type="radio" name="time_scape" id="time_scape" value="1" onClick="checkEnable(1)">เลือกแบบวัน<br>
				ปี : 
				<select name="YYYY" id="YYYY">
					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
				</select>
				เดือน : 
				<select name="MM" id="MM">
					<?php for($i=1;$i<=12;$i++){?>
						<?php if($i<=9){?>
						<option value="0<?=$i?>">0<?=$i?></option>
						<?php }else{?>
						<option value="<?=$i?>"><?=$i?></option>
						<?php }?>            
					<?php }?>
				</select>
				วัน : 
				<select name="DD" id="DD">
					<?php for($i=1;$i<=31;$i++){?>
						<?php if($i<=9){?>
						<option value="0<?=$i?>">0<?=$i?></option>
						<?php }else{?>
						<option value="<?=$i?>"><?=$i?></option>
						<?php }?>
					<?php }?>
				</select>
			</td>
			<td "width: 100%">
				<input type="radio" name="time_scape" id="time_scape" value="2" onClick="checkEnable(2)">ไม่กำหนดระยะเวลา<br>
			</td>
		</tr>
	</table>
	<script language="JavaScript">        
		function checkEnable(type){
			if(type == 0)
			{
				document.getElementById('YYYY_lower').disabled =false;
				document.getElementById('MM_lower').disabled =false;
				document.getElementById('DD_lower').disabled =false;
				document.getElementById('YYYY_upper').disabled =false;
				document.getElementById('MM_upper').disabled =false;
				document.getElementById('DD_upper').disabled =false;
				document.getElementById('YYYY').disabled =true;
				document.getElementById('MM').disabled =true;
				document.getElementById('DD').disabled =true;
			}
			else if(type == 1)
			{
				document.getElementById('YYYY_lower').disabled =true;
				document.getElementById('MM_lower').disabled =true;
				document.getElementById('DD_lower').disabled =true;
				document.getElementById('YYYY_upper').disabled =true;
				document.getElementById('MM_upper').disabled =true;
				document.getElementById('DD_upper').disabled =true;
				document.getElementById('YYYY').disabled =false;
				document.getElementById('MM').disabled =false;
				document.getElementById('DD').disabled =false;
			}
			else if(type == 2)
			{
				document.getElementById('YYYY_lower').disabled =true;
				document.getElementById('MM_lower').disabled =true;
				document.getElementById('DD_lower').disabled =true;
				document.getElementById('YYYY_upper').disabled =true;
				document.getElementById('MM_upper').disabled =true;
				document.getElementById('DD_upper').disabled =true;
				document.getElementById('YYYY').disabled =true;
				document.getElementById('MM').disabled =true;
				document.getElementById('DD').disabled =true;
			}
		}
    </script>
	
	<form id="formzone" action="manage_match_submit_quick_single.php" method="POST">
		<input type="hidden" name="matchid" id="matchid" value="">
		<input type="hidden" name="matchstatus" id="matchstatus" value="">
		<input type="hidden" name="status" id="status" value="">
		<input type="hidden" name="team1score" id="team1score" value="">
		<input type="hidden" name="team2score" id="team2score" value="">
		<input type="hidden" name="isExtra" id="isExtra" value="">
		<input type="hidden" name="team1scoreET" id="team1scoreET" value="">
		<input type="hidden" name="team2scoreET" id="team2scoreET" value="">
		<input type="hidden" name="isPK" id="isPK" value="">
		<input type="hidden" name="team1scorePK" id="team1scorePK" value="">
		<input type="hidden" name="team2scorePK" id="team2scorePK" value="">
		<input type="hidden" name="inputodds" id="inputodds" value="">
		<input type="hidden" name="inputteamodds" id="inputteamodds" value="">
		<input type="hidden" name="tvList0" id="tvList0" value="">
		<input type="hidden" name="tvList1" id="tvList1" value="">
		<input type="hidden" name="tvList2" id="tvList2" value="">
	</form>
		<hr>
		เลือกดู
		<select id="filter_status" name="filter_status">
			<option value="-1">ดูทั้งหมด</option>
			<option value="0" style="background-color: #FFCC8F">เฉพาะข้อมูลปิด</option>
			<option value="1" style="background-color: #BDE6C1">เฉพาะข้อมูลเปิด</option>
		</select>
		<hr>
		เลือกลีกที่ต้องการแสดงผล<br>
		โซน
		<select name="filterzone" id="filterzone">
			<option value="0">ทั้งหมด</option>
			<?php
				$dataZone = $collectionZone->find();
				$dataZone->sort(array( 'NameEN' => 1 ));
				$countZone		=	$dataZone->count();
				$dataZone->next();
				for( $i=0 ; $i<$countZone ; $i++ )
				{
					$tmpZone 	= 	$dataZone->current();
					$zonename 	= 	empty($tmpZone['NameTH']) 	? $tmpZone['NameEN'] 	: $tmpZone['NameTH'];
					$strValue 	= 	$tmpZone['id'];
					echo '<option value="' . $strValue . '" >' . $zonename . '</option>';
					$dataZone->next();
				}
			?>
		</select>
		ลีก <select name="filterleague" id="filterleague">
			<option value="0">ทั้งหมด</option>
			<?php
				$dataZone = $collectionZone->find();
				$dataZone->sort(array( 'NameEN' => 1 ));
				$countZone		=	$dataZone->count();
				$dataZone->next();
				for( $i=0 ; $i<$countZone ; $i++ )
				{
					$tmpZone 	= 	$dataZone->current();
					$dataLeague = 	$collectionLeague->find( array( 'KPZoneID' => $tmpZone['id'] ) );
					$dataLeague->sort(array( 'NameEN' => 1 ));
					$countLeague		=	$dataLeague->count();
					$dataLeague->next();
					for( $j=0 ; $j<$countLeague ; $j++ )
					{
						$tmpLeague 	= 	$dataLeague->current();
						$zonename 	= 	empty($tmpZone['NameTH']) 	? $tmpZone['NameEN'] 	: $tmpZone['NameTH'];
						$leaguename = 	empty($tmpLeague['NameTH']) ? $tmpLeague['NameEN'] 	: $tmpLeague['NameTH'];
						$strValue 	= 	$tmpLeague['id'];
						echo '<option value="' . $strValue . '" >' . $zonename . '->' . $leaguename . '</option>';
						
						$dataLeague->next();
					}
					$dataZone->next();
				}
			?>
		</select>
		
        <script language="JavaScript">
			document.getElementById('YYYY_lower').value = '<?php echo $dateArr_lower[0];?>';
			document.getElementById('MM_lower').value = '<?php echo $dateArr_lower[1];?>';
			document.getElementById('DD_lower').value = '<?php echo $dateArr_lower[2];?>';
			document.getElementById('YYYY_upper').value = '<?php echo $dateArr_upper[0];?>';
			document.getElementById('MM_upper').value = '<?php echo $dateArr_upper[1];?>';
			document.getElementById('DD_upper').value = '<?php echo $dateArr_upper[2];?>';
			document.getElementById('YYYY').value = '<?php echo $dateArr_lower[0];?>';
			document.getElementById('MM').value = '<?php echo $dateArr_lower[1];?>';
			document.getElementById('DD').value = '<?php echo $dateArr_lower[2];?>';
			document.getElementById('filterleague').value = '<?php echo empty($_SESSION['filterleague']) ? 0 : $_SESSION['filterleague'];?>';
			document.getElementById('filter_status').value = '<?php echo !(isset($_SESSION['filter_status'])) ? -1 : $_SESSION['filter_status'];?>';
			
			var radios = document.getElementsByName('time_scape');
			radios[<?php echo $_SESSION['time_scape']; ?>].checked = true;
			
			checkEnable(<?php echo $_SESSION['time_scape']; ?>);
			
            function LoadMatch(){
				var radios = document.getElementsByName('time_scape');

				for (var i = 0, length = radios.length; i < length; i++) {
					if (radios[i].checked) {
						if (radios[i].value == '0')
							window.location='?filter_status='+document.getElementById('filter_status').value+'&time_scape=0&date_lower='+document.getElementById('YYYY_lower').value+'-'+document.getElementById('MM_lower').value+'-'+document.getElementById('DD_lower').value+'&date_upper='+document.getElementById('YYYY_upper').value+'-'+document.getElementById('MM_upper').value+'-'+document.getElementById('DD_upper').value+'&filterleague='+document.getElementById('filterleague').value;
						else if (radios[i].value == '1')
							window.location='?filter_status='+document.getElementById('filter_status').value+'&time_scape=1&date='+document.getElementById('YYYY').value+'-'+document.getElementById('MM').value+'-'+document.getElementById('DD').value+'&filterleague='+document.getElementById('filterleague').value;
						else if (radios[i].value == '2')
							window.location='?filter_status='+document.getElementById('filter_status').value+'&time_scape=2&filterleague='+document.getElementById('filterleague').value;
						break;
					}
				}				
            }
        </script>
        <input type="button" value="OK" onclick="LoadMatch();">
        <hr>
		

	<!--$data['TVLiveList']	-->
	<form action="manage_match_submit_quick_multi.php" method="POST">
	<input type="submit" value="บันทึกรวม">
	<table border="1" style="font-family: tahoma; font-size: 10px;">
	<?php
        
        $ColorArr['1'][0]='#BDE6C1';
        $ColorArr['0'][0]='#FFCC8F';
		
		$ColorArr['1'][1]='#8EAD91';
        $ColorArr['0'][1]='#B89264';
		
		$dataMongo 			= 	$collection->find($FindArr)->sort(array( 'MatchDateTimeMongo' => -1 ));
		$countMongo			=	$dataMongo->count();
		$dataMongo->next();
	?>
		<tr>
			<td><b>ID</b></td>
			<td><b>XscoreID</b></td>
			<td><b>DateTime</b></td>
			<td><b>Zone Name</b></td>
			<td colspan="3"><b>League Name</b></td>
			<td><b>Team 1</b></td>
			<td colspan="3"><b>score</b></td>
			<td><b>Team 2</b></td>
			<td><b>Match Status</b></td>
			<td><b>Status</b></td>
			<td><b>บันทึก?</b></td>
			<td><b>รายละเอียด</b></td>
			<td><b>Link</b></td>
		</tr>
		<?php
		for( $i=0 ; $i<$countMongo ; $i++ )
		{
			$data 	= 	$dataMongo->current();
			echo '<tr bgcolor="'.$ColorArr[$data['Status']][$i%2].'";>';
			
				// ID
				echo '<td rowspan="3">';
					echo $data['id'];
				echo '</td>';
				
				// XscoreID
				echo '<td>';
					echo $data['XSMatchID'];
				echo '</td>';
				
				// DateTime
				echo '<td>';
					echo $data['MatchDateTime'];

				echo '</td>';
				
				// Zone Name
				echo '<td>';
					$dataZone			= 	$collectionZone->findOne(array( 'id' => $data['KPLeagueCountryID'] ));
					if(!empty($dataZone))
					{	if(empty($dataZone['NameTH']))
							echo $dataZone['NameEN'];
						else
							echo $dataZone['NameTH'];
					}
				echo '</td>';
				
				// League Name
				echo '<td colspan="3">';
					$dataLeague		= 	$collectionLeague->findOne(array( 'id' => $data['KPLeagueID'] ));
					if(!empty($dataLeague))
					{	if(empty($dataLeague['NameTH']))
							echo $dataLeague['NameEN'];
						else
							echo $dataLeague['NameTH'];
					}
				echo '</td>';

				// Team 1
				echo '<td rowspan="3">';
					echo '<a href="manage_team_detail.php?id='. $data['Team1KPID'] .'" target="_blank">';
					$dataTeam1		= 	$collectionTeam->findOne(array( 'id' => $data['Team1KPID'] ));
					if(!empty($dataTeam1))
					{	if(empty($dataTeam1['NameTH']))
							echo $dataTeam1['NameEN'];
						else
							echo $dataTeam1['NameTH'];
					}
					echo '</a>';
				echo '</td>';
				
				// Score Team 1
				echo '<td align="right">';
					echo '<input type="input" id="ScoreT1M'. $data['id'] .'" name="ScoreT1M['. $data['id'] .']" value="'. $data['Team1FTScore'] .'" size="3" style="text-align: right;">';
				echo '</td>';
				// 90' Full Time
				echo '<td>';
					echo '<center>เวลาปกติ</center>';
				echo '</td>';	
				// Score Team 2
				echo '<td>';
					echo '<input type="input" id="ScoreT2M'. $data['id'] .'" name="ScoreT2M['. $data['id'] .']" value="'. $data['Team2FTScore'] .'" size="3" >';
				echo '</td>';
				
				// Team 2
				echo '<td rowspan="3">';
					echo '<a href="manage_team_detail.php?id='. $data['Team2KPID'] .'" target="_blank">';
					$dataTeam2		= 	$collectionTeam->findOne(array( 'id' => $data['Team2KPID'] ));
					if(!empty($dataTeam2))
					{	if(empty($dataTeam2['NameTH']))
							echo $dataTeam2['NameEN'];
						else
							echo $dataTeam2['NameTH'];
					}
					echo '</a>';
				echo '</td>';
				
				// Match Status		
				echo '<td>';
					echo '<select name="matchstatus['. $data['id'] .']" id="matchstatus'. $data['id'] .'">';			
						for( $j=0,$sizeList=count($list_matchstatus) ; $j<$sizeList ; $j++ )
						{
							echo '<option value="'. $list_matchstatus[$j] .'" ';
							echo ($list_matchstatus[$j]==$data['MatchStatus']) ? 'selected' : '';
							echo '>'. $list_matchstatus[$j] .'</option>';
						}
					echo '</select>';
				echo '</td>';
				// Show Status
				echo '<td>';
					echo '<select name="status['. $data['id'] .']" id="status'. $data['id'] .'">';
						echo '<option value="0" ';
						echo empty($data['Status']) ? 'selected' : '';
						echo '>ปิด</option>';
						echo '<option value="1" ';
						echo empty($data['Status']) ? '' : 'selected';
						echo '>เปิด</option>';
					echo '</select>';
				echo '</td>';
				// Quick Save
				echo '<td>';
					echo '<input type="button" value="บันทึกด่วน"  onclick="myFunction('.$data['id'].')">';
				echo '</td>';
				// Detail
				echo '<td>';
					echo '<a href="manage_match_detail.php?id='. $data['id'] .'" target="_blank">รายละเอียด</a>';
				echo '</td>';
				// See in website
				echo '<td>';
					$matchURL = strtolower('http://football.kapook.com/match-'.$data['id'].'-'. $dataTeam1['NameEN'] .'-'.$dataTeam2['NameEN']);
					echo '<a href="' . preg_replace('/\s+/', '-', $matchURL) . '" target="_blank">ไปยังเว็บไซด์</a>';
				echo '</td>';
				
			echo '</tr>';
			
			echo '<tr bgcolor="'.$ColorArr[$data['Status']][$i%2].'";>';
			
				echo '<td colspan="6">';
					echo '<center><b>ช่องการถ่ายทอดสด</b><center>';
				echo '</td>';
				
				//Extra Time	
				$dataETscore	=	explode('-',$data['ETScore']);
				// Score Team 1
				echo '<td align="right">';
					echo '<input type="input" id="team1scoreET'. $data['id'] .'" size="3" name="team1scoreET['. $data['id'] .']" dir="rtl" value="';
					echo $dataETscore[0];
					echo '" >';
				echo '</td>';
				
				// is Extra?
				echo '<td>';
					echo '<center><input name="isExtra['. $data['id'] .']" id="isExtra'. $data['id'] .'" type="checkbox" value="1" ';
					if(strlen($data['ETScore'])>1)
						echo 'CHECKED';
					echo ' onclick="checkEnableExtra('. $data['id'] .')"> ต่อเวลาพิเศษ</center>';
				echo '</td>';				
				
				// Score Team 2
				echo '<td>';
					echo '<input type="input" id="team2scoreET'. $data['id'] .'" size="3" name="team2scoreET['. $data['id'] .']" value="';
					echo $dataETscore[1];
					echo '" >';
				echo '</td>';
				
				echo '<td colspan="5">';
					echo '<center><b>อัตราต่อรอง</b><center>';
				echo '</td>';
				
			echo '</tr>';
			

			echo '<tr bgcolor="'.$ColorArr[$data['Status']][$i%2].'";>';
				
				// TV List
				echo '<td colspan="2">';
					
					echo '<select name="TVLiveList['. $data['id'] .'][0]" id="TVLiveList'. $data['id'] .'_0">';
						echo '<option value="">-------------------</option>';
						foreach ($list_TV as $sublistTV) {
							foreach($sublistTV['listTV'] as $k => $v){
								echo '<option value="' . $k . '" ';
								if($data['TVLiveList'][0]==$k){
									echo 'selected';
								}
								echo '>' . $v . '</option>';
							}
						}
					echo '</select>';
				echo '</td>';
				
				echo '<td colspan="2">';
					echo '<select name="TVLiveList['. $data['id'] .'][1]" id="TVLiveList'. $data['id'] .'_1">';
						echo '<option value="">-------------------</option>';
						foreach ($list_TV as $sublistTV) {
							foreach($sublistTV['listTV'] as $k => $v){
								echo '<option value="' . $k . '" ';
								if($data['TVLiveList'][1]==$k){
									echo 'selected';
								}
								echo '>' . $v . '</option>';
							}
						}
					echo '</select>';
				echo '</td>';
				
				echo '<td colspan="2">';
					echo '<select name="TVLiveList['. $data['id'] .'][2]" id="TVLiveList'. $data['id'] .'_2">';
						echo '<option value="">-------------------</option>';
						foreach ($list_TV as $sublistTV) {
							foreach($sublistTV['listTV'] as $k => $v){
								echo '<option value="' . $k . '" ';
								if($data['TVLiveList'][2]==$k){
									echo 'selected';
								}
								echo '>' . $v . '</option>';
							}
						}
					echo '</select>';
				echo '</td>';
				
				//PK
				$dataPKscore	=	explode('-',$data['PNScore']);
				// Score Team 1
				echo '<td align="right">';
					echo '<input type="input" id="team1scorePK'. $data['id'] .'" size="3" name="team1scorePK['. $data['id'] .']" dir="rtl" value="';
					echo $dataPKscore[0];
					echo '" >';
				echo '</td>';
				
				// is PK?
				echo '<td>';
					echo '<center><input name="isPK['. $data['id'] .']" id="isPK'. $data['id'] .'" type="checkbox" value="1" ';
					if(strlen($data['PNScore'])>1)
						echo 'CHECKED';
					echo ' onclick="checkEnablePK('. $data['id'] .')"> ดวลจุดโทษ</center>';
				echo '</td>';				
				
				// Score Team 2
				echo '<td>';
					echo '<input type="input" id="team2scorePK'. $data['id'] .'" size="3" name="team2scorePK['. $data['id'] .']" value="';
					echo $dataPKscore[1];
					echo '" >';
				echo '</td>';
				
				echo '<td colspan="2">';
					echo 'ทีมที่ต่อ : <br>';
					echo '<select name="inputteamodds['. $data['id'] .']" id="inputteamodds'. $data['id'] .'">';
						if(isset($dataTeam['TeamOdds']))
							$dataTeam['TeamOdds'] = 0;
						?>
						<option value="0"> - - เลือกทีม - - </option>
						<option value="1"<?php echo ((int)$data['TeamOdds']==1) ? 'selected' : ''; ?> >
						<?php 
							$dataTeam		= 	$collectionTeam->findOne(array( 'id' => $data['Team1KPID'] ));
							if(!empty($dataTeam))
							{
								if(empty($dataTeam['NameTH']))
									echo $dataTeam['NameEN'];
								else
									echo $dataTeam['NameTH'];
							} ?>
						</option>
						<option value="2"<?php echo ((int)$data['TeamOdds']==2) ? 'selected' : ''; ?> >
						<?php 
							$dataTeam		= 	$collectionTeam->findOne(array( 'id' => $data['Team2KPID'] ));
							if(!empty($dataTeam))
							{
								if(empty($dataTeam['NameTH']))
									echo $dataTeam['NameEN'];
								else
									echo $dataTeam['NameTH'];
							} ?>
						</option>
						<?php
					echo '</select>';
				echo '</td>';
				
				echo '<td colspan="3">';
					echo 'อัตราต่อรอง : <br>';
					echo '<select name="inputodds['. $data['id'] .']" id="inputodds'. $data['id'] .'">';
						echo '<option value="-1.0">-- select --</option>';
						if(!isset($data['Odds']))
							$data['Odds'] = -1.0;
						foreach ($listOdd as $k => $v) {
							echo '<option value="' . (float)$k . '" ';
							if((float)$data['Odds']===(float)$k)
							{
								echo 'selected';
							}
							echo '>' . $v . '</option>';
						}
					echo '</select>';
				echo '</td>';
				
				
			echo '</tr>';
			
			?>
			<script language="JavaScript">
				if(!document.getElementById('isExtra<?php echo $data['id']; ?>').checked)
				{
					document.getElementById('team1scoreET<?php echo $data['id']; ?>').disabled = true;
					document.getElementById('team2scoreET<?php echo $data['id']; ?>').disabled = true;
				}
				
				if(!document.getElementById('isPK<?php echo $data['id']; ?>').checked)
				{
					document.getElementById('team1scorePK<?php echo $data['id']; ?>').disabled = true;
					document.getElementById('team2scorePK<?php echo $data['id']; ?>').disabled = true;
				}
				
				$("#filterzone").change( function(){
					var Url;
					if( $("#filterzone").val() == 0 ){
						Url = 'http://202.183.165.189/api/get_league_by_zone.php';
					}else{
						Url = 'http://202.183.165.189/api/get_league_by_zone.php?id=' + $('#filterzone').val();
					}
					
					$.ajax({
						url: Url,
						type: "POST",
						dataType: "json",
						success: function(data) {
							$("#filterleague").empty();
							$("#filterleague").append('<option value="0">ทั้งหมด</option>');
							for(var i=0 ; i < data.count ; i++ ){
								if(data.list[i].NameTH.length>0){
									$("#filterleague").append('<option value="' + data.list[i].id + '">' + data.list[i].XSLeagueCountry + ':' + data.list[i].NameTH + '</option>');
								}else{
									$("#filterleague").append('<option value="' + data.list[i].id + '">' + data.list[i].XSLeagueCountry + ':' + data.list[i].NameEN + '</option>');
								}
							}
						}
					});
				});
			</script>
			<?php
			
			$dataMongo->next();
		}
		?>
	<table>
	<input type="submit" value="บันทึกรวม">
	</form>
	
	*/ ?>