<?php header( "refresh: 0; url=http://football.kapook.com/api/adminfootball/league" ); /*
	include 'manage_checklogin.php';
	include 'manage_index.php';
	
	// init MongoDB
	$connectMongo 		= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB	=	$connectMongo->selectDB("football");
	$collection			=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone		=	new MongoCollection($DatabaseMongoDB,"football_zone");
	
	if(		(isset($_POST['leagueid']))
		&& 	(isset($_POST['nameth'])) 
		&& 	(isset($_POST['status'])) 
		&& 	(isset($_POST['autostatus']))
		&& 	(isset($_POST['namethshort']))
	)
	{
		$dataMongo 		= 	$collection->findOne(array( 'id' => (int)$_POST['leagueid'] ));
		if(!empty($dataMongo))
		{
			$dataupdate = array(
				'NameTH' 		=> 	$_POST['nameth'],
				'Status' 		=> 	(int)$_POST['status'],
				'AutoStatus' 	=> 	(int)$_POST['autostatus'],
				'NameTHShort'	=>	$_POST['namethshort']
			);
			
			if(empty($_POST['priority']))
				$dataupdate['Priority'] = 99;
			else if(!is_numeric($_POST['priority']))
				$dataupdate['Priority'] = 99;
			else if(is_float($_POST['priority']))
				$dataupdate['Priority'] = round($_POST['priority']);
			else
				$dataupdate['Priority'] = (int)$_POST['priority'];
				
		
			$collection->update(
				array('id' => (int)$_POST['leagueid']),
				array('$set' => $dataupdate)
			);
		}
	}
	
	if(!empty($_POST['L']))
	{
		foreach($_POST['L'] as $k => $value)
		{
			
			$dataupdate = array(
				'NameTH' 		=> 	$value,
				'Status' 		=> 	(int)$_POST['status'][$k],
				'AutoStatus' 	=> 	(int)$_POST['autostatus'][$k],
				'NameTHShort'	=>	$_POST['LS'][$k]
			);
			
			if(empty($_POST['LP'][$k]))
				$dataupdate['Priority'] = 99;
			else if(!is_numeric($_POST['LP'][$k]))
				$dataupdate['Priority'] = 99;
			else if(is_float($_POST['LP'][$k]))
				$dataupdate['Priority'] = round($_POST['LP'][$k]);
			else
				$dataupdate['Priority'] = (int)$_POST['LP'][$k];
		
			$collection->update(
				array('id' => (int)$k),
				array('$set' => $dataupdate)
			);
		}
	}
	
	?>
	
	<script type='text/javascript'>
		function myFunctionTeam(id){
			document.getElementById("filter_team").value		=	id;
			document.getElementById("gototeam").submit();
		}
		
		function myFunction(id){
		
			nameTH=document.getElementById('L' + id ).value;
			nameTHShort=document.getElementById('LS' + id ).value;
			status=document.getElementById('status' + id ).value;
			autostatus=document.getElementById('autostatus' + id ).value;
			priority=document.getElementById('LP' + id ).value;
			
			document.getElementById("leagueid").value		=	id;
			document.getElementById("nameth").value			=	nameTH;
			document.getElementById("status").value			=	status;
			document.getElementById("autostatus").value		=	autostatus;
			document.getElementById("namethshort").value	=	nameTHShort;
			document.getElementById("priority").value		=	priority;
			
			document.getElementById("formzone").submit();
			
		}
	</script>
	เลือกดู : 
	<form id="filter" action="manage_league.php" method="GET">
		<select id="filter_status" name="filter_status">
			<option value="-1">ดูทั้งหมด</option>
			<option value="0" style="background-color: #FFCC8F">เฉพาะข้อมูลปิด</option>
			<option value="1" style="background-color: #BDE6C1">เฉพาะข้อมูลเปิด</option>
		</select>
	โซนที่ต้องการดู : 	
		<select id="filter_zone" name="filter_zone">
			<option value="">ทั้งหมด</option>
			<?php
				$dataZone = $collectionZone->find();
				$dataZone->sort(array( 'NameEN' => 1 ));
				$countZone		=	$dataZone->count();
				$dataZone->next();
				for( $i=0 ; $i<$countZone ; $i++ )
				{
					$tmpZone 	= 	$dataZone->current();
					$zonename 	= 	empty($tmpZone['NameTH']) 	? $tmpZone['NameEN'] 	: $tmpZone['NameTH'];
					echo '<option value="' . $tmpZone['id'] . '" ';
					echo '>' . $zonename . '</option>';

					$dataZone->next();
				}
			?>
		</select>
		
		<input type="submit" value="เลือก">
	</form>
	
	<form id="formzone" action="#" method="POST">
		<input type="hidden" name="leagueid" id="leagueid" value="">
		<input type="hidden" name="nameth" id="nameth" value="">
		<input type="hidden" name="status" id="status" value="">
		<input type="hidden" name="autostatus" id="autostatus" value="">
		<input type="hidden" name="namethshort" id="namethshort" value="">
		<input type="hidden" name="priority" id="priority" value="">
		<?php
			if(isset($_REQUEST['filter_status']))
			{
				?>
				<input type="hidden" name="filter_status" id="filter_status" value="<?php echo $_REQUEST['filter_status'];?>">
				<?php
			}

			if(isset($_REQUEST['filter_zone']))
			{
				?>
				<input type="hidden" name="filter_zone" id="filter_zone" value="<?php echo $_REQUEST['filter_zone'];?>">
				<?php
			}
		?>
	</form>
	
	<form id="gototeam" action="manage_team.php" method="POST">
		<input type="hidden" name="filter_team" id="filter_team" value="">
	</form>

	<?php
		$arr_filter = array();
		if(isset($_REQUEST['filter_status']))
		{
			?>
			<script language="JavaScript">
				document.getElementById('filter_status').value = '<?php echo $_REQUEST['filter_status'];?>';
			</script>
			<?php
			if((int)$_REQUEST['filter_status']!=-1)
				$arr_filter['Status'] = (int)$_REQUEST['filter_status'];
		}
		
		if(isset($_REQUEST['filter_zone']))
		{
			?>
			<script language="JavaScript">
				document.getElementById('filter_zone').value = '<?php echo $_REQUEST['filter_zone'];?>';
			</script>
			<?php
			if((int)$_REQUEST['filter_zone']!=0)
				$arr_filter['KPZoneID'] = (int)$_REQUEST['filter_zone'];
		}
	
		$dataMongo 			= 	$collection->find($arr_filter);
		$dataMongo->sort(array('id' => -1));
		$countMongo			=	$dataMongo->count();
		$dataMongo->next();
		
		echo 'Data Amount : ' . $countMongo . '<br>';
	
	?>
	<hr>

	<form action="manage_league.php" method="POST">
	<input type="submit" value="บันทึกรวม">
	<table border="1" style="font-family: tahoma; font-size: 10px;">
	<?php
		if(isset($_REQUEST['filter_status']))
		{
			?>
			<input type="hidden" name="filter_status" id="filter_status" value="<?php echo $_REQUEST['filter_status'];?>">
			<?php
		}

		if(isset($_REQUEST['filter_zone']))
		{
			?>
			<input type="hidden" name="filter_zone" id="filter_zone" value="<?php echo $_REQUEST['filter_zone'];?>">
			<?php
		}

		$ColorArr['1']='#BDE6C1';
		$ColorArr['0']='#FFCC8F';
	?>
		<tr>
			<td><b>Priority</b></td>
			<td><b>ID</b></td>
			<td><b>zone name</b></td>
			<td><b>league nameEN</b></td>
			<td><b>league nameTH</b></td>
			<td><b>league nameTH Short</b></td>
			<td><b>Description</b></td>
			<td><b>Year</b></td>
			<td><b>status</b></td>
			<td><b>auto-status</b></td>
			<td><b>บันทึก?</b></td>
			<td><b>ดู/แก้ไขทีมในลีก</b></td>
		</tr>
	<?php
	for( $i=0 ; $i<$countMongo ; $i++ )
	{
		$data 	= 	$dataMongo->current();
		echo '<tr bgcolor="'.$ColorArr[$data['Status']].'";>';
			// priority
			echo '<td>';
				echo '<input type="input" maxlength="3" size="3" id="LP'. $data['id'] .'" name="LP['. $data['id'] .']" value="'. $data['Priority'] .'" >';
			echo '</td>';
		
			// id
			echo '<td>';
				echo $data['id'];
			echo '</td>';
			
			// zone name(Thai)
			echo '<td>';
				$dataZone			= 	$collectionZone->findOne(array( 'id' => $data['KPZoneID'] ));
				if(!empty($dataZone))
				{	if(empty($dataZone['NameTH']))
						echo $dataZone['NameEN'];
					else
						echo $dataZone['NameTH'];
				}
			echo '</td>';
			
			// name Eng
			echo '<td>';
				echo $data['NameEN'];
			echo '</td>';
			
			// name Thai
			echo '<td>';
				echo '<input type="input" id="L'. $data['id'] .'" name="L['. $data['id'] .']" value="'. $data['NameTH'] .'" >';
			echo '</td>';
			
			// name ThaiShort
			echo '<td>';
				echo '<input type="input" id="LS'. $data['id'] .'" name="LS['. $data['id'] .']" value="'. $data['NameTHShort'] .'" >';
			echo '</td>';
			
			// Description
			echo '<td>';
				echo $data['Description'];
			echo '</td>';
			
			// Year
			echo '<td>';
				echo $data['Year'];
			echo '</td>';
			
			// status
			echo '<td>';
				echo '<select name="status['. $data['id'] .']" id="status'. $data['id'] .'">';
					echo '<option value="0" ';
					echo empty($data['Status']) ? 'selected' : '';
					echo '>ปิด</option>';
					echo '<option value="1" ';
					echo empty($data['Status']) ? '' : 'selected';
					echo '>เปิด</option>';
				echo '</select>';
			echo '</td>';
			
			// autostatus
			echo '<td>';
				echo '<select name="autostatus['. $data['id'] .']" id="autostatus'. $data['id'] .'">';
					echo '<option value="0" ';
					echo empty($data['AutoStatus']) ? 'selected' : '';
					echo '>ปิด</option>';
					echo '<option value="1" ';
					echo empty($data['AutoStatus']) ? '' : 'selected';
					echo '>เปิด</option>';
				echo '</select>';
			echo '</td>';
			
			// submit button
			echo '<td>';
				echo '<input type="button" value="บันทึก"  onclick="myFunction('.$data['id'].')">';
			echo '</td>';
			
			// team view
			echo '<td>';
				echo '<input type="button" value="ดูทีม"  onclick="myFunctionTeam(\'' . $data['id'] . '\')">';
			echo '</td>';
			
		echo '</tr>';
		
		$dataMongo->next();
	}
	?>
	<table>
	<input type="submit" value="บันทึกรวม">
	</form>
	
*/ ?>