<?php
	$data = array();
	
	$listCountry	=	array(
		'1'		=>	array(
			'name_th' => 'อังกฤษ',
			'name_en' => 'England'
		),
		'2'		=>	array(
			'name_th' => 'อิตาลี',
			'name_en' => 'Italy'
		),
		'3'		=>	array(
			'name_th' => 'สเปน',
			'name_en' => 'Spain'
		),
		'4'		=>	array(
			'name_th' => 'เยอรมัน',
			'name_en' => 'Germany'
		),
		'6'		=>	array(
			'name_th' => 'ยูฟ่า แชมเปี้ยนลีก',
			'name_en' => 'UEFA Champions League'
		),
		'8'		=>	array(
			'name_th' => 'ไทย',
			'name_en' => 'Thai'
		),
		'10'	=>	array(
			'name_th' => 'ผรั่งเศส',
			'name_en' => 'France'
		),
		'11'	=>	array(
			'name_th' => 'ยูโรป้า ลีก',
			'name_en' => 'UEFA Europa League'
		),
		'12'	=>	array(
			'name_th' => 'เดอะ แชมเปี้ยนชิพ',
			'name_en' => 'The Championship'
		),
		'13'	=>	array(
			'name_th' => 'ตลาดซื้อขายนักเตะ',
			'name_en' => 'Transfer'
		),
		
		'_all'	=>	array(
			'name_th' => 'ทั้งหมด',
			'name_en' => 'All'
		),
	);
	
	$listCup	=	array(
		'aff'		=>	array(
			'name_th' 		=> 		'เอเอฟเอฟ ซูซุกิ คัพ',
			'name_en' 		=> 		'AFF Suzuki Cup',
			'name_short'	=>		'aff'
		),
		'euro2016'		=>	array(
			'name_th' 		=> 		'ฟุตบอลชิงแชมป์แห่งชาติยุโรป 2016',
			'name_en' 		=> 		'UEFA Euro 2016',
			'name_short'	=>		'euro2016'
		),
	);
	
	function err_print( $errorType )
	{
		switch($errorType)
		{
			case 1 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'news_id is not numeric'
			);
			break;
			case 2 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'Content Not Found'
			);
			break;
			case 3 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'page_size is not numeric'
			);
			break;
			case 4 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'page_size is not integer'
			);
			break;
			case 5 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'page_size < 0'
			);
			break;
			case 6 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'page_no is not numeric'
			);
			break;
			case 7 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'page_no is not integer'
			);
			break;
			case 8 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'page_no < 0'
			);
			break;
			case 9 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'sort value is not valid'
			);
			break;
			case 10 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'country value is not valid'
			);
			break;
			case 11 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'not_id is not numeric'
			);
			break;
			case 12 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'not_id is not integer'
			);
			break;
			case 13 : $data = array(
				'status_code' 	=> 520,
				'error_msg'		=> 'not_id < 0'
			);
			break;
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	// init MySQL
	$connect = mysql_connect('192.168.1.206', 'football','zxcvbgfdsa');
	//$connect = mysql_connect('localhost','root');
	if (!$connect) {
		die('Could not connect: ' . mysql_error());
	}
	mysql_select_db('kapookfootball', $connect);
	//mysql_query("SET NAMES TIS620");
	
	// News Detail
	if(isset($_GET['news_id']))
	{
		if(!is_numeric($_GET['news_id']))
		{
			err_print(1);
			mysql_close($connect);
			exit;
		}
		
		$sql 	= 	'SELECT * FROM kapookfootball_News WHERE kapookfootball_News_ID=' . $_GET['news_id'];
		$result = 	mysql_query($sql);
		if(mysql_num_rows($result)<=0)
		{
			err_print(2);
			mysql_close($connect);
			exit;
		}
		else
		{
			$thumb_l = null;
			$thumb_s = null;
			$row = mysql_fetch_assoc($result);
			// prepare a picture info
			if((!empty($row['kapookfootball_News_ThumbnailPicture']))&&(fopen('http://football.kapook.com/uploadnew/news/thumbnail/' . $row['kapookfootball_News_ThumbnailPicture'],'r')))
			{
				$thumb_l['src'] = 'http://football.kapook.com/uploadnew/news/thumbnail/' . $row['kapookfootball_News_ThumbnailPicture'];
				
				$datapic = getimagesize($thumb_l['src']);
				$thumb_l['width'] = $datapic['0'];
				$thumb_l['height'] = $datapic['1'];
			}
			else
			{
				$thumb_l['src'] = null;
				$thumb_l['width'] = 0;
				$thumb_l['height'] = 0;
			}
			// Large picture
			if((!empty($row['kapookfootball_News_PictureFile']))&&(fopen('http://football.kapook.com/uploadnew/news/picture/' . $row['kapookfootball_News_PictureFile'],'r')))
			{
				$thumb_s['src'] = 'http://football.kapook.com/uploadnew/news/picture/' . $row['kapookfootball_News_PictureFile'];
				
				$datapic = getimagesize($thumb_s['src']);
				$thumb_s['width'] = $datapic['0'];
				$thumb_s['height'] = $datapic['1'];
			}
			else
			{
				$thumb_s['src'] = null;
				$thumb_s['width'] = 0;
				$thumb_s['height'] = 0;
			}
			
			// content
			if(!empty($row['kapookfootball_News_Detail']))
				$content = $row['kapookfootball_News_Detail'];
			else if(empty($row['kapookfootball_News_HTMLFileName']))
				$content = null;
			else if(fopen('http://football.kapook.com/uploadnew/news/htmlfiles/' . $row['kapookfootball_News_HTMLFileName'],'r'))
				$content = file_get_contents('http://football.kapook.com/uploadnew/news/htmlfiles/' . $row['kapookfootball_News_HTMLFileName']);
			
			$jsonRelate = file_get_contents('http://202.183.165.189/api/news.php?team_news=' . $row['kapookfootball_News_CountryOtherKeyword3'] . '&page_size=5&not_id=' . $row['kapookfootball_News_ID']);
			//$jsonRelate = file_get_contents('http://localhost/football/api/news.php?team_news=' . $row['kapookfootball_News_CountryOtherKeyword3'] . '&page_size=5&not_id=' . $row['kapookfootball_News_ID']);
			$arrayRelate = json_decode($jsonRelate, true);
		
			$data = array(
				'rowContent' => array(
					0 => array(
						'subject' 	=> iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Subject'] ),
						'title'		=> iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Title'] ),
						'question'		=> iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Question'] ),
						'content' 	=> iconv('TIS-620', 'UTF-8//IGNORE', $content ),
						'link'		=> 'http://football.kapook.com/news_inside.php?id='. $_GET['news_id'] .'&key=news'
					)
				),
				'CreateDate'		=>	strtotime ( $row['kapookfootball_News_CreateDate'] ),
				'LastUpdate'		=>	strtotime ( $row['kapookfootball_News_LastUpdate'] ),
				'VDOPremium'		=> 	str_replace('&#039;','"',htmlspecialchars_decode($row['kapookfootball_News_VDO_Premium'])),
				'attr' => array(
					/*'kapookfootball_News_ModuleKey' 			=> $row['kapookfootball_News_ModuleKey'],
					'kapookfootball_News_Language' 				=> $row['kapookfootball_News_Language'],
					'kapookfootball_News_Country' 				=> $row['kapookfootball_News_Country'],
					'kapookfootball_News_CountryOtherKeyword' 	=> $row['kapookfootball_News_CountryOtherKeyword'],
					'kapookfootball_News_CountryOtherKeyword2' 	=> $row['kapookfootball_News_CountryOtherKeyword2'],
					'kapookfootball_News_CountryOtherKeyword3' 	=> $row['kapookfootball_News_CountryOtherKeyword3'],
					'kapookfootball_News_Team' 					=> $row['kapookfootball_News_Team'],
					'kapookfootball_News_HilightPage' 			=> (int)$row['kapookfootball_News_HilightPage'],
					'kapookfootball_News_BreakNews' 			=> (int)$row['kapookfootball_News_BreakNews'],
					'kapookfootball_News_ShowNews' 				=> (int)$row['kapookfootball_News_ShowNews'],
					'kapookfootball_News_Zonenews' 				=> (int)$row['kapookfootball_News_Zonenews'],
					'kapookfootball_News_Group' 				=> $row['kapookfootball_News_Group'],
					'kapookfootball_News_Status' 				=> $row['kapookfootball_News_Status'],
					'kapookfootball_News_Order' 				=> (int)$row['kapookfootball_News_Order'],
					'kapookfootball_News_EuroHilight' 			=> (int)$row['kapookfootball_News_EuroHilight'],
					'kapookfootball_News_URL' 					=> $row['kapookfootball_News_URL'],
					'kapookfootball_News_VDO_Premium' 			=> $row['kapookfootball_News_VDO_Premium'],
					'kapookfootball_News_HTMLFileName' 			=> iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_HTMLFileName'] )*/
				),
				'relate' =>	$arrayRelate['data'],
				'picture' => array(
					'thumb_l' => empty($thumb_l) ? array() : $thumb_l,
					'thumb_s' => empty($thumb_s) ? array() : $thumb_s
				),
				'seo' => array(
					'fb' => array(
						'title' => empty($row['kapookfootball_News_SubjectSMO']) 	? iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Subject'] ) : iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_SubjectSMO'] ),
						'des' 	=> empty($row['kapookfootball_News_TitleSMO']) 	? iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Title'] ) : iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_TitleSMO'] ),
						'img' 	=> empty($thumb_s) ? null : $thumb_s['src']
					),
					'meta' => array(
						'title' 	=> empty($row['kapookfootball_News_SubjectSMO']) 	? iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Subject'] ) : iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_SubjectSMO'] ),
						'des' 		=> empty($row['kapookfootball_News_TitleSMO']) 	? iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Title'] ) : iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_TitleSMO'] ),
						'tag'		=> empty($row['kapookfootball_News_KeywordSEO']) 	? '' : iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_KeywordSEO'] ),
						'thumnail'	=> empty($thumb_s) ? null : $thumb_s['src'],
						'truehits'	=> null
					)
				),
				'portal' => array(
					'content_id'	=> 	(int)$row['kapookfootball_News_ID'],
					'portal_id' 	=> 	27,
					'zone_id'		=>	null
				),
				'Views'				=>	(int)$row['kapookfootball_News_View']
			);
			
		}
	}
	// List News
	else
	{
		// Parameter $_GET
		// page_size
		$size = isset($_GET['page_size']) ? $_GET['page_size'] : 12 ;
		if(!is_numeric($size))
		{
			err_print(3);
			mysql_close($connect);
			exit;
		}
		if((is_string($size))&&(!ctype_digit($size)))
		{
			err_print(4);
			mysql_close($connect);
			exit;
		}
		if((!is_string($size))&&(is_float($size)))
		{
			err_print(4);
			mysql_close($connect);
			exit;
		}
		if($size < 0)
		{
			err_print(5);
			mysql_close($connect);
			exit;
		}
		else if($size > 100)
			$size = 100;
	
		// page_no
		$page = isset($_GET['page_no']) ? $_GET['page_no'] : 1 ;
		if(!is_numeric($page))
		{
			err_print(6);
			mysql_close($connect);
			exit;
		}
		if((is_string($page))&&(!ctype_digit($page)))
		{
			err_print(7);
			mysql_close($connect);
			exit;
		}
		if((!is_string($page))&&(is_float($page)))
		{
			err_print(7);
			mysql_close($connect);
			exit;
		}
		if($page<1)
		{
			err_print(8);
			mysql_close($connect);
			exit;
		}

		// sort
		$sort = 'ORDER BY kapookfootball_News_ID DESC';
		if(!empty($_GET['sort']))
		{
			switch($_GET['sort'])
			{
				case 1  : $sort = 'ORDER BY kapookfootball_News_ID DESC';
						  break;
				case 2  : $sort = 'ORDER BY kapookfootball_News_ID ASC';
						  break;
				case 3  : $sort = 'ORDER BY kapookfootball_News_View DESC';
						  break;
				case 4  : $sort = 'ORDER BY kapookfootball_News_View ASC';
						  break;
				default : err_print(9);
						  mysql_close($connect);
						  exit;
			}
		}
		
		$where = array();
		
		// country & cup[tagKeyword2]
		if(isset($_GET['country_news']))
		{
			$country = $_GET['country_news'];
			if(empty($listCountry[$country]))
			{
				err_print(10);
				mysql_close($connect);
				exit;
			}
			else
			{
				$where[]='( (FIND_IN_SET("'. $country .'", kapookfootball_News_Country) > 0) OR (FIND_IN_SET("0", kapookfootball_News_CountryOtherKeyword3) > 0) )';
				$data['country_news'] = $listCountry[$country];
			}
		}else if($_GET['cup_news']){
			$cup = $_GET['cup_news'];
			if(empty($listCup[$cup])){
				err_print(10);
				mysql_close($connect);
				exit;
			}
			else{
				$where[]='( (kapookfootball_News_CountryOtherKeyword2 LIKE "%' . $cup . '%") OR (FIND_IN_SET("0", kapookfootball_News_CountryOtherKeyword3) > 0) )';
				$data['cup_news'] = $listCup[$cup];
			}
		}else{
			$data['country_news'] = $listCountry['_all'];
		}
			
		// team (keyword03) (for related news)
		if(isset($_GET['team_news']))
		{
			$whereArr = array();
			
			$team = explode( ',' , $_GET['team_news'] );
			for( $i=0,$max=count($team) ; $i<$max ; $i++)
			{
				if(is_numeric($team[$i]))
				{
					if($team[$i] == 0)
					{
						$whereArr	=	null;
						break;
					}
					$whereArr[]='(FIND_IN_SET("'. $team[$i] .'", kapookfootball_News_CountryOtherKeyword3) > 0)';
				}
			}
			$whereArr[]='(FIND_IN_SET("0", kapookfootball_News_CountryOtherKeyword3) > 0)';
			
			if(!empty($whereArr))
			{
				$where[] = '(' . implode(' OR ',$whereArr) . ')';
			}
		}

		// not show by id(for related news)
		if(isset($_GET['not_id']))
		{
			if(!is_numeric($_GET['not_id']))
			{
				err_print(11);
				mysql_close($connect);
				exit;
			}
			if((is_string($_GET['not_id']))&&(!ctype_digit($_GET['not_id'])))
			{
				err_print(12);
				mysql_close($connect);
				exit;
			}
			if((!is_string($_GET['not_id']))&&(is_float($_GET['not_id'])))
			{
				err_print(12);
				mysql_close($connect);
				exit;
			}
			if($_GET['not_id']<1)
			{
				err_print(13);
				mysql_close($connect);
				exit;
			}
			
			$where[]='kapookfootball_News_ID <> ' . $_GET['not_id'];
			
		}
			
		// type search
		if(!empty($_GET['type']))
			$type = strtolower($_GET['type']);
		else
			$type = null;
		// setting a query
		if( $type == 'top10' )
		{
			$sort 				= 	'ORDER BY kapookfootball_News_ID DESC';
			$where[]			=	'(FIND_IN_SET("top10", kapookfootball_News_CountryOtherKeyword2) > 0)';
		}
		else if( $type == 'tophit' )
		{
			$sort 				= 	'ORDER BY kapookfootball_News_View DESC';
			$where[]			=	'kapookfootball_News_CreateDate >= "' . date('Y/m/d 00:00:00', strtotime("-3 days")) . '"';
		}
		
		// Query it!!
		if(count($where))
			$sql 			= 	'SELECT * FROM kapookfootball_News WHERE ('. implode( ' AND ' , $where ) .') '. $sort .' LIMIT ' . (($page-1)*$size) . ',' . $size;
		else
			$sql 			= 	'SELECT * FROM kapookfootball_News '. $sort .' LIMIT ' . (($page-1)*$size) . ',' . $size;
		//echo $sql;
		$result 			= 	mysql_query($sql);
		$count				=	mysql_num_rows($result);
		
		if($count<=0)
		{
			err_print(2);
			mysql_close($connect);
			exit;
		}
		else
		{
			if(count($where))
				$sqlMax 			= 	'SELECT count(*) as max FROM kapookfootball_News WHERE '. implode( ' AND ' , $where );
			else
				$sqlMax 			= 	'SELECT count(*) as max FROM kapookfootball_News';
				
			$resultMax 			= 	mysql_query($sqlMax);
			$rowMax			 	= 	mysql_fetch_assoc($resultMax);
			$countAll			=	(int)$rowMax['max'];
		
			$data['status_code']	=	200;
			$data['total_rows']		=	$countAll;
			$data['total_pages']	=	ceil($countAll/$size);
			$data['paged']			=	$page;
			$data['rows']			=	$count;

			
			while ($row = mysql_fetch_assoc($result)) {
                            
                                if($row['kapookfootball_News_URL']==''){ 
                                    $URL='http://football.kapook.com/news-'. $row['kapookfootball_News_ID'];                             
                                }else{
                                    $URL=$row['kapookfootball_News_URL'];   
                                }
                                
				$data['data'][] = array(
					'_id'				=>	$row['kapookfootball_News_ID'],
					'content_id'		=>	$row['kapookfootball_News_ID'],
					'zone_id'			=>	'',
					'portal_id'			=>	27,
					'subject'			=>	iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Subject'] ),
					'title'				=>	iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_Title'] ),
					'thumbnail'			=>	empty($row['kapookfootball_News_ThumbnailPicture']) ? '' : 'http://football.kapook.com/uploadnew/news/thumbnail/' . $row['kapookfootball_News_ThumbnailPicture'],
					'picture'			=>	empty($row['kapookfootball_News_PictureFile']) ? '' : 'http://football.kapook.com/uploadnew/news/picture/' . $row['kapookfootball_News_PictureFile'],
					'link'				=> $URL,
					'CreateDate'		=>	strtotime ( $row['kapookfootball_News_CreateDate'] ),
					'LastUpdate'		=>	strtotime ( $row['kapookfootball_News_LastUpdate'] ),
					'Views'				=>	$row['kapookfootball_News_View'],
					'attr'				=>	array(/*
						'kapookfootball_News_ModuleKey' 			=> $row['kapookfootball_News_ModuleKey'],
						'kapookfootball_News_Language' 				=> $row['kapookfootball_News_Language'],
						'kapookfootball_News_Country' 				=> $row['kapookfootball_News_Country'],
						'kapookfootball_News_CountryOtherKeyword' 	=> $row['kapookfootball_News_CountryOtherKeyword'],
						'kapookfootball_News_CountryOtherKeyword2' 	=> $row['kapookfootball_News_CountryOtherKeyword2'],
						'kapookfootball_News_CountryOtherKeyword3' 	=> $row['kapookfootball_News_CountryOtherKeyword3'],
						'kapookfootball_News_Team' 					=> $row['kapookfootball_News_Team'],
						'kapookfootball_News_HilightPage' 			=> (int)$row['kapookfootball_News_HilightPage'],
						'kapookfootball_News_BreakNews' 			=> (int)$row['kapookfootball_News_BreakNews'],
						'kapookfootball_News_ShowNews' 				=> (int)$row['kapookfootball_News_ShowNews'],
						'kapookfootball_News_Zonenews' 				=> (int)$row['kapookfootball_News_Zonenews'],
						'kapookfootball_News_Group' 				=> $row['kapookfootball_News_Group'],
						'kapookfootball_News_Status' 				=> $row['kapookfootball_News_Status'],
						'kapookfootball_News_Order' 				=> (int)$row['kapookfootball_News_Order'],
						'kapookfootball_News_EuroHilight' 			=> (int)$row['kapookfootball_News_EuroHilight'],
						'kapookfootball_News_URL' 					=> $row['kapookfootball_News_URL'],
						'kapookfootball_News_VDO_Premium' 			=> $row['kapookfootball_News_VDO_Premium'],
						'kapookfootball_News_HTMLFileName' 			=> iconv('TIS-620', 'UTF-8//IGNORE', $row['kapookfootball_News_HTMLFileName'] )
					*/)
				);
			}
		}
	}
	header('Content-Type: application/json');
	echo json_encode($data);
	mysql_close($connect);
?>