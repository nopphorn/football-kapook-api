<?php
	// init memcache
	$memcache 	=	new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;

	$teaminfo_MC = $memcache->get( 'Football2014-teamInfo-'.$_GET['id'] );

	if(($teaminfo_MC)&&($_GET['clear']!='1')){
		$teaminfo	= 	$teaminfo_MC;
	}
	else
	{
		// init MongoDB
		$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
		$DatabaseMongoDB		=	$connectMongo->selectDB("football");
		$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
		
		$dataTeam 				= 	$collectionTeam->findOne( array('id' => (int)$_GET['id'] ) );
		
		if(!$dataTeam)
		{
			$teaminfo	=	array(
				'id'	=>	(int)$_GET['id'],
				'error'	=>	1,
				'text'	=>	'Error:Not found.'
			);
			$memcache->set( 'Football2014-teamInfo-'.$_GET['id'] , null , MEMCACHE_COMPRESSED, $expire );
		}
		else
		{
			$teaminfo['id']				=	(int)$_GET['id'];
			$teaminfo['NameEN']			=	$dataTeam['NameEN'];
			$teaminfo['NameTH']			=	$dataTeam['NameTH'];
			$teaminfo['NameTHShort']	=	$dataTeam['NameTHShort'];
			
			$Logo 						= 	str_replace(' ','-',$dataTeam['NameEN']).'.png';
			$Logo_MC					=	$memcache->get('Football2014-Team-Logo-' . $Logo);
			if($Logo_MC){
				$teaminfo['Logo'] 		= 	'http://football.kapook.com/uploads/logo/' . $Logo;
			}else{
				$teaminfo['Logo'] 		= 	'http://football.kapook.com/uploads/logo/default.png';
			}
			
			$teaminfo['Info']			=	empty($dataTeam['Info']) 			? null 		: 	$dataTeam['Info'];
			$teaminfo['EmbedCode']		=	empty($dataTeam['EmbedCode']) 		? null 		: 	$dataTeam['EmbedCode'];
			
			$teaminfo['DuplicateList']	=	empty($dataTeam['DuplicateList']) 	? array()	:	$dataTeam['DuplicateList'];
			
			$memcache->set( 'Football2014-teamInfo-'.$_GET['id'] , $teaminfo , MEMCACHE_COMPRESSED, $expire );
		}
	}
	
	echo json_encode($teaminfo);
	
?>