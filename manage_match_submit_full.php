<?php
session_start();
	include 'manage_checklogin.php';

	
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collection             =	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionLeague		=	new MongoCollection($DatabaseMongoDB,"football_league");
	$collectionZone			=	new MongoCollection($DatabaseMongoDB,"football_zone");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");

		$dataMongo 		= 	$collection->findOne(array( 'id' => (int)$_POST['matchid'] ));
		if(!empty($dataMongo))
		{
		
			//---------------------------------------------------------------------------------------------//
			// Scorers Data
			//---------------------------------------------------------------------------------------------//
			if(isset($_REQUEST['inputNameScorers'])){
				$size	=	count($_REQUEST['inputNameScorers']);
				for( $i=0 ; $i < $size ; $i++ ){
					//var_dump($_REQUEST['inputTeamScorers']);
					if(($_REQUEST['inputTeamScorers'][$i]!='1')&&($_REQUEST['inputTeamScorers'][$i]!='2')){
						echo 'Error:Missing a team selected in scorers.';
						echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
						exit;
					}
					
					//var_dump($_REQUEST['inputPN']);
					
					echo array_search($i, $_REQUEST['inputPN']);
					
					$addword	=	'';
					if(in_array($i, $_REQUEST['inputET'])){
						$addword	=	$addword . 'ET';
					}
					
					if(in_array($i, $_REQUEST['inputPN'])){
						$addword	=	$addword . 'Pen';
					}
					
					if(in_array($i, $_REQUEST['inputOwnGoal'])){
						$addword	=	$addword . 'Own';
					}
					
					if($_REQUEST['inputTeamScorers'][$i]=='1'){
						$_REQUEST['inputNameScorers'][$i]	=	'(' . $addword . $_REQUEST['inputMinScorers'][$i] . ')' . $_REQUEST['inputNameScorers'][$i];
					}else{
						$_REQUEST['inputNameScorers'][$i]	=	$_REQUEST['inputNameScorers'][$i] . '(' . $addword . $_REQUEST['inputMinScorers'][$i] . ')';
					}
				}
			}else{
				$_REQUEST['inputNameScorers']	=	array();
			}
		
			$listfile 		= 	array();
			$listnewfile 	= 	array();
			$extension 		=	array();
			
			/*
			 * Upload a news picture.
			 */
			//---------------------------------------------------------------------------------------------//
			for( $i=0,$sizepic=count($_FILES['fileUpload']['error']) ; $i<$sizepic ; $i++ )
			{
				if	(	($_FILES['fileUpload']['error'][$i] != 4) &&
						($_FILES['fileUpload']['error'][$i] != 0)
				)
				{
					echo 'Error:Cannot Upload File.<br>';
					echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
					exit;
				}
				
				if($_FILES['fileUpload']['error'][$i] == 0)
				{
					if 	(	($_FILES['fileUpload']['type'][$i] != 'image/gif')
						&& 	($_FILES['fileUpload']['type'][$i] != 'image/jpeg')
						&& 	($_FILES['fileUpload']['type'][$i] != 'image/jpg')
						&& 	($_FILES['fileUpload']['type'][$i] != 'image/pjpeg')
						&& 	($_FILES['fileUpload']['type'][$i] != 'image/x-png')
						&& 	($_FILES['fileUpload']['type'][$i] != 'image/png')
					)
					{
						echo 'Error:Wrong type file.<br>';
						echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
						exit;
					}
					
					// gif
					if($_FILES['fileUpload']['type'][$i] == 'image/gif')
					{
						$extension[]	=	'gif';
					}
					
					// jpeg
					if(	($_FILES['fileUpload']['type'][$i] == 'image/jpeg')
					||	($_FILES['fileUpload']['type'][$i] == 'image/jpg')
					||	($_FILES['fileUpload']['type'][$i] == 'image/pjpeg')
					)
					{
						$extension[]	=	'jpg';
					}
					
					// png
					if(	($_FILES['fileUpload']['type'][$i] == 'image/x-png')
					||	($_FILES['fileUpload']['type'][$i] == 'image/png'))
					{
						$extension[]	=	'png';
					}
				
					$listfile[] 	= 	$_FILES['fileUpload']['tmp_name'][$i];
				}
				
				// Check directory for upload
				if(!is_dir('../uploads/match/' . $_POST['matchdate']))
				{
					if(!mkdir('../uploads/match/' . $_POST['matchdate'],0777,true))
					{
						echo 'Error:Cannot Create a directory.<br>';
						echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
						exit;
					}
				}
			}
			// loop upload
				
			for( $i=0,$sizepic=count($listfile) ; $i<$sizepic ; $i++ )
			{
				$gtod = gettimeofday();
				$usec = $gtod['usec'];
					
				$filename = $_POST['matchid'] . '-' . date('YmdHis') . (int)$usec . '.' . $extension[$i];
				if(!move_uploaded_file($listfile[$i], '../uploads/match/' . $_POST['matchdate'] . '/' . $filename ))
				{
					echo 'Error:Cannot Upload File.<br>';
					echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
					exit;
				}
				$listnewfile[] = $filename;
			}
			//---------------------------------------------------------------------------------------------//
			/*
			 * Delete a picture.
			 */
			//---------------------------------------------------------------------------------------------//
			if(!empty($_POST['listDeletePicture']))
			{
				for($i=0,$sizedelete=count($_POST['listDeletePicture']) ; $i<$sizedelete ; $i++)
				{
					$indexDelete = (int)$_POST['listDeletePicture'][$i];
					if(!empty($dataMongo['Picture'][$indexDelete]))
					{
						unlink('../uploads/match/' . $_POST['matchdate'] . '/' . $dataMongo['Picture'][$indexDelete]);
						unset($dataMongo['Picture'][$indexDelete]);
					}
				}
			}
			//---------------------------------------------------------------------------------------------//
			
			// Merge a old and new picture.
			if(!empty($dataMongo['Picture']))
				$listnewfile = array_merge($dataMongo['Picture'],$listnewfile);
			//---------------------------------------------------------------------------------------------//
			/*
			 * Delete a pictureOG.
			 */
			//---------------------------------------------------------------------------------------------//
			if($_POST['listDeleteOG']==1)
			{
				echo 'testttttttttttttttttttttttttt';
				unlink('../uploads/match/og/' . $dataMongo['PictureOG']);
				$dataMongo['PictureOG']		=	null;
			}
			//---------------------------------------------------------------------------------------------//
			/*
			 * Upload a og picture.
			 */
			//---------------------------------------------------------------------------------------------//
			if(isset($_FILES['fileog']))
			{
				if	(	($_FILES['fileog']['error'] != 4) &&
						($_FILES['fileog']['error'] != 0)
				)
				{
					echo 'Error:Cannot Upload File.<br>';
					echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
					exit;
				}
					
				if($_FILES['fileog']['error'] == 0)
				{
					if 	(	($_FILES['fileog']['type'] != 'image/gif')
						&& 	($_FILES['fileog']['type'] != 'image/jpeg')
						&& 	($_FILES['fileog']['type'] != 'image/jpg')
						&& 	($_FILES['fileog']['type'] != 'image/pjpeg')
						&& 	($_FILES['fileog']['type'] != 'image/x-png')
						&& 	($_FILES['fileog']['type'] != 'image/png')
					)
					{
						echo 'Error:Wrong type file.<br>';
						echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
						exit;
					}

					// gif
					if($_FILES['fileog']['type'] == 'image/gif')
					{
						$extensionOG	=	'gif';
					}
						
					// jpeg
					if(	($_FILES['fileog']['type'] == 'image/jpeg')
					||	($_FILES['fileog']['type'] == 'image/jpg')
					||	($_FILES['fileog']['type'] == 'image/pjpeg')
					)
					{
						$extensionOG	=	'jpg';
					}
						
					// png
					if(	($_FILES['fileog']['type'] == 'image/x-png')
					||	($_FILES['fileog']['type'] == 'image/png'))
					{
						$extensionOG	=	'png';
					}
					
					$newfileOG 	= 	$_FILES['fileog']['tmp_name'];
					
					// Check directory for upload
					if(!is_dir('../uploads/match/og'))
					{
						if(!mkdir('../uploads/match/og',0777,true))
						{
							echo 'Error:Cannot Create a directory.<br>';
							echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
							exit;
						}
					}
					// loop upload
					$filenameOG = $_POST['matchid'] . '.' . $extensionOG;
					if(!move_uploaded_file($newfileOG, '../uploads/match/og/' . $filenameOG ))
					{
						echo '../uploads/match/og/' . $filenameOG ;
						echo 'Error:Cannot Upload File.<br>';
						echo '<a href="manage_match_detail.php?id='.$_POST['matchid'].'">กลับไปแก้ไข</a>';
						exit;
					}
				}
			}
			//---------------------------------------------------------------------------------------------//
			// Setting a data 
			//---------------------------------------------------------------------------------------------//
			
			$collection->update(
				array('id' => (int)$_POST['matchid']),
				array('$set' => array(
					'Team1FTScore' 	=> 	(int)$_POST['team1score'],
					'Team2FTScore' 	=> 	(int)$_POST['team2score'],
					'MatchStatus'	=> 	$_POST['matchstatus'],	
					'Status' 		=> (int)$_POST['status'],
					'TVLiveList'	=>	empty($_POST['inputBroadCastChannel']) 	? null 	: 	$_POST['inputBroadCastChannel'],
					'Analysis'		=>	empty($_POST['analysis']) 				? null 	: 	$_POST['analysis'],
					'Predict'		=>	empty($_POST['predict']) 				? null 	: 	$_POST['predict'],
					'question'		=>	empty($_POST['question']) 				? null 	: 	$_POST['question'],
					'TeamSelect'	=>	empty($_POST['inputteamselect']) 		? 0 	: 	(int)$_POST['inputteamselect'],
					'TeamOdds'		=>	empty($_POST['inputteamodds']) 			? 0 	: 	(int)$_POST['inputteamodds'],
					'Odds'			=>	empty($_POST['inputodds']) 				? 0.0 	: 	(float)$_POST['inputodds'],
					'Result'		=>	empty($_POST['result']) 				? null 	: 	$_POST['result'],
					'Picture'		=>	count($listnewfile)<=0					? null	:	$listnewfile,
					'PictureOG'		=>	isset($filenameOG)						? $filenameOG : $dataMongo['PictureOG'],
					'EmbedCode'		=>	empty($_POST['embedCode']) 				? null 	: 	$_POST['embedCode'],
					'ETScore'		=>	empty($_POST['isExtra'])				? '-'	:	(int)$_POST['team1scoreET'] . '-' . (int)$_POST['team2scoreET'],
					'PNScore'		=>	empty($_POST['isPK'])					? '-'	:	(int)$_POST['team1scorePK'] . '-' . (int)$_POST['team2scorePK'],
					'LastScorers'	=>	$_REQUEST['inputNameScorers'],
					'LastUpdate_IP'		=>	$ip,
					'LastUpdate_UserID'	=>	isset($_COOKIE["UserID"]) 			? intval($_COOKIE["UserID"]) : "UNKNOWN",
					'LastUpdate_Time'	=>	date('Y-m-d H:i:s')
				))
			);
			//var_dump($_REQUEST['inputNameScorers']);
			// clear memcache
			$memcache->delete('Football2014-MatchContents-' . $_POST['matchid']);
		}
		
		header( "location: " . $_SERVER['HTTP_REFERER'] );
		exit(0);
?>