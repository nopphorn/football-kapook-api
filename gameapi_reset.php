<?php
//exit;
session_start();

$authorized = false;

if(isset($_GET['logout']) && ($_SESSION['auth'])) {
	$_SESSION['auth'] = null;
	session_destroy();
	echo "logging out...";
	?>
	<META HTTP-EQUIV="Refresh" CONTENT="5;URL=http://football.kapook.com/" />
	<?php
}

if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = 'football';
	$pass = 'cityunited';
	if (($user == $_SERVER['PHP_AUTH_USER']) && ($pass == ($_SERVER['PHP_AUTH_PW'])) && (!empty($_SESSION['auth']))) {
		$authorized = true;
	}
}

if ((! $authorized)) {
	header('WWW-Authenticate: Basic Realm="Login please"');
	header('HTTP/1.0 401 Unauthorized');
	$_SESSION['auth'] = true;
	print('Login now or forever hold your clicks...');
	exit;
}

else{

	//init 
	$game						=	50;
	
	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	
	$collectionMatch        	=	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionGame				=	new MongoCollection($DatabaseMongoDB,"football_game");
	$collectionMember			=	new MongoCollection($DatabaseMongoDB,"football_member");
	$collectionReward			=	new MongoCollection($DatabaseMongoDB,"football_reward");
	
	$collectionRanking_all		=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	$collectionRanking_season	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	$collectionRanking_month	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	$collectionRanking_day		=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	
	//remove all record in member
	$collectionMember->remove(array());
	
	//remove all record in all ranking
	$collectionRanking_all->remove(array());
	$collectionRanking_season->remove(array());
	$collectionRanking_month->remove(array());
	$collectionRanking_day->remove(array());
	
	//set all subgame status form 2 -> 1
	/*$FindArrGameType['listGame']	=	array(
		'$elemMatch'	=>	array(
			'status' 	=>	2,
		)
	);*/
	$FindArrGameType['game_type']	=	2;
	$FindArrGameType['status']		=	array( '$gte' => 1 );
	
	
	$dataGame					= 	$collectionGame->find($FindArrGameType);
	$countGame					=	$dataGame->count();
	$dataGame->next();
		
	//echo $countGame;
	
	for( $i=0 ; $i<$countGame ; $i++ )
	{
		$rowGame				=	$dataGame->current();
		$subgame				=	$rowGame['listGame'];
		$countSubGame			=	count($subgame);
		for( $j=0 ; $j<$countSubGame ; $j++ )
		{
			$subgame[$j]['status']		=	1;
			unset($subgame[$j]['isWDL']);
		}
		
		$collectionGame->update(
			array(
				'id' 	=> (int)$rowGame['id']
			),
			array('$set' =>
				array(
					'status'		=>	1,
					'point_return'	=> 	0.0,
					'multiple'		=>	1.0,
					'listGame'		=>	$subgame
				)
			)
		);
		
		$dataGame->next();
	}
	
	//set all game status form 2 -> 1
	$collectionGame->update(
		array(
			'status'	=>		array( '$gte' => 1 )
		),
		array('$set' =>
			array(
				'status'		=>	1,
				'point_return'	=> 	0.0,
				'multiple'		=>	1.0
			)
		),
		array('multiple' => true)
	);
	
	//set all game status form 1 -> 0
	$collectionMatch->update(
		array(
			'MatchDateTime'	=>		array( '$gte' => '2014-11-05 06:00:00' )
		),
		array('$set' =>
			array(
				'GameStatus'	=>	0
			)
		),
		array('multiple' => true)
	);
	
	//set all reward status form true -> false
	$collectionReward->update(
		array(),
		array('$set' =>
			array(
				'isReward'	=>	false
			)
		),
		array('multiple' => true)
	);
	
	//include 'gameapi_restore_reward.php';

	echo 'Finish';
}
?>