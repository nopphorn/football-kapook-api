<?php
header('Content-Type: application/json');
### Connect To VPS Singapore
        $mongo          = new MongoClient();
        $db             = $mongo->football;  
        
        $memcache = new Memcache;
        $memcache->connect('localhost', 11211);

        $zone           = $db->football_zone;
        $league         = $db->football_league;
        $match          = $db->football_match;
        $team           = $db->football_team;
        $ZoneIDByNameEN_MC = $memcache->get('Football2014-ZoneIDByNameEN');
        if($ZoneIDByNameEN_MC){            
            $ZoneIDByNameEN = $ZoneIDByNameEN_MC;
        }else{
            $AllZone        = $zone->find();
            foreach($AllZone as $Zone){
                $ZoneIDByNameEN[$Zone['NameEN']] = $Zone['id'];
            }
            $expire = (3600*24); 
            $memcache->set('Football2014-ZoneIDByNameEN',$ZoneIDByNameEN, MEMCACHE_COMPRESSED, $expire);
        }
        
        $AllLeague      = $league->find(array('Status'=>intval(1)));
        
        foreach($AllLeague as $League){
            $LeagueIDByXSLeagueID[$League['XSLeagueID']] = $League['id'];
            $EnableLeagueArr[]=$League['id'];

        } 
 

        /*
        $AllTeam        = $team->find();
        foreach($AllTeam as $Team){
            $tmpArr = $Team["PlayingZoneLeagueName"];
            foreach($tmpArr as $PlayingZL){                
                $TeamMappingIDArr[$PlayingZL.':'.$Team['NameEN']] = intval($Team['id']);
            }
        } */
        
        
        if($_REQUEST['date']==''){ 
echo            $date = date("Y-m-d");
        }else{
            $date = $_REQUEST['date'];
        } 
        

        
       if($_REQUEST['type']=='lastfinished'){
           
            $FindArr['MatchStatus'] = 'Fin';
            
       }else if($_REQUEST['type']=='next'){
           
            $FindArr['MatchStatus'] = 'Sched';   
            $FromDate  = $date.' 06:00:00';
            $ToDate    = date("Y-m-d 05:59:59",strtotime($date . ' + 3 days'));
            
       }else if($_REQUEST['type']=='program'){
           
            if(date('G')<6){
                $date = date("Y-m-d",strtotime($date . ' -1 days'));
            }
            $FromDate  = $date.' 06:00:00';
            $ToDate    = date("Y-m-d 05:59:59",strtotime($date . ' + 5 days'));
            
       }else if($_REQUEST['type']=='league'){    
           
            if(($_REQUEST['from_date']!='') && ($_REQUEST['to_date']!='')){                
                $FromDate  = date("Y-m-d 00:00:00",strtotime($_REQUEST['from_date']));
                $ToDate    = date("Y-m-d 23:59:59",strtotime($_REQUEST['to_date']));
            }else{    
                $date      = date("Y-m-d");
                $FromDate  = date("Y-m-d 06:00:00",strtotime($date . ' - 3 days'));
                $ToDate    = date("Y-m-d 05:59:59",strtotime($date . ' + 15 days'));
            }
            
       }else if($_REQUEST['type']=='league_fin'){             
           
            if(date('G')<12){
                $date = date("Y-m-d",strtotime($date . ' -1 days'));
            }
            $FromDate  = $date.' 06:00:00';
            $ToDate    = date("Y-m-d 05:59:59",strtotime($date . ' + 1 days'));
            $FindArr['MatchStatus'] = 'Fin';
            
       }else if($_REQUEST['type']=='league_sched'){
           
            if(date('G')<12){
                $date = date("Y-m-d",strtotime($date . ' -1 days'));
            }
            $FromDate  = $date.' 06:00:00';
            $ToDate    = date("Y-m-d 05:59:59",strtotime($date . ' + 15 days'));
            //$FindArr['MatchStatus'] = 'Sched';
            
       }else if($_REQUEST['type']==='analysis'){
			//$date      = date("Y-m-d");
            $FromDate  = date("Y-m-d 06:00:00",strtotime($date . ' - 1 days'));
            $ToDate    = date("Y-m-d 05:59:59",strtotime($date . ' + 1 days'));
	   // $FindArr['Analysis'] = array('$ne'=>null,'$exists'=>true);
            $FindArr['$or'] = array(array('Predict' => array('$ne'=>null,'$exists'=>true)));
                        
       }else{
           
            if(($_REQUEST['from_date']!='') && ($_REQUEST['to_date']!='')){
                $FromDate  = date("Y-m-d 00:00:00",strtotime($_REQUEST['from_date']));
                $ToDate    = date("Y-m-d 23:59:59",strtotime($_REQUEST['to_date']));
            }else{
                if($_REQUEST['date']==''){ 
                    $date = date("Y-m-d");
					if(date('G')<6){
						$date = date("Y-m-d",strtotime($date . ' -1 days'));
					}
                }else{
                    $date = $_REQUEST['date'];
                } 

                $FromDate  = $date.' 06:00:00';
                $ToDate    = date("Y-m-d 05:59:59",strtotime($date . ' + 1 day'));
            }
            
       }

        

        //$FindArr['Status'] = intval(1);
        //$FindArr['KPLeagueID'] = array('$in'=>$EnableLeagueArr);
        
        

        
        if(isset($_REQUEST['team_id'])){
			$listTeam['$in']				=	array();
			$tmpTeam						=	explode(',' , $_REQUEST['team_id']);
			
			foreach($tmpTeam as $value){
				if(intval($value)>0){
					$listTeam['$in'][]	=	intval($value);
				}
			}
			$FindArr['$or'] = 	array(  array('Team1KPID' => $listTeam),
										array('Team2KPID' => $listTeam)
								);
			if(($FromDate!='') && ($ToDate!='')){
				$FindArr['MatchDateTime'] = array	( 	'$gte'=>$FromDate,
														'$lte'=>$ToDate
													);
			}else{
				$FindArr['MatchDateTime'] = array('$lte'=>strval(date("Y-m-d H:i:s")));
			}
			$FindArr['Status'] = intval(1);             
            
        }else{
            
			if(($FromDate!='') && ($ToDate!='')){
				$FindArr['MatchDateTime'] = array(
					'$gte'=>$FromDate,
					'$lte'=>$ToDate
				);
			}
			/*
			$FindArr['$or'] = array(
				array('Status' => intval(1)),
				array('KPLeagueID' => array('$in'=>$EnableLeagueArr))
			);
			*/
				
			/*if(intval($_REQUEST['league'])>0){ 
				$FindArr['KPLeagueID'] = intval($_REQUEST['league']);
			}*/
				
			if(isset($_REQUEST['league'])){
				$listLeague['$in']				=		array();
				$tempLeague						=		explode(',' , $_REQUEST['league']);
				foreach($tempLeague as $value){
					if(intval($value)>0){
						$listLeague['$in'][]	=		intval($value);
					}
				}
				$FindArr['KPLeagueID'] 			= 		$listLeague;
			}
			$FindArr['Status'] = intval(1);
        }
        

        if(($_REQUEST['type']=='lastfinished') || (intval($_REQUEST['team_id'])>0)){
            $SortArr['MatchDateTime'] = -1;            
        }else{        
            $SortArr['MatchDateTime'] = 1;
        }
        
        if(intval($_REQUEST['page_size'])>1000){            
            $page_size = 1000;
        }else{
            $page_size = intval($_REQUEST['page_size']); 
        }
        $Query = $match->find($FindArr)->sort($SortArr)->limit($page_size);
        
        
        
        $index=0;
		
		$ResultMobile['data'] = array();
		$ResultMobile['match'] = array();
        
        $Result['FromDate'] = $FromDate;
        $Result['ToDate']   = $ToDate;
        $Result['TotalMatch'] = $Query->count(true);
		
		$ResultMobile['data']['FromDate'] 	= $FromDate;
		$ResultMobile['data']['ToDate']   	= $ToDate;
		$ResultMobile['data']['TotalMatch'] = $Query->count(true);
		

        foreach($Query as $Match){
            $index+=1;

			// Fix Date 
			
			//$tmpSourceDate = $Match['SourceDate'].' '.$Match['SourceTime'].':00';
			//if($tmpSourceDate>="2014-10-26 00:00:00"){
				//$Match['MatchDateTime'] = date("Y-m-d H:i:s",strtotime($tmpSourceDate.' + 5 hour'));
			//}
            // End Fix
			
            $Result[$index]['id'] = $Match['id'];
            $Result[$index]['MatchStatus'] = $Match['MatchStatus'];
            $Result[$index]['MatchDate'] = substr($Match['MatchDateTime'],0,10);
            $Result[$index]['MatchTime'] = substr($Match['MatchDateTime'],11,5);
            $Result[$index]['Minute'] = $Match['Minute'];
            
            
            $Result[$index]['Team1'] = $Match['Team1'];
            $Result[$index]['Team2'] = $Match['Team2'];
			
			$Result[$index]['Team1EN'] = $Match['Team1'];
            $Result[$index]['Team2EN'] = $Match['Team2'];

            $Result[$index]['Team1'] = $memcache->get('Football2014-Team-NameTHShort-'.$Match['Team1KPID']);
            $Result[$index]['Team2'] = $memcache->get('Football2014-Team-NameTHShort-'.$Match['Team2KPID']);
			
			$matchURL = strtolower('http://football.kapook.com/match-'.$Match['id'].'-'.$Match['Team1'].'-'.$Match['Team2']);
            $Result[$index]['MatchPageURL'] = preg_replace('/\s+/', '-', $matchURL);
            
            $Logo1 		= 	str_replace(' ','-',$Match['Team1']).'.png';
			$Logo1_MC	=	$memcache->get('Football2014-Team-Logo-' . $Logo1);
            $Logo2 		= 	str_replace(' ','-',$Match['Team2']).'.png';
			$Logo2_MC	=	$memcache->get('Football2014-Team-Logo-' . $Logo2);
			
            if($Logo1_MC){
                $Result[$index]['Team1Logo'] = 'http://football.kapook.com/uploads/logo/' . $Logo1;
            }else{
                $Result[$index]['Team1Logo'] = 'http://football.kapook.com/uploads/logo/default.png';
            }
            if($Logo2_MC){
                $Result[$index]['Team2Logo'] = 'http://football.kapook.com/uploads/logo/' . $Logo2;
            }else{
                $Result[$index]['Team2Logo'] = 'http://football.kapook.com/uploads/logo/default.png';
            }
            
            $Result[$index]['Team1LP'] = $Match['Team1LP'];
            $Result[$index]['Team2LP'] = $Match['Team2LP'];
            $Result[$index]['Team1YC'] = $Match['Team1YC'];
            $Result[$index]['Team2YC'] = $Match['Team2YC'];
            $Result[$index]['Team1RC'] = $Match['Team1RC'];
            $Result[$index]['Team2RC'] = $Match['Team2RC'];
            $Result[$index]['FTScore'] = $Match['FTScore'];
            $Result[$index]['ETScore'] = $Match['ETScore'];
            $Result[$index]['PNScore'] = $Match['PNScore'];
            $Result[$index]['LastScorers'] = $Match['LastScorers'];
            $Result[$index]['Team1FTScore'] = $Match['Team1FTScore'];
            $Result[$index]['Team2FTScore'] = $Match['Team2FTScore'];
            $Result[$index]['KPLeagueID'] = $Match['KPLeagueID'];
            $Result[$index]['KPLeagueCountryID'] = $Match['KPLeagueCountryID'];
            $Result[$index]['Status'] = $Match['Status'];
            $Result[$index]['Team1KPID'] = $Match['Team1KPID'];
            $Result[$index]['Team2KPID'] = $Match['Team2KPID'];
            $Result[$index]['Analysis'] = $Match['Analysis'];
			$Result[$index]['Result'] = $Match['Result'];
            $Result[$index]['Predict'] = $Match['Predict'];
            $Result[$index]['TVLiveList'] = $Match['TVLiveList'];
            $Result[$index]['TeamOdds'] = $Match['TeamOdds'];
            $Result[$index]['Odds'] = $Match['Odds'];
			$Result[$index]['count_playside_team1'] 	= 	$Match['count_playside_team1'];
			$Result[$index]['count_playside_team2'] 	= 	$Match['count_playside_team2'];
			$Result[$index]['count_playside']		 	= 	$Match['count_playside'];
			
			if(($_REQUEST['type']=='analysis')&&($Match['MatchStatus']=='Fin')&&($Match['Odds']>=0)){
				if(($Match['Odds']>0)&&($Match['TeamOdds']>0))
					$Match['Team'.$Match['TeamOdds'].'FTScore']		=	($Match['Team'.$Match['TeamOdds'].'FTScore']-$Match['Odds']);
				$Result[$index]['Team1FTScoreHandicap'] = $Match['Team1FTScore'];
				$Result[$index]['Team2FTScoreHandicap'] = $Match['Team2FTScore'];
				$Result[$index]['TeamSelect']			= $Match['TeamSelect'];
			
				if(($Match['TeamSelect']==1)&&($Match['Odds']>=0))
				{
					if($Match['Team1FTScore']>$Match['Team2FTScore']){
						$Result[$index]['IsSelectCollect'] = 1;
					}
					else if($Match['Team1FTScore']==$Match['Team2FTScore']){
						$Result[$index]['IsSelectCollect'] = 0;
					}
					else{
						$Result[$index]['IsSelectCollect'] = -1;
					}
				}
				else if(($Match['TeamSelect']==2)&&($Match['Odds']>=0))
				{
					if($Match['Team1FTScore']>$Match['Team2FTScore']){
						$Result[$index]['IsSelectCollect'] = -1;
					}
					else if($Match['Team1FTScore']==$Match['Team2FTScore']){
						$Result[$index]['IsSelectCollect'] = 0;
					}
					else{
						$Result[$index]['IsSelectCollect'] = 1;
					}
				}
            }
            
            $Result['KPLeagueID'][$Match['KPLeagueID']] += 1;
			$Result['KPLeagueZoneID'][$Match['KPLeagueID']] = $Match['KPLeagueCountryID'];
			
			$ResultMobile['data']['KPLeagueID'][$Match['KPLeagueID']] += 1;
			$ResultMobile['data']['KPLeagueZoneID'][$Match['KPLeagueID']] += 1;
			
			if(!isset($Result['KPLeagueNameTH'][$Match['KPLeagueID']])){
				$tmpLeague		=		$league->findOne( array('id'=>intval($Match['KPLeagueID'])) );
				
				$Result['KPLeagueNameTH'][$Match['KPLeagueID']]			=		$tmpLeague['NameTH'];
				$Result['KPLeagueNameTHShort'][$Match['KPLeagueID']]	=		$tmpLeague['NameTHShort'];
				$Result['KPLeagueNameEN'][$Match['KPLeagueID']]			=		$tmpLeague['NameEN'];
				
				$ResultMobile['data']['KPLeagueNameTH'][$Match['KPLeagueID']]			=		$tmpLeague['NameTH'];
				$ResultMobile['data']['KPLeagueNameTHShort'][$Match['KPLeagueID']]		=		$tmpLeague['NameTHShort'];
				$ResultMobile['data']['KPLeagueNameEN'][$Match['KPLeagueID']]			=		$tmpLeague['NameEN'];
				
			}
			$tmpMatchMobile = $Result[$index];
			$Team1ScorersArr = array();
			$Team2ScorersArr = array();
			
			foreach ($Match['LastScorers'] as $ScoreInfo) {
				$ScoreInfo = str_replace('(Own', '(ทำเข้าประตูตัวเอง ', $ScoreInfo);
				$ScoreInfo = str_replace('(Pen', '(จุดโทษ ', $ScoreInfo);
				$ScoreInfo = str_replace(')', '\') ', $ScoreInfo);
				$ScoreInfo = str_replace('+', '\' ทดนาทีที่ ', $ScoreInfo);
				if (substr($ScoreInfo, '0', 1) == '(') {
					$Team1ScorersArr[] = $ScoreInfo;
				} else {
					$Team2ScorersArr[] = $ScoreInfo;
				}
			}
			$tmpMatchMobile['Team1Scorers'] = $Team1ScorersArr;
			$tmpMatchMobile['Team2Scorers'] = $Team2ScorersArr;
			
			$ResultMobile['match'][] = $tmpMatchMobile;
            
            if($_REQUEST['type']=='lastfinished'){
                $memcache->set('Football2014-LastFinish-'.$index,$Result[$index]);
            }else  if($_REQUEST['type']=='next'){
                $memcache->set('Football2014-Next-'.$index,$Result[$index]);
            }
        }
    $tmpOrder = 0;    
    foreach($Result['KPLeagueID'] as $LID => $MTotal){
        $tmpOrder+=1;
        $LeagueOrder = intval($memcache->get('Football2014-League-Order-'.$LID));
        if($LeagueOrder==0){ $LeagueOrder=99; }
        
        if(($tmpArr[$LeagueOrder])>0){
			$tmpArr[($LeagueOrder+$tmpOrder)] = $LID;
		}else{
			$tmpArr[($LeagueOrder)] = $LID;
        }
	}
	$ResultMobile['data']['KPLeagueIDOrder'] = $tmpArr;
    $Result['KPLeagueIDOrder'] = $tmpArr;
    
if($_REQUEST['device']=='mobile'){
    
    if ($_REQUEST['callback'] != '') {
        echo $_REQUEST['callback'] . '(' . json_encode($ResultMobile) . ')';
	} else {
        echo json_encode($ResultMobile);
	}
    
}else{
    $Result['Find'] = $FindArr;
    if ($_REQUEST['callback'] != '') {
        echo $_REQUEST['callback'] . '(' . json_encode($Result) . ')';
    } else {
        echo json_encode($Result);
    }    
}
        
?>
