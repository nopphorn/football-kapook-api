<?php
	header('Content-Type: application/json');
	// init MongoDB
	$connectMongo 			= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	
	if(isset($_REQUEST['year']))
	{
		$indexdata			=	'season';
		$indexsection		=	$_REQUEST['year'];
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_season");
	}
	else if(isset($_REQUEST['month']))
	{
		$indexdata			=	'month';
		$indexsection		=	$_REQUEST['month'];
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_month");
	}
	else if(isset($_REQUEST['date']))
	{
		$indexdata			=	'date';
		$indexsection		=	$_REQUEST['date'];
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_day");
	}
	else
	{
		$indexdata			=	'all-time';
		$indexsection		=	null;
		$collectionRanking	=	new MongoCollection($DatabaseMongoDB,"football_ranking_all");
	}
	
	$uid		=	(int)$_REQUEST['id'];
	
	$findArr['id']					=	$uid;
	if($indexdata!='all-time'){
		$findArr[$indexdata]		=	$indexsection;
	}
	$data		=	$collectionRanking->findOne($findArr);
	
	if(!$data)
	{
		$returnJson		=	array(
			'uid'		=>	$uid,
			'ratetype'	=>	$indexdata,
			'ratetime'	=>	$indexsection,
			'rate'		=>	0.0
		);
	}
	else
	{
		$returnJson		=	array(
			'uid'		=>	$uid,
			'ratetype'	=>	$indexdata,
			'ratetime'	=>	$indexsection,
			'rate'		=>	empty($data['total_rate_calculate']) ? 0.0 : $data['total_rate_calculate']
		);
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>