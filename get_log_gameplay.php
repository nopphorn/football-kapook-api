<?php
	//$start_date['day']		=	1;
	//$start_date['month']	=	8;

	session_start();
	// init memcache
	$memcache = new Memcache;
	$memcache->connect('localhost', 11211) or die ("Could not connect");
	$expire		=	3600;
	
	header('Content-Type: application/json');
	// init MongoDB
	$connectMongo 			=	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB		=	$connectMongo->selectDB("football");
	$collectionMatch		=	new MongoCollection($DatabaseMongoDB,"football_match");
	$collectionTeam			=	new MongoCollection($DatabaseMongoDB,"football_team");
	$collectionGame			=	new MongoCollection($DatabaseMongoDB,"football_game");

	if(!isset($_REQUEST['start_date'])){
		$_REQUEST['start_date']		=		date('Y-m-d',strtotime('-7 day'));
	}
	
	if(!isset($_REQUEST['end_date'])){
		$_REQUEST['end_date']		=		date('Y-m-d');
	}else{
		$_REQUEST['end_date']		=		date('Y-m-d',strtotime($_REQUEST['end_date'] . ' 00:00:00'));
	}
	
	$FindArr['date_play']			=		array('$gte' => $_REQUEST['start_date'] , '$lte' => $_REQUEST['end_date']);
	
	if(!isset($_REQUEST['size'])){
		$_REQUEST['size']			=		50;
	}
	
	if(!isset($_REQUEST['start'])){
		$_REQUEST['start']			=		0;
	}
	
	if(!empty($_REQUEST['username'])){
	
		$ProfileArr								=		$memcache->get( 'Football2014-member-byname-'.$_REQUEST['username'] );
				
		if(!$ProfileArr){
			$APIProfile 						= 		'http://kapi.kapook.com/profile/member/username/' . $_REQUEST['username'];
			$ProfileArr 						=		json_decode(file_get_contents($APIProfile), true);

			if($ProfileArr){
				$memcache->set( 'Football2014-member-byname-'.$_REQUEST['username'] , $ProfileArr , MEMCACHE_COMPRESSED, (60*60*24) );
			}
		}
		
		if(isset($ProfileArr['data']['userid'])){
			$memcache->set( 'Football2014-member-'.$ProfileArr['data']['userid'] , $ProfileArr , MEMCACHE_COMPRESSED, (60*60*24) );
			$FindArr['user_id']			=		$ProfileArr['data']['userid'];
		}else{
			$FindArr['user_id']			=		-1;
		}

	}
	
	$dataMongo 						= 		$collectionGame->find($FindArr)
														   ->sort(array( 'timestamp' => -1 ))
														   ->skip($_REQUEST['start'])
														   ->limit($_REQUEST['size']);
	$returnJson['total_record']		=		$dataMongo->count();
	$returnJson['total_display']	=		$dataMongo->count(true);
	$returnJson['record_start']		=		intval($_REQUEST['start']);
	
	foreach($dataMongo as $tmpGame){
		$sizeMulti	=	count($tmpGame['listGame']);
		
		$ProfileArr							=		$memcache->get( 'Football2014-member-'.$tmpGame['user_id'] );			
		if(!$ProfileArr){
			$APIProfile 					= 		'http://kapi.kapook.com/profile/member/userid/' . $tmpGame['user_id'];
			$ProfileArr 					=		json_decode(file_get_contents($APIProfile), true);
			if($ProfileArr){
				$memcache->set( 'Football2014-member-'.$data['id'] , $ProfileArr , MEMCACHE_COMPRESSED, (60*60*24) );
				$memcache->set( 'Football2014-member-byname-'.$ProfileArr['data']['username'] , $ProfileArr , MEMCACHE_COMPRESSED, (60*60*24) );
			}
		}
		$tmpGame['username']				=		$ProfileArr['data']['username'];
		
		for( $j=0 ; $j<$sizeMulti ; $j++ ){
						
			$dataPlayTmp					=		$tmpGame['listGame'][$j];
					
			// Match Data
			if(!isset($dataMatch[$dataPlayTmp['match_id']])){
				$dataMatch[$dataPlayTmp['match_id']]		=		$collectionMatch->findOne(	array(	'id' =>	$dataPlayTmp['match_id']));
			}
			
			// Team1 data
			$dataTeam1						=	$memcache->get( 'Football2014-teamInfo-'.$dataMatch[$dataPlayTmp['match_id']]['Team1KPID'] );
			
			if(!$dataTeam1){
				
				$tmpTeam					= 	$collectionTeam->findOne(array('id' => $dataMatch[$dataPlayTmp['match_id']]['Team1KPID']));
				
				$dataTeam1['id']			=	$dataMatch[$dataPlayTmp['match_id']]['Team1KPID'];
				$dataTeam1['NameEN']		=	$tmpTeam['NameEN'];
				$dataTeam1['NameTH']		=	$tmpTeam['NameTH'];
				$dataTeam1['NameTHShort']	=	$tmpTeam['NameTHShort'];
			
				$Logo 						= 	str_replace(' ','-',$tmpTeam['NameEN']).'.png';
				$Logo_MC					=	$memcache->get('Football2014-Team-Logo-' . $Logo);
				if($Logo_MC){
					$dataTeam1['Logo'] 		= 	'http://football.kapook.com/uploads/logo/' . $Logo;
				}else{
					$dataTeam1['Logo'] 		= 	'http://football.kapook.com/uploads/logo/default.png';
				}
			
				$dataTeam1['Info']			=	empty($tmpTeam['Info']) 		? null : $tmpTeam['Info'];
				$dataTeam1['EmbedCode']		=	empty($tmpTeam['EmbedCode']) 	? null : $tmpTeam['EmbedCode'];
			
				$memcache->set( 'Football2014-teamInfo-'.$dataMatch[$dataPlayTmp['match_id']]['Team1KPID'] , $dataTeam1 , MEMCACHE_COMPRESSED, $expire );
			}

			// Team2 data
			$dataTeam2						=	$memcache->get( 'Football2014-teamInfo-'.$dataMatch[$dataPlayTmp['match_id']]['Team2KPID'] );
			if(!$dataTeam2){
				
				$tmpTeam					= 	$collectionTeam->findOne(array('id' => $dataMatch[$dataPlayTmp['match_id']]['Team2KPID']));
				
				$dataTeam2['id']			=	$dataMatch[$dataPlayTmp['match_id']]['Team2KPID'];
				$dataTeam2['NameEN']		=	$tmpTeam['NameEN'];
				$dataTeam2['NameTH']		=	$tmpTeam['NameTH'];
				$dataTeam2['NameTHShort']	=	$tmpTeam['NameTHShort'];
			
				$Logo 						= 	str_replace(' ','-',$tmpTeam['NameEN']).'.png';
				$Logo_MC					=	$memcache->get('Football2014-Team-Logo-' . $Logo);
				if($Logo_MC){
					$dataTeam2['Logo'] 		= 	'http://football.kapook.com/uploads/logo/' . $Logo;
				}else{
					$dataTeam2['Logo'] 		= 	'http://football.kapook.com/uploads/logo/default.png';
				}
			
				$dataTeam2['Info']			=	empty($tmpTeam['Info']) 		? null : $tmpTeam['Info'];
				$dataTeam2['EmbedCode']		=	empty($tmpTeam['EmbedCode']) 	? null : $tmpTeam['EmbedCode'];
			
				$memcache->set( 'Football2014-teamInfo-'.$dataMatch[$dataPlayTmp['match_id']]['Team2KPID'] , $dataTeam2 , MEMCACHE_COMPRESSED, $expire );
			}
						
			$Team1LogoPath 					= 		$dataTeam1['Logo'];
			$Team2LogoPath 					= 		$dataTeam2['Logo'];
			
			$tmpGame['listGame'][$j]['MatchDateTime']		=		$dataMatch[$dataPlayTmp['match_id']]['MatchDateTime'];
			$tmpGame['listGame'][$j]['Team1KPID']			=		$dataMatch[$dataPlayTmp['match_id']]['Team1KPID'];
			$tmpGame['listGame'][$j]['Team2KPID']			=		$dataMatch[$dataPlayTmp['match_id']]['Team2KPID'];
			$tmpGame['listGame'][$j]['Team1']				=		empty($dataTeam1['NameTH']) 									? 	$dataTeam1['NameEN'] 		: $dataTeam1['NameTH'];
			$tmpGame['listGame'][$j]['Team2']				=		empty($dataTeam2['NameTH']) 									? 	$dataTeam2['NameEN'] 		: $dataTeam2['NameTH'];
			$tmpGame['listGame'][$j]['Team1Logo']			=		$Team1LogoPath;
			$tmpGame['listGame'][$j]['Team2Logo']			=		$Team2LogoPath;
			$tmpGame['listGame'][$j]['Odds']				=		($dataMatch[$dataPlayTmp['match_id']]['Odds']==-1)				?	null						:	$dataMatch[$dataPlayTmp['match_id']]['Odds'];
			$tmpGame['listGame'][$j]['TeamOdds']			=		($dataMatch[$dataPlayTmp['match_id']]['Odds']==-1)				?	null						:	$dataMatch[$dataPlayTmp['match_id']]['TeamOdds'];
			$tmpGame['listGame'][$j]['Team1FTScore']		=		empty($dataMatch[$dataPlayTmp['match_id']]['Team1FTScore'])		?	0							:	$dataMatch[$dataPlayTmp['match_id']]['Team1FTScore'];
			$tmpGame['listGame'][$j]['Team2FTScore']		=		empty($dataMatch[$dataPlayTmp['match_id']]['Team2FTScore'])		?	0							:	$dataMatch[$dataPlayTmp['match_id']]['Team2FTScore'];
			$tmpGame['listGame'][$j]['MatchStatus']			=		$dataMatch[$dataPlayTmp['match_id']]['MatchStatus'];
			$tmpGame['listGame'][$j]['MatchPageURL']		= 		'http://football.kapook.com/match-'.$dataPlayTmp['match_id'].'-'.$dataTeam1['NameEN'].'-'.$dataTeam2['NameEN'];
			
		}
		$returnJson['datalist'][]	=		$tmpGame;
	}
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($returnJson) . ')';
	} else {
		echo json_encode($returnJson);
	}
?>