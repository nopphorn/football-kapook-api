<?php

session_start();

$authorized = false;

$start_date['day']		=	1;
$start_date['month']	=	8;

if(date('G')>=6){
	$monthToday				=	date("Y-m");
}else{
	$monthToday				=	date("Y-m",strtotime("now -1 day"));
}

if(isset($_GET['logout']) && ($_SESSION['auth'])) {
	$_SESSION['auth'] = null;
	session_destroy();
	echo "logging out...";
	?>
	<META HTTP-EQUIV="Refresh" CONTENT="5;URL=http://football.kapook.com/" />
	<?php
}

if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	$user = 'football';
	$pass = 'cityunited';
	if (($user == $_SERVER['PHP_AUTH_USER']) && ($pass == ($_SERVER['PHP_AUTH_PW'])) && (!empty($_SESSION['auth']))) {
		$authorized = true;
	}
}

if ((! $authorized)) {
	header('WWW-Authenticate: Basic Realm="Login please"');
	header('HTTP/1.0 401 Unauthorized');
	$_SESSION['auth'] = true;
	print('Login now or forever hold your clicks...');
	exit;
}

else{

	// init MongoDB
	$connectMongo 				= 	new MongoClient( 'mongodb://localhost:27017' );
	$DatabaseMongoDB			=	$connectMongo->selectDB("football");
	
	$collectionGame				=	new MongoCollection($DatabaseMongoDB,"football_game");
	
	//---------------------------------------------------------------------------------------//
	// Find A Game By Match
	$dataGame = $collectionGame->find(
		array(
			'listGame' => array(
				'$elemMatch' => array(
					'match_id' => 108843,
					'team_win_nameTHShort' => 'ราชบุรี'
				)
			)
		)
	);
	foreach($dataGame as $data){

		$countSubGame			=	count($data['listGame']);
		for( $j=0 ; $j<$countSubGame ; $j++ )
		{
			if($data['listGame'][$j]['match_id']==108843){
				$data['listGame'][$j]['team_win'] = 2;
			}
		}
		
		$collectionGame->update(
			array(
				'id' 	=> (int)$data['id']
			),
			array('$set' =>
				array(
					'listGame'		=>	$data['listGame']
				)
			)
		);

		echo $data['id'] . "\n";
	}
	echo 'Finish';
}
?>